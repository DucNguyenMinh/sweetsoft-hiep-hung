﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Building.aspx.cs" Inherits="SweetSoft.HiepHung.Building" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="error-container">
        <div class="well">
            <h1 class="grey lighter smaller">
                <span class="blue bigger-125">
                    <i class="ace-icon fa fa-sitemap"></i>

                </span>
                Chức năng đang được hoàn thiện <b>
                    <asp:Literal ID="ltrPage" runat="server"></asp:Literal></b>
            </h1>

            <h3 class="lighter smaller">Xin lỗi, chức năng này đang trong quá trình hoàn thiện, vui lòng quay trở lại khi có thông báo mới!</h3>

            <div>
            </div>

            <hr>
            <div class="space"></div>

            <div class="center">
                <a class="btn btn-grey" href="javascript:history.back()">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Quay trở lại
                </a>

                <a class="btn btn-primary" href="/trang-chu">
                    <i class="ace-icon fa fa-tachometer"></i>
                    Trang điều khiển
                </a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
</asp:Content>
