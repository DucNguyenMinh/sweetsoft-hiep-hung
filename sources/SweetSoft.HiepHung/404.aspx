﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="404.aspx.cs" Inherits="SweetSoft.HiepHung._404" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="error-container">
        <div class="well">
            <h1 class="grey lighter smaller">
                <span class="blue bigger-125">
                    <i class="ace-icon fa fa-sitemap"></i>
                    404
                </span>
                Không tìm thấy chức năng <b>
                    <asp:Literal ID="ltrPage" runat="server"></asp:Literal></b>
            </h1>

            <h3 class="lighter smaller">Xin lỗi, hệ thống không tìm thấy chức năng này!</h3>

            <div>

                <div class="space"></div>
                <h4 class="smaller">Trong lúc này, xin vui lòng :</h4>

                <ul class="list-unstyled spaced inline bigger-110 margin-15">
                    <li>
                        <i class="ace-icon fa fa-hand-o-right blue"></i>
                        Kiểm tra lại chính xác đường link của bạn
                    </li>

                
                </ul>
            </div>

            <hr>
            <div class="space"></div>

            <div class="center">
                <a class="btn btn-grey" href="javascript:history.back()">
                    <i class="ace-icon fa fa-arrow-left"></i>
                    Quay trở lại
                </a>

                <a class="btn btn-primary" href="#">
                    <i class="ace-icon fa fa-tachometer"></i>
                    Trang điều khiển
                </a>
            </div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
</asp:Content>
