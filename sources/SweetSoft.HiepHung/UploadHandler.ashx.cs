﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung
{
    /// <summary>
    /// Summary description for UploadHandler
    /// </summary>
    public class UploadHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            var uploadType = context.Request.Form["UploadType"];
            if (uploadType == "MauIn" || uploadType == "SanPham")
            {
                var httpPostedFile = context.Request.Files;
                var httpPostedFile2 = context.Request.Files["UploadedExcelFile"];

                var id = context.Request.Form["MauInID"];
                if (uploadType == "SanPham")
                    id = context.Request.Form["SanPhamID"];

                var ImgType = context.Request.Form["ImgType"];
                if (!string.IsNullOrEmpty(id) && httpPostedFile.Count > 0)
                {
                    for (int i = 0; i < httpPostedFile.Count; i++)
                    {
                        try
                        {
                            int u = 1;
                            var file = context.Request.Files["file_" + i];
                            #region folder
                            string folder = HttpContext.Current.Server.MapPath(string.Format(@"\Uploads\Images\{0}", uploadType));
                            if (!Directory.Exists(folder)) Directory.CreateDirectory(folder);
                            #endregion
                            string extension = Path.GetExtension(file.FileName);
                            string name = string.Format("[{0}]_(0)_{1}{2}", id, UrlRewriteHelper.ConvertToSEOFriendly(file.FileName.Replace(extension, ""), 200, true), extension);
                            string path = string.Format(@"\Uploads\Images\{1}\{0}", name, uploadType);
                            string serverPath = HttpContext.Current.Server.MapPath(path);
                            while (File.Exists(serverPath))
                            {
                                name = string.Format("[{0}]_({1})_{2}{3}", id, u, UrlRewriteHelper.ConvertToSEOFriendly(file.FileName.Replace(extension, ""), 200, true), extension);
                                path = string.Format(@"\Uploads\Images\{1}\{0}", name, uploadType);
                                serverPath = HttpContext.Current.Server.MapPath(path);
                                u++;
                            }

                            file.SaveAs(serverPath);

                            TblImage image = new TblImage();
                            image.ImagePath = path;
                            image = CategoryManager.InsertImage(image);

                            if (uploadType == "SanPham")
                            {
                                TblSanPhamImage spi = new TblSanPhamImage();
                                spi.IDSanPham = int.Parse(id);
                                spi.Img = image.Id;
                                spi.IsCategory = true;
                                spi.ImgType = ImgType;
                                spi = SanPhamManager.InsertSanPhamImage(spi);
                            }
                            else if (uploadType == "MauIn")
                            {
                                TblLoaiMauImage lmi = new TblLoaiMauImage();
                                lmi.IDLoaiMau = int.Parse(id);
                                lmi.Img = image.Id;
                                lmi = CategoryManager.InsertLoaiMauImage(lmi);
                            }
                        }
                        catch (Exception ex) { }

                    }

                    context.Response.Write("success");
                }
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}