﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace SweetSoft.HiepHung
{
    public static class HiepHungWebAPIConfig
    {
        public static void Register(HttpConfiguration config)
        {

                config.Routes.MapHttpRoute(
                name: "HiepHungAPI", routeTemplate: "api/{action}/{id}",
                defaults: new { id = RouteParameter.Optional, controller = "HiepHungAPI", action = "Hello" }
                );
        }
    }
}
