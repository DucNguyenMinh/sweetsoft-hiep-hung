﻿; (function ($) {
    
    $.extend($.fn.datetimepicker.defaults, {
        tooltips: {
            today: 'Đến ngày hiện tại',
            clear: 'Xóa lựa chọn',
            close: 'Đóng cửa sổ chọn ngày',
            selectMonth: 'Chọn tháng',
            prevMonth: 'Tháng trước',
            nextMonth: 'Tháng sau',
            selectYear: 'Chọn năm',
            prevYear: 'Năm trước',
            nextYear: 'Năm sau',
            selectDecade: 'Chọn thập kỷ',
            prevDecade: 'Thập kỷ trước',
            nextDecade: 'Thập kỷ sau',
            prevCentury: 'Thế kỷ trước',
            nextCentury: 'Thế kỷ sau',
            pickHour: 'Chọn giờ',
            incrementHour: 'Tăng giờ lên',
            decrementHour: 'Giảm giờ xuống',
            pickMinute: 'Chọn phút',
            incrementMinute: 'Tăng phút lên',
            decrementMinute: 'Giảm phút xuống',
            pickSecond: 'Chọn giây',
            incrementSecond: 'Tăng giây lên',
            decrementSecond: 'Giảm giây xuống',
            togglePeriod: 'Khoảng thời gian sáng/chiều',
            selectTime: 'Chọn thời gian'
        }
    });
    
}(jQuery));