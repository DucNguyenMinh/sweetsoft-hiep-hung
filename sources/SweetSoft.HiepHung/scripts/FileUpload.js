﻿
var SweetSoft_Input = function (id, dom, array) {
    console.log("dom: ", dom, typeof (dom));
    if (typeof (dom) !== 'undefined') {
        var Input = $(this)[0];
        this.ID = id;
        this.DOM = dom;
        this.FileList = array;
        this.Form = $("div.e-upload-form[input-id='" + id + "']");

        this.DOM.on("change", function (e) {
            var files = e.target.files;

            for (var i = 0, f; f = files[i]; i++) {
               
                var T = i;
                console.log(f);
                var reader = new FileReader();
                reader.onload = (function (theFile) {
                    return function (e) {
                       
                        var image = e.target.result;
                        var form = Input.Form;
                        Input.FileList.push(theFile);
                        if (form.length > 0) {
                            var pre_view = $("<div class='e-preview new' id='pre_" + Input.ID + "_" + Input.FileList.length + "'></div>");
                           
                            var content = $("<div class='e-preview-content'></div>");
                            var img = "<img class='e-file-image' src='" + image + "'>";
                            content.append(img);
                            pre_view.append(content);

                            var footer = $("<div class='e-preview-footer'></div>");
                            var caption = $("<div class='footer-caption'></div>");
                            caption.html(theFile.name);
                            footer.append(caption);
                            var action_l = $("<div class='footer-action-left'></div>");
                            footer.append(action_l);
                            var action = $("<div class='footer-action-right'></div>");
                            var del = $("<a class='btn btn-danger  btn-white'><i class='fa fa-trash'></i></a>");
                            del.attr("onclick", "SweetSoft_Upload.Functions.RemoveFileUpload('" + Input.ID + "', " + Input.FileList.length + ")");
                            action.append(del);
                            footer.append(action);
                            pre_view.append(footer);


                            form.append(pre_view);
                        }
                    };

                })(f);

                reader.readAsDataURL(f);
            }
        });
    }
};

var SweetSoft_Upload = {
    Inputs: [],
    Functions: {
        InitUpload: function () {
            var f_upload = $("input[type='file'].e-upload-input");
            if (f_upload.length > 0) {
                f_upload.each(function (idx, item) {
                    var t_his = $(item);
                    var id = t_his.attr("id");

                    var button_choose_image = $("a[input-id='" + id + "'].btn-select-image");
                    if (button_choose_image.length > 0) {
                        button_choose_image.click(function () {
                            t_his.click();
                        });
                    }

                    var sw_item = SweetSoft_Upload.Inputs.filter(function (input, idx) { return input.ID === id; });

                    if (typeof (sw_item) === 'undefined' || sw_item.length === 0) {
                        sw_item = new SweetSoft_Input(id, t_his, []);
                        SweetSoft_Upload.Inputs.push(sw_item);
                    }
                });
            }
            addRequestHanlde(SweetSoft_Upload.Functions.InitUpload);
        },
        ChooseFile: function (id) {
            var f_upload = $("#" + id);

            if (f_upload.length > 0) f_upload.click();
            return false;
        },
        RemoveFileUpload: function (id, idx) {
            console.log("remove:", id," - ", idx);
            var sw_item = SweetSoft_Upload.Inputs.filter(function (input, idx1) { return input.ID === id; });

            if (sw_item.length > 0 && sw_item[0].FileList.length > idx - 1) {
                sw_item[0].FileList[idx-1]=null;//.splice(idx - 1, 1);

                var preview = $("div#pre_" + sw_item[0].ID + "_" + idx);
                console.log("preview: ", preview, " id: ", "div#pre_" + sw_item[0].ID + "_" + idx);
                preview.fadeOut(400).remove();
            }
        }
    }
};

$(function () {
    SweetSoft_Upload.Functions.InitUpload();
});
