﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
namespace SweetSoft.HiepHung
{
    public partial class DMSanPhamChiTiet : BasePage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Cat_SanPham;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Cat_SanPham;
            }
        }
        #endregion


        protected string CurrentName
        {
            get
            {
                string rs = "sản phẩm";
                return rs;
            }
        }

        public long? CurrentID
        {
            get
            {
                long? result = 0;
                if (!string.IsNullOrEmpty(CommonHelper.QueryString("ID")))
                {
                    long u = -1;
                    if (long.TryParse(CommonHelper.QueryString("ID"), out u)) result = u;
                }
                return result;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.SP_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.SP_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.SP_Delete);

            return rs;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            breadTitle.Text = "Chi tiết sản phẩm";
            if (!IsPostBack)
            {

                List<TblLoaiGiay> loaiGiayCol = CategoryManager.GetLoaiGiayALL(true);
                ctrsanPham.LoadLoaiGiay(loaiGiayCol);

                List<TblDonViTinh> donViTinhCol = CategoryManager.GetDonViTinhALL(true);
                ctrsanPham.LoadDonViTinh(donViTinhCol);

                List<TblLoaiMauIn> loaiMau = CategoryManager.GetLoaiMauInALL(true, string.Empty);
                ctrsanPham.LoadMauIn(loaiMau.FindAll(p => p.CType == "LMI"));
                ctrsanPham.LoadMauThung(loaiMau.FindAll(p => p.CType != "LMI"));

                ctrsanPham.LoadSanPham(CurrentID.Value);
                if (Session["InsertSPMoi"] != null)
                {
                    Session["InsertSPMoi"] = null;
                    ShowNotifyStartUp("Thông báo", "Hệ thống thêm mới thành công thông tin sản phẩm", false, ExtraControls.NotifyType.Success);
                }
            }
        }

        protected void ctrsanPham_Inserted(object sender, EventArgs e)
        {
            if (sender != null && sender is TblSanPham)
            {
                TblSanPham sanPham = sender as TblSanPham;
                Session["InsertSPMoi"] = sanPham;
                Response.Redirect(string.Format("/san-pham/{0}", sanPham.Id.ToString()));
            }
        }

        protected void ctrsanPham_Updated(object sender, EventArgs e)
        {
            ShowNotify("Thông báo", "Hệ thống cập nhật thành công thông tin sản phẩm", ExtraControls.NotifyType.Success);
        }


    }
}