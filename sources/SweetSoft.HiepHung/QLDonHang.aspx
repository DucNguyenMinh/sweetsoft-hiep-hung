﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="QLDonHang.aspx.cs" Inherits="SweetSoft.HiepHung.QLDonHang" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>

        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý 
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,3,4,5">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mã đơn hàng" AccessibleHeaderText="Mã đơn hàng" HeaderStyle-Width="150px" ItemStyle-Width="150px" SortExpression="dh.MaDonHang">
                                    <ItemTemplate>
                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" NavigateUrl='<%# string.Format("/don-hang/{0}",Eval("ID")) %>' runat="server" Text=' <%# Eval("MaDonHang")  %>' CommandArgument='<%# Eval("ID") %>' Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("MaDonHang")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Khách hàng" AccessibleHeaderText="Khách hàng" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="kh.TenKhachHang">
                                    <ItemTemplate>
                                        <%# Eval("TenKhachHang")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Người liên hệ" AccessibleHeaderText="Người liên hệ" HeaderStyle-Width="180px" ItemStyle-Width="180px">
                                    <ItemTemplate>
                                        <%#  Eval("NguoiLienHe")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Thành tiền" AccessibleHeaderText="Thành tiền" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                        <%#  ParseDecimal(Eval("TongTien"))  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" NavigateUrl='<%# string.Format("/san-pham/{0}",Eval("ID")) %>' CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm_delete_khachHang">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog runat="server" ID="dialogSearch" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            Tìm kiếm đơn hàng
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã đơn hàng </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchMaDonHang" runat="server" PlaceHolder="Nhập mã đơn hàng"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Khách hàng </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchKhachHang" runat="server" PlaceHolder="Chọn khách hàng tìm kiếm"></SweetSoft:ExtraTextBox>
                        <asp:HiddenField ID="hiddenKhachHangID" runat="server" />
                    </div>
                </div>
            </div>
        </ContentTemplate>
         <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
    <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        SearchCustomer();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SearchCustomer);
        function SearchCustomer(s, a) {

            $("input[type='text'][id$='txtSearchKhachHang']").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/Default.aspx/GetCustomer",
                        data: "{'KeyWord':'" + $("input[type='text'][id$='txtSearchKhachHang']").val() + "'}",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (result) {
                            $("input[type='hidden'][id$='hiddenKhachHangID']").val("");
                            response($.map($.parseJSON(result.d), function (item) {
                                return { Id: item.Id, TenKhachHang: item.TenKhachHang };
                            }));
                        }
                    });
                },
                messages: {
                    noResults: '',
                    results: function () { }
                },
                focus: function (event, ui) {
                    return false;
                },
                search: function (event, ui) {
                    console.log("search: ");
                },
                select: function (event, ui) {
                    $("input[type='text'][id$='txtSearchKhachHang']").val(ui.item.TenKhachHang);
                    $("input[type='hidden'][id$='hiddenKhachHangID']").val(ui.item.Id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .data("ui-autocomplete-item", item)
                    .append("<a><span style='width:30px;'>" + item.TenKhachHang + '</span>' + "</a>")
                    .appendTo(ul);
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
