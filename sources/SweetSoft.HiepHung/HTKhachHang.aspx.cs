﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
namespace SweetSoft.HiepHung
{
    public partial class HTKhachHang : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Cat_KhachHang; 
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Cat_KhachHang;
            }
        }
        #endregion


        protected string CurrentName
        {
            get
            {
                string rs = "Khách hàng";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.KhachHang_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.KhachHang_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.KhachHang_Delete);

            return rs;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            gridLienHe.CurrentPageSize = 50;
            gridLienHe.AllowPaging = false;
            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);
            txtSearchMaSoThue.EnterSubmitClientID = txtSearchTaiKhoan.EnterSubmitClientID = txtSearchSoDienThoai.EnterSubmitClientID =
            txtSearchDiaChi.EnterSubmitClientID = txtSearchTenKhachHang.EnterSubmitClientID =
            txtSearchTenNguoiDaiDien.EnterSubmitClientID = txtSearchSDTDaiDien.EnterSubmitClientID = txtSearchDiaChiDaiDien.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");

            txtDetailDiaChi.MaxTextLength = TblKhachHang.DiaChiColumn.MaxLength;
            txtDetailMaSoThue.MaxTextLength = TblKhachHang.MaSoThueColumn.MaxLength;
            txtDetailSoDienThoai.MaxTextLength = TblKhachHang.SoDienThoaiColumn.MaxLength;
            txtDetailSoDienThoai.ValidationType = TextBoxValidateType.Number;

            txtDetailTaiKhoanNganHang.MaxTextLength = TblKhachHang.TaiKhoanNganHangColumn.MaxLength;
            txtDetailTen.MaxTextLength = TblKhachHang.TenKhachHangColumn.MaxLength;

            txtDetailDiaChi.EnterSubmitClientID = txtDetailMaSoThue.EnterSubmitClientID = txtDetailSoDienThoai.EnterSubmitClientID =
                txtDetailTaiKhoanNganHang.EnterSubmitClientID = txtDetailTen.EnterSubmitClientID = btnSave.ClientID;



            if (!IsPostBack)
            {

                grvData.CurrentSortExpression = TblKhachHang.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        private void RefreshDetail()
        {
            btnAddNewLienHe.Visible = gridLienHe.Visible = false;
            btnSaveFirst.Visible = true;

            txtDetailDiaChi.Text = txtDetailMaSoThue.Text = txtDetailSoDienThoai.Text = txtDetailTen.Text = string.Empty;
            dialogDetail.UpdateContentDialog();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            RefreshDetail();
            hdfDetailID.Value = string.Empty;
            txtDetailTen.Focus();

            lblDialogDetailTitle.Text = "Thêm mới khách hàng";
            dialogDetail.ShowDialog(true);
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" kh.{0} desc ", TblKhachHang.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" kh.* ", TblCommonUser.Columns.UserName, TblPhongBan.Columns.TenPhongBan);

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchDiaChi.Text.Trim()))
                    filterClause += string.Format(" kh.{0} like N'%{1}%' AND ",
                                                  TblKhachHang.Columns.DiaChi,
                                                  txtSearchDiaChi.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchMaSoThue.Text.Trim()))
                    filterClause += string.Format(" kh.{0} like N'%{1}%' AND ",
                                                  TblKhachHang.Columns.MaSoThue,
                                                  txtSearchMaSoThue.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTaiKhoan.Text.Trim()))
                    filterClause += string.Format(" kh.{0} like N'%{1}%' AND ",
                                                  TblKhachHang.Columns.TaiKhoanNganHang,
                                                  txtSearchTaiKhoan.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchSoDienThoai.Text.Trim()))
                    filterClause += string.Format(" kh.{0} like N'%{1}%' AND ",
                                                  TblKhachHang.Columns.SoDienThoai,
                                                  txtSearchSoDienThoai.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTenKhachHang.Text.Trim()))
                    filterClause += string.Format(" kh.{0} like N'%{1}%' AND ",
                                                  TblKhachHang.Columns.TenKhachHang,
                                                  txtSearchTenKhachHang.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTenNguoiDaiDien.Text.Trim()))
                    filterClause += string.Format(" khlh.{0} like N'%{1}%' AND ",
                                                  TblKhachHangLienHe.Columns.TenDaiDien,
                                                  txtSearchTenNguoiDaiDien.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchDiaChiDaiDien.Text.Trim()))
                    filterClause += string.Format(" khlh.{0} like N'%{1}%' AND ",
                                                  TblKhachHangLienHe.Columns.DiaChi,
                                                  txtSearchDiaChiDaiDien.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchSDTDaiDien.Text.Trim()))
                    filterClause += string.Format(" khlh.{0} like N'%{1}%' AND ",
                                                  TblKhachHangLienHe.Columns.SoDienThoai,
                                                  txtSearchSDTDaiDien.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = string.Format(" left join {0} as khlh on kh.id = khlh.{1} ", TblKhachHangLienHe.Schema.TableName, TblKhachHangLienHe.Columns.IDKhachHang);

                int total = 0;
                List<TblKhachHang> lst = KhachHangManager.SearchKhachHang(rowIndex, pageSize, column, tableJoin,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblKhachHang> lstEmpty = new List<TblKhachHang>();
                lstEmpty.Add(new TblKhachHang());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void gridLienHe_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" khlh.{0} desc ", TblKhachHangLienHe.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("khlh.{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" khlh.* ", TblCommonUser.Columns.UserName, TblPhongBan.Columns.TenPhongBan);

                #endregion

                #region set value for filter clause

                filterClause += string.Format(" khlh.{0} = {1} AND ", TblKhachHangLienHe.Columns.IDKhachHang, hdfDetailID.Value);

                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = "";

                int total = 0;
                List<TblKhachHangLienHe> lst = KhachHangManager.SearchKhachHangLienHe(rowIndex, pageSize, column,
                                                                       filterClause, orderClause, out total);
                if (gridLienHe.IsInsert)
                    lst.Insert(0, new TblKhachHangLienHe());
                grv.DataSource = lst;
                List<TblKhachHangLienHe> lstEmpty = new List<TblKhachHangLienHe>();
                lstEmpty.Add(new TblKhachHangLienHe());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void grvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = gridLienHe.Rows[e.RowIndex];
            if (row != null)
            {
                TextBox txtEditTenDaiDien = row.FindControl("txtEditTenDaiDien") as TextBox;
                TextBox txtEditSoDienThoai = row.FindControl("txtEditSoDienThoai") as TextBox;
                TextBox txtEditDiaChi = row.FindControl("txtEditDiaChi") as TextBox;
                HiddenField hdfEditID = row.FindControl("hdfEditID") as HiddenField;

                #region Validation
                if (string.IsNullOrEmpty(txtEditTenDaiDien.Text))
                {
                    AddValidPromt(txtEditTenDaiDien.ClientID, string.Format("Nhập tên người đại diện", CurrentName));
                    txtEditTenDaiDien.Focus();
                }



                #endregion

                e.Cancel = !PageIsValid;
                if (PageIsValid)
                {
                    if (gridLienHe.IsInsert)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Insert, string.Format("Thêm mới người liên hệ <b>{0}</b>", txtEditTenDaiDien.Text, CurrentName));

                        #region Insert value for donvitinh
                        TblKhachHangLienHe lienHe = new TblKhachHangLienHe();
                        lienHe.TenDaiDien = txtEditTenDaiDien.Text.Trim();
                        lienHe.SoDienThoai = txtEditSoDienThoai.Text.Trim();
                        lienHe.DiaChi = txtEditDiaChi.Text;
                        lienHe.IDKhachHang = int.Parse(hdfDetailID.Value);
                        lienHe.CreatedBy = lienHe.UpdatedBy = ApplicationContext.Current.CommonUser.UserName;
                        lienHe.CreatedDate = lienHe.UpdatedDate = DateTime.Now;
                        lienHe = KhachHangManager.InsertKhachHangLienHe(lienHe);
                        #endregion

                        #region Add log value
                        CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, string.Format("Tên đại diện", CurrentName),
                                                 TblKhachHangLienHe.Columns.TenDaiDien,
                                                 txtEditTenDaiDien.Text);
                        CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Địa chỉ",
                                                 TblKhachHangLienHe.Columns.DiaChi,
                                                 txtEditDiaChi.Text);
                        CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Số điện thoại",
                                                 TblKhachHangLienHe.Columns.SoDienThoai,
                                                 txtEditSoDienThoai.Text);
                        #endregion

                        lienHe = KhachHangManager.InsertKhachHangLienHe(lienHe);
                        ShowNotify(string.Format("Lưu thông tin liên hệ thành công!", CurrentName),
                                   string.Format("Thêm mới thông tin liên hệ <b>{0}</b> thành công.",
                                                 lienHe.TenDaiDien, CurrentName),
                                   NotifyType.Success);
                        CurrentLog.SaveLog(RightPageName.KhachHang_Insert);
                        gridLienHe.Rebind();
                    }
                    else
                    {
                        TblKhachHangLienHe lienHe = KhachHangManager.GetKhachHangLienHeByID(hdfEditID.Value);
                        if (lienHe != null)
                        {
                            #region Update value for liên hệ
                            lienHe.TenDaiDien = txtEditTenDaiDien.Text.Trim();
                            lienHe.SoDienThoai = txtEditSoDienThoai.Text.Trim();
                            lienHe.DiaChi = txtEditDiaChi.Text;
                            lienHe.UpdatedBy = ApplicationContext.Current.CommonUser.UserName;
                            lienHe.UpdatedDate = DateTime.Now;
                            #endregion

                            #region Add log value
                            CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                                    TblKhachHangLienHe.Columns.TenDaiDien,
                                                    txtEditTenDaiDien.Text);
                            CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                                    TblKhachHangLienHe.Columns.SoDienThoai,
                                                    txtEditSoDienThoai.Text);
                            CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                                    TblKhachHangLienHe.Columns.DiaChi,
                                                    txtEditDiaChi.Text);
                            #endregion

                            lienHe = KhachHangManager.UpdateKhachHangLienHe(lienHe);
                            ShowNotify(string.Format("Lưu thông tin liên hệ thành công!", CurrentName),
                                       string.Format("Cập nhật thông tin liên hệ <b>{0}</b> thành công.",
                                                     lienHe.TenDaiDien, CurrentName),
                                       NotifyType.Success);
                            CurrentLog.SaveLog(RightPageName.KhachHang_Update);
                            gridLienHe.Rebind();
                        }
                    }
                    gridLienHe.CancelAllCommand();
                }
                if (!PageIsValid)
                    ShowValidPromt();
            }
        }


        protected void grvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            gridLienHe.CancelAllCommand();
        }

        protected void grvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            ExtraButton btnEdit = e.Row.FindControl("btnEdit") as ExtraButton;
            if (btnEdit != null) btnEdit.Visible = GetButtonRole("Update");

            ExtraButton btnDelete = e.Row.FindControl("btnDelete") as ExtraButton;
            if (btnDelete != null) btnDelete.Visible = GetButtonRole("Delete");

            if (gridLienHe.EditIndex == e.Row.RowIndex && !gridLienHe.IsInsert)
            {
                TblKhachHangLienHe lienHe = e.Row.DataItem as TblKhachHangLienHe;
                if (lienHe != null)
                {
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Update,
                                                 string.Format("Cập nhật thông tin liên hệ <b>{0}</b> của khách hàng <b></b>",
                                                                lienHe.TenDaiDien, txtDetailTen.Text, CurrentName));
                    TextBox txtEditTenDaiDien = e.Row.FindControl("txtEditTenDaiDien") as TextBox;
                    TextBox txtEditSoDienThoai = e.Row.FindControl("txtEditSoDienThoai") as TextBox;
                    TextBox txtEditDiaChi = e.Row.FindControl("txtEditDiaChi") as TextBox;
                    HiddenField hdfEditID = e.Row.FindControl("hdfEditID") as HiddenField;

                    if (txtEditTenDaiDien != null)
                        txtEditTenDaiDien.Text = lienHe.TenDaiDien;
                    if (txtEditSoDienThoai != null)
                        txtEditSoDienThoai.Text = lienHe.SoDienThoai;
                    if (txtEditDiaChi != null)
                        txtEditDiaChi.Text = lienHe.DiaChi;
                    if (hdfEditID != null)
                        hdfEditID.Value = lienHe.Id.ToString();

                    #region Add log value
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, string.Format("Tên đại diện", CurrentName),
                                                  TblKhachHangLienHe.Columns.TenDaiDien,
                                                  txtEditTenDaiDien.Text);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Địa chỉ",
                                             TblKhachHangLienHe.Columns.DiaChi,
                                             txtEditDiaChi.Text);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Số điện thoại",
                                             TblKhachHangLienHe.Columns.SoDienThoai,
                                             txtEditSoDienThoai.Text);
                    #endregion
                }
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchTaiKhoan.Text = string.Empty;
            txtSearchDiaChiDaiDien.Text = txtSearchSDTDaiDien.Text = txtSearchTenNguoiDaiDien.Text = string.Empty;
            txtSearchSoDienThoai.Text = txtSearchMaSoThue.Text = txtSearchDiaChi.Text = txtSearchTenKhachHang.Text = string.Empty;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (hiddenDelete.Value == "delete_khachHang")
                    {
                        if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                        {
                            if (CurrentMessageBoxResult.CommandName == "delete_KhachHang")
                            {
                                TblKhachHang khachHang = CurrentMessageBoxResult.Value as TblKhachHang;
                                if (khachHang != null)
                                {
                                    string result = "";
                                    try
                                    {
                                        CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Delete,
                                                                     string.Format("Xóa {0}", CurrentName));
                                        CurrentLog.AddFirstValue(RightPageName.KhachHang_Delete, string.Format("Tên {0}", CurrentName),
                                                                 TblKhachHang.Columns.TenKhachHang,
                                                                 khachHang.TenKhachHang);
                                        result = KhachHangManager.DeleteKhachHang(khachHang.Id);
                                        resultDelete = result == "success";
                                    }
                                    catch
                                    {
                                    }
                                    if (resultDelete)
                                    {
                                        grvData.Rebind();
                                        ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                    string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                                  khachHang.TenKhachHang, CurrentName),
                                                    NotifyType.Success);
                                        CurrentLog.SaveLog(RightPageName.KhachHang_Delete);
                                    }
                                    else
                                    {
                                        ShowNotify(string.Format("Xóa {0} thất bại. {2}", CurrentName),
                                                    string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                                   khachHang.TenKhachHang, CurrentName, result),
                                                    NotifyType.Error);
                                    }
                                    CloseMessageBox();
                                }
                            }
                        }
                    }
                    else
                    {
                        TblKhachHangLienHe khachHang = CurrentMessageBoxResult.Value as TblKhachHangLienHe;
                        if (khachHang != null)
                        {
                            string result = "";
                            try
                            {
                                CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Delete,
                                                             string.Format("Xóa thông tin liên hệ <b>{0}</b> của khách hàng <b>{1}</b>", khachHang.TenDaiDien, txtDetailTen.Text));
                                CurrentLog.AddFirstValue(RightPageName.KhachHang_Delete, string.Format("Tên đại diện", CurrentName),
                                                         TblKhachHang.Columns.TenKhachHang,
                                                         khachHang.TenDaiDien);
                                KhachHangManager.DeleteKhachHangLienHe(khachHang.Id);
                                resultDelete = true;
                            }
                            catch
                            {
                            }
                            if (resultDelete)
                            {
                                gridLienHe.Rebind();
                                ShowNotify(string.Format("Xóa thông tin liên hệ thành công", CurrentName),
                                            string.Format("Bạn vừa xóa thông tin liên hệ <b>{0}</b>",
                                                          khachHang.TenDaiDien, CurrentName),
                                            NotifyType.Success);
                                CurrentLog.SaveLog(RightPageName.KhachHang_Delete);
                            }
                            else
                            {
                                ShowNotify(string.Format("Xóa thông tin liên hệ thất bại.", CurrentName),
                                            string.Format("Có lỗi xảy ra khi xóa thông tin liên hệ <b>{0}</b>",
                                                           khachHang.TenDaiDien, CurrentName, result),
                                            NotifyType.Error);
                            }
                            CloseMessageBox();
                        }
                    }
                }
                else if (cmdName == "confirm_delete_khachHang")
                {
                    TblKhachHang khachHang = KhachHangManager.GetKhachHangByID(btn.CommandArgument);
                    if (khachHang != null)
                    {
                        hiddenDelete.Value = "delete_khachHang";
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = khachHang;
                        result.Submit = true;
                        result.CommandName = "delete_KhachHang";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     khachHang.TenKhachHang, CurrentName));
                    }
                }
                else if (cmdName == "confirm_delete_lienHe")
                {
                    TblKhachHangLienHe khachHang = KhachHangManager.GetKhachHangLienHeByID(btn.CommandArgument);
                    if (khachHang != null)
                    {
                        hiddenDelete.Value = "delete_lienHe";
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = khachHang;
                        result.Submit = true;
                        result.CommandName = "delete_lienHe";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa thông tin liên hệ", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa thông tin liên hệ <b>{0}</b> ?",
                                                     khachHang.TenDaiDien, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RefreshDetail();
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    LoadKhachHang(cmd);
                    dialogDetail.ShowDialog();
                }
            }
        }

        private void LoadKhachHang(object id)
        {
            TblKhachHang khachHang = KhachHangManager.GetKhachHangByID(id);
            if (khachHang != null)
            {

                CurrentLog.Clear();
                CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Update,
                                             string.Format("Cập nhật khách hàng <b>{0}</b>", khachHang.TenKhachHang));

                txtDetailTen.Text = khachHang.TenKhachHang;
                txtDetailDiaChi.Text = khachHang.DiaChi;
                txtDetailMaSoThue.Text = khachHang.MaSoThue;
                txtDetailSoDienThoai.Text = khachHang.SoDienThoai;
                txtDetailTaiKhoanNganHang.Text = khachHang.TaiKhoanNganHang;

                CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, string.Format("Tên khách hàng"),
                                    TblKhachHang.Columns.TenKhachHang,
                                    khachHang.TenKhachHang);
                CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Địa chỉ",
                                  TblKhachHang.Columns.DiaChi,
                                  txtDetailDiaChi.Text);
                CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Mã số thuế",
                                  TblKhachHang.Columns.MaSoThue,
                                  txtDetailMaSoThue.Text);
                CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Số điện thoại",
                                  TblKhachHang.Columns.SoDienThoai,
                                  txtDetailSoDienThoai.Text);
                CurrentLog.AddFirstValue(RightPageName.KhachHang_Update, "Tài khoản ngân hàng",
                                  TblKhachHang.Columns.TaiKhoanNganHang,
                                  txtDetailTaiKhoanNganHang.Text);


                hdfDetailID.Value = khachHang.Id.ToString();
                gridLienHe.Rebind();
                lblDialogDetailTitle.Text = "Cập nhật thông tin khách hàng";
                btnAddNewLienHe.Visible = gridLienHe.Visible = true;
                btnSaveFirst.Visible = false;
            }
        }

        private void Save(bool addingLienHe)
        {
            #region Validation
            if (string.IsNullOrEmpty(txtDetailTen.Text))
            {
                AddValidPromt(txtDetailTen.ClientID, "Nhập tên khách hàng");
                txtDetailTen.Focus();
            }


            #endregion

            if (PageIsValid)
            {
                TblKhachHang khachHang = new TblKhachHang();
                if (!string.IsNullOrEmpty(hdfDetailID.Value))
                {
                    #region update
                    khachHang = KhachHangManager.GetKhachHangByID(hdfDetailID.Value);
                    if (khachHang != null)
                    {
                        khachHang.TenKhachHang = txtDetailTen.Text;
                        khachHang.SoDienThoai = txtDetailSoDienThoai.Text;
                        khachHang.DiaChi = txtDetailDiaChi.Text;
                        khachHang.MaSoThue = txtDetailMaSoThue.Text;
                        khachHang.TaiKhoanNganHang = txtDetailTaiKhoanNganHang.Text;
                        khachHang = KhachHangManager.UpdateKhachHang(khachHang);

                        #region log
                        CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                          TblKhachHang.Columns.TenKhachHang,
                                          txtDetailTen.Text);
                        CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                          TblKhachHang.Columns.DiaChi,
                                          txtDetailDiaChi.Text);
                        CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                          TblKhachHang.Columns.MaSoThue,
                                          txtDetailMaSoThue.Text);
                        CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                         TblKhachHang.Columns.TaiKhoanNganHang,
                                         txtDetailTaiKhoanNganHang.Text);
                        CurrentLog.AddLastValue(RightPageName.KhachHang_Update,
                                         TblKhachHang.Columns.SoDienThoai,
                                         txtDetailSoDienThoai.Text);
                      
                        CurrentLog.SaveLog(RightPageName.KhachHang_Update);
                        #endregion
                        ShowNotify("Thông báo", string.Format("Cập nhật thông tin khách hàng <b>{0}</b> thành công", khachHang.TenKhachHang), NotifyType.Success);

                        grvData.Rebind();
                    }
                    else
                        ShowNotify("Thông báo", "Không tìm thấy thông tin khách hàng trên hệ thống", NotifyType.Error);
                    #endregion
                }
                else
                {
                    #region insert
                    khachHang.TenKhachHang = txtDetailTen.Text;
                    khachHang.SoDienThoai = txtDetailSoDienThoai.Text;
                    khachHang.DiaChi = txtDetailDiaChi.Text;
                    khachHang.MaSoThue = txtDetailMaSoThue.Text;
                    khachHang.TaiKhoanNganHang = txtDetailTaiKhoanNganHang.Text;
                    khachHang = KhachHangManager.InsertKhachHang(khachHang);

                    #region log
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.KhachHang_Insert,
                                                 string.Format("Thêm mới khách hàng <b>{0}</b>", khachHang.TenKhachHang));

                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, string.Format("Tên khách hàng"),
                                             TblKhachHang.Columns.TenKhachHang,
                                             khachHang.TenKhachHang);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Địa chỉ",
                                      TblKhachHang.Columns.DiaChi,
                                      txtDetailDiaChi.Text);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Mã số thuế",
                                      TblKhachHang.Columns.MaSoThue,
                                      txtDetailMaSoThue.Text);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Số điện thoại",
                                      TblKhachHang.Columns.SoDienThoai,
                                      txtDetailSoDienThoai.Text);
                    CurrentLog.AddFirstValue(RightPageName.KhachHang_Insert, "Tài khoản ngân hàng",
                                      TblKhachHang.Columns.TaiKhoanNganHang,
                                      txtDetailTaiKhoanNganHang.Text);
                    CurrentLog.SaveLog(RightPageName.KhachHang_Insert);
                    #endregion
                    ShowNotify("Thông báo", string.Format("Thêm mới thông tin khách hàng <b>{0}</b> thành công", khachHang.TenKhachHang), NotifyType.Success);
                    LoadKhachHang(khachHang.Id);
                    if (addingLienHe) gridLienHe.InsertItem();
                    grvData.Rebind();
                    #endregion
                }
            }
            else ShowValidPromt();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save(false);
        }


        protected void btnSaveFirst_Click(object sender, EventArgs e)
        {
            Save(true);
        }

        protected void btnAddNewLienHe_Click(object sender, EventArgs e)
        {
            gridLienHe.InsertItem();
        }
    }
}