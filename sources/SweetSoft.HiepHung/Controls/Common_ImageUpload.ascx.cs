﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.HiepHung.Controls
{
    public partial class Common_ImageUpload : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hiddenUploadControlID.Value = fileUpload.ClientID;
            btnChooseImage.Attributes.Add("input-id", fileUpload.ClientID);
            div_form.Attributes.Add("input-id", fileUpload.ClientID);
        }
    }
}