﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Controls
{
    public partial class HH_LeftSidebar : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadMenu();
            }
        }

        private void LoadMenu()
        {
            string result = string.Empty;
            List<TblCommonFunction> functions = FuntionManager.GetAllFunction();
            if (functions != null)
            {
                List<TblCommonFunction> head = functions.FindAll(p => string.IsNullOrEmpty(p.ParentCode));
                if (head.Count > 0)
                {
                    foreach (TblCommonFunction f in head)
                    {
                        bool isActive = f.FunctionCode == CURRENT_PAGE.FUNCTION_CODE; ;
                        string liClass = "";
                        string liContent = "<a" + (!string.IsNullOrEmpty(f.NavigateUrl) ? string.Format(" href=\"{0}\"", f.NavigateUrl) : "") + ">";
                        liContent = "<a" + (!string.IsNullOrEmpty(f.NavigateUrl) ? string.Format(" href=\"{0}\"", f.NavigateUrl) : "") + ">";
                        liContent += string.Format("<i class=\"{0}\"></i><span>{1}</span>", f.CssClass, f.FunctionName);

                        string subMenu = LoadSubMenu(f.FunctionCode, functions, ref isActive);
                        if (!string.IsNullOrEmpty(subMenu))
                        {
                            liClass = " class=\"treeview\"";
                            if (isActive) liClass = " class=\"treeview active menu-open\"";
                            liContent += "<span class=\"pull-right-container\"><i class=\"fa fa-angle-left pull-right\"></i></span>";
                        }
                        else
                            if (isActive) liClass = " class=\"active\"";
                        liContent += "</a>";
                        liContent += subMenu;


                        result += string.Format("<li {0}>{1}</li>", liClass, liContent);
                    }

                }
            }
            ltrMenu.Text = result;
        }

        private string LoadSubMenu(string code, List<TblCommonFunction> function, ref bool isActive)
        {
            string result = string.Empty;
            List<TblCommonFunction> subMenu = function.FindAll(p => !string.IsNullOrEmpty(p.ParentCode) && p.ParentCode.Trim() == code.Trim());
            if (subMenu.Count > 0)
            {
                result += " <ul class=\"treeview-menu\">";
                foreach (TblCommonFunction f in subMenu)
                {
                    isActive |= f.FunctionCode == CURRENT_PAGE.FUNCTION_CODE;
                    if (isActive)
                        result += string.Format("<li><a href=\"{0}\"><i class=\"fa fa-circle-o\"></i>{1}</a></li>", f.NavigateUrl, f.FunctionName);
                    else
                        result += string.Format("<li class=\"active\"><a href=\"{0}\"><i class=\"fa fa-circle-o\"></i>{1}</a></li>", f.NavigateUrl, f.FunctionName);
                }
                result += "</ul>";
            }

            function.RemoveAll(p => !string.IsNullOrEmpty(p.ParentCode) && p.ParentCode.Trim() == code.Trim());
            return result;
        }
    }
}