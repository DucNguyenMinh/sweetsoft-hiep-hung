﻿ <%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HH_MayMocCongViec.ascx.cs" Inherits="SweetSoft.HiepHung.Controls.HH_MayMocCongViec" %>
<asp:UpdatePanel ID="update" runat="server">
    <ContentTemplate>
        <asp:HiddenField ID="hiddenID" runat="server" />
        <asp:HiddenField ID="hiddenName" runat="server" />
        <div class="row">
            <div class="col-lg-12">
                <div class="col-lg-12">
                    <div class="bootstrap-duallistbox-container row moveonselect">
                        <div class="box1 col-md-6 col-lg-6 col-xs-12">
                            <span class="info-container">
                                <span class="info">Danh sách công việc hệ thống</span>
                            </span>
                            <asp:Repeater ID="repAdd" runat="server">
                                <HeaderTemplate>
                                    <table class="table dataTable table-bordered table-hover dt-responsive responsive">
                                        <thead>
                                            <tr>
                                                <td align="center">STT</td>
                                                <td>Tên công việc</td>
                                                <td></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center"><%# Container.ItemIndex +1 %></td>
                                        <td><%# Eval("TenCongViec") %></td>
                                        <td align="center">
                                            <asp:LinkButton ID="btnAdd" ToolTip="Thêm công việc" runat="server" OnClick="btnAdd_Click" CssClass="btn btn-primary" CommandArgument='<%# Eval("ID") %>' CommandName='<%# Eval("TenCongViec") %>'>
                                               <i class="fa fa-arrow-right"></i>
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                   </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                        <div class="box1 col-md-6 col-lg-6 col-xs-12">
                            <span class="info-container">
                                <span class="info">Công việc máy đảm nhận</span>
                            </span>

                            <asp:Repeater ID="repRemove" runat="server">
                                <HeaderTemplate>
                                    <table class="table dataTable table-bordered table-hover dt-responsive responsive">
                                        <thead>
                                            <tr>

                                                <td></td>
                                                <td align="center">STT</td>
                                                <td>Tên công việc</td>

                                            </tr>
                                        </thead>
                                        <tbody>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <tr>
                                        <td align="center"><%# Container.ItemIndex +1 %></td>
                                        <td><%# Eval("TenCongViec") %></td>
                                        <td align="center">
                                            <asp:LinkButton ID="btnRemove" ToolTip="Xóa công việc" runat="server" OnClick="btnRemove_Click" CssClass="btn btn-warning" CommandArgument='<%# Eval("ID") %>' CommandName='<%# Eval("TenCongViec") %>'>
                                               <i class="fa fa-arrow-left"></i>
                                            </asp:LinkButton>
                                        </td>
                                    </tr>
                                </ItemTemplate>
                                <FooterTemplate>
                                    </tbody>
                                   </table>
                                </FooterTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </ContentTemplate>
</asp:UpdatePanel>
