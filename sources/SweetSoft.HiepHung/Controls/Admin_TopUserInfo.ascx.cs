﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Controls
{
    public partial class Admin_TopUserInfo : BaseUserControl
    {
        private TblCommonUser CurrentUser
        {
            get
            {
                return Session[SessionName.User_CurrentUser] as TblCommonUser;
            }
        }

        //vi.ho 14-06-16
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                LoadUserInfo();
            }
        }

        private void LoadUserInfo()
        {
            if (CurrentUser != null && CurrentUser.CurrentNhanVien != null)
            {
                if (!string.IsNullOrEmpty(CurrentUser.CurrentNhanVien.HoVaTen))
                    ltrUserName.Text = CurrentUser.UserName;
                else
                    ltrUserName.Text = CurrentUser.CurrentNhanVien.HoVaTen;
            }
        }

        //protected string GetAvatarUrl()
        //{
        //    string url = "/Images/defaultavatar.png";
        //    if (CurrentUser != null)
        //    {
        //        if (!string.IsNullOrEmpty(CurrentUser.Sex))
        //        {
        //            if (CurrentUser.Sex.Equals("Nữ"))
        //                url = "/Images/no-avatar-female.jpg";
        //            else
        //                url = "/Images/no-avatar-male.jpg";
        //        }
        //    }
        //    return url;
        //}
    }
}