﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HH_SanPham.ascx.cs" Inherits="SweetSoft.HiepHung.Controls.HH_SanPham" %>

 
<asp:UpdatePanel ID="updateDetail" runat="server">
    <ContentTemplate>
        <div class="widget-box">
            <div class="widget-header">
                <h5 class="widget-title bigger lighter">
                    <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                <div class=" widget-toolbar filter no-border">
                    <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                </div>
                <div class="widget-toolbar no-border">
                    <asp:HiddenField ID="hiddenSanPhamID" runat="server" />
                    <SweetSoft:ExtraButton ID="btnSave" Transparent="true" runat="server" EIcon="A_Save" Text="Lưu sản phẩm" OnClick="btnSave_Click">
                    </SweetSoft:ExtraButton>
                    <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                    </SweetSoft:ExtraButton>
                </div>
            </div>
            <div class="widget-body">
                <div class="widget-main ">
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Mã sản phẩm (*) </label>
                                <SweetSoft:ExtraTextBox ID="txtEditMaSanPham" runat="server" PlaceHolder="Nhập mã sản phẩm"> </SweetSoft:ExtraTextBox>

                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Tên sản phẩm (*) </label>
                                <SweetSoft:ExtraTextBox ID="txtEditTenSanPham" runat="server" PlaceHolder="Nhập tên sản phẩm"> </SweetSoft:ExtraTextBox>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Khách hàng </label>
                                <SweetSoft:ExtraComboBox ID="cbEditKhachHang" runat="server" EmptyMessage="Không chọn khách hàng" Visible="false"></SweetSoft:ExtraComboBox>
                                <SweetSoft:ExtraTextBox ID="txtEditCustomer" runat="server" PlaceHolder="Chọn khách hàng" CssClass="chooseCustomer"></SweetSoft:ExtraTextBox>
                                <asp:HiddenField ID="hiddenKhachHangID" runat="server" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Kích thước (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditKichThuoc" runat="server" EmptyMessage="Chọn Kích thước">
                                    <asp:ListItem Value="Lọt lòng" Text="Lọt lòng"></asp:ListItem>
                                    <asp:ListItem Value="Phủ bì" Text="Phủ bì"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Dài x Rộng x Cao (*) </label>
                                <div class="input-group">
                                    <SweetSoft:ExtraTextBox ID="txtEditDai" ValidStyle="Tooltip" runat="server" PlaceHolder="Nhập dài" Required="true" ValidationType="Currency"></SweetSoft:ExtraTextBox>
                                    <span class="input-group-addon"><b>X</b></span>
                                    <SweetSoft:ExtraTextBox ID="txtEditRong" ValidStyle="Tooltip" runat="server" PlaceHolder="Nhập rộng" Required="true" ValidationType="Currency"></SweetSoft:ExtraTextBox>
                                    <span class="input-group-addon"><b>X</b></span>
                                    <SweetSoft:ExtraTextBox ID="txtEditCao" ValidStyle="Tooltip" runat="server" PlaceHolder="Nhập cao" Required="true" ValidationType="Currency"></SweetSoft:ExtraTextBox>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Cán lằn (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditCanLan" runat="server" EmptyMessage="Chọn cán lằn">
                                    <asp:ListItem Value="đơn" Text="Đơn"></asp:ListItem>
                                    <asp:ListItem Value="kép" Text="Kép"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Bồi Offset (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditBoiOffset" runat="server" EmptyMessage="Chọn bồi Offset">
                                    <asp:ListItem Value="Có" Text="Có"></asp:ListItem>
                                    <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Bể khuôn (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditBeKhuon" runat="server" EmptyMessage="Chọn bể khuôn">
                                    <asp:ListItem Value="Có" Text="Có"></asp:ListItem>
                                    <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Tráng hóa chất (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditTrangHoaChat" runat="server" EmptyMessage="Chọn cách tráng">
                                    <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                                    <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                                    <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                                    <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Tráng BOPP (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditTrangBOPP" runat="server" EmptyMessage="Chọn cách tráng">

                                    <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                                    <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                                    <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                                    <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Tráng UV (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditTrangUV" runat="server" EmptyMessage="Chọn cách tráng">

                                    <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                                    <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                                    <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                                    <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Loại giấy mặt (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditLoaiGiayMat" runat="server" EmptyMessage="Chọn loại giấy">
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Loại giấy sóng (*)</label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditLoaiGiaySong" runat="server" EmptyMessage="Chọn loại giấy">
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Loại giấy đáy (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditLoaiGiayDay" runat="server" EmptyMessage="Chọn loại giấy">
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Số lớp (*)</label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditSoLop" EmptyMessage="Chọn số lớp" runat="server" AutoPostBack="true" OnSelectedIndexChanged="cbEditSoLop_SelectedIndexChanged">
                                    <asp:ListItem Value="3" Text="3 lớp"></asp:ListItem>
                                    <asp:ListItem Value="5" Text="5 lớp"></asp:ListItem>
                                    <asp:ListItem Value="7" Text="7 lớp"></asp:ListItem>
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Sóng (*) </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditSong" runat="server" EmptyMessage="Chọn sóng">
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">
                                    Đơn vị tính (*)
                                </label>
                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" ID="cbEditDonViTinh" runat="server" EmptyMessage="Chọn đơn vị tính">
                                </SweetSoft:ExtraComboBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">
                                    <asp:Literal ID="ltrDonGia" runat="server"></asp:Literal>
                                </label>
                                <SweetSoft:ExtraTextBox ID="txtEditDonGia" runat="server" ValidationType="Currency" PlaceHolder="Nhập đơn giá" Text="0"></SweetSoft:ExtraTextBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">
                                    Số lượng
                                </label>
                                <SweetSoft:ExtraTextBox ID="txtEditSoLuong" runat="server" ValidationType="Currency" PlaceHolder="Nhập số lượng" Text="0"></SweetSoft:ExtraTextBox>
                            </div>
                        </div>
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <div class="form-group">
                                <label class="extra-label">Trạng thái sử dụng </label>
                                <SweetSoft:ExtraCheckBox ID="chkEditTrangThaiSuDung" CheckboxType="Switch7" Checked="true" runat="server" DataOn="Có" DataOff="Không"></SweetSoft:ExtraCheckBox>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="widget-box transparent ui-sortable-handle">
                                    <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">Hình ảnh mẫu thùng</h5>
                                        <div class=" widget-toolbar filter no-border">
                                            <div class="form-group">
                                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" Width="200px" ID="cbEditLoaiThung" runat="server" EmptyMessage="Chọn loại thùng" AutoPostBack="true" OnSelectedIndexChanged="cbEditLoaiThung_SelectedIndexChanged">
                                                </SweetSoft:ExtraComboBox>
                                                <SweetSoft:ExtraButton ID="btnAddImageThung" runat="server" EIcon="A_Add" EStyle="Default_Bold" OnClientClick="ImgType='Thung';$('#fileUpload').click();return false;"></SweetSoft:ExtraButton>
                                                <input type="file" id="fileUpload" multiple="multiple" accept=".jpg,.jpeg,.png" style="display: none;" />
                                                <asp:Button ID="btnReload" runat="server" Style="display: none" OnClick="btnReload_Click" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <asp:Literal ID="ltrImageThung" runat="server" Text="Không có hình ảnh tham chiếu"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="col-lg-12">
                                <div class="widget-box transparent  ui-sortable-handle">
                                    <div class="widget-header">
                                        <h5 class="widget-title bigger lighter">Hình ảnh mẫu in</h5>
                                        <div class=" widget-toolbar filter no-border">
                                            <div class="form-group">
                                                <SweetSoft:ExtraComboBox ComboStyle="Dropdown_White" Width="200px" ID="cbEditLoaiMauIn" runat="server" EmptyMessage="Chọn loại mẫu in" AutoPostBack="true" OnSelectedIndexChanged="cbEditLoaiMauIn_SelectedIndexChanged">
                                                </SweetSoft:ExtraComboBox>
                                                <SweetSoft:ExtraButton ID="btnAddImageMauIn" runat="server" EIcon="A_Add" EStyle="Default_Bold" OnClientClick="ImgType='LMI';$('#fileUpload').click();return false;"></SweetSoft:ExtraButton>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="widget-body">
                                        <asp:Literal ID="ltrImageMauIn" runat="server" Text="Không có hình ảnh tham chiếu"></asp:Literal>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </ContentTemplate>
</asp:UpdatePanel>

<script src="/scripts/jquery.light.js"></script>
<script type="text/javascript">
    var ImgType = "";
    $(function () {
        InnitUpload();
        addRequestHanlde(InnitUpload);
    });
    function InnitUpload() {
        $('a[rel=light]').light();
        $("#fileUpload").on("change", function (e) {
            var data = new FormData();
            var files = e.target.files;

            var id = $("input[type='hidden'][id$='hiddenSanPhamID']").val();

            // if (files.FileList > 0)
            {
                data.append("UploadedExcelFile", files[0]);
                data.append("UploadType", "SanPham");
                for (var i = 0; i < files.length; i++) {
                    data.append("file_" + i, files[i]);
                }
                data.append("SanPhamID", id);
                data.append("ImgType", ImgType);
            }
         
            triggerUpload = true;
            // Make Ajax request with the contentType = false, and procesDate = false
            var ajaxRequest = $.ajax({
                type: "POST",
                url: "/UploadHandler.ashx",
                contentType: false,
                processData: false,
                data: data
            });

            ajaxRequest.done(function (xhr, textStatus) {
                triggerUpload = false;

                if (textStatus === "success") {
                    var btnReload = $("input[id$='btnReload']");
                    if (btnReload.length > 0) btnReload.click();
                }
            });
        });
    }

    function RemoveImage(id) {
        var data = { "id": id, "type": "SanPham" };
        var result = AjaxPost(data, "/Default.aspx/RemoveImage");

        var btnReload = $("input[id$='btnReload']");
        if (btnReload.length > 0) btnReload.click();
    }


</script>

