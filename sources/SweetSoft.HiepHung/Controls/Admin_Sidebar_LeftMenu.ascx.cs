﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.Core.Helper;

namespace SweetSoft.HiepHung.Controls
{
    public partial class Admin_Sidebar_LeftMenu : BaseUserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                TblCommonUser u = Session[SessionName.User_CurrentUser] as TblCommonUser;
                if (u != null)
                {
                    //   LoadMenu();
                }
                LoadMenu();
            }
        }

        private void LoadMenu()
        {
            string html = string.Empty;
            List<TblCommonFunction> userFunction
                = FuntionManager.GetAllFunction();
            List<TblCommonFunction> headFunction
                = userFunction.FindAll(p => string.IsNullOrEmpty(p.ParentCode));
            if (headFunction != null)
            {
                foreach (TblCommonFunction F in headFunction)
                {
                    bool isActive = false;
                    string subHtml = LoadSubMenu(F, userFunction, ref isActive);
                    if (!string.IsNullOrEmpty(subHtml))
                        html += subHtml;
                    else
                        html += string.Format(@" <li class='{3}'>
                                                <a href='{0}'><i class='{1}'></i>{2}
                                                 </a><b class='arrow'></b>
                                               </li>",
                                              F.NavigateUrl, F.CssClass, string.Format("<span style='margin-left:5px' class='menu-text'>{0}</span>", F.FunctionName),
                                              FUNCTION_CODE.Equals(F.FunctionCode) ? "active" : string.Empty);
                }
            }
            ltrHtml.Text = html;
        }

        private string LoadSubMenu(TblCommonFunction F, List<TblCommonFunction> lstFunction, ref bool active)
        {
            string result = string.Empty;
            List<TblCommonFunction> subFunction
                = lstFunction.FindAll(p => !string.IsNullOrEmpty(p.NavigateUrl) && p.ParentCode == F.FunctionCode);
            if (subFunction.Count > 0)
            {
                bool isActive = false;

                foreach (TblCommonFunction SF in subFunction)
                {
                    bool ac =  false;
                    string subSubHtml = LoadSubMenu(SF, lstFunction, ref ac);
                    isActive |= ac || FUNCTION_CODE.Equals(SF.FunctionCode.Trim());
                    if (!string.IsNullOrEmpty(subSubHtml))
                        result += subSubHtml;
                    else
                        result += string.Format(@" <li class='{3}'>
                                                <a href='{0}'><i class='{1}'></i>{2}
                                                 </a><b class='arrow'></b>
                                               </li>",
                                                SF.NavigateUrl, SF.CssClass, SF.FunctionName,
                                                FUNCTION_CODE.Equals(SF.FunctionCode.Trim()) ? "active" : string.Empty);
                }
                result = string.Format(@"<li class='{2}'>
                                            <a href='#' class='dropdown-toggle'><i class='{0}'></i>
                                                <span style='margin-left:5px' class='menu-text'>{1} </span>
                                                <b class='arrow fa fa-angle-down'></b>
                                            </a>
                                            <b class='arrow'></b><ul class='submenu' >",
                                        F.CssClass, F.FunctionName, isActive ? "active open" : string.Empty)
                       + result;
                result += " </ul></li>";
                active = isActive;
            }
            lstFunction.RemoveAll(p => !string.IsNullOrEmpty(p.FunctionCode) &&
                                       p.ParentCode == F.FunctionCode);

            return result;
        }

    }
}