﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="HH_Header.ascx.cs" Inherits="SweetSoft.HiepHung.Controls.HH_Header" %>

<!-- Logo -->
<a href="javascript:void(0)" class="logo">

    <span class="logo-mini">
        <img src="../Images/logo.small.png" width="45" height="45" alt="Hiệp Hưng Nha Trang" title="Hiệp Hưng Nha Trang" />
    </span>

    <span class="logo-lg">
        <img src="../Images/logo.big.png" width="210" height="50" alt="Hiệp Hưng Nha Trang" title="Hiệp Hưng Nha Trang" /></span>
</a>

<nav class="navbar navbar-static-top">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Ẩn / hiện danh sách chức năng</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <ul class="dropdown-menu">

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="#" class="btn btn-default btn-flat">Thông tin người dùng</a>
                        </div>
                        <div class="pull-right">
                            <a href="#" class="btn btn-default btn-flat">Đăng xuất</a>
                        </div>
                    </li>
                </ul>
            </li>
            <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
            </li>
        </ul>
    </div>
</nav>

