﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Controls
{
    public partial class HH_MayMocCongViec : BaseUserControl
    {
        public event EventHandler changed;
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        public void ShowData(object id, string name)
        {
            if (id != null) hiddenID.Value = id.ToString();
            hiddenName.Value = name;
            ShowData();
        }
        private void ShowData()
        {
            if (!string.IsNullOrEmpty(hiddenID.Value))
            {
                List<TblCongViec> congViecCol = CategoryManager.GetCongViecALL(true);
                List<TblMayMocCongViec> mayMocCongViecCol = MayMocManager.GetMayMocCongViecByMayMocID(hiddenID.Value);

                repRemove.DataSource = congViecCol.FindAll(p => mayMocCongViecCol.Find(x => x.IDCongViec.Value == p.Id) != null);

                repRemove.DataBind();

                repAdd.DataSource = congViecCol.FindAll(p => mayMocCongViecCol.Find(x => x.IDCongViec.Value == p.Id) == null);

                repAdd.DataBind();
            }
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                string name = btn.CommandName;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblMayMocCongViec congViec = new TblMayMocCongViec();
                    congViec.IDCongViec = int.Parse(cmd);
                    congViec.IDMayMoc = int.Parse(hiddenID.Value);
                    congViec = MayMocManager.InsertMayMocCongViec(congViec);

                    #region Log
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.MM_Update,
                                                 string.Format("Thêm công việc <b>{0}</b> cho máy móc <b>{1}</b>", name, hiddenName));
                    CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tên máy móc",
                                            TblMayMoc.Columns.TenMay,
                                            hiddenName.Value);
                    CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tên công việc",
                                            TblMayMoc.Columns.Id,
                                            name);
                    CurrentLog.SaveLog(RightPageName.MM_Update);
                    #endregion
                    ShowNotify("Thông báo", "Hệ thống thêm công việc cho máy móc thành công", ExtraControls.NotifyType.Success);
                    ShowData();
                    if (changed != null) changed(sender, e);
                }
                else
                    ShowNotify("Thông báo", "Vui lòng chọn công việc cần chuyển", ExtraControls.NotifyType.Warning);
            }

        }

        protected void btnRemove_Click(object sender, EventArgs e)
        {
            LinkButton btn = sender as LinkButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                string name = btn.CommandName;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblMayMocCongViec congViec = MayMocManager.GetMayMocCongViec(hiddenID.Value, cmd);
                    if (congViec != null)
                    {
                        MayMocManager.DeleteMayMocCongViec(congViec.Id);
                        #region Log
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.MM_Update,
                                                     string.Format("Xóa công việc <b>{0}</b> khỏi máy móc <b>{1}</b>", name, hiddenName));
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tên máy móc",
                                                TblMayMoc.Columns.TenMay,
                                                hiddenName.Value);
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tên công việc",
                                                TblMayMoc.Columns.Id,
                                                name);
                        CurrentLog.SaveLog(RightPageName.MM_Update);
                        #endregion
                        ShowNotify("Thông báo", "Hệ thống xóa công việc khỏi máy móc thành công", ExtraControls.NotifyType.Success);
                        ShowData();
                        if (changed != null) changed(sender, e);
                    }

                    else
                        ShowNotify("Thông báo", "Không tìm thấy thông tin chuyển", ExtraControls.NotifyType.Warning);

                }
                else
                    ShowNotify("Thông báo", "Vui lòng chọn công việc cần chuyển", ExtraControls.NotifyType.Warning);
            }
        }
    }
}