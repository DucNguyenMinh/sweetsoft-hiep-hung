﻿

using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core;

namespace SweetSoft.HiepHung.Controls
{
    public partial class HH_SanPham : BaseUserControl
    {
        public event EventHandler Inserted;
        public event EventHandler Updated;

        private bool addNew = false;
        public bool EnableInlineAddNew { get { return addNew; } set { addNew = value; } }

        private bool runSingle = true;
        public bool RunSingle { get { return runSingle; } set { runSingle = value; } }

        public long CurrentSPID
        {
            get
            {
                long result = 0;
                if (!long.TryParse(hiddenSanPhamID.Value, out result)) result = 0;
                return result;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ltrDonGia.Text = "Nhập đơn giá cơ bản";

                btnAddNew.Visible = UserHasRight(RightPageName.SP_Insert) && EnableInlineAddNew;
                btnSave.Visible = (UserHasRight(RightPageName.SP_Update) && !string.IsNullOrEmpty(hiddenSanPhamID.Value)) ||
                                  (UserHasRight(RightPageName.SP_Insert) && string.IsNullOrEmpty(hiddenSanPhamID.Value));


            }
        }

        public void LoadMauThung(List<TblLoaiMauIn> loaiThung)
        {
            List<TblLoaiMauIn> loaiThung2 = new List<TblLoaiMauIn>(loaiThung);
            TblLoaiMauIn all = new TblLoaiMauIn();
            all.Id = -1;
            all.TenMau = "Không chọn mẫu";
            loaiThung2.Insert(0, all);
            cbEditLoaiThung.DataSource = loaiThung2;
            cbEditLoaiThung.DataValueField = TblLoaiMauIn.Columns.Id;
            cbEditLoaiThung.DataTextField = TblLoaiMauIn.Columns.TenMau;
            cbEditLoaiThung.DataBind();
            cbEditLoaiThung.SelectedValue = "-1";
        }

        public void LoadMauIn(List<TblLoaiMauIn> loaiThung)
        {
            List<TblLoaiMauIn> loaiThung2 = new List<TblLoaiMauIn>(loaiThung);
            TblLoaiMauIn all = new TblLoaiMauIn();
            all.Id = -1;
            all.TenMau = "Không chọn mẫu";
            loaiThung2.Insert(0, all);
            cbEditLoaiMauIn.DataSource = loaiThung2;
            cbEditLoaiMauIn.DataValueField = TblLoaiMauIn.Columns.Id;
            cbEditLoaiMauIn.DataTextField = TblLoaiMauIn.Columns.TenMau;
            cbEditLoaiMauIn.DataBind();
            cbEditLoaiThung.SelectedValue = "-1";
        }

        public void LoadLoaiGiay(List<TblLoaiGiay> loaiGiayCol)
        {
            List<TblLoaiGiay> loaiGiayColn = new List<TblLoaiGiay>(loaiGiayCol);
            cbEditLoaiGiayDay.DataSource = loaiGiayColn;
            cbEditLoaiGiayDay.DataValueField = TblLoaiGiay.Columns.Id;
            cbEditLoaiGiayDay.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
            cbEditLoaiGiayDay.DataBind();

            List<TblLoaiGiay> loaiGiayCol2 = new List<TblLoaiGiay>(loaiGiayCol);
            cbEditLoaiGiayMat.DataSource = loaiGiayCol2;
            cbEditLoaiGiayMat.DataValueField = TblLoaiGiay.Columns.Id;
            cbEditLoaiGiayMat.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
            cbEditLoaiGiayMat.DataBind();

            List<TblLoaiGiay> loaiGiayCol3 = new List<TblLoaiGiay>(loaiGiayCol);
            cbEditLoaiGiaySong.DataSource = loaiGiayCol3;
            cbEditLoaiGiaySong.DataValueField = TblLoaiGiay.Columns.Id;
            cbEditLoaiGiaySong.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
            cbEditLoaiGiaySong.DataBind();
        }

        public void LoadDonViTinh(List<TblDonViTinh> donViTinhCol)
        {
            List<TblDonViTinh> donViTinhCol2 = new List<TblDonViTinh>(donViTinhCol);
            cbEditDonViTinh.DataSource = donViTinhCol2;
            cbEditDonViTinh.DataValueField = TblDonViTinh.Columns.Id;
            cbEditDonViTinh.DataTextField = "DisplayText";
            cbEditDonViTinh.DataBind();
        }


        public void LoadSanPham(object id)
        {
            hiddenSanPhamID.Value = string.Empty;

            if (id != null && id.ToString().Trim() != "0")
            {
                TblSanPham sanPham = SanPhamManager.GetSanPhamByID(id);
                if (sanPham != null)
                {

                    ltrDonGia.Text = "Nhập đơn giá sản phẩm";
                    hiddenSanPhamID.Value = id.ToString();
                    txtEditMaSanPham.Text = sanPham.MaSanPham;
                    txtEditTenSanPham.Text = sanPham.TenSanPham;
                    if (sanPham.IDKhachHangRieng.HasValue)
                    {
                        txtEditCustomer.Text = sanPham.TenKhachHang;
                        hiddenKhachHangID.Value = sanPham.IDKhachHangRieng.Value.ToString();
                    }
                    txtEditDai.Text = sanPham.Dai.Value.ExecptionMod().ToStringWithLanguage();
                    txtEditRong.Text = sanPham.Rong.Value.ExecptionMod().ToStringWithLanguage();
                    txtEditCao.Text = sanPham.Cao.Value.ExecptionMod().ToStringWithLanguage();

                    cbEditKichThuoc.SelectedValue = sanPham.KichThuoc;
                    cbEditCanLan.SelectedValue = sanPham.CanLan;

                    cbEditBoiOffset.SelectedIndex = sanPham.BoiOffset.HasValue && sanPham.BoiOffset.Value ? 0 : 1;
                    cbEditBeKhuon.SelectedIndex = sanPham.BeKhuon.HasValue && sanPham.BeKhuon.Value ? 0 : 1;

                    cbEditTrangHoaChat.SelectedValue = sanPham.TrangHoaChat;
                    cbEditTrangBOPP.SelectedValue = sanPham.TrangBOPP;
                    cbEditTrangUV.SelectedValue = sanPham.TrangUV;

                    ListItem item = cbEditLoaiGiayMat.Items.FindByValue(sanPham.IDLoaiGiayMat.Value.ToString());
                    if (item != null)
                        cbEditLoaiGiayMat.SelectedValue = sanPham.IDLoaiGiayMat.Value.ToString();

                    item = cbEditLoaiGiayDay.Items.FindByValue(sanPham.IDLoaiGiayDay.Value.ToString());
                    if (item != null)
                        cbEditLoaiGiayDay.SelectedValue = sanPham.IDLoaiGiayDay.Value.ToString();

                    item = cbEditLoaiGiaySong.Items.FindByValue(sanPham.IDLoaiGiaySong.Value.ToString());
                    if (item != null)
                        cbEditLoaiGiaySong.SelectedValue = sanPham.IDLoaiGiaySong.Value.ToString();
                    item = cbEditSoLop.Items.FindByValue(sanPham.SoLop.Value.ToString());
                    if (item != null)
                    {
                        cbEditSoLop.SelectedValue = sanPham.SoLop.Value.ToString();
                        LoadSoLop(sanPham.SoLop.Value.ToString());
                    }

                    item = cbEditSong.Items.FindByValue(sanPham.SoSong);
                    if (item != null)
                        cbEditSong.SelectedValue = sanPham.SoSong;

                    item = cbEditDonViTinh.Items.FindByValue(sanPham.IDDonViTinh.Value.ToString());
                    if (item != null)
                        cbEditDonViTinh.SelectedValue = sanPham.IDDonViTinh.Value.ToString();

                    if (sanPham.DonGia.HasValue) txtEditDonGia.Text = sanPham.DonGia.Value.ExecptionMod().ToStringWithLanguage();
                    if (sanPham.SoLuong.HasValue) txtEditSoLuong.Text = sanPham.SoLuong.Value.ExecptionMod().ToStringWithLanguage();
                    chkEditTrangThaiSuDung.Checked = sanPham.TrangThaiSuDung.HasValue && sanPham.TrangThaiSuDung.Value;

                    if (sanPham.IDLoaiMauIn.HasValue)
                    {
                        item = cbEditLoaiMauIn.Items.FindByValue(sanPham.IDLoaiMauIn.Value.ToString());
                        if (item != null) cbEditLoaiMauIn.SelectedValue = sanPham.IDLoaiMauIn.Value.ToString();
                    }

                    if (sanPham.IDKieuThung.HasValue)
                    {
                        item = cbEditLoaiThung.Items.FindByValue(sanPham.IDKieuThung.Value.ToString());
                        if (item != null) cbEditLoaiThung.SelectedValue = sanPham.IDKieuThung.Value.ToString();
                    }

                    RefreshImage("both");
                }
            }

        }

        private void RefreshImage(string code)
        {
            if (code == "LMI" || code == "both")
            {
                ltrImageMauIn.Text = string.Empty;
                if (cbEditLoaiMauIn.HasValue) ltrImageMauIn.Text = GetImage(cbEditLoaiMauIn.SelectedValue);

                if (CurrentSPID > 0)
                {
                    List<TblSanPhamImage> images2 = SanPhamManager.GetSanPhamImageBySPID(CurrentSPID, "LMI");
                    if (images2.Count > 0)
                    {

                        foreach (TblSanPhamImage img in images2)
                        {
                            string imgs = string.Format(@"<a href='{0}' data-gallery='{1}' rel='light'><img class='e-file-image' src='{0}' /></a>", img.ImagePath, "1");
                            string remove = string.Format(@"<a class='btn btn-danger btn-white' onclick='RemoveImage({0});'><i class='fa fa-trash'></i></a>", img.Id.ToString());
                            ltrImageMauIn.Text += string.Format(@"<div class='e-preview new'><div class='e-preview-content'>{0}</div><div class='e-preview-footer'>{1}</div></div>", imgs, remove);
                        }
                    }
                }

                if (string.IsNullOrEmpty(ltrImageMauIn.Text)) ltrImageMauIn.Text = "Không có hình ảnh tham chiếu";
            }
            if (code == "Thung" || code == "both")
            {
                ltrImageThung.Text = "";
                if (cbEditLoaiThung.HasValue) ltrImageThung.Text = GetImage(cbEditLoaiThung.SelectedValue);
                if (CurrentSPID > 0)
                {
                    List<TblSanPhamImage> images2 = SanPhamManager.GetSanPhamImageBySPID(CurrentSPID, "Thung");
                    if (images2.Count > 0)
                    {

                        foreach (TblSanPhamImage img in images2)
                        {
                            string imgs = string.Format(@"<a href='{0}' data-gallery='{1}' rel='light'><img class='e-file-image' src='{0}' /></a>", img.ImagePath, "1");
                            string remove = string.Format(@"<a class='btn btn-danger btn-white' onclick='RemoveImage({0});'><i class='fa fa-trash'></i></a>", img.Id.ToString());
                            ltrImageThung.Text += string.Format(@"<div class='e-preview new'><div class='e-preview-content'>{0}</div><div class='e-preview-footer'>{1}</div></div>", imgs, remove);
                        }
                    }
                }
                if (string.IsNullOrEmpty(ltrImageThung.Text)) ltrImageThung.Text = "Không có hình ảnh tham chiếu";
            }
        }

        protected void cbEditSoLop_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadSoLop(cbEditSoLop.SelectedValue);
        }

        private void LoadSoLop(string value)
        {
            cbEditSong.Items.Clear();
            switch (value)
            {
                case "3":
                    cbEditSong.Items.Add(new ListItem("A", "A"));
                    cbEditSong.Items.Add(new ListItem("B", "B"));
                    cbEditSong.Items.Add(new ListItem("E", "E"));
                    break;
                case "5":
                    cbEditSong.Items.Add(new ListItem("AB", "AB"));
                    cbEditSong.Items.Add(new ListItem("BE", "BE"));
                    cbEditSong.Items.Add(new ListItem("AE", "AE"));
                    break;
                case "7":
                    cbEditSong.Items.Add(new ListItem("ABE", "ABE"));
                    break;
            }

        }

        protected string GetImage(object id)
        {
            string result = string.Empty;
            List<TblImage> images = CategoryManager.GetLoaiMauImage(id);
            if (images.Count > 0)
            {
                foreach (TblImage img in images)
                {
                    string imgs = string.Format(@"<a href='{0}' data-gallery='{1}' rel='light'><img class='e-file-image' src='{0}' /></a>", img.ImagePath, "1");
                    result += string.Format(@"<div class='e-preview new'><div class='e-preview-content'>{0}</div><div class='e-preview-footer'></div></div>", imgs);
                }
            }

            return result;
        }

        public void Save()
        {
            #region Validation
            TblSanPham currentSanPham = null;
            if (!string.IsNullOrEmpty(txtEditMaSanPham.Text.Trim()))
            {
                currentSanPham = SanPhamManager.GetSanPhamByCode(txtEditMaSanPham.Text.Trim());
                if (currentSanPham != null)
                {
                    if (!string.IsNullOrEmpty(hiddenSanPhamID.Value))
                    {
                        if (currentSanPham.Id.ToString() != hiddenSanPhamID.Value)
                        {
                            AddValidPromt(txtEditMaSanPham.ClientID, "Mã sản phẩm đã được sử dụng, Vui lòng kiểm tra lại hệ thống.");
                            txtEditMaSanPham.Focus();
                        }
                    }
                    else
                    {

                        AddValidPromt(txtEditMaSanPham.ClientID, "Mã sản phẩm đã được sử dụng, Vui lòng kiểm tra lại hệ thống.");
                        txtEditMaSanPham.Focus();
                    }
                }
            }

            if (string.IsNullOrEmpty(txtEditTenSanPham.Text))
            {
                AddValidPromt(txtEditTenSanPham.ClientID, "Nhập tên sản phẩm");
                txtEditTenSanPham.Focus();
            }

            if (!cbEditKichThuoc.HasValue)
            {
                AddValidPromt(cbEditKichThuoc.ClientID, "Chọn kích thước");
                cbEditKichThuoc.Focus();
            }

            decimal dai = 0;
            if (string.IsNullOrEmpty(txtEditDai.Text))
            {
                AddValidPromt(txtEditDai.ClientID, "Nhập chiều dài");
                txtEditDai.Focus();
            }
            else
                dai = CommonHelper.ParseDecimal(txtEditDai.Text.Trim());

            decimal rong = 0;
            if (string.IsNullOrEmpty(txtEditRong.Text))
            {
                AddValidPromt(txtEditRong.ClientID, "Nhập chiều rộng");
                txtEditRong.Focus();
            }
            else
                rong = CommonHelper.ParseDecimal(txtEditRong.Text.Trim());


            decimal cao = 0;
            if (string.IsNullOrEmpty(txtEditCao.Text))
            {
                AddValidPromt(txtEditCao.ClientID, "Nhập chiều cao");
                txtEditCao.Focus();
            }
            else
                cao = CommonHelper.ParseDecimal(txtEditCao.Text.Trim());


            if (!cbEditCanLan.HasValue)
            {
                AddValidPromt(cbEditCanLan.ClientID, "Chọn cán lằn");
                cbEditCanLan.Focus();
            }

            if (!cbEditBoiOffset.HasValue)
            {
                AddValidPromt(cbEditBoiOffset.ClientID, "Chọn bồi offset");
                cbEditBoiOffset.Focus();
            }

            if (!cbEditBeKhuon.HasValue)
            {
                AddValidPromt(cbEditBeKhuon.ClientID, "Chọn bể khuôn");
                cbEditBeKhuon.Focus();
            }

            if (!cbEditTrangHoaChat.HasValue)
            {
                AddValidPromt(cbEditTrangHoaChat.ClientID, "Chọn tráng hóa chất");
                cbEditTrangHoaChat.Focus();
            }

            if (!cbEditTrangBOPP.HasValue)
            {
                AddValidPromt(cbEditTrangBOPP.ClientID, "Chọn tráng BOPP");
                cbEditTrangBOPP.Focus();
            }

            if (!cbEditTrangUV.HasValue)
            {
                AddValidPromt(cbEditTrangUV.ClientID, "Chọn tráng UV");
                cbEditTrangUV.Focus();
            }

            if (!cbEditLoaiGiayMat.HasValue)
            {
                AddValidPromt(cbEditLoaiGiayMat.ClientID, "Chọn loại giấy mặt");
                cbEditLoaiGiayMat.Focus();
            }

            if (!cbEditLoaiGiaySong.HasValue)
            {
                AddValidPromt(cbEditLoaiGiaySong.ClientID, "Chọn loại giấy sóng");
                cbEditLoaiGiaySong.Focus();
            }

            if (!cbEditLoaiGiayDay.HasValue)
            {
                AddValidPromt(cbEditLoaiGiayDay.ClientID, "Chọn loại giấy đáy");
                cbEditLoaiGiayDay.Focus();
            }

            if (!cbEditSoLop.HasValue)
            {
                AddValidPromt(cbEditSoLop.ClientID, "Chọn số lớp");
                cbEditSoLop.Focus();
            }

            if (!cbEditSong.HasValue)
            {
                AddValidPromt(cbEditSong.ClientID, "Chọn sóng");
                cbEditSong.Focus();
            }

            if (!cbEditDonViTinh.HasValue)
            {
                AddValidPromt(cbEditDonViTinh.ClientID, "Chọn đơn vị tính");
                cbEditDonViTinh.Focus();
            }

            decimal donGia = 0;
            decimal soLuong = 0;
            if (RunSingle)
            {
                if (string.IsNullOrEmpty(txtEditDonGia.Text.Trim()))
                {
                    AddValidPromt(txtEditDonGia.ClientID, "Nhập đơn giá");
                    txtEditDonGia.Focus();
                }
                else
                {
                    donGia = CommonHelper.ParseDecimal(txtEditDonGia.Text.Trim());
                    if (donGia < 1)
                    {
                        AddValidPromt(txtEditDonGia.ClientID, "Đơn giá phải lớn hơn 0");
                        txtEditDonGia.Focus();
                    }
                }

                if (string.IsNullOrEmpty(txtEditSoLuong.Text.Trim()))
                {
                    AddValidPromt(txtEditSoLuong.ClientID, "Nhập số lượng đặt hàng");
                    txtEditSoLuong.Focus();
                }
                else
                {
                    soLuong = CommonHelper.ParseDecimal(txtEditSoLuong.Text.Trim());
                    if (soLuong < 1)
                    {
                        AddValidPromt(txtEditSoLuong.ClientID, "Số lượng đặt phải lớn hơn 0");
                        txtEditSoLuong.Focus();
                    }
                }
            }
            #endregion

            if (ControlIsValid)
            {
                if (!string.IsNullOrEmpty(hiddenSanPhamID.Value))
                {
                    #region Update
                    TblSanPham sanPham = SanPhamManager.GetSanPhamByID(hiddenSanPhamID.Value);
                    if (sanPham != null)
                    {
                        #region Log First
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.SP_Update, string.Format("Cập nhật sản phẩm <b>[{0}] - {1}</b>", sanPham.MaSanPham, sanPham.TenSanPham));

                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Mã sản phẩm",
                                                    TblSanPham.Columns.MaSanPham,
                                                    sanPham.MaSanPham);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Tên sản phẩm",
                                                    TblSanPham.Columns.TenSanPham,
                                                    sanPham.TenSanPham);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Tên Khách hàng",
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    sanPham.TenSanPham);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Sản phẩm của khách hàng",
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    "");
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Độ dài",
                                                   TblSanPham.Columns.Dai,
                                                   sanPham.Dai.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Độ rộng",
                                                   TblSanPham.Columns.Rong,
                                                    sanPham.Rong.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Độ cao",
                                                   TblSanPham.Columns.Cao,
                                                    sanPham.Cao.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Kích thước",
                                                  TblSanPham.Columns.KichThuoc,
                                                   sanPham.KichThuoc);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Cán lằn",
                                                  TblSanPham.Columns.CanLan,
                                                   sanPham.CanLan);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Bồi Offset",
                                                  TblSanPham.Columns.BoiOffset,
                                                  sanPham.BoiOffset.HasValue && sanPham.BoiOffset.Value ? "Có" : "Không");
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Bể khuôn",
                                                  TblSanPham.Columns.BeKhuon,
                                                   sanPham.BeKhuon.HasValue && sanPham.BeKhuon.Value ? "Có" : "Không");
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Tráng BOPP",
                                                 TblSanPham.Columns.TrangBOPP,
                                                  sanPham.TrangBOPP);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Tráng hóa chất",
                                                 TblSanPham.Columns.TrangHoaChat,
                                                  sanPham.TrangHoaChat);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Tráng UV",
                                                 TblSanPham.Columns.TrangUV,
                                                  sanPham.TrangUV);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Loại giấy đáy",
                                                TblSanPham.Columns.IDLoaiGiayDay,
                                                 sanPham.LoaiGiayDay);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Loại giấy mặt",
                                                TblSanPham.Columns.IDLoaiGiayMat,
                                                 sanPham.LoaiGiayMat);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Loại giấy sóng",
                                                TblSanPham.Columns.IDLoaiGiaySong,
                                                sanPham.LoaiGiaySong);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Số lớp",
                                                TblSanPham.Columns.SoLop,
                                                 sanPham.SoLop.Value.ToString());
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Số sóng",
                                                TblSanPham.Columns.SoSong,
                                                 sanPham.SoSong);
                        CurrentLog.AddFirstValue(RightPageName.SP_Update, "Đơn vị tính",
                                                TblSanPham.Columns.IDDonViTinh,
                                                 sanPham.TenDonVi);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Đơn giá / sản phẩm",
                                                TblSanPham.Columns.DonGia,
                                                sanPham.DonGia.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Số lượng",
                                                TblSanPham.Columns.SoLuong,
                                                 sanPham.SoLuong.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Trạng thái sử dụng",
                                                TblSanPham.Columns.TrangThaiSuDung,
                                                sanPham.TrangThaiSuDung.HasValue && sanPham.TrangThaiSuDung.Value ? "Có sử dụng" : "Không sử dụng");
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Loại mẫu in",
                                                   TblSanPham.Columns.IDLoaiMauIn,
                                                   sanPham.TenMauIn);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Kiểu thùng",
                                                   TblSanPham.Columns.IDKieuThung,
                                                   sanPham.TenKieuThung);
                        #endregion

                        sanPham.MaSanPham = txtEditMaSanPham.Text.Trim();
                        sanPham.TenSanPham = txtEditTenSanPham.Text;
                        sanPham.IDKhachHangRieng = null;
                        if (!string.IsNullOrEmpty(hiddenKhachHangID.Value))
                            sanPham.IDKhachHangRieng = int.Parse(hiddenKhachHangID.Value);
                        sanPham.Dai = dai; sanPham.Rong = rong; sanPham.Cao = cao;
                        sanPham.KichThuoc = cbEditKichThuoc.SelectedValue;
                        sanPham.CanLan = cbEditCanLan.SelectedValue;
                        sanPham.BoiOffset = cbEditBoiOffset.SelectedIndex == 0;
                        sanPham.BeKhuon = cbEditKichThuoc.SelectedIndex == 0;
                        sanPham.TrangBOPP = cbEditTrangBOPP.SelectedValue;
                        sanPham.TrangHoaChat = cbEditTrangHoaChat.SelectedValue;
                        sanPham.TrangUV = cbEditTrangUV.SelectedValue;
                        sanPham.IDLoaiGiayMat = int.Parse(cbEditLoaiGiayMat.SelectedValue);
                        sanPham.IDLoaiGiayDay = int.Parse(cbEditLoaiGiayDay.SelectedValue);
                        sanPham.IDLoaiGiaySong = int.Parse(cbEditLoaiGiaySong.SelectedValue);
                        sanPham.SoLop = int.Parse(cbEditSoLop.SelectedValue);
                        sanPham.SoSong = cbEditSong.SelectedValue;
                        sanPham.IDDonViTinh = int.Parse(cbEditDonViTinh.SelectedValue);
                        sanPham.SoLuong = soLuong;
                        sanPham.DonGia = donGia;
                        sanPham.TrangThaiSuDung = chkEditTrangThaiSuDung.Checked;
                        sanPham.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        sanPham.NgayCapNhat = DateTime.Now;
                        sanPham.IDKieuThung = null;
                        if (cbEditLoaiThung.HasValue)
                            sanPham.IDKieuThung = int.Parse(cbEditLoaiThung.SelectedValue);
                        sanPham.IDLoaiMauIn = null;
                        if (cbEditLoaiMauIn.HasValue)
                            sanPham.IDLoaiMauIn = int.Parse(cbEditLoaiMauIn.SelectedValue);

                        #region Log
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                    TblSanPham.Columns.MaSanPham,
                                                    txtEditMaSanPham.Text);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                    TblSanPham.Columns.TenSanPham,
                                                    txtEditTenSanPham.Text);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    txtEditTenSanPham.Text);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                    TblSanPham.Columns.IDKieuThung,
                                                    cbEditLoaiThung.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                   TblSanPham.Columns.IDLoaiMauIn,
                                                   cbEditLoaiMauIn.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    "");
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                   TblSanPham.Columns.Dai,
                                                   txtEditDai.Text.Trim());
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                   TblSanPham.Columns.Rong,
                                                    txtEditRong.Text.Trim());
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                   TblSanPham.Columns.Cao,
                                                    txtEditCao.Text.Trim());
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                  TblSanPham.Columns.KichThuoc,
                                                   cbEditKichThuoc.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                  TblSanPham.Columns.CanLan,
                                                   cbEditCanLan.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                  TblSanPham.Columns.BoiOffset,
                                                   cbEditBoiOffset.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                  TblSanPham.Columns.BeKhuon,
                                                   cbEditBeKhuon.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                 TblSanPham.Columns.TrangBOPP,
                                                  cbEditTrangBOPP.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                 TblSanPham.Columns.TrangHoaChat,
                                                  cbEditTrangHoaChat.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                 TblSanPham.Columns.TrangUV,
                                                  cbEditTrangUV.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.IDLoaiGiayDay,
                                                 cbEditLoaiGiayDay.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.IDLoaiGiayMat,
                                                 cbEditLoaiGiayMat.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.IDLoaiGiaySong,
                                                 cbEditLoaiGiaySong.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.SoLop,
                                                 cbEditSoLop.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.SoSong,
                                                 cbEditSong.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.IDDonViTinh,
                                                 cbEditDonViTinh.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.DonGia,
                                                sanPham.DonGia.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.SoLuong,
                                                 sanPham.SoLuong.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddLastValue(RightPageName.SP_Update,
                                                TblSanPham.Columns.TrangThaiSuDung,
                                                chkEditTrangThaiSuDung.Checked ? "Có sử dụng" : "Không sử dụng");
                        #endregion

                        if (string.IsNullOrEmpty(sanPham.MaSanPham))
                            sanPham.MaSanPham = string.Format("SP-{0}", sanPham.Id.ToString());
                        sanPham = SanPhamManager.UpdateSanPham(sanPham);

                        CurrentLog.SaveLog(RightPageName.SP_Update);
                        if (Updated != null) Updated(sanPham, new EventArgs());
                    }
                    #endregion
                }
                else
                {
                    #region Insert
                    TblSanPham sanPham = new TblSanPham();
                    if (sanPham != null)
                    {
                        sanPham.MaSanPham = txtEditMaSanPham.Text.Trim();

                        sanPham.TenSanPham = txtEditTenSanPham.Text;
                        if (!string.IsNullOrEmpty(hiddenKhachHangID.Value))
                            sanPham.IDKhachHangRieng = int.Parse(hiddenKhachHangID.Value);
                        sanPham.Dai = dai; sanPham.Rong = rong; sanPham.Cao = cao;
                        sanPham.KichThuoc = cbEditKichThuoc.SelectedValue;
                        sanPham.CanLan = cbEditCanLan.SelectedValue;
                        sanPham.BoiOffset = cbEditBoiOffset.SelectedIndex == 0;
                        sanPham.BeKhuon = cbEditKichThuoc.SelectedIndex == 0;
                        sanPham.TrangBOPP = cbEditTrangBOPP.SelectedValue;
                        sanPham.TrangHoaChat = cbEditTrangHoaChat.SelectedValue;
                        sanPham.TrangUV = cbEditTrangUV.SelectedValue;
                        sanPham.IDLoaiGiayMat = int.Parse(cbEditLoaiGiayMat.SelectedValue);
                        sanPham.IDLoaiGiayDay = int.Parse(cbEditLoaiGiayDay.SelectedValue);
                        sanPham.IDLoaiGiaySong = int.Parse(cbEditLoaiGiaySong.SelectedValue);
                        sanPham.SoLop = int.Parse(cbEditSoLop.SelectedValue);
                        sanPham.SoSong = cbEditSong.SelectedValue;
                        sanPham.IDDonViTinh = int.Parse(cbEditDonViTinh.SelectedValue);
                        sanPham.SoLuong = soLuong;
                        sanPham.DonGia = donGia;
                        sanPham.TrangThaiSuDung = chkEditTrangThaiSuDung.Checked;
                        sanPham.NguoiTao = sanPham.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        sanPham.NgayCapNhat = sanPham.NgayTao = DateTime.Now;
                        if (cbEditLoaiThung.HasValue)
                            sanPham.IDKieuThung = int.Parse(cbEditLoaiThung.SelectedValue);
                        if (cbEditLoaiMauIn.HasValue)
                            sanPham.IDLoaiMauIn = int.Parse(cbEditLoaiMauIn.SelectedValue);

                        #region Log
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.SP_Insert, string.Format("Thêm mới sản phẩm <b>[{0}] - {1}</b>", sanPham.MaSanPham, sanPham.TenSanPham));

                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Mã sản phẩm",
                                                    TblSanPham.Columns.MaSanPham,
                                                    txtEditMaSanPham.Text);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Tên sản phẩm",
                                                    TblSanPham.Columns.TenSanPham,
                                                    txtEditTenSanPham.Text);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Tên khách hàng",
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    txtEditTenSanPham.Text);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Loại mẫu in",
                                                   TblSanPham.Columns.IDLoaiMauIn,
                                                   cbEditLoaiMauIn.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Kiểu thùng",
                                                   TblSanPham.Columns.IDKieuThung,
                                                   cbEditLoaiThung.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Sản phẩm của khách hàng",
                                                    TblSanPham.Columns.IDKhachHangRieng,
                                                    "");
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Độ dài",
                                                   TblSanPham.Columns.Dai,
                                                   txtEditDai.Text.Trim());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Độ rộng",
                                                   TblSanPham.Columns.Rong,
                                                    txtEditRong.Text.Trim());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Độ cao",
                                                   TblSanPham.Columns.Cao,
                                                    txtEditCao.Text.Trim());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Kích thước",
                                                  TblSanPham.Columns.KichThuoc,
                                                   cbEditKichThuoc.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Cán lằn",
                                                  TblSanPham.Columns.CanLan,
                                                   cbEditCanLan.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Bồi Offset",
                                                  TblSanPham.Columns.BoiOffset,
                                                   cbEditBoiOffset.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Bể khuôn",
                                                  TblSanPham.Columns.BeKhuon,
                                                   cbEditBeKhuon.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Tráng BOPP",
                                                 TblSanPham.Columns.TrangBOPP,
                                                  cbEditTrangBOPP.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Tráng hóa chất",
                                                 TblSanPham.Columns.TrangHoaChat,
                                                  cbEditTrangHoaChat.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Tráng UV",
                                                 TblSanPham.Columns.TrangUV,
                                                  cbEditTrangUV.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Loại giấy đáy",
                                                TblSanPham.Columns.IDLoaiGiayDay,
                                                 cbEditLoaiGiayDay.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Loại giấy mặt",
                                                TblSanPham.Columns.IDLoaiGiayMat,
                                                 cbEditLoaiGiayMat.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Loại giấy sóng",
                                                TblSanPham.Columns.IDLoaiGiaySong,
                                                 cbEditLoaiGiaySong.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Số lớp",
                                                TblSanPham.Columns.SoLop,
                                                 cbEditSoLop.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Số sóng",
                                                TblSanPham.Columns.SoSong,
                                                 cbEditSong.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Đơn vị tính",
                                                TblSanPham.Columns.IDDonViTinh,
                                                 cbEditDonViTinh.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Đơn giá / sản phẩm",
                                                TblSanPham.Columns.DonGia,
                                                sanPham.DonGia.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Số lượng",
                                                TblSanPham.Columns.SoLuong,
                                                 sanPham.SoLuong.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.SP_Insert, "Trạng thái sử dụng",
                                                TblSanPham.Columns.TrangThaiSuDung,
                                                chkEditTrangThaiSuDung.Checked ? "Có sử dụng" : "Không sử dụng");
                        #endregion

                        sanPham = SanPhamManager.InsertSanPham(sanPham);
                        if (string.IsNullOrEmpty(sanPham.MaSanPham))
                            sanPham.MaSanPham = string.Format("SP-{0}", sanPham.Id.ToString());
                        sanPham = SanPhamManager.UpdateSanPham(sanPham);
                        CurrentLog.SaveLog(RightPageName.SP_Insert);
                        if (Inserted != null) Inserted(sanPham, new EventArgs());
                    }

                    #endregion
                }
            }
            else ShowValidPromt();
        }

        private void RefreshDetail()
        {

        }



        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            if (!RunSingle)
            {
                Response.Redirect("/san-pham/0");
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save();
        }

        protected void cbEditLoaiMauIn_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshImage("LMI");
        }

        protected void cbEditLoaiThung_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshImage("Thung");
        }

        protected void btnReload_Click(object sender, EventArgs e)
        {
            RefreshImage("both");
        }


        protected void btnAddNewImage_Click(object sender, EventArgs e)
        {

        }
    }
}
