﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Admin_TopUserInfo.ascx.cs" 
    Inherits="SweetSoft.HiepHung.Controls.Admin_TopUserInfo" %>

<ul class="nav ace-nav">
    <li class="light-blue light-red-fix">
        <a data-toggle="dropdown" href="#" class="dropdown-toggle">
            <img class="nav-user-photo" src='/Images/defaultavatar.png' alt="avatar" />
            <span class="user-info">
                <small>
                    <asp:Literal ID="ltrWelcome" runat="server" Text="Xin chào,">
                    </asp:Literal>
                </small>
                <asp:Literal ID="ltrUserName" runat="server" Text="Username">
                </asp:Literal>
            </span>
            <i class="ace-icon fa fa-caret-down"></i>
        </a>

        <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
            <%--<li>
                <a href="#">
                    <i class="ace-icon fa fa-cog"></i>
                    <asp:Literal ID="ltrSetting" runat="server" Text="Thiết lập hệ thống">
                    </asp:Literal>
                </a>
            </li>--%>

            <li>
                <a href="/thay-mat-khau">
                    <i class="ace-icon fa fa-key"></i>
                    <asp:Literal ID="ltrProfile" runat="server" Text="Thay đổi mật khẩu">
                    </asp:Literal>
                </a>
            </li>

            <li class="divider"></li>

            <li>
                <asp:LinkButton
                    ID="lbtnLogout"
                    runat="server"
                    OnClientClick="OpenDialogClient('ctl00_dialogLogout');return false;">
                    <i class="ace-icon fa fa-power-off"></i>
                    Đăng xuất
                </asp:LinkButton>
            </li>
        </ul>
    </li>
</ul>
