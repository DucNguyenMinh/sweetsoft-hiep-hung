﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Common_ImageUpload.ascx.cs" Inherits="SweetSoft.HiepHung.Controls.Common_ImageUpload" %>


<asp:UpdatePanel ID="panelUpdate" runat="server">
    <ContentTemplate>

        <asp:HiddenField ID="hiddenID" runat="server" />
        <asp:HiddenField ID="hiddenUploadControlID" runat="server" />
        <asp:FileUpload ID="fileUpload" runat="server" CssClass="e-upload-input" multiple="multiple" />

        <div class="e-upload-form" runat="server" id="div_form">
        </div>
        <SweetSoft:ExtraButton ID="btnChooseImage" OnClientClick="return false;" CssClass="btn-select-image" runat="server" EIcon="A_Picture" Text="Chọn hình ảnh"></SweetSoft:ExtraButton>
    </ContentTemplate>
</asp:UpdatePanel>
<script type="text/javascript">

    
</script>
