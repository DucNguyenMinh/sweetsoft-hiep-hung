﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.Core;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Manager;

namespace SweetSoft.HiepHung.Controls
{
    public partial class QLTTLT_UCUserInRole : BaseUserControl
    {
        #region Properties
        private bool cr_ishotel = false;

        public bool IsHotelRole
        {
            get
            {
                return cr_ishotel;
            }
            set
            {
                cr_ishotel = value;
            }
        }

        public int? CurrentUserID
        {
            get
            {
                int? g = null;
                if (ViewState["CurrentUserID"] != null)
                {
                    int u = -1;
                    if (int.TryParse(ViewState["CurrentUserID"].ToString(), out u)) g = u;
                }
                return g;
            }
            set
            {
                ViewState["CurrentUserID"] = value.ToString();
            }
        }

        private bool cr_isadmin = true;

        public bool IsAdmin
        {
            get
            {
                return cr_isadmin;
            }
            set
            {
                cr_isadmin = value;
            }
        }

        private List<ApplicationUserRight> CurrentRoleList
        {
            get
            {
                if (Session["CurrentRole"] != null)
                    return Session["CurrentRole"] as List<ApplicationUserRight>;
                return new List<ApplicationUserRight>();
            }
            set
            {
                Session["CurrentRole"] = value;
            }
        }


        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public void LoadRole(TblCommonUser user)
        {
            LoadRole(user.Id, user.IsAdmin);
        }

        public void LoadRole(int? id, bool? isAdmin)
        {
            CurrentUserID = id;
            CurrentRoleList = RoleManager.GetRoleOfUser(id.Value,isAdmin,true);

            List<ApplicationUserRight> lstEmpty = new List<ApplicationUserRight>();
            lstEmpty.Add(new ApplicationUserRight());

            grvRole.DataSource = CurrentRoleList;
            grvRole.DataSourceEmpty = lstEmpty;
            grvRole.Rebind();
        }

        public bool Save()
        {
            bool save = false;
            foreach (GridViewRow row in grvRole.Rows)
            {
                CheckBox chkItem = row.FindControl("chkItem") as CheckBox;
                HiddenField hdfRoleID = row.FindControl("hdfRoleID") as HiddenField;
                HiddenField hdfUserInRoleID = row.FindControl("hdfUserInRoleID") as HiddenField;

                if (chkItem != null && hdfUserInRoleID != null && hdfRoleID != null)
                {
                    if (chkItem.Checked)
                    {
                        int roleId = -1;
                        if (int.TryParse(hdfRoleID.Value, out roleId))
                        {

                            TblCommonUserInRole uRole = new TblCommonUserInRole();
                            uRole.IDUser = CurrentUserID;
                            uRole.IDRole = roleId;
                            save |= RoleManager.InsertUserInRole(uRole) != null;

                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(hdfUserInRoleID.Value))
                        {
                            save |= RoleManager.DeleteUserInRole(hdfUserInRoleID.Value);
                        }
                    }
                }
            }
            return save;
        }
    }
}