﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SweetSoft.HiepHung
{
    public partial class DMCongViec : BaseGridPage
    {
        #region Properties

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Cat_Cong_Viec;
            }
        }

        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Cat_Cong_Viec;
            }
        }

        protected string CurrentType
        {
            get
            {
                return "CV";
            }
        }

        protected string CurrentName
        {
            get
            {
                string rs = string.Empty;
                if (CurrentType == "CV") rs = "công việc";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;
            if (CurrentType == "CV")
            {
                if (rightName == "Insert") rs = UserHasRight(RightPageName.CV_Insert);
                else if (rightName == "Update") rs = UserHasRight(RightPageName.CV_Update);
                else if (rightName == "Delete") rs = UserHasRight(RightPageName.CV_Delete);
            }
            return rs;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);
            ltrSearchTenCongViec.Text = string.Format("Tên {0}", CurrentName);
            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);
            txtSearchTenCongViec.EnterSubmitClientID =btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");
            if (!IsPostBack)
            {
                grvData.CurrentSortExpression = TblCongViec.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();
                    item.Text = grvData.Columns[i].HeaderText;
                    item.Selected = grvData.CheckColumnVisible(i);
                    if (item.Text == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            grvData.InsertItem();
        }

        protected void grvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvData.CancelAllCommand();
        }

        protected void grvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grvData.Rows[e.RowIndex];
            if (row != null)
            {
                TextBox txtEditTenCongViec = row.FindControl("txtEditTenCongViec") as TextBox;
                ExtraCheckBox chkEditStatus = row.FindControl("chkEditStatus") as ExtraCheckBox;
                HiddenField hdfEditID = row.FindControl("hdfEditID") as HiddenField;

                #region Validation
                if (string.IsNullOrEmpty(txtEditTenCongViec.Text))
                {
                    AddValidPromt(txtEditTenCongViec.ClientID, string.Format("Nhập tên {0}", CurrentName));
                    txtEditTenCongViec.Focus();
                }



                #endregion

                e.Cancel = !PageIsValid;
                if (PageIsValid)
                {
                    if (grvData.IsInsert)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.PB_Insert, string.Format("Thêm mới {1} <b>{0}</b>", txtEditTenCongViec.Text, CurrentName));

                        #region Insert value for CongViec
                        TblCongViec CongViec = new TblCongViec();
                        CongViec.TenCongViec = txtEditTenCongViec.Text.Trim();
                        CongViec.TrangThaiSuDung = chkEditStatus.Checked;
                        CongViec.NguoiTao = CongViec.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        CongViec.NgayTao = CongViec.NgayCapNhat = DateTime.Now;
                       
                        #endregion

                        #region Add log value
                        CurrentLog.AddFirstValue(RightPageName.CV_Insert, string.Format("Tên {0}", CurrentName),
                                                 TblCongViec.Columns.TenCongViec,
                                                 txtEditTenCongViec.Text);
                     
                        CurrentLog.AddFirstValue(RightPageName.CV_Insert, "Trạng thái",
                                                 TblCongViec.Columns.TrangThaiSuDung,
                                                 chkEditStatus.Text);
                        #endregion

                        CongViec = CategoryManager.InsertCongViec(CongViec);
                        ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                   string.Format("Thêm mới {1} <b>{0}</b> thành công.",
                                                 CongViec.TenCongViec, CurrentName),
                                   NotifyType.Success);
                        CurrentLog.SaveLog(RightPageName.CV_Insert);
                        grvData.Rebind();
                    }
                    else
                    {
                        TblCongViec CongViec = CategoryManager.GetCongViecByID(hdfEditID.Value);
                        if (CongViec != null)
                        {
                            #region Update value for CongViec
                            CongViec.TenCongViec = txtEditTenCongViec.Text.Trim();
                      
                            CongViec.TrangThaiSuDung = chkEditStatus.Checked;
                            CongViec.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                            CongViec.NgayCapNhat = DateTime.Now;

                            #endregion

                            #region Add log value
                            CurrentLog.AddLastValue(RightPageName.CV_Update,
                                                    TblCongViec.Columns.TenCongViec,
                                                    txtEditTenCongViec.Text);
                            CurrentLog.AddLastValue(RightPageName.CV_Update,
                                                    TblCongViec.Columns.TrangThaiSuDung,
                                                    chkEditStatus.Text);
                            #endregion

                            CongViec = CategoryManager.UpdateCongViec(CongViec);
                            ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                       string.Format("Cập nhật {1} <b>{0}</b> thành công.",
                                                     CongViec.TenCongViec, CurrentName),
                                       NotifyType.Success);
                            CurrentLog.SaveLog(RightPageName.CV_Update);
                            grvData.Rebind();
                        }
                    }
                    grvData.CancelAllCommand();
                }
                if (!PageIsValid)
                    ShowValidPromt();
            }
        }

        protected void grvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            ExtraButton btnEdit = e.Row.FindControl("btnEdit") as ExtraButton;
            if (btnEdit != null) btnEdit.Visible = GetButtonRole("Update");

            ExtraButton btnDelete = e.Row.FindControl("btnDelete") as ExtraButton;
            if (btnDelete != null) btnDelete.Visible = GetButtonRole("Delete");

            if (grvData.EditIndex == e.Row.RowIndex && !grvData.IsInsert)
            {
                TblCongViec CongViec = e.Row.DataItem as TblCongViec;
                if (CongViec != null)
                {
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.CV_Update,
                                                 string.Format("Cập nhật {1} <b>{0}</b>",
                                                                CongViec.TenCongViec, CurrentName));
                    TextBox txtEditTenCongViec
                        = e.Row.FindControl("txtEditTenCongViec") as TextBox;
                   
                    ExtraCheckBox chkEditStatus
                        = e.Row.FindControl("chkEditStatus") as ExtraCheckBox;
                    HiddenField hdfEditID
                        = e.Row.FindControl("hdfEditID") as HiddenField;

                    if (txtEditTenCongViec != null)
                        txtEditTenCongViec.Text = CongViec.TenCongViec;
                   
                    if (chkEditStatus != null)
                        chkEditStatus.Checked = CongViec.TrangThaiSuDung.HasValue && CongViec.TrangThaiSuDung.Value;
                    if (hdfEditID != null)
                        hdfEditID.Value = CongViec.Id.ToString();

                    #region Add log value
                    CurrentLog.AddFirstValue(RightPageName.CV_Update, string.Format("Tên {0}", CurrentName),
                                             TblCongViec.Columns.TenCongViec,
                                             txtEditTenCongViec.Text);
                   
                    CurrentLog.AddFirstValue(RightPageName.CV_Update, "Trạng thái sử dụng",
                                             TblCongViec.Columns.TrangThaiSuDung,
                                             chkEditStatus.Text);
                    #endregion
                }
            }
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string orderClause = string.Format("{0} desc", TblCongViec.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                string column = "*";

                if (!string.IsNullOrEmpty(txtSearchTenCongViec.Text.Trim()))
                    filterClause += string.Format(" {0} like N'%{1}%' AND ",
                                                  TblCongViec.Columns.TenCongViec,
                                                  txtSearchTenCongViec.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
               
                if (!string.IsNullOrEmpty(cbbSearchStatus.SelectedValue))
                    filterClause += string.Format(" {0} = {1} AND ",
                                                  TblCongViec.Columns.TrangThaiSuDung,
                                                  cbbSearchStatus.SelectedValue);

                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
              
                int total = 0;
                List<TblCongViec> lst = CategoryManager.SearchCongViec(rowIndex, pageSize, column,
                                                                filterClause, orderClause, out total);
                if (grvData.IsInsert)
                    lst.Insert(0, new TblCongViec());
                grv.DataSource = lst;
                List<TblCongViec> lstEmplty = new List<TblCongViec>();
                lstEmplty.Add(new TblCongViec());
                grvData.DataSourceEmpty = lstEmplty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchTenCongViec.Text = string.Empty;
            cbbSearchStatus.SelectedIndex = 0;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_CongViec")
                        {
                            TblCongViec CongViec = CurrentMessageBoxResult.Value as TblCongViec;
                            if (CongViec != null)
                            {
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.CV_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.CV_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblCongViec.Columns.TenCongViec,
                                                             CongViec.TenCongViec);
                                    resultDelete = CategoryManager.DeleteCongViec(CongViec.Id);
                                }
                                catch
                                {
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              CongViec.TenCongViec, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.CV_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa {0} thất bại", CurrentName),
                                                string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                               CongViec.TenCongViec, CurrentName),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblCongViec CongViec = CategoryManager.GetCongViecByID(btn.CommandArgument);
                    if (CongViec != null)
                    {
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = CongViec;
                        result.Submit = true;
                        result.CommandName = "delete_CongViec";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     CongViec.TenCongViec, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }
    }
}