﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="TestAPI.aspx.cs" Inherits="SweetSoft.HiepHung.TestAPI" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <asp:Literal ID="ltrDemo" runat="server"></asp:Literal>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
    <script type="text/javascript">
        $(function () {
            var user = {
                Username: "demo",
                Password: "123456"
            }

            var data = {
                TokenKey: "TokenKeyDemo",
                Code: "LZT-001",
                SoLuongThanhPham: 200,
                SoLuongBanThanhPham:200
            };
            $.ajax(
            {
                url: "http://project.hiephung.local/api/GetProductionCommand",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(data),
                success: function (result) {
                    console.log("result: ", result);
                }
            });

        });
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
</asp:Content>
