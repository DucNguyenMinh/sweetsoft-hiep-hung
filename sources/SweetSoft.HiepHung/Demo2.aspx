﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="Demo2.aspx.cs" Inherits="SweetSoft.HiepHung.Demo2" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="pantxtDateelUpdate" runat="server">
        <ContentTemplate>
            <SweetSoft:ExtraDateTimePicker TimeZone="SE Asia Standard Time" ID="date" runat="server" DateTimeMask="Date" Visible="false" />
          
            <asp:Button ID="btnExample" runat="server" OnClick="btnExample_Click" Text="Click me"  Visible="false" />
              <asp:TextBox ID="txtTest" runat="server" Text="28/02/2017"></asp:TextBox>
          
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Click me 2" />
              <asp:Literal ID="ltrDemo" runat="server"></asp:Literal>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
</asp:Content>
