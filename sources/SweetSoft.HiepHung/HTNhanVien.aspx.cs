﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
using System.Globalization;

namespace SweetSoft.HiepHung
{
    public partial class HTNhanVien : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.QLNhanVien;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.QLNhanVien;
            }
        }
        #endregion


        protected string CurrentName
        {
            get
            {
                string rs = "Nhân viên";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.NhanVien_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.NhanVien_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.NhanVien_Delete);

            return rs;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);

            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);
            txtSeachCMND.EnterSubmitClientID = txtSearchTenTaiKhoan.EnterSubmitClientID = txtSearchSoDienThoai.EnterSubmitClientID =
            txtSearchDiaChi.EnterSubmitClientID = txtSearchHoTen.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");

            txtTenTaiKhoan.ExtenRegex = @"^([0-9a-zA-Z\.]){6," + TblCommonUser.UserNameColumn.MaxLength + "}$";
            txtTenTaiKhoan.ValidRegexMessage = string.Format("Tài khoản có ít nhất 6 - {0} ký tự, chỉ bao gồm số và chữ cái.", TblCommonUser.UserNameColumn.MaxLength);
            txtTenTaiKhoan.Attributes.Add("data-promptPosition", "centerLeft");

            txtDetailHo.MaxTextLength = TblNhanVien.FirstNameColumn.MaxLength;
            txtDetailTen.MaxTextLength = TblNhanVien.LastNameColumn.MaxLength;
            txtDiaChi.MaxTextLength = TblNhanVien.DiaChiColumn.MaxLength;
            txtTenTaiKhoan.MaxTextLength = TblCommonUser.UserNameColumn.MaxLength;

            if (!IsPostBack)
            {
                LoadPhongBan(cbPhongBan);
                LoadPhongBan(cbSeachPhongBan);

                grvData.CurrentSortExpression = TblNhanVien.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        private void LoadPhongBan(ExtraComboBox cb)
        {
            List<TblPhongBan> phongBanCol = CategoryManager.GetPhongBanAll(true);
            if (cb.ClientID == cbSeachPhongBan.ClientID)
            {
                TblPhongBan empty = new TblPhongBan();
                empty.Id = -1;
                empty.TenPhongBan = "Tất cả";
                phongBanCol.Insert(0, empty);
            }
            cb.DataSource = phongBanCol;
            cb.DataValueField = TblPhongBan.Columns.Id;
            cb.DataTextField = TblPhongBan.Columns.TenPhongBan;
            cb.DataBind();
        }

        private void RefreshDetail()
        {

            txtDetailHo.Text = txtDetailTen.Text = txtTenTaiKhoan.Text = txtCMND.Text = txtSoDienThoai.Text = txtDiaChi.Text = string.Empty;
            dateEditNgaySinh.DateValue = dateNgayVaoCongTy.DateValue = DateTime.MinValue;
            cbEditGioiTinh.SelectedIndex = 2;
           
            txtTenTaiKhoan.Enabled = false;
            chkEditCoTaiKhoanNguoiDung.Checked = false;
            chkDetailTrangThaiSuDung.Checked = true;
            dialogDetail.UpdateContentDialog();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            RefreshDetail();
            hdfDetailUserID.Value = string.Empty;
            txtDetailHo.Focus();

            lblDialogDetailTitle.Text = "Thêm mới nhân viên";
            dialogDetail.ShowDialog(true);
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" nv.{0} desc ", TblNhanVien.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" nv.*, us.{0},pb.{1}  ", TblCommonUser.Columns.UserName, TblPhongBan.Columns.TenPhongBan);

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchTenTaiKhoan.Text.Trim()))
                    filterClause += string.Format(" us.{0} like N'%{1}%' AND ",
                                                  TblCommonUser.Columns.UserName,
                                                  txtSearchTenTaiKhoan.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchHoTen.Text.Trim()))
                    filterClause += string.Format(" (nv.{0} like N'%{1}%' or nv.{2} like N'%{1}%' or concat(nv.{2},' ',nv.{0}) like N'%{1}%' or concat(nv.{2},'',nv.{0}) like N'%{1}%' ) AND ",
                                                  TblNhanVien.Columns.LastName,
                                                  txtSearchHoTen.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"),
                                                  TblNhanVien.Columns.FirstName);
                if (cbSearchGioiTinh.SelectedIndex > 0)
                    filterClause += string.Format(" nv.{0} = N'{1}' AND ",
                                                  TblNhanVien.Columns.GioiTinh,
                                                  cbSearchGioiTinh.SelectedValue);
                if (cbbSearchStatus.SelectedIndex > 0)
                    filterClause += string.Format(" nv.{0} = {1} AND ",
                                                  TblNhanVien.Columns.TrangThaiSuDung,
                                                  cbbSearchStatus.SelectedValue);
                if (!string.IsNullOrEmpty(txtSeachCMND.Text.Trim()))
                    filterClause += string.Format(" nv.{0} like N'%{1}%' AND ",
                                                  TblNhanVien.Columns.Cmnd,
                                                  txtSeachCMND.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchSoDienThoai.Text.Trim()))
                    filterClause += string.Format(" nv.{0} like N'%{1}%' AND ",
                                                  TblNhanVien.Columns.SoDienThoai,
                                                  txtSearchSoDienThoai.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchDiaChi.Text.Trim()))
                    filterClause += string.Format(" nv.{0} like N'%{1}%' AND ",
                                                  TblNhanVien.Columns.DiaChi,
                                                  txtSearchDiaChi.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));

                if (cbSeachPhongBan.HasValue)
                    filterClause += string.Format(" nv.{0} = {1} AND ",
                                                  TblNhanVien.Columns.IDPhongBan,
                                                  cbSeachPhongBan.SelectedValue);

                if (cbSearchTaiKhoanDangNhap.SelectedIndex > 0)
                    filterClause += string.Format(" nv.{0} {1} AND ",
                                                  TblNhanVien.Columns.IDUser,
                                                  cbSearchTaiKhoanDangNhap.SelectedValue);

                if (dateSearchNgaySinhFrom.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" nv.{0} >= '{1}' AND ",
                                                  TblNhanVien.Columns.NgaySinh,
                                                  dateSearchNgaySinhFrom.DateValue.ToString("yyyy-MM-dd 00:00:00"));
                if (dateSearchNgaySinhTo.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" nv.{0} <= '{1}' AND ",
                                                  TblNhanVien.Columns.NgaySinh,
                                                  dateSearchNgaySinhTo.DateValue.ToString("yyyy-MM-dd 23:59:59"));

                if (dateSearchNgayVaoCongTyTu.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" nv.{0} >= '{1}' AND ",
                                                  TblNhanVien.Columns.NgayVaoCongTy,
                                                  dateSearchNgayVaoCongTyTu.DateValue.ToString("yyyy-MM-dd 00:00:00"));
                if (dateSearchNgayVaoCongTyToi.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" nv.{0} <= '{1}' AND ",
                                                  TblNhanVien.Columns.NgayVaoCongTy,
                                                  dateSearchNgayVaoCongTyToi.DateValue.ToString("yyyy-MM-dd 23:59:59"));
                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = string.Format(" left join {0} as us on us.id = nv.{1} ", TblCommonUser.Schema.TableName, TblNhanVien.Columns.IDUser);
                tableJoin += string.Format(" left join {0} as pb on pb.id = nv.{1} ", TblPhongBan.Schema.TableName, TblNhanVien.Columns.IDPhongBan);

                int total = 0;
                List<TblNhanVien> lst = UserManager.SearchNhanVien(rowIndex, pageSize, string.Format(" {0} as nv ", TblNhanVien.Schema.TableName), tableJoin, column,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblNhanVien> lstEmpty = new List<TblNhanVien>();
                lstEmpty.Add(new TblNhanVien());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSeachCMND.Text = txtSearchSoDienThoai.Text = string.Empty;
            txtSearchDiaChi.Text = txtSearchHoTen.Text = txtSearchTenTaiKhoan.Text = string.Empty;
            cbSeachPhongBan.SelectedIndex = cbbSearchStatus.SelectedIndex = cbSearchGioiTinh.SelectedIndex = cbSearchTaiKhoanDangNhap.SelectedIndex = 0;
            dateSearchNgayVaoCongTyToi.DateValue = dateSearchNgayVaoCongTyTu.DateValue = dateSearchNgaySinhFrom.DateValue = dateSearchNgaySinhTo.DateValue = DateTime.MinValue;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_NhanVien")
                        {
                            TblNhanVien nhanVien = CurrentMessageBoxResult.Value as TblNhanVien;
                            if (nhanVien != null)
                            {

                                string result = "";
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.NhanVien_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblNhanVien.Columns.FirstName,
                                                             nhanVien.HoVaTen);
                                    result = UserManager.DeleteNhanVien(nhanVien.Id);
                                    resultDelete = result == "success";
                                    if (result == "success" && nhanVien.IDUser.HasValue && nhanVien.IDUser.Value > 0)
                                        UserManager.DeleteCommonUser(nhanVien.IDUser);


                                }
                                catch
                                {
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              nhanVien.HoVaTen, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.NhanVien_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa {0} thất bại. {2}", CurrentName),
                                                string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                               nhanVien.HoVaTen, CurrentName, result),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblNhanVien nhanVien = UserManager.GetNhanVienByID(btn.CommandArgument);
                    if (nhanVien != null)
                    {
                        if (!string.IsNullOrEmpty(nhanVien.UserName) && nhanVien.UserName.Trim().ToUpper() == "DEMO")
                        {
                            ShowNotify("Thông báo", "Bạn không thể xóa tài khoản demo, vui lòng tạo mới tài khoản", NotifyType.Warning);
                            return;
                        }
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = nhanVien;
                        result.Submit = true;
                        result.CommandName = "delete_NhanVien";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     nhanVien.HoVaTen, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            #region Validation
            if (string.IsNullOrEmpty(txtDetailTen.Text))
            {
                AddValidPromt(txtDetailTen.ClientID, "Nhập họ tên nhân viên");
                txtDetailTen.Focus();
            }

            if (string.IsNullOrEmpty(txtDetailHo.Text))
            {
                AddValidPromt(txtDetailHo.ClientID, "Nhập họ tên nhân viên");
                txtDetailHo.Focus();
            }



            TblNhanVien nhanVien = new TblNhanVien();
            if (!string.IsNullOrEmpty(hdfDetailUserID.Value)) nhanVien = UserManager.GetNhanVienByID(hdfDetailUserID.Value);
            if (chkEditCoTaiKhoanNguoiDung.Checked)
            {
                if (string.IsNullOrEmpty(txtTenTaiKhoan.Text.Trim()))
                {
                    AddValidPromt(txtTenTaiKhoan.ClientID, "Nhập tài khoản đăng nhập");
                    txtTenTaiKhoan.Focus();
                }
                else
                {
                    TblNhanVien nvCheck = UserManager.GetNhanVienByUserName(txtTenTaiKhoan.Text.Trim());
                    if (nvCheck != null)
                    {
                        if (string.IsNullOrEmpty(hdfDetailUserID.Value))
                        {
                            AddValidPromt(txtTenTaiKhoan.ClientID, "Tài khoản đã tồn tại trên hệ thống");
                            txtTenTaiKhoan.Focus();
                        }
                        else
                        {

                            if (nhanVien != null && nhanVien.Id != nvCheck.Id)
                            {
                                AddValidPromt(txtTenTaiKhoan.ClientID, "Tài khoản đã tồn tại trên hệ thống");
                                txtTenTaiKhoan.Focus();
                            }
                        }
                    }
                }
            }
            #endregion

            if (PageIsValid)
            {

                if (!string.IsNullOrEmpty(hdfDetailUserID.Value))
                {
                    #region update

                    if (nhanVien != null)
                    {
                        nhanVien.FirstName = txtDetailHo.Text;
                        nhanVien.LastName = txtDetailTen.Text;
                        nhanVien.Cmnd = txtCMND.Text;
                        nhanVien.SoDienThoai = txtSoDienThoai.Text;
                        nhanVien.DiaChi = txtDiaChi.Text;
                        nhanVien.GioiTinh = cbEditGioiTinh.SelectedValue;
                        nhanVien.NgaySinh = null;
                        if (dateEditNgaySinh.DateValue != DateTime.MinValue)
                            nhanVien.NgaySinh = dateEditNgaySinh.DateValue;
                        nhanVien.NgayVaoCongTy = null;
                        if (dateNgayVaoCongTy.DateValue != DateTime.MinValue)
                            nhanVien.NgayVaoCongTy = dateNgayVaoCongTy.DateValue;
                        nhanVien.IDPhongBan = null;
                        if (cbPhongBan.HasValue)
                            nhanVien.IDPhongBan = int.Parse(cbPhongBan.SelectedValue);
                        nhanVien.TrangThaiSuDung = chkDetailTrangThaiSuDung.Checked;


                        if (chkEditCoTaiKhoanNguoiDung.Checked)
                        {
                            TblCommonUser user = new TblCommonUser();
                            if (nhanVien.IDUser.HasValue && nhanVien.IDUser.Value > 0)
                                user = UserManager.GetUserByID(nhanVien.IDUser.Value);
                            if (user == null) user = new TblCommonUser();

                            if (user.Id == 0)
                            {
                                user.UserName = txtTenTaiKhoan.Text;
                                user.Password = SecurityHelper.Encrypt128("123456");
                                user = UserManager.InsertCommonUser(user);

                            }
                            else if (user.Id > 0 && user.UserName.Trim() != txtTenTaiKhoan.Text.Trim())
                            {
                                user.UserName = txtTenTaiKhoan.Text.Trim();
                                user = UserManager.UpdateCommonUser(user);
                            }
                            nhanVien.IDUser = user.Id;
                        }
                        else if (nhanVien.IDUser.HasValue)
                        {
                            UserManager.DeleteCommonUser(nhanVien.IDUser.Value);
                            nhanVien.IDUser = null;
                        }
                        nhanVien = UserManager.UpdateNhanVien(nhanVien);

                        #region log
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                          TblNhanVien.Columns.FirstName,
                                          txtDetailHo.Text);
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                          TblNhanVien.Columns.LastName,
                                          txtDetailTen.Text);
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                          TblNhanVien.Columns.GioiTinh,
                                          cbEditGioiTinh.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                         TblNhanVien.Columns.Cmnd,
                                         txtCMND.Text);
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                         TblNhanVien.Columns.DiaChi,
                                         txtDiaChi.Text);
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                          TblNhanVien.Columns.NgaySinh,
                                          dateEditNgaySinh.DateValue != DateTime.MinValue ? dateEditNgaySinh.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                         TblNhanVien.Columns.NgayVaoCongTy,
                                         dateNgayVaoCongTy.DateValue != DateTime.MinValue ? dateNgayVaoCongTy.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddLastValue(RightPageName.NhanVien_Update,
                                          TblNhanVien.Columns.IDUser,
                                          txtTenTaiKhoan.Text);
                        CurrentLog.SaveLog(RightPageName.NhanVien_Update);
                        #endregion
                        ShowNotify("Thông báo", string.Format("Cập nhật thông tin nhân viên <b>{0}</b> thành công", nhanVien.HoVaTen), NotifyType.Success);
                        dialogDetail.CloseDialog();
                        grvData.Rebind();
                    }
                    else
                        ShowNotify("Thông báo", "Không tìm thấy thông tin nhân viên trên hệ thống", NotifyType.Error);
                    #endregion
                }
                else
                {
                    #region insert
                    nhanVien.FirstName = txtDetailHo.Text;
                    nhanVien.LastName = txtDetailTen.Text;
                    nhanVien.Cmnd = txtCMND.Text;
                    nhanVien.SoDienThoai = txtSoDienThoai.Text;
                    nhanVien.DiaChi = txtDiaChi.Text;
                    nhanVien.GioiTinh = cbEditGioiTinh.SelectedValue;
                    nhanVien.NgaySinh = null;
                    if (dateEditNgaySinh.DateValue != DateTime.MinValue)
                        nhanVien.NgaySinh = dateEditNgaySinh.DateValue;
                    nhanVien.NgayVaoCongTy = null;
                    if (dateNgayVaoCongTy.DateValue != DateTime.MinValue)
                        nhanVien.NgayVaoCongTy = dateNgayVaoCongTy.DateValue;
                    nhanVien.TrangThaiSuDung = chkDetailTrangThaiSuDung.Checked;
                    if (cbPhongBan.HasValue)
                        nhanVien.IDPhongBan = int.Parse(cbPhongBan.SelectedValue);


                    if (chkEditCoTaiKhoanNguoiDung.Checked)
                    {
                        TblCommonUser user = new TblCommonUser();
                        user.UserName = txtTenTaiKhoan.Text;
                        user.Password = SecurityHelper.Encrypt128("123456");
                        user = UserManager.InsertCommonUser(user);
                        nhanVien.IDUser = user.Id;
                    }

                    nhanVien = UserManager.InsertNhanVien(nhanVien);
                    #region log
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.NhanVien_Insert,
                                                 string.Format("Thêm mới nhân viên <b>{0}</b>", nhanVien.HoVaTen));

                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, string.Format("Họ tên"),
                                         TblNhanVien.Columns.FirstName,
                                         string.Format("{0} {1}", nhanVien.FirstName, nhanVien.LastName));
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Giới tính",
                                      TblNhanVien.Columns.GioiTinh,
                                      cbEditGioiTinh.DisplayText);
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Số CMND",
                                      TblNhanVien.Columns.Cmnd,
                                      txtCMND.Text);

                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Số điện thoại",
                                      TblNhanVien.Columns.SoDienThoai,
                                      txtSoDienThoai.Text);
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Phòng ban / kho",
                                      TblNhanVien.Columns.IDPhongBan,
                                      cbPhongBan.DisplayText);
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Địa chỉ",
                                      TblNhanVien.Columns.DiaChi,
                                      txtDiaChi.Text);
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Ngày sinh",
                                      TblNhanVien.Columns.NgaySinh,
                                      dateEditNgaySinh.DateValue != DateTime.MinValue ? dateEditNgaySinh.DateValue.ToString("dd/MM/yyyy") : "");
                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Ngày vào công ty",
                                     TblNhanVien.Columns.NgayVaoCongTy,
                                     dateNgayVaoCongTy.DateValue != DateTime.MinValue ? dateNgayVaoCongTy.DateValue.ToString("dd/MM/yyyy") : "");

                    CurrentLog.AddFirstValue(RightPageName.NhanVien_Insert, "Tài khoản đăng nhập",
                                      TblNhanVien.Columns.IDUser,
                                      txtTenTaiKhoan.Text);
                    CurrentLog.SaveLog(RightPageName.NhanVien_Insert);
                    #endregion
                    ShowNotify("Thông báo", string.Format("Thêm mới thông tin nhân viên <b>{0}</b> thành công", nhanVien.HoVaTen), NotifyType.Success);
                    dialogDetail.CloseDialog();
                    grvData.Rebind();
                    #endregion
                }
            }
            else ShowValidPromt();
        }

        protected void chkEditCoTaiKhoanNguoiDung_CheckedChanged(object sender, EventArgs e)
        {
            txtTenTaiKhoan.Enabled = chkEditCoTaiKhoanNguoiDung.Checked;
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RefreshDetail();

            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblNhanVien nv = UserManager.GetNhanVienByID(cmd);
                    if (nv != null)
                    {
                        if (!string.IsNullOrEmpty(nv.UserName) && nv.UserName.Trim().ToUpper() == "DEMO")
                        {
                            ShowNotify("Thông báo", "Bạn không thể cập nhật tài khoản demo, vui lòng tạo mới tài khoản", NotifyType.Warning);
                            return;
                        }
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.NhanVien_Update,
                                                     string.Format("Cập nhật nhân viên <b>{0}</b>", nv.HoVaTen));

                        txtDetailHo.Text = nv.FirstName;
                        txtDetailTen.Text = nv.LastName;
                        txtCMND.Text = nv.Cmnd;
                        txtDiaChi.Text = nv.DiaChi;
                        txtSoDienThoai.Text = nv.SoDienThoai;
                        if (nv.IDPhongBan.HasValue)
                        {
                            ListItem it = cbPhongBan.Items.FindByValue(nv.IDPhongBan.Value.ToString());
                            if (it != null) cbPhongBan.SelectedValue = nv.IDPhongBan.Value.ToString();
                        }
                        ListItem item = cbEditGioiTinh.Items.FindByValue(nv.GioiTinh);
                        if (item != null) cbEditGioiTinh.SelectedValue = nv.GioiTinh;
                        if (nv.NgaySinh.HasValue && nv.NgaySinh.Value != DateTime.MinValue)
                            dateEditNgaySinh.DateValue = nv.NgaySinh.Value;
                        if (nv.NgayVaoCongTy.HasValue && nv.NgayVaoCongTy.Value != DateTime.MinValue)
                            dateNgayVaoCongTy.DateValue = nv.NgayVaoCongTy.Value;
                        //if (nv.IDPhongBan.HasValue)
                        //{
                        //    ListItem itemPB = cbEditPhongBan.Items.FindByValue(nv.IDPhongBan.Value.ToString());
                        //    if (itemPB != null)
                        //    {
                        //        cbEditPhongBan.SelectedValue = nv.IDPhongBan.Value.ToString();
                        //        LoadToNhanVien(nv.IDPhongBan.Value.ToString(), cbEditToNhanVien);
                        //    }
                        //}


                        if (nv.IDUser.HasValue && nv.IDUser.Value > 0 && !string.IsNullOrEmpty(nv.UserName))
                        {
                            chkEditCoTaiKhoanNguoiDung.Checked = true;
                            txtTenTaiKhoan.Text = nv.UserName;
                            txtTenTaiKhoan.Enabled = true;
                        }
                        chkDetailTrangThaiSuDung.Checked = nv.TrangThaiSuDung.HasValue && nv.TrangThaiSuDung.Value;
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, string.Format("Họ tên"),
                                            TblNhanVien.Columns.FirstName,
                                            string.Format("{0} {1}", nv.FirstName, nv.LastName));
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Giới tính",
                                          TblNhanVien.Columns.GioiTinh,
                                          cbEditGioiTinh.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Số CMND",
                                          TblNhanVien.Columns.Cmnd,
                                          txtCMND.Text);
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Số điện thoại",
                                          TblNhanVien.Columns.SoDienThoai,
                                          txtSoDienThoai.Text);
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Phòng ban / kho",
                                          TblNhanVien.Columns.IDPhongBan,
                                          cbPhongBan.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Địa chỉ",
                                          TblNhanVien.Columns.DiaChi,
                                          txtDiaChi.Text);
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Ngày sinh",
                                          TblNhanVien.Columns.NgaySinh,
                                          dateEditNgaySinh.DateValue != DateTime.MinValue ? dateEditNgaySinh.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Ngày vào công ty",
                                         TblNhanVien.Columns.NgayVaoCongTy,
                                         dateNgayVaoCongTy.DateValue != DateTime.MinValue ? dateNgayVaoCongTy.DateValue.ToString("dd/MM/yyyy") : "");

                        CurrentLog.AddFirstValue(RightPageName.NhanVien_Update, "Tài khoản đăng nhập",
                                          TblNhanVien.Columns.IDUser,
                                          txtTenTaiKhoan.Text);
                        hdfDetailUserID.Value = nv.Id.ToString();
                        lblDialogDetailTitle.Text = "Cập nhật thông tin nhân viên";
                        dialogDetail.ShowDialog();
                    }
                }
            }
        }

        protected void btnRoleForUser_Click(object sender, EventArgs e)
        {
            if (ucUserInRole.Save())
            {
                dialogRole.CloseDialog();
                ShowNotify("Thông báo", "Đã phân quyền cho tài khoản người dùng", NotifyType.Success);
            }
        }

        protected void btnAuthorization_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;

                if (!string.IsNullOrEmpty(cmd))
                {
                    TblNhanVien nhanVien = UserManager.GetNhanVienByID(cmd);
                    if (nhanVien.IDUser.HasValue)
                    {
                        TblCommonUser nguoiDung = UserManager.GetUserByID(nhanVien.IDUser.Value);
                        if (nguoiDung != null)
                        {
                            if (nguoiDung.UserName.Trim().ToUpper() != "DEMO")
                            {
                                ucUserInRole.LoadRole(nguoiDung);
                                dialogRole.ShowDialog();
                            }
                            else
                                ShowNotify("Thông báo", "Bạn không thể phân quyền tài khoản Demo", NotifyType.Warning);
                        }
                        else
                            ShowNotify("Thông báo", "Không tìm thấy thông tin tài khoản", NotifyType.Error);
                    }
                    else
                        ShowNotify("Thông báo", "Nhân viên không có tài khoản sử dụng để phân quyền", NotifyType.Error);

                }
                else
                    ShowNotify("Thông báo", "Nhân viên chưa được cấp tài khoản đăng nhập hệ thống", NotifyType.Error);
            }
        }

        protected void btnResetPass_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;

                if (!string.IsNullOrEmpty(cmd))
                {
                    TblNhanVien nhanVien = UserManager.GetNhanVienByID(cmd);
                    if (nhanVien.IDUser.HasValue)
                    {
                        TblCommonUser nguoiDung = UserManager.GetUserByID(nhanVien.IDUser.Value);
                        if (nguoiDung != null)
                        {
                            if (nguoiDung.UserName.Trim().ToUpper() != "DEMO")
                            {
                                nguoiDung.Password = SecurityHelper.Encrypt128("123456");
                                nguoiDung = UserManager.UpdateCommonUser(nguoiDung);
                                ShowNotify("Thông báo", "Tài khoản đã được đổi về mật khẩu mặc định", NotifyType.Success);
                                CurrentLog.Clear();
                                CurrentLog.AddNewLogFunction(RightPageName.NhanVien_Role,
                                                             string.Format("Reset mật khẩu tài khoản <b>{0}</b>", nguoiDung.UserName));

                                CurrentLog.AddFirstValue(RightPageName.NhanVien_Role, "Người reset", TblCommonUser.Columns.Password, ApplicationContext.Current.CommonUser.UserName);
                                CurrentLog.SaveLog(RightPageName.NhanVien_Role);
                            }
                            else
                                ShowNotify("Thông báo", "Bạn không thể reset mật khẩu tài khoản Demo", NotifyType.Warning);

                        }
                        else
                            ShowNotify("Thông báo", "Không tìm thấy thông tin tài khoản", NotifyType.Error);
                    }
                    else
                        ShowNotify("Thông báo", "Nhân viên không có tài khoản để reset mật khẩu", NotifyType.Error);

                }
                else
                    ShowNotify("Thông báo", "Nhân viên chưa được cấp tài khoản đăng nhập hệ thống", NotifyType.Error);
            }
        }
    }
}