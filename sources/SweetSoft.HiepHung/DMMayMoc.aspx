﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DMMayMoc.aspx.cs" Inherits="SweetSoft.HiepHung.DMMayMoc" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<%@ Register Src="~/Controls/HH_MayMocCongViec.ascx" TagName="MayMoc" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
        <li>
            <a href="#">Quản lý danh mục</a>
        </li>
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý danh sách
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,4,5,6,7,8">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tên máy" AccessibleHeaderText="Tên máy" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="TenMay">
                                    <ItemTemplate>
                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" runat="server" Text=' <%# Eval("TenMay")  %>' CommandName="Edit" Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("TenMay")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mã máy" AccessibleHeaderText="Mã máy" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="MaSoMay">
                                    <ItemTemplate>
                                        <%# Eval("MaSoMay")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ngày mua" AccessibleHeaderText="Ngày mua" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="NgayMua">
                                    <ItemTemplate>
                                        <%# Eval("NgayMuaToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ngày bảo trì" AccessibleHeaderText="Ngày bảo trì" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="NgayBaoTri">
                                    <ItemTemplate>
                                        <%# Eval("NgayBaoTriToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ghi chú" AccessibleHeaderText="Ghi chú" HeaderStyle-Width="200px" ItemStyle-Width="100px" SortExpression="GhiChu">
                                    <ItemTemplate>
                                        <%# Eval("GhiChu")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Công việc xử lý" AccessibleHeaderText="Công việc xử lý" HeaderStyle-Width="200px" ItemStyle-Width="200px">
                                    <ItemTemplate>
                                        <%# GetListCongViec(Eval("ID"))  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" ItemStyle-Width="150px" SortExpression="TrangThaiSuDung">
                                    <ItemTemplate>
                                        <%# GetTrangThaiSuDung(Eval("TinhTrangSuDung")) %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <SweetSoft:ExtraCheckBox ID="chkEditStatus" runat="server" CheckboxType="Switch7" DataOn="Bật" DataOff="Tắt" Checked="true">
                                        </SweetSoft:ExtraCheckBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnConfig" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Config" ToolTip="Phân công việc" OnClick="btnConfig_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog runat="server" ID="dialogMayMoc" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Label runat="server" ID="Label1" Text="Phân công việc cho máy móc"> </asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <SweetSoft:MayMoc ID="mayMocControl" runat="server" Onchanged="mayMocControl_changed" />
            </div>
        </ContentTemplate>
    </SweetSoft:ExtraDialog>
    <SweetSoft:ExtraDialog runat="server" ID="dialogDetail"  EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Label runat="server" ID="lblDialogDetailTitle"> </asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên máy (*) </label>
                        <SweetSoft:ExtraTextBox ID="txtTenMay" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblMayMoc.TenMayColumn.MaxLength %>' runat="server" PlaceHolder="Nhập tên máy"> </SweetSoft:ExtraTextBox>
                        <asp:HiddenField ID="hdfDetailUserID" runat="server" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã máy </label>
                        <SweetSoft:ExtraTextBox ID="txtMaMay" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblMayMoc.MaSoMayColumn.MaxLength %>' runat="server" PlaceHolder="Nhập mã máy"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày mua </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateNgayMua" runat="server" Width="100%" DateTimeMask="Date" PlaceHolder="Chọn ngày mua" />
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày bảo trì tiếp theo </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateNgayBaoTri" Width="100%" runat="server" DateTimeMask="Date" PlaceHolder="Chọn ngày bảo trì tiếp theo" />
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Trạng thái hoạt động </label>
                        <br />
                        <SweetSoft:ExtraComboBox ID="cbTrangThaiSuDung" runat="server">
                            <asp:ListItem Value="DangSuDung" Text="Đang sử dụng"></asp:ListItem>
                            <asp:ListItem Value="HuHong" Text="Hư hỏng"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ghi chú</label>
                        <br />
                        <SweetSoft:ExtraTextBox ID="txtGhiChu" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblMayMoc.GhiChuColumn.MaxLength %>' runat="server" TextMode="MultiLine" Height="50px" PlaceHolder="Nhập ghi chú"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSave" runat="server" EIcon="A_Save" Text="Lưu thông tin" EStyle="Success_Bold" ESubmitAndCheckValid="true" EnableSnipButton="true" ValidationGroup="Detail" OnClick="btnSave_Click">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>

    <SweetSoft:ExtraDialog runat="server" ID="dialogSearch" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            Tìm kiếm máy 
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên máy </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenMay" runat="server" PlaceHolder="Nhập tên máy cần tìm"> </SweetSoft:ExtraTextBox>

                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã máy </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchMaMay" runat="server" PlaceHolder="Nhập mã máy cần tìm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày mua từ </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayMuaTu" Width="100%" runat="server" DateTimeMask="Date" PlaceHolder="Chọn ngày mua từ ngày" />
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">tới </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayMuaToi" Width="100%" runat="server" DateTimeMask="Date" PlaceHolder="Tới ngày" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày bảo trì từ </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayBaoTriTu" Width="100%" runat="server" DateTimeMask="Date" PlaceHolder="Chọn ngày bảo trì từ ngày" />
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">tới </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayBaoTriToi" Width="100%" runat="server" DateTimeMask="Date" PlaceHolder="Tới ngày" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Trạng thái hoạt động </label>
                        <br />
                        <SweetSoft:ExtraComboBox ID="cbSearchStatus" runat="server">
                            <asp:ListItem Text="Tất cả" Value=""></asp:ListItem>
                            <asp:ListItem Value="DangSuDung" Text="Đang sử dụng"></asp:ListItem>
                            <asp:ListItem Value="HuHong" Text="Hư hỏng"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Công việc đảm nhận </label>
                        <br />
                        <SweetSoft:ExtraComboBox ID="cbSearchCongViec" runat="server">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="extra-label">Ghi chú</label>
                        <br />
                        <SweetSoft:ExtraTextBox ID="txtSearchGhiChu" runat="server" TextMode="MultiLine" Height="50px" PlaceHolder="Nhập ghi chú cần tìm"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
