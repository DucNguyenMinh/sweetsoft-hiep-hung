﻿using SweetSoft.HiepHung.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System.IO;
using System.Web.Services;
using System.Web.Script.Serialization;

namespace SweetSoft.HiepHung
{
    public partial class Default1 : BasePage
    {
        #region Properties
        public override bool IsUserRole
        {
            get
            {
                return false;
            }
        }
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region Web Method
        [WebMethod(EnableSession = true)]
        public static string GetCustomer(string KeyWord)
        {
            string ret = string.Empty;
            if (!string.IsNullOrEmpty(KeyWord))
            {
                List<TblKhachHang> khachHang = KhachHangManager.SearchKhachHangByText(KeyWord);
                ret = new JavaScriptSerializer().Serialize(khachHang);
            }

            return ret;
        }

        [WebMethod(EnableSession = true)]
        public static object RemoveImage(string id, string type = "MauIn")
        {
            string path = string.Empty;
            if (type == "MauIn")
            {
                TblLoaiMauImage mau = CategoryManager.GetLoaiMauImageByID(id);
                if (mau != null && mau.Img.HasValue)
                {
                    TblImage image = CategoryManager.GetImageByID(mau.Img.Value.ToString());
                    if (image != null)
                        path = image.ImagePath;
                    CategoryManager.DeleteLoaiMauImage(id);
                }
            }
            else if (type == "SanPham")
            {
                TblSanPhamImage mau = SanPhamManager.GetSanPhamImageByID(id);
                if (mau != null && mau.Img.HasValue)
                {
                    TblImage image = CategoryManager.GetImageByID(mau.Img.Value.ToString());
                    if (image != null)
                        path = image.ImagePath;
                    SanPhamManager.DeleteSanPhamImage(id);
                }
            }

            try
            {
                string serverPath = HttpContext.Current.Server.MapPath(path);
                if (File.Exists(serverPath)) File.Delete(serverPath);
            }
            catch (Exception) { }



            return true;

        }
        #endregion
    }
}