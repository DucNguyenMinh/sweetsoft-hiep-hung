﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DMSanPhamChiTiet.aspx.cs" Inherits="SweetSoft.HiepHung.DMSanPhamChiTiet" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<%@ Register Src="~/Controls/HH_SanPham.ascx" TagName="SanPham" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/FileUpload.css" rel="stylesheet" />
    <link href="/css/jquery.light.css" rel="stylesheet" />

    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }

        .lb-donvitinh
        {
            font-weight: bold;
        }
    </style>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
        <li>
            <a href="/san-pham">Danh mục sản phẩm</a>
        </li>
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Thông tin chi tiết sản phẩm
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <SweetSoft:SanPham ID="ctrsanPham" runat="server" EnableInlineAddNew="true" RunSingle="false" OnInserted="ctrsanPham_Inserted" OnUpdated="ctrsanPham_Updated" />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
    <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        SearchCustomer();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SearchCustomer);
        function SearchCustomer(s, a) {

            $("input[type='text'][id$='txtEditCustomer']").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/Default.aspx/GetCustomer",
                        data: "{'KeyWord':'" + $("input[type='text'][id$='txtEditCustomer']").val() + "'}",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (result) {
                           
                          
                           
                            response($.map($.parseJSON(result.d), function (item) {
                                return { Id: item.Id, TenKhachHang: item.TenKhachHang };
                            }));
                        }
                    });
                },
                messages: {
                    noResults: '',
                    results: function () { }
                },
                focus: function (event, ui) {

                },
                change: function (event, ui) {
                  
                    if (ui.item == null) {
                        $("input[type='hidden'][id$='hiddenKhachHangID']").val("");
                        $("input[type='text'][id$='txtEditCustomer']").val("");
                    }
                },
                search: function (event, ui) {

                },
                select: function (event, ui) {
                    $("input[type='text'][id$='txtEditCustomer']").val(ui.item.TenKhachHang);
                    $("input[type='hidden'][id$='hiddenKhachHangID']").val(ui.item.Id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .data("ui-autocomplete-item", item)
                    .append("<a><span style='width:30px;'>" + item.TenKhachHang + '</span>' + "</a>")
                    .appendTo(ul);
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
</asp:Content>
