﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DMSanPham.aspx.cs" Inherits="SweetSoft.HiepHung.DMSanPham" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>

        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý danh mục
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,3,4,10,11,13">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tên sản phẩm" AccessibleHeaderText="Tên sản phẩm" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="sp.TenSanPham">
                                    <ItemTemplate>
                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" NavigateUrl='<%# string.Format("/san-pham/{0}",Eval("ID")) %>' runat="server" Text=' <%# Eval("TenSanPham")  %>' CommandArgument='<%# Eval("ID") %>' Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("TenSanPham")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mã SP" AccessibleHeaderText="Mã SP" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="sp.MaSanPham">
                                    <ItemTemplate>
                                        <%# Eval("MaSanPham")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Khách hàng" AccessibleHeaderText="Khách hàng" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="kh.TenKhachHang">
                                    <ItemTemplate>
                                        <%# Eval("TenKhachHang")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Kích thước" AccessibleHeaderText="Kích thước" HeaderStyle-Width="120px" ItemStyle-Width="120px">
                                    <ItemTemplate>
                                        <%#  Eval("Dai")  %> x <%#  Eval("Rong")  %> x <%#  Eval("Cao")  %> - <%#  Eval("KichThuoc")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Cán lằn" AccessibleHeaderText="Cán lằn" HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <%#  Eval("CanLan")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bồi Offset" AccessibleHeaderText="Bồi Offset" HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <%#  Eval("BoiOffsetToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tráng" AccessibleHeaderText="Tráng" HeaderStyle-Width="200px" ItemStyle-Width="200px">
                                    <ItemTemplate>
                                        Hóa chất: <%#  Eval("TrangHoaChat")  %> | BOPP: <%#  Eval("TrangBOPP")  %>  | UV: <%#  Eval("TrangUV")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Màu in" AccessibleHeaderText="Màu in" HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <%#  Eval("SoMauIn")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bế Khuôn" AccessibleHeaderText="Bế Khuôn" HeaderStyle-Width="70px" ItemStyle-Width="70px">
                                    <ItemTemplate>
                                        <%#  Eval("BeKhuonToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Số lớp / sóng" AccessibleHeaderText="Số lớp / sóng" HeaderStyle-Width="150px" ItemStyle-Width="150px">
                                    <ItemTemplate>
                                        <%#  Eval("SoLop")  %> / <%#  Eval("SoSong")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Loại giấy" AccessibleHeaderText="Loại giấy" HeaderStyle-Width="250px" ItemStyle-Width="250px">
                                    <ItemTemplate>
                                        Mặt: <%#  Eval("LoaiGiayMat")  %> | Sóng: <%#  Eval("LoaiGiaySong")  %> | Đáy: <%#  Eval("LoaiGiayDay")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Yêu cầu" AccessibleHeaderText="Yêu cầu" HeaderStyle-Width="100px" ItemStyle-Width="100px">
                                    <ItemTemplate>
                                        Mặt: <%#  Eval("YeuCau")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" NavigateUrl='<%# string.Format("/san-pham/{0}",Eval("ID")) %>' CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm_delete_khachHang">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog runat="server" ID="dialogSearch" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            Tìm kiếm sản phẩm
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã sản phẩm </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchMaSanPham" runat="server" PlaceHolder="Nhập mã sản phẩm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên sản phẩm </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenSanPham" runat="server" PlaceHolder="Nhập tên sản phẩm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Khách hàng </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchKhachHang" runat="server" PlaceHolder="Chọn khách hàng tìm kiếm"></SweetSoft:ExtraTextBox>
                        <asp:HiddenField ID="hiddenKhachHangID" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Kích thước </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchKichThuoc" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Lọt lòng" Text="Lọt lòng"></asp:ListItem>
                            <asp:ListItem Value="Phủ bì" Text="Phủ bì"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Cán lằn </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchCanLan" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="đơn" Text="Đơn"></asp:ListItem>
                            <asp:ListItem Value="kép" Text="Kép"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Bồi Offset </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchBoiOffset" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Có" Text="Có"></asp:ListItem>
                            <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tráng hóa chất </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchTrangHoaChat" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                            <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                            <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                            <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tráng BOPP </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchTrangBOPP" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                            <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                            <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                            <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tráng UV </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchTrangUV" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                            <asp:ListItem Value="Trong" Text="Trong"></asp:ListItem>
                            <asp:ListItem Value="Ngoài" Text="Ngoài"></asp:ListItem>
                            <asp:ListItem Value="2 mặt" Text="2 mặt"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Bể khuôn </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchBeKhuon" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Có" Text="Có"></asp:ListItem>
                            <asp:ListItem Value="Không" Text="Không"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số lớp</label>
                        <SweetSoft:ExtraComboBox ID="cbSearchSoLop" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="3" Text="3 lớp"></asp:ListItem>
                            <asp:ListItem Value="5" Text="5 lớp"></asp:ListItem>
                            <asp:ListItem Value="7" Text="7 lớp"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Sóng </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchSong" runat="server">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="A" Text="A"></asp:ListItem>
                            <asp:ListItem Value="B" Text="B"></asp:ListItem>
                            <asp:ListItem Value="E" Text="E"></asp:ListItem>
                            <asp:ListItem Value="AB" Text="AB"></asp:ListItem>
                            <asp:ListItem Value="BE" Text="BE"></asp:ListItem>
                            <asp:ListItem Value="AE" Text="AE"></asp:ListItem>
                            <asp:ListItem Value="ABE" Text="ABE"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Loại giấy mặt </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchLoaiGiayMat" runat="server">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Loại giấy sóng</label>
                        <SweetSoft:ExtraComboBox ID="cbSearchLoaiGiaySong" runat="server">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Loại giấy đáy </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchLoaiGiayDay" runat="server">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
     <link href="/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="/scripts/jquery-ui.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        SearchCustomer();
        Sys.WebForms.PageRequestManager.getInstance().add_endRequest(SearchCustomer);
        function SearchCustomer(s, a) {

            $("input[type='text'][id$='txtSearchKhachHang']").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "/Default.aspx/GetCustomer",
                        data: "{'KeyWord':'" + $("input[type='text'][id$='txtSearchKhachHang']").val() + "'}",
                        dataType: "json",
                        async: true,
                        cache: false,
                        success: function (result) {


                            console.log("result: ", result);
                            response($.map($.parseJSON(result.d), function (item) {
                                return { Id: item.Id, TenKhachHang: item.TenKhachHang };
                            }));
                        }
                    });
                },
                messages: {
                    noResults: '',
                    results: function () { }
                },
                focus: function (event, ui) {

                },
                change: function (event, ui) {

                    if (ui.item == null) {
                        $("input[type='hidden'][id$='hiddenKhachHangID']").val("");
                        $("input[type='text'][id$='txtSearchKhachHang']").val("");
                    }
                },
                search: function (event, ui) {

                },
                select: function (event, ui) {
                    $("input[type='text'][id$='txtSearchKhachHang']").val(ui.item.TenKhachHang);
                    $("input[type='hidden'][id$='hiddenKhachHangID']").val(ui.item.Id);

                    return false;
                }
            }).data("ui-autocomplete")._renderItem = function (ul, item) {
                return $("<li>")
                    .data("ui-autocomplete-item", item)
                    .append("<a><span style='width:30px;'>" + item.TenKhachHang + '</span>' + "</a>")
                    .appendTo(ul);
            };
        }
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
