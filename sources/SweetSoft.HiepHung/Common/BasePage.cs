﻿using SweetSoft.ExtraControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using SweetSoft.ExtraControls.icons;
using SweetSoft.HiepHung.Core;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Manager;
using SweetSoft.HiepHung.DataAccess;

using System.Net.Mail;
using SweetSoft.HiepHung.Core.Manager;
using System.Web.UI.WebControls;


namespace SweetSoft.HiepHung.Common
{
    public class BasePage : Page
    {
        #region Properties

        public BaseMasterPage CURRENT_MASTERPAGE
        {
            get
            {
                if (this.Master != null && this.Master is BaseMasterPage)
                    return this.Master as BaseMasterPage;
                return null;
            }
        }

        public virtual bool IsLoginPage
        {
            get { return false; }
        }

        public virtual bool LoginRequired { get { return true; } }

        public ExtraScriptRegister CURRENT_SCRIPTMANAGER
        {
            get
            {
                if (this.Master != null && this.Master is BaseMasterPage)
                {
                    BaseMasterPage m = this.Master as BaseMasterPage;
                    return m.CURRENT_SCRIPT_MANAGER;
                }
                return null;
            }
        }
        /// <summary>
        /// Mã ngôn ngữ mặc định
        /// </summary>
        public string DefaultLanguage
        {
            get
            {
                string lang = CultureHelper.DEFAULT_LANGUAGE_CODE;
                if (CURRENT_SCRIPTMANAGER != null) lang = CURRENT_SCRIPTMANAGER.DefaultLanguage;
                return lang;
            }
        }

        public bool AllowEmbedGridStyle
        {
            get
            {
                bool allow = false;
                if (CURRENT_SCRIPTMANAGER != null) allow = CURRENT_SCRIPTMANAGER.AllowEmbedGridStyle;
                return allow;
            }
            set
            {
                if (CURRENT_SCRIPTMANAGER != null) CURRENT_SCRIPTMANAGER.AllowEmbedGridStyle = value;
            }
        }

        protected bool USE_DEFAULT_LANGUAGE
        {
            get
            {
                return ApplicationContext.Current.CurrentLanguageCode == DefaultLanguage;
            }
        }

        protected virtual string URL_REWRITE
        {
            get
            {
                return string.Empty;
            }
        }

        public string CurrentReturnURL
        {
            get
            {
                return CommonHelper.QueryString("ReturnURL");
            }
        }


        public MessageBoxResult CurrentMessageBoxResult
        {
            set
            {
                Session["CR_MESSAGEBOXRESULT"] = value;
            }
            get
            {
                if (Session["CR_MESSAGEBOXRESULT"] != null)
                    return Session["CR_MESSAGEBOXRESULT"] as MessageBoxResult;
                return new MessageBoxResult();
            }
        }

        #endregion

        #region Function

        protected void CheckTextControlLength(TextBox ctl, int limit)
        {
            if (ctl != null && ctl.Text.Length > limit)
            {
                AddValidPromt(ctl.ClientID, string.Format("Độ dài không vượt quá {0} ký tự.", limit));
                ctl.Focus();
            }
        }

        protected string ConvertStatusToText(object value, string defaultValue)
        {
            if (value != null)
            {
                if (value.GetType().Equals(typeof(System.Boolean)))
                {
                    bool b = false;
                    bool.TryParse(value.ToString(), out b);
                    if (b) return GetResourceText(ResourceHelper.BE.Default_Status_Active);
                }
                else
                    if (value.GetType().Equals(typeof(System.String)) && value.ToString().Equals(ResourceHelper.BE.Default_Status_Active))
                        return GetResourceText(ResourceHelper.BE.Default_Status_Active);
                return GetResourceText(ResourceHelper.BE.Default_Status_InActive);
            }
            return defaultValue;
        }

        protected string GetStatusStyle(object value)
        {
            return GetStatusStyle(value, "Kích hoạt", "Khóa");
        }

        protected string GetStatusStyle(object value, string textOn, string textOff)
        {
            if (value != null)
            {
                if (value.GetType().Equals(typeof(System.Boolean)))
                {
                    bool b = false;
                    bool.TryParse(value.ToString(), out b);
                    if (b)
                        return string.Format("<span class='label label-success'>{0}</span>", textOn);
                }
                else
                    if (value.GetType().Equals(typeof(System.String)) &&
                        value.ToString().Equals(ResourceHelper.BE.Default_Status_Active))
                        return string.Format("<span class='label'>{0}</span>", textOff);
                return string.Format("<span class='label'>{0}</span>", textOff);
            }
            return string.Format("<span class='label'>{0}</span>", textOff);
        }

        protected string ParseDecimal(object value, bool replace = false)
        {
            string rs = string.Empty;
            if (value != null)
            {
                decimal d = -1;
                if (replace)
                {
                    if (decimal.TryParse(value.ToString().Replace(".", "").Replace(",", "."), out d))
                        rs = d.ExecptionMod().ToStringWithLanguage();
                }
                else if (decimal.TryParse(value.ToString(), out d))
                    rs = d.ExecptionMod().ToStringWithLanguage();
            }
            return rs;
        }

        public string GetYesNoStyle(object value)
        {
            if (value != null)
            {
                if (value.GetType().Equals(typeof(System.Boolean)))
                {
                    bool b = false;
                    bool.TryParse(value.ToString(), out b);
                    if (b)
                        return "<span class='label label-info'>Duyệt</span>";
                }
                else
                    if (value.GetType().Equals(typeof(System.String)) &&
                        value.ToString().Equals(ResourceHelper.BE.Default_Status_Active))
                        return "<span class='label'>Chưa duyệt</span>";
                return "<span class='label'>Chưa duyệt</span>";
            }
            return "<span class='label'>Chưa duyệt</span>";
        }

        protected string GetStatusStyle2(object stt)
        {
            string span = "<big><i class='ace-icon fa fa-times-circle red'></i></big>";
            if (stt != null)
            {
                if (stt.GetType().Equals(typeof(System.Boolean)))
                {
                    bool b = false;
                    bool.TryParse(stt.ToString(), out b);
                    if (b)
                        span = "<big><i class='ace-icon fa fa-check-circle green'></i></big>";
                }
                else if (stt.GetType().Equals(typeof(System.String)) &&
                         stt.ToString().Equals(ResourceHelper.BE.Default_Status_Active))
                    span = "<big><i class='ace-icon fa fa-times-circle red'></i></big>";
            }
            return span;
        }

        /// <summary>
        /// Hàm lấy cờ cho dữ liệu theo người dùng công an
        /// </summary>
        /// <param name="districtId">id quận/huyện</param>
        /// <param name="wardId">id phường/xã</param>




        public bool ButtonState(string rightName)
        {
            bool a = ApplicationContext.Current.CommonUser.IsAdmin.HasValue &&
                     ApplicationContext.Current.CommonUser.IsAdmin.Value;
            bool b = UserHasRight(rightName) || a;
            return b;
        }

        protected string ConvertStatusToText(object value)
        {
            return ConvertStatusToText(value, string.Empty);
        }

        protected string ConvertCurrencyToText(object value, string defaultValue)
        {
            decimal d = 0;
            if (value != null)
            {
                decimal.TryParse(value.ToString(), out d);
                return d.ToStringWithLanguage();
            }
            return defaultValue;
        }

        protected string ConvertCurrencyToText(object value)
        {
            return ConvertCurrencyToText(value, string.Empty);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        protected override void OnLoadComplete(EventArgs e)
        {
            base.OnLoadComplete(e);
            ResourceText();

        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ResourceText();
            if (ApplicationContext.Current.CommonUser == null)
            {
                HttpCookie ck = Request.Cookies["SweetSoft_HiepHung"];
                if (ck != null)
                {
                    string email = SecurityHelper.Decrypt128(ck.Value);
                    TblCommonUser user = UserManager.GetUserByUserName(email);
                    if (user != null)
                    {
                        if (user.CurrentNhanVien.TrangThaiSuDung.HasValue && user.CurrentNhanVien.TrangThaiSuDung.Value)
                        {

                            ApplicationContext.Current.AddCommonUser(user);//lưu user vào session
                            CurrentUserRight = RoleManager.GetRightOfUser(user.Id);
                        }
                    }
                }
                else if (!IsLoginPage && LoginRequired)
                {
                    if (!string.IsNullOrEmpty(Request.RawUrl) &&
                       !Request.RawUrl.ToLower().Contains("default.aspx"))
                    {
                        string url = string.Format("/loginURL{0}",
                                                    HttpUtility.UrlEncode(Request.RawUrl.Replace("/", "_")));
                        Response.Redirect(url);
                    }
                    else
                    {
                        Response.Redirect("/dang-nhap");
                    }
                }
            }

            {
                if (this.FUNCTION_CODE != "SweetSoft_HiepHung" && !UserHasFunctionPage(this.FUNCTION_CODE) && !IsErrorpage && IsUserRole && !IsLoginPage)
                {
                    Session["403Error"] = FUNCTION_NAME;
                    Session["403Function"] = FUNCTION_CODE;
                    Response.Redirect("/403");
                }
            }
            this.Page.Title = string.Format("Hiệp Hưng | {0}", FUNCTION_NAME);
        }

        protected virtual void ResourceText()
        {

        }
        /// <summary>
        /// Refresh panel giữa
        /// </summary>
        protected void UpdateContainerPanel()
        {
            if (CURRENT_MASTERPAGE != null)
                CURRENT_MASTERPAGE.UpdateContainerPanel();
        }
        /// <summary>
        /// Hàm lấy resource text cho hệ thống
        /// </summary>
        /// <param name="resourceKey">Từ khóa</param>
        /// <param name="fileName">File Resource</param>
        /// <returns></returns>
        public string GetResourceText(string resourceKey, string fileName)
        {
            object value = GetGlobalResourceObject(fileName, resourceKey);
            if (value != null)
                return value.ToString();
            return resourceKey;
        }
        /// <summary>
        /// Hàm lấy resource text cho hệ thống
        /// </summary>
        /// <param name="resourceKey">từ khóa</param>
        /// <returns></returns>
        public string GetResourceText(string resourceKey)
        {
            return GetResourceText(ResourceType.Default, resourceKey);
        }

        public string GetResourceText(ResourceType type, string resourceKey)
        {
            return GetResourceText(resourceKey,
                                   string.Format("{0}_{1}", type.EnumToString(),
                                   ApplicationContext.Current.CurrentLanguageCode.Replace("-", "_")));
        }

        public void RunScript(string scriptName, string param)
        {
            string script = string.Format("{0}({1});", scriptName, param);
            ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "RunScript", script, true);
        }

        public void RunScriptC(string scriptName, string param)
        {
            string script = string.Format("{0}({1});", scriptName, param);
            AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(this, typeof(string), "runScript",
                                       script, true);
        }

        public void RunScriptWithString(string script)
        {
            AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(this, typeof(string), "runScript",
                                       script, true);
        }
        #endregion

        #region Right
        public virtual string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Base_Code;
            }
        }

        public virtual string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Base_Name;
            }
        }

        public virtual bool IsUserRole
        {
            get
            {
                return true; // Thay đổi lại khi đã phân quyền
            }
        }

        public virtual bool IsErrorpage
        {
            get { return false; }
        }

        public List<ApplicationUserRight> CurrentUserRight
        {
            get
            {
                List<ApplicationUserRight> list = new List<ApplicationUserRight>();
                if (Session[SessionName.Right_CurrentUserRight] != null)
                    list = Session[SessionName.Right_CurrentUserRight] as List<ApplicationUserRight>;

                if (list.Count == 0)
                {
                    //if (ApplicationContext.Current.CommonUser != null)
                    //{
                    //    Session[SessionName.Right_CurrentUserRight] = RoleManager.GetRightByCommonUserID(ApplicationContext.Current.CommonUser.Id);
                    //    if (Session[SessionName.Right_CurrentUserRight] != null)
                    //        list = Session[SessionName.Right_CurrentUserRight] as List<ApplicationUserRight>;
                    //}
                }
                return list;
            }
            set
            {
                Session[SessionName.Right_CurrentUserRight] = value;
            }
        }

        public bool UserHasFunctionPage(string functionCode)
        {
            bool hasRight = ApplicationContext.Current.CommonUser != null && ApplicationContext.Current.CommonUser.IsAdmin.HasValue && ApplicationContext.Current.CommonUser.IsAdmin.Value && (ApplicationContext.Current.CommonUser.UserName == "administrator" || ApplicationContext.Current.CommonUser.UserName.ToUpper() == "DEMO");
            if (CurrentUserRight.Count > 0)
            {
                ApplicationUserRight right = CurrentUserRight.FirstOrDefault(p => functionCode.Trim().StartsWith(p.FunctionCode.Trim()));
                hasRight |= right != null;
            }
            return hasRight;
        }

        public void AddRightFunction()
        {
            //ApplicationUserRight right = CurrentUserRight.FirstOrDefault(p => p.FunctionCode.Trim() == f.Code.Trim());
            //if (right == null)
            //{
            //    right = new ApplicationUserRight();
            //    right.FunctionCode = f.Code;
            //    right.FunctionID = f.Id;
            //    right.FunctionName = !string.IsNullOrEmpty(f.TitleLang) ? f.TitleLang : f.Title;
            //    CurrentUserRight.Add(right);
            //}
        }

        public bool UserHasRight(string rightCode)
        {
            if (CurrentUserRight.Count > 0)
            {
                ApplicationUserRight right = CurrentUserRight.FirstOrDefault(p => p.RightCode.Trim().Equals(rightCode));
                return right != null || ApplicationContext.Current.CommonUser.UserName.ToUpper() == "DEMO" || (ApplicationContext.Current.CommonUser.IsAdmin.HasValue && ApplicationContext.Current.CommonUser.IsAdmin.Value);
            }
            return true;
        }
        #endregion

        #region Notification


        /// <summary>
        /// Hàm hiển thị thông báo notification
        /// </summary>
        /// <param name="title">Tiêu đề</param>
        /// <param name="message">Tin nhắn</param>
        /// <param name="Type">Loại tin nhắn</param>
        public void ShowNotify(string title, string message, NotifyType Type)
        {
            ShowNotify(title, message, string.Empty, Type, false, false, false, false);

        }
        /// <summary>
        /// Hàm hiển thị thông báo notification
        /// </summary>
        /// <param name="title">Tiêu đề</param>
        /// <param name="message">Nội dung</param>
        /// <param name="image">Hình ảnh</param>
        /// <param name="Type">Loại notification</param>
        /// <param name="sticky">Đính lại trang</param>
        /// <param name="light">Opacity màu nền</param>
        /// <param name="center">Xuất hiện ở giữa màn hình</param>
        /// <param name="runStartup">Bật khi load trang</param>
        public void ShowNotify(string title, string message, string image, NotifyType Type, bool sticky, bool light, bool center, bool runStartup)
        {
            string script = string.Empty;
            if (ExtraSkinManager.DEFAULT_THEME == ExtraTheme.ACE)
            {
                script = string.Format("ACEnotify.Show('{0}','{1}','{2}',{3},'{4}',{5},{6});", title, message, image, sticky.ToString().ToLower(), "gritter-" + Type.EnumToString(), center.ToString().ToLower(), light.ToString().ToLower());
            }
            else
                script = string.Format(" SPnotify.Show('{0}','{1}','{2}','{3}','{4}');", Type.EnumToString(), title, message, AnimationType.SlideInRight.EnumToString(), AnimationType.SlideInRight.EnumToString());

            if (!string.IsNullOrEmpty(script))
            {
                if (!runStartup)
                    AjaxControlToolkit.ToolkitScriptManager.RegisterClientScriptBlock(this, typeof(string), "runNotifyScript",
                                         script, true);
                else
                    ExtraScriptRegister.ScriptRunStartUp += script;
            }
        }

        protected void ShowNotifyStartUp(string title, string message, bool isSticky, NotifyType Type)
        {
            ShowNotifyStartUp(title, message, string.Empty, Type, isSticky, false, false, true);
        }

        public void ShowNotifyStartUp(string title, string message, string image, NotifyType Type, bool sticky, bool light, bool center, bool runStartup)
        {
            string script = string.Format("ACEnotify.Show('{0}','{1}','{2}',{3},'{4}',{5},{6});", title, message, string.Empty, sticky.ToString().ToLower(), "gritter-" + Type.EnumToString(), center.ToString().ToLower(), light.ToString().ToLower());
            ExtraScriptRegister.ScriptRunStartUp += script;
        }
        #endregion

        #region Logging
        public LogPage CurrentLog
        {
            get
            {
                if (Session["Log_" + FUNCTION_CODE] == null)
                {
                    LogPage lg = new LogPage();
                    lg.FunctionCode = FUNCTION_CODE;
                    Session["Log_" + FUNCTION_CODE] = lg;
                }
                return Session["Log_" + FUNCTION_CODE] as LogPage;
            }
        }
        #endregion

        #region Validation
        private StringBuilder cr_listvalidationpromt = new StringBuilder();

        public void AddValidPromt(string validClientID, string message)
        {
            cr_listvalidationpromt.AppendFormat("$('#{0}').validationEngine('showPrompt', '{1}', 'error', true);", validClientID, message);
        }



        public void ShowValidPromt()
        {
            if (cr_listvalidationpromt.Length > 0)
                ScriptManager.RegisterStartupScript(this.Page, this.Page.GetType(), "RunScript", cr_listvalidationpromt.ToString(), true);
        }
        /// <summary>
        /// Kiếm tra 
        /// </summary>
        public bool PageIsValid { get { return cr_listvalidationpromt.Length == 0; } }
        #endregion

        #region Send Mail

        protected void ProcessException(Exception ex)
        {
            CommonHelper.SendEmailError(ex.Message, ex);
        }

        #endregion

        #region Message + Confirm
        public virtual void CloseMessageBox()
        {
            if (CURRENT_MASTERPAGE != null)
            {
                CURRENT_MASTERPAGE.CloseMessageBox();
            }
        }

        public void OpenMessageBox(string title, string message)
        {
            if (CURRENT_MASTERPAGE != null) CURRENT_MASTERPAGE.OpenMessageBox(title, message);
        }

        public void OpenMessageBox(string title, string message, ButtonStyle submitStyle, ExtraIcon submitIcon, string submitText,
                                                                         ButtonStyle cancelStyle, ExtraIcon cancelIcon, string cancelText, MessageBoxResult result)
        {
            if (CURRENT_MASTERPAGE != null)
            {
                result.Submit = true;
                CurrentMessageBoxResult = result;
                CURRENT_MASTERPAGE.OpenMessageBox(title, message, submitStyle, submitIcon, submitText, cancelStyle, cancelIcon, cancelText);
            }
        }

        public void OpenMessageBox(string title, string message, ButtonStyle submitStyle, ExtraIcon submitIcon, string submitText)
        {
            if (CURRENT_MASTERPAGE != null)
            {
                CurrentMessageBoxResult.Submit = true;
                CURRENT_MASTERPAGE.OpenMessageBox(title, message, submitStyle, submitIcon, submitText);
            }
        }

        public virtual void MessageBoxSubmitHanlder()
        {

        }

        public virtual void MessageBoxCancelHanlder()
        {

        }
        #endregion

        #region Send via Amazon
        public static bool SendEmailViaAmazon(string accessKey, string secretAccessKey, string emailSender, string emailSenderAccount, string subject, string strMessage, List<string> emailToList)
        {
            foreach (string email in emailToList)
            {

            }
            return true;
        }
        #endregion
    }


    #region Control Event
    public delegate void ControlDialogEvent(object sender, ControlEventArgs e);

    public delegate void MenuClickEvent(object sender, ControlEventArgs e);


    public class ControlEventArgs
    {
        private string message = string.Empty;
        private object data = null;
        private DateTime date = DateTime.Now;
        public ControlEventArgs() { }
        public string Message { get { return message; } set { message = value; } }
        public object Data { get { return data; } set { data = value; } }
        public DateTime Date { get { return date; } set { date = value; } }
    }

    public class MessageBoxResult
    {
        private bool submit = false;
        private bool fireInControl = false;
        private object value = null;
        private string controlName = string.Empty;
        private string commandName = string.Empty;
        public MessageBoxResult() { this.Submit = true; }
        public bool Submit { get; set; }
        public object Value { get; set; }
        public bool FireInControl { get; set; }
        public string ControlName { get; set; }
        public string CommandName { get; set; }
    }
    #endregion

    #region Right
    public class RightPageName
    {
        #region Quản lý nhân viên
        public static string NhanVien_View = "NhanVien_View";
        public static string NhanVien_Update = "NhanVien_Update";
        public static string NhanVien_Delete = "NhanVien_Delete";
        public static string NhanVien_Role = "NhanVien_Role";
        public static string NhanVien_Insert = "NhanVien_Insert";
        #endregion

        #region Quản lý khách hàng
        public static string KhachHang_View = "KhachHang_View";
        public static string KhachHang_Update = "KhachHang_Update";
        public static string KhachHang_Delete = "KhachHang_Delete";
        public static string KhachHang_Insert = "KhachHang_Insert";
        #endregion

        #region Quản lý quyền
        public static string Role_Insert = "Role_Insert";
        public static string Role_Update = "Role_Update";
        public static string Role_Delete = "Role_Delete";
        public static string Role_Role = "Role_Role";
        public static string Role_View = "Role_View";
        #endregion

        #region Phòng ban / kho
        public static string PB_Insert = "PB_Insert";
        public static string PB_Update = "PB_Update";
        public static string PB_Delete = "PB_Delete";
        public static string PB_View = "PB_View";
        #endregion

        #region Đơn vị tính
        public static string DVT_Insert = "DVT_Insert";
        public static string DVT_Update = "DVT_Update";
        public static string DVT_Delete = "DVT_Delete";
        public static string DVT_View = "DVT_View";
        #endregion

        #region Công việc
        public static string CV_Insert = "CV_Insert";
        public static string CV_Update = "CV_Update";
        public static string CV_Delete = "CV_Delete";
        public static string CV_View = "CV_View";
        #endregion

        #region Máy móc
        public static string MM_Insert = "MM_Insert";
        public static string MM_Update = "MM_Update";
        public static string MM_Delete = "MM_Delete";
        public static string MM_View = "MM_View";
        #endregion

        #region Loại giấy
        public static string LG_Insert = "LG_Insert";
        public static string LG_Update = "LG_Update";
        public static string LG_Delete = "LG_Delete";
        public static string LG_View = "LG_View";
        #endregion

        #region Mẫu in
        public static string MI_Insert = "MI_Insert";
        public static string MI_Update = "MI_Update";
        public static string MI_Delete = "MI_Delete";
        public static string MI_Image = "MI_Image";
        public static string MI_View = "MI_View";
        #endregion

        #region Loại thùng
        public static string LT_Insert = "LT_Insert";
        public static string LT_Update = "LT_Update";
        public static string LT_Delete = "LT_Delete";
        public static string LT_Image = "LT_Image";
        public static string LT_View = "LT_View";
        #endregion

        #region Nguyên liệu
        public static string NL_Insert = "NL_Insert";
        public static string NL_Update = "NL_Update";
        public static string NL_Delete = "NL_Delete";
        public static string NL_View = "NL_View";
        #endregion

        #region Nguyên liệu Chi Tiết
        public static string NLCT_Insert = "NLCT_Insert";
        public static string NLCT_Update = "NLCT_Update";
        public static string NLCT_Delete = "NLCT_Delete";
        public static string NLCT_View = "NLCT_View";
        #endregion

        #region Sản phẩm
        public static string SP_Insert = "SP_Insert";
        public static string SP_Update = "SP_Update";
        public static string SP_Delete = "SP_Delete";
        public static string SP_View = "SP_View";
        #endregion


        #region Đơn hàng
        public static string DH_Insert = "DH_Insert";
        public static string DH_Update = "DH_Update";
        public static string DH_Delete = "DH_Delete";
        public static string DH_View = "DH_View";
        #endregion
    }
    #endregion
}