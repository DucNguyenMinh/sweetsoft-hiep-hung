﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="NguyenLieuChiTiet.aspx.cs" Inherits="SweetSoft.HiepHung.NguyenLieuChiTiet" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }

        .lb-donvitinh
        {
            font-weight: bold;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý thông tin
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,3,4,6,7,8,9,10">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nguyên liệu chi tiết" AccessibleHeaderText="Nguyên liệu chi tiết" HeaderStyle-Width="450px" ItemStyle-Width="450px" SortExpression="chiTiet.TenChiTietNguyenLieu">
                                    <ItemTemplate>
                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" runat="server" Text=' <%# Eval("TenChiTietNguyenLieu")  %>' OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("TenChiTietNguyenLieu")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mã Code" AccessibleHeaderText="Mã Code" HeaderStyle-Width="100px" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="100px" SortExpression="chiTiet.Code">
                                    <ItemTemplate>
                                        <%# Eval("Code")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Nguyên liệu" AccessibleHeaderText="Nguyên liệu" HeaderStyle-Width="100px" ItemStyle-Width="300px" SortExpression="nl.TenNguyenLieu">
                                    <ItemTemplate>
                                        <%# Eval("TenNguyenLieu")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SL lưu kho" AccessibleHeaderText="SL lưu kho" HeaderStyle-Width="200px" ItemStyle-Width="300px" ItemStyle-HorizontalAlign="Right" SortExpression="chiTiet.SoLuongLuuKho">
                                    <ItemTemplate>
                                        <%# ParseDecimal(Eval("SoLuongLuuKho"))  %> (<%# Eval("TenVietTatLuuKho") %>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tồn đầu" AccessibleHeaderText="Tồn đầu" HeaderStyle-Width="200px" ItemStyle-Width="300px" SortExpression="chiTiet.TonDau">
                                    <ItemTemplate>
                                        <%# ParseDecimal(Eval("TonDau"))  %> (<%# Eval("TenVietTat") %>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tồn cuối" AccessibleHeaderText="Tồn cuối" HeaderStyle-Width="200px" ItemStyle-Width="300px" ItemStyle-HorizontalAlign="Right" SortExpression="chiTiet.TonCuoi">
                                    <ItemTemplate>
                                        <%# ParseDecimal(Eval("TonCuoi"))  %> (<%# Eval("TenVietTat") %>)
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ngày hết hạn" AccessibleHeaderText="Ngày hết hạn" HeaderStyle-Width="200px" ItemStyle-Width="400px" SortExpression="chiTiet.NgayHetHan">
                                    <ItemTemplate>
                                        <%# Eval("NgayHetHanToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Vị trí trong kho" AccessibleHeaderText="Vị trí trong kho" HeaderStyle-Width="400px" ItemStyle-Width="400px">
                                    <ItemTemplate>
                                        <%# Eval("ViTriTrongKho")  %>, kho <%# Eval("TenPhongBan")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm_delete_khachHang">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog runat="server" ID="dialogSearch" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            Tìm kiếm nguyên liệu / vật tư
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã Code </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchMaCode" runat="server" PlaceHolder="Nhập mã code"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 no-padding">
                    <div class="form-group">
                        <label class="extra-label">Tên chi tiết </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenChiTiet" runat="server" PlaceHolder="Nhập tên chi tiết nguyên liệu"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Nguyên liệu / vật tư</label>
                        <SweetSoft:ExtraComboBox ID="cbSearchNguyenLieu" runat="server" EmptyMessage="Chọn tất cả"></SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Kho</label>
                        <SweetSoft:ExtraComboBox ID="cbSearchKho" runat="server" EmptyMessage="Chọn tất cả"></SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Vị trí trong kho</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchViTri" PlaceHolder="Nhập vị trí trong kho" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày hết hạn từ ngày</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateHetHanTu" runat="server" Width="100%" PlaceHolder="Chọn ngày hết hạn" DateTimeMask="Date" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">tới ngày</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateHetHanToi" runat="server" Width="100%" PlaceHolder="Chọn ngày hết hạn" DateTimeMask="Date" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số lượng lưu kho từ</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchSoLuongLuuKhoTu" PlaceHolder="Nhập số lượng lưu kho" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số lượng lưu kho tới</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchSoLuongLuuKhoToi" PlaceHolder="Nhập số lượng lưu kho" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số lượng tính tồn từ</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTonTu" PlaceHolder="Nhập số lượng tồn" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số lượng tính tồn tới</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTonToi" PlaceHolder="Nhập số lượng tồn" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label class="extra-label">Ghi chú</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchGhiChu" PlaceHolder="Nhập ghi chú" TextMode="MultiLine" Height="70px" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
    <SweetSoft:ExtraDialog runat="server" ID="dialogDetail" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Label ID="lbDetail" runat="server" Text=" Chi tiết nguyên liệu / vật tư"></asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã Code  (*)</label>
                        <SweetSoft:ExtraTextBox ID="txtEditMaCode" runat="server" PlaceHolder="Nhập mã code"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên nguyên liệu  (*)</label>
                        <SweetSoft:ExtraTextBox ID="txtEditTenNguyenLieu" runat="server" PlaceHolder="Nhập tên chi tiết nguyên liệu"> </SweetSoft:ExtraTextBox>
                        <asp:HiddenField ID="hiddenEditID" runat="server" />
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Nguyên liệu / vật tư (*)</label>
                        <SweetSoft:ExtraComboBox ID="cbEditNguyenLieu" AutoPostBack="true" OnSelectedIndexChanged="cbEditNguyenLieu_SelectedIndexChanged" runat="server" EmptyMessage="Chọn nguyên liệu / vật tư"></SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                    <div class="form-group">
                        <label class="extra-label">
                            SL lưu kho
                            <asp:Label CssClass="lb-donvitinh" ID="lbDonViLuuKho" runat="server"></asp:Label>(*)
                            <asp:HiddenField ID="hiddenDonViLuuKho" runat="server" />
                        </label>
                        <SweetSoft:ExtraTextBox ID="txtEditSoLuongLuuKho" Text="1" PlaceHolder="Nhập số lượng lưu kho" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>

                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">
                            SL tính còn
                            <asp:Label CssClass="lb-donvitinh" ID="lbDonViTinh" runat="server"></asp:Label></label>(*)
                        <asp:HiddenField ID="hiddenDonViTinhRoot" runat="server" />
                        <SweetSoft:ExtraTextBox ID="txtEditSoLuongTon" PlaceHolder="Nhập số lượng tồn" ValidationType="Currency" runat="server"> </SweetSoft:ExtraTextBox>

                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Kho (*)</label>
                        <SweetSoft:ExtraComboBox ID="cbEditKho" runat="server" EmptyMessage="Chọn kho"></SweetSoft:ExtraComboBox>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Vị trí trong kho (*)</label>
                        <SweetSoft:ExtraTextBox ID="txtEditViTri" PlaceHolder="Nhập vị trí trong kho" Required="true" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày hết hạn</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateNgayHetHan" runat="server" Width="100%" PlaceHolder="Chọn ngày hết hạn" DateTimeMask="Date" />
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ghi chú</label>
                        <SweetSoft:ExtraTextBox ID="txtEditGhiChu" PlaceHolder="Nhập ghi chú" TextMode="MultiLine" Height="70px" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSave" EnableRound="false" EStyle="Success_Bold" runat="server" Text="Lưu dữ liệu" EIcon="A_Save" OnClick="btnSave_Click">  </SweetSoft:ExtraButton>
            <SweetSoft:ExtraButton ID="btnAddNewSecond" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Tạo mới" EIcon="A_Add_Square" OnClick="btnAddNew_Click">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" Visible="false" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
    <SweetSoft:ExtraButton ID="btnSubmitSave" Visible="false" runat="server" OnClick="btnSubmitSave_Click" Text="Đồng ý" EIcon="A_Check" EStyle="Primary_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
