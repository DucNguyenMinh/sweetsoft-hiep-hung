﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;

namespace SweetSoft.HiepHung
{
    public partial class QLDonHang : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.DonHang;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.DonHang;
            }
        }
        #endregion


        protected string CurrentName
        {
            get
            {
                string rs = "đơn hàng";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.DH_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.DH_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.DH_Delete);

            return rs;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
          
            txtSearchMaDonHang.EnterSubmitClientID = txtSearchKhachHang.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");

            if (!IsPostBack)
            {
                grvData.CurrentSortExpression = TblDonHang.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }

                cbbFilterColumns.EmptyMessage = "Chọn tất cả";
                cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
            }
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" sp.{0} desc ", TblDonHang.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" dh.*, kh.{0} ",
                                                TblKhachHang.Columns.TenKhachHang, TblLoaiGiay.Columns.TenLoaiGiay, TblLoaiMauIn.Columns.TenMau);

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchMaDonHang.Text.Trim()))
                    filterClause += string.Format(" sp.{0} like N'%{1}%' AND ",
                                                  TblDonHang.Columns.MaDonHang,
                                                  txtSearchMaDonHang.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));

                if (!string.IsNullOrEmpty(hiddenKhachHangID.Value))
                    filterClause += string.Format(" sp.{0} = {1} AND ",
                                                  TblDonHang.Columns.IDKhachHang,
                                                  hiddenKhachHangID.Value);

                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = string.Format(@" left join TblKhachHang kh on dh.IDKhachHang = kh.Id 
                                                    ");

                int total = 0;
                List<TblDonHang> lst = SanPhamManager.SearchDonHang(rowIndex, pageSize, string.Format(" TblDonHang as dh "), column, tableJoin,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblDonHang> lstEmpty = new List<TblDonHang>();
                lstEmpty.Add(new TblDonHang());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("/don-hang/0");
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchKhachHang.Text = hiddenKhachHangID.Value = txtSearchKhachHang.Text = string.Empty;
            grvData.Rebind();
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {

                }
                else
                {
                    TblDonHang sanPham = SanPhamManager.GetDonHangByID(btn.CommandArgument);
                    if (sanPham != null)
                    {

                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = sanPham;
                        result.Submit = true;
                        result.CommandName = "delete_donHang";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa thông tin sản phẩm", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa thông tin đơn hàng <b>{0}</b> ?",
                                                     sanPham.MaDonHang, CurrentName));
                    }
                }
            }
        }
    }
}