﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="HTKhachHang.aspx.cs" Inherits="SweetSoft.HiepHung.HTKhachHang" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
      
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý thông tin
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,5,6,7,8,9,10">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tên khách hàng" AccessibleHeaderText="Tên khách hàng" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="kh.TenKhachHang">
                                    <ItemTemplate>
                                          <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" runat="server" Text=' <%# Eval("TenKhachHang")  %>' OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# GetButtonRole("Update") %>' ></SweetSoft:ExtraButton>
                                      <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("TenKhachHang")  %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Số điện thoại" AccessibleHeaderText="Số điện thoại" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="kh.SoDienThoai">
                                    <ItemTemplate>
                                        <%# Eval("SoDienThoai")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mã số thuế" AccessibleHeaderText="Mã số thuế" HeaderStyle-Width="100px" ItemStyle-Width="300px" SortExpression="kh.DiaChi">
                                    <ItemTemplate>
                                        <%# Eval("MaSoThue")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tài khoản ngân hàng" AccessibleHeaderText="Tài khoản ngân hàng" HeaderStyle-Width="300px" ItemStyle-Width="300px" SortExpression="kh.TaiKhoanNganHang">
                                    <ItemTemplate>
                                        <%# Eval("TaiKhoanNganHang")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Địa chỉ" AccessibleHeaderText="Địa chỉ" HeaderStyle-Width="300px" ItemStyle-Width="300px" SortExpression="kh.Địa chỉ">
                                    <ItemTemplate>
                                        <%# Eval("DiaChi")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm_delete_khachHang">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog runat="server" ID="dialogSearch" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            Tìm kiếm khách hàng
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên khách hàng </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenKhachHang" runat="server" PlaceHolder="Nhập tên khách hàng"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số điện thoại </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchSoDienThoai" PlaceHolder="Nhập số điện thoại" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Mã số thuế</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchMaSoThue" PlaceHolder="Nhập mã số thuế" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Địa chỉ</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchDiaChi" PlaceHolder="Nhập địa chỉ" Height="70px" TextMode="MultiLine" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tài khoản ngân hàng</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTaiKhoan" PlaceHolder="Nhập thông tin tài khoản ngân hàng" Height="70px" TextMode="MultiLine" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên người đại diện</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenNguoiDaiDien" PlaceHolder="Nhập tên người đại diện" runat="server"> </SweetSoft:ExtraTextBox>
                        <label class="extra-label">Số điện thoại đại diện</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchSDTDaiDien" PlaceHolder="Nhập số điện thoại đại diện" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Địa chỉ nơi đại diện</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchDiaChiDaiDien" PlaceHolder="Nhập địa chỉ người đại diện" Height="70px" TextMode="MultiLine" runat="server"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
    <SweetSoft:ExtraDialog runat="server" ID="dialogDetail" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Large" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Label runat="server" ID="lblDialogDetailTitle"> </asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    Thông tin khách hàng
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="extra-label">Tên khách hàng * </label>
                                    <SweetSoft:ExtraTextBox ID="txtDetailTen" runat="server" PlaceHolder="Nhập tên khách hàng"> </SweetSoft:ExtraTextBox>
                                    <asp:HiddenField ID="hdfDetailID" runat="server" />
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="extra-label">Số điện thoại </label>
                                    <SweetSoft:ExtraTextBox ID="txtDetailSoDienThoai" ValidationGroup="Detail" PlaceHolder="Nhập số điện thoại" ValidationType="Number"  data-errormessage-custom-error="Sai định dạng số điện thoại" runat="server"> </SweetSoft:ExtraTextBox>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="extra-label">Mã số thuế</label>
                                    <SweetSoft:ExtraTextBox ID="txtDetailMaSoThue" ValidationGroup="Detail" PlaceHolder="Nhập mã số thuế" runat="server" ValidationType="Number"  data-errormessage-custom-error="Sai định dạng mã số thuế"> </SweetSoft:ExtraTextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="extra-label">Địa chỉ</label>
                                    <SweetSoft:ExtraTextBox ID="txtDetailDiaChi" PlaceHolder="Nhập địa chỉ" Height="70px" TextMode="MultiLine" runat="server"> </SweetSoft:ExtraTextBox>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="extra-label">Tài khoản ngân hàng</label>
                                    <SweetSoft:ExtraTextBox ID="txtDetailTaiKhoanNganHang" PlaceHolder="Nhập thông tin tài khoản ngân hàng" Height="70px" TextMode="MultiLine" runat="server"> </SweetSoft:ExtraTextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Thông tin liên hệ</h5>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNewLienHe" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới liên hệ" OnClick="btnAddNewLienHe_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <div style="text-align: center;">
                            <SweetSoft:ExtraButton ID="btnSaveFirst" runat="server" EIcon="A_Save" EStyle="Success_Bold" Text="Lưu thông tin khách hàng và thêm mới thông tin liên hệ" OnClick="btnSaveFirst_Click"></SweetSoft:ExtraButton>
                        </div>
                        <div>
                            <SweetSoft:ExtraGrid Width="100%" ID="gridLienHe" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                                runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true" OnRowCancelingEdit="grvData_RowCancelingEdit" OnRowUpdating="grvData_RowUpdating" OnRowCreated="grvData_RowCreated"
                                OnNeedDataSource="gridLienHe_NeedDataSource" PagingControlName="paging2">
                                <EmptyDataTemplate>
                                    Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                                </EmptyDataTemplate>
                                <Columns>
                                    <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                        <ItemTemplate>
                                            <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Tên người đại diện" AccessibleHeaderText="Tên người đại diện" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="TenDaiDien">
                                        <ItemTemplate>
                                            <%# Eval("TenDaiDien")  %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:HiddenField ID="hdfEditID" runat="server" />
                                            <SweetSoft:ExtraTextBox ID="txtEditTenDaiDien" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblKhachHangLienHe.TenDaiDienColumn.MaxLength %>' EnterSubmitClientID='ctl00_ContentPlaceHolder1_gridLienHe_ctl02_btnSubmit'  ValidationGroup="chiTiet" Required="true" CssClass="textbox-edit-width" PlaceHolder="Nhập tên người đại diện" ValidRequiredMessage="Nhập tên người đại diện" runat="server">
                                            </SweetSoft:ExtraTextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Số điện thoại" AccessibleHeaderText="Số điện thoại" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="SoDienThoai">
                                        <ItemTemplate>
                                            <%# Eval("SoDienThoai")  %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <SweetSoft:ExtraTextBox ID="txtEditSoDienThoai"  data-errormessage-custom-error="Sai định dạng số điện thoại"  ValidationType="Number" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblKhachHangLienHe.SoDienThoaiColumn.MaxLength %>' EnterSubmitClientID='ctl00_ContentPlaceHolder1_gridLienHe_ctl02_btnSubmit' CssClass="textbox-edit-width" PlaceHolder="Nhập số điện thoại" ValidationGroup="chiTiet" ValidRequiredMessage="Nhập số điện thoại" runat="server">
                                            </SweetSoft:ExtraTextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Địa chỉ" AccessibleHeaderText="Địa chỉ" HeaderStyle-Width="300px" ItemStyle-Width="200px" >
                                        <ItemTemplate>
                                            <%# Eval("DiaChi")  %>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <SweetSoft:ExtraTextBox ID="txtEditDiaChi"  MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblKhachHangLienHe.DiaChiColumn.MaxLength %>' EnterSubmitClientID='ctl00_ContentPlaceHolder1_gridLienHe_ctl02_btnSubmit' CssClass="textbox-edit-width" PlaceHolder="Nhập địa chỉ" ValidRequiredMessage="Nhập địa chỉ" runat="server" TextMode="MultiLine" Height="70px">
                                            </SweetSoft:ExtraTextBox>
                                        </EditItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="50px" ItemStyle-Width="100px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                        <ItemTemplate>
                                            <div class="hidden-sm hidden-xs btn-group">
                                                <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" CommandName="Edit">
                                                </SweetSoft:ExtraButton>
                                                <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm_delete_lienHe">
                                                </SweetSoft:ExtraButton>
                                            </div>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <div class="hidden-sm hidden-xs btn-group">
                                                <SweetSoft:ExtraButton ID="btnSubmit" runat="server" CssClass="btn-xs" EStyle="Success" EnableSmallSize="true" EIcon="A_Submit" ToolTip="Cập nhật" CommandName="Update" ESubmitAndCheckValid="true" ValidationGroup="chiTiet">
                                                </SweetSoft:ExtraButton>
                                                <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy" CommandName="Cancel">
                                                </SweetSoft:ExtraButton>
                                            </div>
                                        </EditItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerTemplate>
                                    <SweetSoft:Paging ID="paging2" runat="server" />
                                </PagerTemplate>
                            </SweetSoft:ExtraGrid>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSave" runat="server" EIcon="A_Save" Text="Lưu thông tin" EStyle="Success_Bold" ESubmitAndCheckValid="true" EnableSnipButton="true" ValidationGroup="Detail" OnClick="btnSave_Click">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <asp:HiddenField ID="hiddenDelete" runat="server" />
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
