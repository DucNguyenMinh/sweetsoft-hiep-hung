﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;

namespace SweetSoft.HiepHung
{
    public partial class DMSanPham : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Cat_SanPham;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Cat_SanPham;
            }
        }
        #endregion


        protected string CurrentName
        {
            get
            {
                string rs = "sản phẩm";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.SP_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.SP_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.SP_Delete);

            return rs;
        }

        protected void Page_Load(object sender, EventArgs e)
        {


            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);
            txtSearchMaSanPham.EnterSubmitClientID = txtSearchTenSanPham.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");

            if (!IsPostBack)
            {

                List<TblLoaiGiay> loaiGiayCol = CategoryManager.GetLoaiGiayALL(true);
                TblLoaiGiay all = new TblLoaiGiay();
                all.Id = -1;
                all.TenLoaiGiay = "Chọn tất cả";
                loaiGiayCol.Insert(0, all);
                cbSearchLoaiGiayDay.DataSource = loaiGiayCol;
                cbSearchLoaiGiayDay.DataValueField = TblLoaiGiay.Columns.Id;
                cbSearchLoaiGiayDay.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
                cbSearchLoaiGiayDay.DataBind();

                List<TblLoaiGiay> loaiGiayCol2 = new List<TblLoaiGiay>(loaiGiayCol);
                cbSearchLoaiGiayMat.DataSource = loaiGiayCol2;
                cbSearchLoaiGiayMat.DataValueField = TblLoaiGiay.Columns.Id;
                cbSearchLoaiGiayMat.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
                cbSearchLoaiGiayMat.DataBind();

                List<TblLoaiGiay> loaiGiayCol3 = new List<TblLoaiGiay>(loaiGiayCol);
                cbSearchLoaiGiaySong.DataSource = loaiGiayCol3;
                cbSearchLoaiGiaySong.DataValueField = TblLoaiGiay.Columns.Id;
                cbSearchLoaiGiaySong.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
                cbSearchLoaiGiaySong.DataBind();

                grvData.CurrentSortExpression = TblKhachHang.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            Response.Redirect("/san-pham/0");
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" sp.{0} desc ", TblSanPham.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" sp.*, kh.{0}, lgm.{1} as LoaiGiayMat,lgs.{1} as LoaiGiaySong, lgd.{1} as LoaiGiayDay,lmt.{2} as TenKieuThung, lmi.{2} as TenMauIn ",
                                                TblKhachHang.Columns.TenKhachHang, TblLoaiGiay.Columns.TenLoaiGiay, TblLoaiMauIn.Columns.TenMau);

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchMaSanPham.Text.Trim()))
                    filterClause += string.Format(" sp.{0} like N'%{1}%' AND ",
                                                  TblSanPham.Columns.MaSanPham,
                                                  txtSearchMaSanPham.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTenSanPham.Text.Trim()))
                    filterClause += string.Format(" sp.{0} like N'%{1}%' AND ",
                                                  TblSanPham.Columns.TenSanPham,
                                                  txtSearchTenSanPham.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(hiddenKhachHangID.Value))
                    filterClause += string.Format(" sp.{0} = {1} AND ",
                                                  TblSanPham.Columns.IDKhachHangRieng,
                                                  hiddenKhachHangID.Value);
                if (cbSearchKichThuoc.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.KichThuoc,
                                                  cbSearchKichThuoc.SelectedValue);
                if (cbSearchCanLan.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.CanLan,
                                                  cbSearchCanLan.SelectedValue);
                if (cbSearchBoiOffset.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.BoiOffset,
                                                  cbSearchBoiOffset.SelectedValue);
                if (cbSearchTrangHoaChat.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.TrangHoaChat,
                                                  cbSearchTrangHoaChat.SelectedValue);
                if (cbSearchTrangBOPP.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.TrangBOPP,
                                                  cbSearchTrangBOPP.SelectedValue);
                if (cbSearchTrangUV.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.TrangUV,
                                                  cbSearchTrangUV.SelectedValue);
                if (cbSearchBeKhuon.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.BeKhuon,
                                                  cbSearchBeKhuon.SelectedValue);
                if (cbSearchSoLop.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.SoLop,
                                                  cbSearchSoLop.SelectedValue);
                if (cbSearchSong.HasValue)
                    filterClause += string.Format(" sp.{0} = N'{1}' AND ",
                                                  TblSanPham.Columns.SoSong,
                                                  cbSearchSong.SelectedValue);
                if (cbSearchLoaiGiayMat.HasValue)
                    filterClause += string.Format(" sp.{0} = {1} AND ",
                                                  TblSanPham.Columns.IDLoaiGiayMat,
                                                  cbSearchLoaiGiayMat.SelectedValue);
                if (cbSearchLoaiGiaySong.HasValue)
                    filterClause += string.Format(" sp.{0} = {1} AND ",
                                                  TblSanPham.Columns.IDLoaiGiaySong,
                                                  cbSearchLoaiGiaySong.SelectedValue);
                if (cbSearchLoaiGiayDay.HasValue)
                    filterClause += string.Format(" sp.{0} = {1} AND ",
                                                  TblSanPham.Columns.IDLoaiGiayDay,
                                                  cbSearchLoaiGiayDay.SelectedValue);
                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = string.Format(@" left join TblKhachHang kh on sp.IDKhachHangRieng = kh.Id 
                                                    left join TblLoaiGiay lgm on sp.IDLoaiGiayMat = lgm.Id 
                                                    left join TblLoaiGiay lgs on sp.IDLoaiGiaySong = lgs.Id 
                                                    left join TblLoaiGiay lgd on sp.IDLoaiGiayDay = lgd.Id 
                                                    left join TblLoaiMauIn lmt on sp.IDKieuThung = lmt.Id 
                                                    left join TblLoaiMauIn lmi on sp.IDLoaiMauIn = lmi.Id ");

                int total = 0;
                List<TblSanPham> lst = SanPhamManager.SearchSanPham(rowIndex, pageSize, string.Format(" TblSanPham as sp "), column, tableJoin,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblSanPham> lstEmpty = new List<TblSanPham>();
                lstEmpty.Add(new TblSanPham());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchMaSanPham.Text = txtSearchTenSanPham.Text = string.Empty;
            cbSearchBeKhuon.SelectedIndex = cbSearchBeKhuon.SelectedIndex = cbSearchBoiOffset.SelectedIndex =
            cbSearchCanLan.SelectedIndex = cbSearchKichThuoc.SelectedIndex =
            cbSearchLoaiGiayDay.SelectedIndex = cbSearchLoaiGiayMat.SelectedIndex = cbSearchLoaiGiaySong.SelectedIndex =
            cbSearchSoLop.SelectedIndex = cbSearchSong.SelectedIndex = cbSearchTrangBOPP.SelectedIndex = cbSearchTrangHoaChat.SelectedIndex =
            cbSearchTrangUV.SelectedIndex = 0;
            hiddenKhachHangID.Value = txtSearchKhachHang.Text = string.Empty;
            grvData.Rebind();
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {

                }
                else
                {
                    TblSanPham sanPham = SanPhamManager.GetSanPhamByID(btn.CommandArgument);
                    if (sanPham != null)
                    {

                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = sanPham;
                        result.Submit = true;
                        result.CommandName = "delete_sanPham";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa thông tin sản phẩm", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa thông tin sản phẩm <b>{0}</b> ?",
                                                     sanPham.TenSanPham, CurrentName));
                    }
                }
            }
        }

        protected void cbSearchSoLop_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}