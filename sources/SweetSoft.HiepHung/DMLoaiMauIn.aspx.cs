﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace SweetSoft.HiepHung
{
    public partial class DMLoaiMauIn : BaseGridPage
    {
        #region Properties

        public override string FUNCTION_CODE
        {
            get
            {
                if (CurrentType == "LMI")
                    return FunctionSystem.Code.Cat_Loai_Mau_In;
                return FunctionSystem.Code.Cat_Loai_Thung;
            }
        }

        public override string FUNCTION_NAME
        {
            get
            {

                if (CurrentType == "LMI")
                    return FunctionSystem.Name.Cat_Loai_Mau_In;
                return FunctionSystem.Name.Cat_Loai_Thung;
            }
        }

        protected string CurrentType
        {
            get
            {
                return CommonHelper.QueryString("CType");
            }
        }

        protected string CurrentName
        {
            get
            {
                string rs = string.Empty;
                if (CurrentType == "LMI") rs = "Mẫu in";
                else if (CurrentType == "Thung") rs = "loại thùng";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;
            if (CurrentType == "LMI")
            {
                if (rightName == "Insert") rs = UserHasRight(RightPageName.MI_Insert);
                else if (rightName == "Update") rs = UserHasRight(RightPageName.MI_Update);
                else if (rightName == "Delete") rs = UserHasRight(RightPageName.MI_Delete);
                else if (rightName == "Image") rs = UserHasRight(RightPageName.MI_Image);

            }
            else if (CurrentType == "Thung")
            {
                if (rightName == "Insert") rs = UserHasRight(RightPageName.LT_Insert);
                else if (rightName == "Update") rs = UserHasRight(RightPageName.LT_Update);
                else if (rightName == "Delete") rs = UserHasRight(RightPageName.LT_Delete);
                else if (rightName == "Image") rs = UserHasRight(RightPageName.LT_Image);
            }

            return rs;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);

            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);

            btnAddNew.Visible = GetButtonRole("Insert");
            if (!IsPostBack)
            {
                grvData.CurrentSortExpression = TblLoaiMauIn.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();
                    item.Text = grvData.Columns[i].HeaderText;
                    item.Selected = grvData.CheckColumnVisible(i);
                    if (item.Text == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            grvData.InsertItem();
        }

        protected void grvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvData.CancelAllCommand();
        }

        protected void grvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grvData.Rows[e.RowIndex];
            if (row != null)
            {
                TextBox txtEditTenLoaiMau = row.FindControl("txtEditTenLoaiMau") as TextBox;
                ExtraCheckBox chkEditStatus = row.FindControl("chkEditStatus") as ExtraCheckBox;
                HiddenField hdfEditID = row.FindControl("hdfEditID") as HiddenField;

                #region Validation
                if (string.IsNullOrEmpty(txtEditTenLoaiMau.Text))
                {
                    AddValidPromt(txtEditTenLoaiMau.ClientID, string.Format("Nhập tên {0}", CurrentName));
                    txtEditTenLoaiMau.Focus();
                }



                #endregion

                e.Cancel = !PageIsValid;
                if (PageIsValid)
                {
                    if (grvData.IsInsert)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.PB_Insert, string.Format("Thêm mới {1} <b>{0}</b>", txtEditTenLoaiMau.Text, CurrentName));

                        #region Insert value for LoaiMauIn
                        TblLoaiMauIn LoaiMauIn = new TblLoaiMauIn();
                        LoaiMauIn.TenMau = txtEditTenLoaiMau.Text.Trim();
                        LoaiMauIn.TrangThaiSuDung = chkEditStatus.Checked;
                        LoaiMauIn.NguoiTao = LoaiMauIn.NguoiCapNhap = ApplicationContext.Current.CommonUser.UserName;
                        LoaiMauIn.NgayTao = LoaiMauIn.NgayCapNhat = DateTime.Now;
                        LoaiMauIn.CType = CurrentType;
                        #endregion

                        #region Add log value
                        CurrentLog.AddFirstValue(RightPageName.MI_Insert, string.Format("Tên {0}", CurrentName),
                                                 TblLoaiMauIn.Columns.TenMau,
                                                 txtEditTenLoaiMau.Text);

                        CurrentLog.AddFirstValue(RightPageName.MI_Insert, "Trạng thái",
                                                 TblLoaiMauIn.Columns.TrangThaiSuDung,
                                                 chkEditStatus.Text);
                        #endregion

                        LoaiMauIn = CategoryManager.InsertLoaiMauIn(LoaiMauIn);
                        ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                   string.Format("Thêm mới {1} <b>{0}</b> thành công.",
                                                 LoaiMauIn.TenMau, CurrentName),
                                   NotifyType.Success);
                        CurrentLog.SaveLog(RightPageName.MI_Insert);
                        grvData.Rebind();
                    }
                    else
                    {
                        TblLoaiMauIn LoaiMauIn = CategoryManager.GetLoaiMauInByID(hdfEditID.Value);
                        if (LoaiMauIn != null)
                        {
                            #region Update value for LoaiMauIn
                            LoaiMauIn.TenMau = txtEditTenLoaiMau.Text.Trim();
                            LoaiMauIn.TrangThaiSuDung = chkEditStatus.Checked;
                            LoaiMauIn.NguoiCapNhap = ApplicationContext.Current.CommonUser.UserName;
                            LoaiMauIn.NgayCapNhat = DateTime.Now;

                            #endregion

                            #region Add log value
                            CurrentLog.AddLastValue(RightPageName.MI_Update,
                                                    TblLoaiMauIn.Columns.TenMau,
                                                    txtEditTenLoaiMau.Text);
                            CurrentLog.AddLastValue(RightPageName.MI_Update,
                                                    TblLoaiMauIn.Columns.TrangThaiSuDung,
                                                    chkEditStatus.Text);
                            #endregion

                            LoaiMauIn = CategoryManager.UpdateLoaiMauIn(LoaiMauIn);
                            ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                       string.Format("Cập nhật {1} <b>{0}</b> thành công.",
                                                     LoaiMauIn.TenMau, CurrentName),
                                       NotifyType.Success);
                            CurrentLog.SaveLog(RightPageName.MI_Update);
                            grvData.Rebind();
                        }
                    }
                    grvData.CancelAllCommand();
                }
                if (!PageIsValid)
                    ShowValidPromt();
            }
        }

        protected void grvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            ExtraButton btnEdit = e.Row.FindControl("btnEdit") as ExtraButton;
            if (btnEdit != null) btnEdit.Visible = GetButtonRole("Update");

            ExtraButton btnDelete = e.Row.FindControl("btnDelete") as ExtraButton;
            if (btnDelete != null) btnDelete.Visible = GetButtonRole("Delete");

            ExtraButton btnConfigImage = e.Row.FindControl("btnConfigImage") as ExtraButton;
            if (btnConfigImage != null) btnDelete.Visible = GetButtonRole("Image");

            if (grvData.EditIndex == e.Row.RowIndex && !grvData.IsInsert)
            {
                TblLoaiMauIn LoaiMauIn = e.Row.DataItem as TblLoaiMauIn;
                if (LoaiMauIn != null)
                {
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.MI_Update,
                                                 string.Format("Cập nhật mẫu in {1} <b>{0}</b>",
                                                                LoaiMauIn.TenMau, CurrentName));
                    TextBox txtEditTenLoaiMau
                        = e.Row.FindControl("txtEditTenLoaiMau") as TextBox;

                    ExtraCheckBox chkEditStatus
                        = e.Row.FindControl("chkEditStatus") as ExtraCheckBox;
                    HiddenField hdfEditID
                        = e.Row.FindControl("hdfEditID") as HiddenField;

                    if (txtEditTenLoaiMau != null)
                        txtEditTenLoaiMau.Text = LoaiMauIn.TenMau;

                    if (chkEditStatus != null)
                        chkEditStatus.Checked = LoaiMauIn.TrangThaiSuDung.HasValue && LoaiMauIn.TrangThaiSuDung.Value;
                    if (hdfEditID != null)
                        hdfEditID.Value = LoaiMauIn.Id.ToString();

                    #region Add log value
                    CurrentLog.AddFirstValue(RightPageName.MI_Update, string.Format("Tên {0}", CurrentName),
                                             TblLoaiMauIn.Columns.TenMau,
                                             txtEditTenLoaiMau.Text);
                    CurrentLog.AddFirstValue(RightPageName.MI_Update, "Trạng thái sử dụng",
                                             TblLoaiMauIn.Columns.TrangThaiSuDung,
                                             chkEditStatus.Text);
                    #endregion
                }
            }
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string orderClause = string.Format("{0} desc", TblLoaiMauIn.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                string column = "*";

                if (!string.IsNullOrEmpty(txtSearchTenLoaiMau.Text.Trim()))
                    filterClause += string.Format(" {0} like N'%{1}%' AND ",
                                                  TblLoaiMauIn.Columns.TenMau,
                                                  txtSearchTenLoaiMau.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(cbbSearchStatus.SelectedValue))
                    filterClause += string.Format(" {0} = {1} AND ",
                                                  TblLoaiMauIn.Columns.TrangThaiSuDung,
                                                  cbbSearchStatus.SelectedValue);

                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause +=  string.Format(" {0} = N'{1}' AND ",
                                                  TblLoaiMauIn.Columns.CType,
                                                  CurrentType);
                filterClause = (filterClause1 + filterClause.Trim()).Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);

                int total = 0;
                List<TblLoaiMauIn> lst = CategoryManager.SearchLoaiMauIn(rowIndex, pageSize, column,
                                                                filterClause, orderClause, out total);
                if (grvData.IsInsert)
                    lst.Insert(0, new TblLoaiMauIn());
                grv.DataSource = lst;
                List<TblLoaiMauIn> lstEmplty = new List<TblLoaiMauIn>();
                lstEmplty.Add(new TblLoaiMauIn());
                grvData.DataSourceEmpty = lstEmplty;
                grv.VirtualRecordCount = total;
            }
        }

        protected string GetImage(object id)
        {
            string result = string.Empty;
            List<TblImage> images = CategoryManager.GetLoaiMauImage(id);
            if (images.Count > 0)
            {
                foreach (TblImage img in images)
                    result += string.Format("<div class='e-preview small'><div class='e-preview-content'><a href='{0}' data-gallery='{1}' rel='light'><img class='e-file-image' src='{0}' /></a></div></div>", img.ImagePath, id);
            }
            return result;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchTenLoaiMau.Text = string.Empty;
            cbbSearchStatus.SelectedIndex = 0;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_LoaiMauIn")
                        {
                            TblLoaiMauIn LoaiMauIn = CurrentMessageBoxResult.Value as TblLoaiMauIn;
                            if (LoaiMauIn != null)
                            {
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.MI_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.MI_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblLoaiMauIn.Columns.TenMau,
                                                             LoaiMauIn.TenMau);
                                    resultDelete = CategoryManager.DeleteLoaiMauIn(LoaiMauIn.Id);
                                }
                                catch
                                {
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              LoaiMauIn.TenMau, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.MI_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa {0} thất bại", CurrentName),
                                                string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                               LoaiMauIn.TenMau, CurrentName),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblLoaiMauIn LoaiMauIn = CategoryManager.GetLoaiMauInByID(btn.CommandArgument);
                    if (LoaiMauIn != null)
                    {
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = LoaiMauIn;
                        result.Submit = true;
                        result.CommandName = "delete_LoaiMauIn";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     LoaiMauIn.TenMau, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        private void ReloadImage()
        {
            if (!string.IsNullOrEmpty(hiddenEditID.Value))
            {
                TblLoaiMauIn mauIn = CategoryManager.GetLoaiMauInByID(hiddenEditID.Value);
                if (mauIn != null)
                {
                    List<TblImage> images = CategoryManager.GetLoaiMauImage(hiddenEditID.Value);
                    repData.DataSource = images;
                    repData.DataBind();
                }
            }
        }

        protected void btnReload_Click(object sender, EventArgs e)
        {
            ReloadImage();
            grvData.Rebind();
        }

        protected void btnConfigImage_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    btnUpload.Visible = true;
                    hiddenEditID.Value = cmd;
                    ReloadImage();
                    dialogImages.ShowDialog();
                }
            }
        }

        protected void repData_ItemCreated(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem ||
                e.Item.ItemType == ListItemType.Item)
            {
                if (e.Item.DataItem != null && e.Item.DataItem is TblImage)
                {
                    TblImage image = e.Item.DataItem as TblImage;
                    Literal ltrImage = e.Item.FindControl("ltrImage") as Literal;
                    if (ltrImage != null) ltrImage.Text = string.Format(@"<a href='{0}' data-gallery='{1}' rel='light'><img class='e-file-image' src='{0}' /></a>", image.ImagePath, hiddenEditID.Value);
                    Literal ltrRemove = e.Item.FindControl("ltrRemove") as Literal;
                    if (ltrRemove != null) ltrRemove.Text = string.Format(@"<a class='btn btn-danger btn-white' onclick='RemoveImage({0});'><i class='fa fa-trash'></i></a>", image.LoaiMauIMGID.Value.ToString());
                }

            }
        }
    }
}