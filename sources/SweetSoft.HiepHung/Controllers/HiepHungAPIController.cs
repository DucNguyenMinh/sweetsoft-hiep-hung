﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.HiepHung.Manager;

namespace SweetSoft.HiepHung.Controllers
{
    public class HiepHungAPIController : ApiController
    {
        [HttpGet]
        public string Hello()
        {
            return "Xin chào, đây là API của công ty Hiệp Hưng!";
        }

        [HttpPost]
        public object GetTokenKey(APIUser User)
        {
            APIResponse response = new APIResponse();
            response.APICode = "GetTokenKey";
            if (User != null)
            {
                TblCommonUser userC = UserManager.GetUserByUserName(User.Username);
                if (userC != null)
                {
                    string pass = SecurityHelper.Decrypt128(userC.Password);
                    if (pass == User.Password)
                    {
                        response.Successfully = true;
                        APIRequest request = new APIRequest();
                        request.Username = User.Username;
                        request.Date = DateTime.Now;
                        response.Request = request;
                        response.Message = "Đăng nhập thành công";
                    }
                    else response.Message = "Mật khẩu không chính xác";
                }
                else response.Message = "Tài khoản không tồn tại";
            }
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object GetMachineList(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                response.APICode = "GetMachineList";
                List<TblMayMoc> mayMocCol = MayMocManager.GetMayMocALL(true);
                if (mayMocCol.Count > 0)
                {
                    response.Successfully = true;

                    foreach (TblMayMoc may in mayMocCol)
                        response.DataCollection.Add(new APIData(may.Id, may.TenMay));
                }
                else response.Message = "Không tìm thấy danh sách máy";
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object AcceptMachine(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            response.MachineID = param.MachineID;
            if (response.Request != null)
            {
                if (param.MachineID != null)
                {
                    TblMayMoc mayMoc = MayMocManager.GetMayMocByID(response.MachineID);
                    if (mayMoc != null)
                    {
                        response.APICode = "AcceptMachine";
                        response.Successfully = true;
                        response.Message = "Chọn máy thành công";
                    }
                    else response.Message = "Thông tin máy không tồn tại.";
                }
                else response.Message = "Thông tin máy không chính xác";
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object GetProductionCommand(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                response.APICode = "GetProductionCommand";
                if (param.TaskID == null || (param.TaskID != null && (param.TaskID.ToString() == "0" || string.IsNullOrEmpty(param.TaskID.ToString()))))
                {
                    List<TblCongViec> congViec = CategoryManager.GetCongViecALL(true);
                    if (congViec.Count > 0)
                    {
                        response.Successfully = true;

                        if (DateTime.Now.Second % 5 == 0)
                        {
                            foreach (TblCongViec cv in congViec)
                                response.DataCollection.Add(new APIData(cv.Id, cv.TenCongViec));
                        }
                        else
                        {
                            response.DataCollection.Add(new APIData(congViec[0].Id, congViec[0].TenCongViec));
                            //if(response.Request.APICode == "GetProductionCommand")
                            //    response.
                        }
                    }
                    else response.Message = "Không tìm thấy danh sách công việc";
                }
                else
                {
                    TblCongViec congViec = CategoryManager.GetCongViecByID(param.TaskID.ToString());
                    if (congViec != null)
                    {
                        response.Successfully = true;
                        response.Message = string.Format("Có 1 công việc");
                        response.DataCollection.Add(new APIData(congViec.Id, congViec.TenCongViec));
                    }
                    else response.Message = "Không tìm thấy công việc";
                }
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object ProgressCommand(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                response.APICode = "ProgressCommand";
                response.Successfully = true;
                response.Message = "Ghi nhận công việc thành công";
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object GetTaskCollection(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                response.APICode = "GetTaskCollection";

                List<TblCongViec> congViec = CategoryManager.GetCongViecALL(true);
                if (congViec.Count > 0)
                {
                    response.Successfully = true;

                    foreach (TblCongViec cv in congViec)
                        response.DataCollection.Add(new APIData(cv.Id, cv.TenCongViec));
                }
                else response.Message = "Không tìm thấy danh sách công việc";
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object JumbTask(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                if (param.TaskID != null)
                {
                    TblCongViec congViec = CategoryManager.GetCongViecByID(param.TaskID);
                    if (congViec != null)
                    {
                        response.Successfully = true;
                        response.APICode = "JumbTask";
                        response.Message = "Chuyển công việc thành công";
                    }
                    else response.Message = "Thông tin công việc không tồn tại";
                }
                else response.Message = "Không tìm thấy thông tin công việc";
            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }

        [HttpPost]
        public object GetPrintImage(APIParam param)
        {
            APIResponse response = new APIResponse();

            response.TokenKey = param.TokenKey;
            if (response.Request != null)
            {
                response.Successfully = true;
                response.APICode = "GetPrintImage";
                List<TblImage> col = CategoryManager.GetLoaiMauImage(null);
                for (int i = 0; i < col.Count; i++)
                {
                    if (i > 5) break;
                    TblImage image = col[i];
                    response.DataCollection.Add(new APIData((i + 1), string.Format("{0}{1}", CommonHelper.GetFullApplicationPath(), image.ImagePath.Replace(@"\", "/"))));
                }

            }
            else response.Message = "TokenKey không chính xác";
            return new { Key = response.TokenKey, Data = response.DataCollection, Message = response.Message, Status = response.Successfully };
        }
    }

    public class APIUser
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class APIData
    {
        public string Name { get; set; }
        public object ID { get; set; }
        public APIData(object id, string name) { this.ID = id; this.Name = name; }
    }

    public class APIParam
    {
        public string TokenKey { get; set; }
        public string Code { get; set; }
        public object TaskID { get; set; }
        public object MachineID { get; set; }
        public int SoLuongThanhPham { get; set; }
        public int SoLuongBanThanhPham { get; set; }
    }

    public class APIRequest
    {
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public string APICode { get; set; }
        public object MachineID { get; set; }
    }

    public class APIResponse
    {
        public bool Successfully { get; set; }
        public string Message { get; set; }
        public string TokenKey
        {
            get
            {
                APIRequest req2 = new APIRequest();
                req2.Username = Request.Username;
                req2.Date = Request.Date;
                req2.MachineID = Request.MachineID;
                req2.APICode = APICode;
                return SecurityHelper.Encrypt128(JsonConvert.SerializeObject(req2));
            }
            set
            {
                try
                {
                    if (value == "TokenKeyDemo")
                    {
                        Request = new APIRequest();
                        Request.Username = "demo";
                        Request.Date = DateTime.Now;
                        Request.APICode = APICode;
                    }
                    else
                    {
                        string json = SecurityHelper.Decrypt128(value);
                        Request = JsonConvert.DeserializeObject<APIRequest>(json);
                    }
                }
                catch (Exception ex)
                {
                    Request = null;
                }
            }
        }
        public string APICode { get; set; }
        public object MachineID { get; set; }
        public APIRequest Request { get; set; }
        public List<APIData> DataCollection { get; set; }
        public APIResponse()
        {
            Successfully = false;
            Message = "";
            DataCollection = new List<APIData>();
        }
    }
}