﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.HiepHung.Manager;
namespace SweetSoft.HiepHung.Controllers
{
    public class GetTokenKeyController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public APIResponse Post(APIUser User)
        {
            APIResponse response = new APIResponse();
            if (User != null)
            {
                TblCommonUser userC = UserManager.GetUserByUserName(User.Username);
                if (userC != null)
                {
                    string pass = SecurityHelper.Decrypt128(userC.Password);
                    if (pass == User.Password)
                    {
                        response.Successfully = true;
                        response.Message = SecurityHelper.Encrypt128(string.Format("{0}|{1}", User.Username, DateTime.Now.Ticks));
                    }
                    else response.Message = "Mật khẩu không chính xác";
                }
                else response.Message = "Tài khoản không tồn tại";
            }
            return response;
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }
    }
}