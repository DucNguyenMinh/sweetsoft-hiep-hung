﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
namespace SweetSoft.HiepHung
{
    public partial class DMMayMoc : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.Cat_May_Moc;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.Cat_May_Moc;
            }
        }
        #endregion

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.MM_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.MM_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.MM_Delete);

            return rs;
        }

        protected string GetTrangThaiSuDung(object value)
        {
            if (value != null)
            {
                if (value.ToString() == "DangSuDung")
                    return "<span class='label label-success'>Đang sử dụng</span>";
                else
                    return "<span class='label label-error'>Hư hỏng</span>";
            }
            return "<span class='label label-error'>Hư hỏng</span>";
        }

        protected string CurrentName
        {
            get
            {
                string rs = "Máy móc";
                return rs;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);

            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);

            txtSearchGhiChu.EnterSubmitClientID = txtSearchMaMay.EnterSubmitClientID = txtSearchTenMay.EnterSubmitClientID = btnSearchNow.ClientID;
            txtTenMay.EnterSubmitClientID = txtGhiChu.EnterSubmitClientID = txtMaMay.EnterSubmitClientID = btnSave.ClientID;

            txtTenMay.MaxTextLength = txtSearchTenMay.MaxTextLength = TblMayMoc.TenMayColumn.MaxLength;
            txtMaMay.MaxTextLength = txtSearchMaMay.MaxTextLength = TblMayMoc.MaSoMayColumn.MaxLength;
            txtGhiChu.MaxTextLength = txtSearchGhiChu.MaxTextLength = TblMayMoc.GhiChuColumn.MaxLength;

            btnAddNew.Visible = GetButtonRole("Insert");
            if (!IsPostBack)
            {


                grvData.CurrentSortExpression = TblMayMoc.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }
                LoadCongViec();
            }
           
            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        private void RefreshDetail()
        {
            hdfDetailUserID.Value = string.Empty;
            txtGhiChu.Text = txtMaMay.Text = txtTenMay.Text = string.Empty;
            dateNgayBaoTri.DateValue = dateNgayMua.DateValue = DateTime.MinValue;
            dialogDetail.UpdateContentDialog();
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            RefreshDetail();
            hdfDetailUserID.Value = string.Empty;
            txtTenMay.Focus();

            lblDialogDetailTitle.Text = "Thêm mới máy móc";
            dialogDetail.ShowDialog(true);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchGhiChu.Text = txtSearchMaMay.Text = txtSearchTenMay.Text = string.Empty;
            dateSearchNgayBaoTriToi.DateValue = dateSearchNgayBaoTriTu.DateValue = dateSearchNgayMuaToi.DateValue = dateSearchNgayMuaTu.DateValue = DateTime.MinValue;
            cbSearchCongViec.SelectedIndex = cbSearchStatus.SelectedIndex = 0;
            grvData.Rebind();
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" nv.{0} desc ", TblMayMoc.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" mm.*  ");

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchTenMay.Text.Trim()))
                    filterClause += string.Format(" mm.{0} like N'%{1}%' AND ",
                                                  TblMayMoc.Columns.TenMay,
                                                  txtSearchTenMay.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchMaMay.Text.Trim()))
                    filterClause += string.Format(" mm.{0} like N'%{1}%' AND ",
                                                  TblMayMoc.Columns.MaSoMay,
                                                  txtSearchMaMay.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchGhiChu.Text.Trim()))
                    filterClause += string.Format(" mm.{0} like N'%{1}%' AND ",
                                                  TblMayMoc.Columns.GhiChu,
                                                  txtSearchGhiChu.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));

                if (cbSearchStatus.SelectedIndex > 0)
                    filterClause += string.Format(" mm.{0} = '{1}' AND ",
                                                  TblMayMoc.Columns.TinhTrangSuDung,
                                                  cbSearchStatus.SelectedValue);

                if (dateSearchNgayBaoTriTu.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" mm.{0} >= '{1}' AND ",
                                                  TblMayMoc.Columns.NgayBaoTri,
                                                  dateSearchNgayBaoTriTu.DateValue.ToString("yyyy-MM-dd 00:00:00"));
                if (dateSearchNgayBaoTriToi.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" mm.{0} <= '{1}' AND ",
                                                  TblMayMoc.Columns.NgayBaoTri,
                                                  dateSearchNgayBaoTriToi.DateValue.ToString("yyyy-MM-dd 23:59:59"));

                if (dateSearchNgayMuaTu.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" mm.{0} >= '{1}' AND ",
                                                  TblMayMoc.Columns.NgayMua,
                                                  dateSearchNgayMuaTu.DateValue.ToString("yyyy-MM-dd 00:00:00"));
                if (dateSearchNgayMuaToi.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" mm.{0} <= '{1}' AND ",
                                                  TblMayMoc.Columns.NgayMua,
                                                  dateSearchNgayMuaToi.DateValue.ToString("yyyy-MM-dd 23:59:59"));

                if (cbSearchCongViec.HasValue)
                    filterClause += string.Format(" mm.{0} in (select {1} from {2} where {3} = {4}) AND ",
                                                    TblMayMoc.Columns.Id,TblMayMocCongViec.Columns.IDMayMoc, 
                                                    TblMayMocCongViec.Schema.TableName,TblMayMocCongViec.Columns.IDCongViec,
                                                    cbSearchCongViec.SelectedValue);
                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = "";

                int total = 0;
                List<TblMayMoc> lst = MayMocManager.SearchMayMoc(rowIndex, pageSize, string.Format(" {0} as mm ", TblMayMoc.Schema.TableName), column, tableJoin,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblMayMoc> lstEmpty = new List<TblMayMoc>();
                lstEmpty.Add(new TblMayMoc());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected string GetListCongViec(object mayMocId)
        {
            string rs = string.Empty;
            List<TblCongViec> congViec = MayMocManager.GetCongViecByMayMocID(mayMocId);
            if (congViec.Count > 0)
            {
                rs = "<ul>";
                foreach (TblCongViec cv in congViec)
                {
                    rs += string.Format("<li>{0}</li>", cv.TenCongViec);
                }
                rs += @"</ul>";
            }
            return rs;
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_MayMoc")
                        {
                            TblMayMoc MayMoc = CurrentMessageBoxResult.Value as TblMayMoc;
                            if (MayMoc != null)
                            {

                                string result = "";
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.MM_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.MM_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblMayMoc.Columns.TenMay,
                                                             MayMoc.TenMay);
                                    MayMocManager.DeleteMayMoc(MayMoc.Id);
                                    resultDelete = true;

                                }
                                catch
                                {
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              MayMoc.TenMay, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.MM_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa {0} thất bại. {2}", CurrentName),
                                                string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                               MayMoc.TenMay, CurrentName, result),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblMayMoc MayMoc = MayMocManager.GetMayMocByID(btn.CommandArgument);
                    if (MayMoc != null)
                    {

                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = MayMoc;
                        result.Submit = true;
                        result.CommandName = "delete_MayMoc";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     MayMoc.TenMay, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }


        protected void btnEdit_Click(object sender, EventArgs e)
        {
            RefreshDetail();

            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblMayMoc mayMoc = MayMocManager.GetMayMocByID(cmd);
                    if (mayMoc != null)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.MM_Update,
                                                     string.Format("Cập nhật máy móc <b>{0}</b>", mayMoc.TenMay));


                        txtTenMay.Text = mayMoc.TenMay;
                        txtMaMay.Text = mayMoc.MaSoMay;
                        txtGhiChu.Text = mayMoc.GhiChu;
                        if (mayMoc.NgayBaoTri.HasValue && mayMoc.NgayBaoTri.Value != DateTime.MinValue) dateNgayBaoTri.DateValue = mayMoc.NgayBaoTri.Value;
                        if (mayMoc.NgayMua.HasValue && mayMoc.NgayMua.Value != DateTime.MinValue) dateNgayMua.DateValue = mayMoc.NgayMua.Value;
                        if (!string.IsNullOrEmpty(mayMoc.TinhTrangSuDung))
                        {
                            ListItem item = cbTrangThaiSuDung.Items.FindByValue(mayMoc.TinhTrangSuDung);
                            if (item != null) cbTrangThaiSuDung.SelectedValue = mayMoc.TinhTrangSuDung;
                        }

                        #region Log
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.MM_Update,
                                                     string.Format("Cập nhật máy móc <b>{0}</b>", mayMoc.TenMay));
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tên máy móc",
                                          TblMayMoc.Columns.TenMay,
                                          txtTenMay.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Mã máy móc",
                                         TblMayMoc.Columns.MaSoMay,
                                         txtMaMay.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Ngày bảo trì",
                                         TblMayMoc.Columns.NgayBaoTri,
                                         dateNgayBaoTri.DateValue != DateTime.MinValue ? dateNgayBaoTri.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Ngày mua",
                                        TblMayMoc.Columns.NgayMua,
                                        dateNgayMua.DateValue != DateTime.MinValue ? dateNgayMua.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Ghi chú",
                                        TblMayMoc.Columns.GhiChu,
                                        txtGhiChu.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Update, "Tình trạng sử dụng",
                                        TblMayMoc.Columns.TinhTrangSuDung,
                                        cbTrangThaiSuDung.DisplayText);
                        #endregion
                        hdfDetailUserID.Value = mayMoc.Id.ToString();
                        lblDialogDetailTitle.Text = "Cập nhật thông tin máy móc";
                        dialogDetail.ShowDialog();
                    }
                }
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            #region Validation
            if (string.IsNullOrEmpty(txtTenMay.Text))
            {
                AddValidPromt(txtTenMay.ClientID, "Nhập tên máy ");
                txtTenMay.Focus();
            }

            #endregion

            if (PageIsValid)
            {

                if (!string.IsNullOrEmpty(hdfDetailUserID.Value))
                {
                    #region Update
                    TblMayMoc mayMoc = MayMocManager.GetMayMocByID(hdfDetailUserID.Value);
                    if (mayMoc != null)
                    {
                        mayMoc.TenMay = txtTenMay.Text;
                        mayMoc.NgayBaoTri = null;
                        if (dateNgayBaoTri.DateValue != DateTime.MinValue) mayMoc.NgayBaoTri = dateNgayBaoTri.DateValue;
                        mayMoc.NgayMua = null;
                        if (dateNgayMua.DateValue != DateTime.MinValue) mayMoc.NgayMua = dateNgayMua.DateValue;
                        mayMoc.TinhTrangSuDung = cbTrangThaiSuDung.SelectedValue;
                        mayMoc.MaSoMay = txtMaMay.Text;
                        mayMoc.GhiChu = txtGhiChu.Text;
                        mayMoc.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        mayMoc.NgayCapNhat = DateTime.Now;
                        mayMoc = MayMocManager.UpdateMayMoc(mayMoc);
                        #region Log
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                          TblMayMoc.Columns.TenMay,
                                          txtTenMay.Text);
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                         TblMayMoc.Columns.MaSoMay,
                                         txtMaMay.Text);
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                         TblMayMoc.Columns.NgayBaoTri,
                                         dateNgayBaoTri.DateValue != DateTime.MinValue ? dateNgayBaoTri.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                        TblMayMoc.Columns.NgayMua,
                                        dateNgayMua.DateValue != DateTime.MinValue ? dateNgayMua.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                        TblMayMoc.Columns.GhiChu,
                                        txtGhiChu.Text);
                        CurrentLog.AddLastValue(RightPageName.MM_Update,
                                        TblMayMoc.Columns.TinhTrangSuDung,
                                        cbTrangThaiSuDung.DisplayText);
                        CurrentLog.SaveLog(RightPageName.MM_Update);
                        #endregion
                        ShowNotify("Thông báo", string.Format("Cập nhật thông tin máy móc <b>{0}</b> thành công", mayMoc.TenMay), NotifyType.Success);
                        dialogDetail.CloseDialog();
                        grvData.Rebind();
                    }
                    #endregion
                }
                else
                {
                    #region Insert
                    TblMayMoc mayMoc = new TblMayMoc();
                    if (mayMoc != null)
                    {
                        mayMoc.TenMay = txtTenMay.Text;
                        mayMoc.NgayBaoTri = null;
                        if (dateNgayBaoTri.DateValue != DateTime.MinValue) mayMoc.NgayBaoTri = dateNgayBaoTri.DateValue;
                        mayMoc.NgayMua = null;
                        if (dateNgayMua.DateValue != DateTime.MinValue) mayMoc.NgayMua = dateNgayMua.DateValue;
                        mayMoc.TinhTrangSuDung = cbTrangThaiSuDung.SelectedValue;
                        mayMoc.MaSoMay = txtMaMay.Text;
                        mayMoc.GhiChu = txtGhiChu.Text;
                        mayMoc.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        mayMoc.NgayCapNhat = DateTime.Now;
                        mayMoc = MayMocManager.InsertMayMoc(mayMoc);
                        #region Log
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.MM_Insert,
                                                     string.Format("Thêm mới máy móc <b>{0}</b>", mayMoc.TenMay));
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Tên máy móc",
                                          TblMayMoc.Columns.TenMay,
                                          txtTenMay.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Mã máy móc",
                                         TblMayMoc.Columns.MaSoMay,
                                         txtMaMay.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Ngày bảo trì",
                                         TblMayMoc.Columns.NgayBaoTri,
                                         dateNgayBaoTri.DateValue != DateTime.MinValue ? dateNgayBaoTri.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Ngày mua",
                                        TblMayMoc.Columns.NgayMua,
                                        dateNgayMua.DateValue != DateTime.MinValue ? dateNgayMua.DateValue.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Ghi chú",
                                        TblMayMoc.Columns.GhiChu,
                                        txtGhiChu.Text);
                        CurrentLog.AddFirstValue(RightPageName.MM_Insert, "Tình trạng sử dụng",
                                        TblMayMoc.Columns.TinhTrangSuDung,
                                        cbTrangThaiSuDung.DisplayText);
                        CurrentLog.SaveLog(RightPageName.MM_Insert);
                        #endregion
                        ShowNotify("Thông báo", string.Format("Thêm mới máy móc <b>{0}</b> thành công", mayMoc.TenMay), NotifyType.Success);
                        dialogDetail.CloseDialog();
                        grvData.Rebind();
                    }
                    #endregion
                }
            }
            else ShowValidPromt();
        }

        protected void btnConfig_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblMayMoc mayMoc = MayMocManager.GetMayMocByID(cmd);
                    if (mayMoc != null)
                    {
                        mayMocControl.ShowData(mayMoc.Id, mayMoc.TenMay);
                        dialogMayMoc.ShowDialog();
                    }
                }
            }
        }

        private void LoadCongViec()
        {
            List<TblCongViec> congViec = CategoryManager.GetCongViecALL(true);
            TblCongViec all = new TblCongViec();
            all.Id = -1;
            all.TenCongViec = "Tất cả";
            congViec.Insert(0, all);
            cbSearchCongViec.DataSource = congViec;
            cbSearchCongViec.DataValueField = TblCongViec.Columns.Id;
            cbSearchCongViec.DataTextField = TblCongViec.Columns.TenCongViec;
            cbSearchCongViec.DataBind();
        }

        protected void mayMocControl_changed(object sender, EventArgs e)
        {
            grvData.Rebind();
        }
    }
}