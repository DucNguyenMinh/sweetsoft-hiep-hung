﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.HiepHung
{
    public partial class Demo2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnExample_Click(object sender, EventArgs e)
        {
            DateTime dt = date.DateValue;
            ltrDemo.Text += string.Format("dt: {0} <br /><br /><br />", dt.ToString());
            ltrDemo.Text += string.Format("Result: {0} <br /><br /><br />", GetCTime(dt));
        }

        public double GetDTime(string yourTimeZoneId)
        {
            try
            {
                var yourTimeZone = TimeZoneInfo.FindSystemTimeZoneById(yourTimeZoneId);
                ltrDemo.Text += string.Format("yourTimeZone: {0} <br /><br /><br />", yourTimeZone.ToString());
                var serverTimeZone = TimeZoneInfo.Local;
                var now = DateTimeOffset.UtcNow;
                TimeSpan yourOffset = yourTimeZone.GetUtcOffset(now);
                ltrDemo.Text += string.Format("yourOffset: {0} <br /><br /><br />", yourOffset.ToString());
                TimeSpan serverOffset = serverTimeZone.GetUtcOffset(now);
                ltrDemo.Text += string.Format("serverOffset: {0} <br /><br /><br />", serverOffset.ToString());
                TimeSpan different = yourOffset - serverOffset;
                ltrDemo.Text += string.Format("different: {0} <br /><br /><br />", different.TotalHours);
                return different.TotalHours;
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }
        private DateTime GetCTime(DateTime yourDateTime)
        {
            try
            {
                ltrDemo.Text += string.Format("yourDateTime Going: {0} <br /><br /><br />", yourDateTime.ToString());
                if (string.IsNullOrEmpty(yourDateTime.ToString()) || yourDateTime == DateTime.MinValue)
                    return yourDateTime.ToLocalTime();
                String timeZoneId = "SE Asia Standard Time";
                double differentTime = GetDTime(timeZoneId);
                ltrDemo.Text += string.Format("differentTime Going: {0} <br /><br /><br />", differentTime.ToString());
                ltrDemo.Text += string.Format("TimeZoneInfo: {0} <br /><br /><br />", TimeZoneInfo.Local.Id);
                if (TimeZoneInfo.Local.Id != "SE Asia Standard Time")
                {

                    if (yourDateTime.Month <= 3)
                    {
                        if (yourDateTime.Month == 3)
                        {
                            return yourDateTime.AddDays(1).ToLocalTime();
                        }
                        else
                            return yourDateTime.AddDays(1).AddHours(differentTime).ToLocalTime();
                    }
                    else
                        return yourDateTime.AddHours(differentTime).ToLocalTime();
                }
                else
                {
                    ltrDemo.Text += string.Format("AddHours: {0} <br /><br /><br />", yourDateTime.AddHours(differentTime).ToLocalTime().ToString());
                    return yourDateTime.AddHours(differentTime).ToLocalTime();
                }
            }
            catch
            {
                return yourDateTime.ToLocalTime();
            }
            ltrDemo.Text += string.Format("yourDateTime.ToLocalTime(): {0} <br /><br /><br />", yourDateTime.ToLocalTime().ToString());
            return yourDateTime.ToLocalTime();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            DateTime dt = DateTime.ParseExact(txtTest.Text, "dd/MM/yyyy",CultureInfo.CurrentCulture);
            //ltrDemo.Text += string.Format("<br /><br /><br />dt: {0} <br /><br /><br />", dt.ToString());
            //DateTime on = GetCTime(dt);
            //ltrDemo.Text += string.Format("Result: {0} <br /><br /><br />", on.ToString());

         //   DateTime dt = DateTime.Parse(txtTest.Text);
            ltrDemo.Text += string.Format("<br /><br /><br />dt: {0} <br /><br /><br />", dt.ToString());
           // DateTime on = GetCTime(dt);
            ltrDemo.Text += string.Format("Result2: {0} <br /><br /><br />", dt.ToString());
        }
    }
}