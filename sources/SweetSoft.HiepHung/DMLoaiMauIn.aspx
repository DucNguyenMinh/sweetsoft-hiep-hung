﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="DMLoaiMauIn.aspx.cs" Inherits="SweetSoft.HiepHung.DMLoaiMauIn" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <link href="/css/FileUpload.css" rel="stylesheet" />
    <link href="/css/jquery.light.css" rel="stylesheet" />

    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
        <li>
            <a href="#">Quản lý danh mục</a>
        </li>
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý thông tin
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true" OnRowCancelingEdit="grvData_RowCancelingEdit" OnRowUpdating="grvData_RowUpdating" OnRowCreated="grvData_RowCreated"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tên loại mẫu in" AccessibleHeaderText="Tên loại mẫu in" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="TenLoaiMau">
                                    <ItemTemplate>
                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" runat="server" Text=' <%# Eval("TenMau")  %>' CommandName="Edit" Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("TenMau")  %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:HiddenField ID="hdfEditID" runat="server" />
                                        <SweetSoft:ExtraTextBox ID="txtEditTenLoaiMau" MaxTextLength='<%# SweetSoft.HiepHung.DataAccess.TblLoaiMauIn.TenMauColumn.MaxLength %>' EnterSubmitClientID='ctl00_ContentPlaceHolder1_grvData_ctl02_btnSubmit' Required="true" CssClass="textbox-edit-width" PlaceHolder="Nhập tên loại mẫu in" ValidRequiredMessage="Nhập tên loại mẫu in" runat="server">
                                        </SweetSoft:ExtraTextBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Hình mẫu" AccessibleHeaderText="Tên viết tắt" HeaderStyle-Width="400px" ItemStyle-Width="200px" ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <%# GetImage(Eval("ID")) %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="TrangThaiSuDung">
                                    <ItemTemplate>
                                        <%# GetStatusStyle(Eval("TrangThaiSuDung")) %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <SweetSoft:ExtraCheckBox ID="chkEditStatus" runat="server" CheckboxType="Switch7" DataOn="Bật" DataOff="Tắt" Checked="true">
                                        </SweetSoft:ExtraCheckBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;", dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" CommandName="Edit">
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnConfigImage" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Picture" ToolTip="Quản lý hình ảnh" OnClick="btnConfigImage_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSubmit" runat="server" CssClass="btn-xs" EStyle="Success" EnableSmallSize="true" EIcon="A_Submit" ToolTip="Cập nhật" CommandName="Update">
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy" CommandName="Cancel">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog ID="dialogImages" runat="server" RestrictionZoneID="html" Size="Large" Height="300px" HeadButtons="Close,Maximize," CloseText="Đóng">
        <HeaderTemplate>
            <SweetSoft:ExtraButton ID="btnAddImage" Visible="false" runat="server" EStyle="Warning" Transparent="false" Text="Thêm hình ảnh" EIcon="A_Add_Square" OnClientClick="$('#fileUpload').click();return false;"></SweetSoft:ExtraButton>
            Quản lý hình ảnh 
        </HeaderTemplate>
        <ContentTemplate>
            <asp:HiddenField ID="hiddenEditID" runat="server" />
            <div class="e-upload-form" runat="server" id="div_form">
                <asp:Repeater ID="repData" runat="server" OnItemCreated="repData_ItemCreated">
                    <ItemTemplate>
                        <div class="e-preview new" id="pre_ctl00_ContentPlaceHolder1_upload_fileUpload_1">
                            <div class="e-preview-content">
                                <asp:Literal ID="ltrImage" runat="server"></asp:Literal>
                            </div>
                            <div class="e-preview-footer">
                                <asp:Literal ID="ltrRemove" runat="server"></asp:Literal>
                                <div class="footer-action-left"></div>
                                <div class="footer-action-right">
                                    <%--<a class="btn btn-danger  btn-white" onclick="Remove("><i class="fa fa-trash"></i></a>--%>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div style="clear: both"></div>
                    </FooterTemplate>
                </asp:Repeater>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <input type="file" id="fileUpload" multiple="multiple" accept=".jpg,.jpeg,.png" style="display: none;" />
            <asp:Button ID="btnReload"  runat="server" Style="display: none" OnClick="btnReload_Click" />
            <SweetSoft:ExtraButton ID="btnUpload" Visible="false" runat="server" EStyle="Warning_Bold" Text="Thêm hình ảnh" EIcon="A_Add" OnClientClick="$('#fileUpload').click();return false;"></SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
    <SweetSoft:ExtraDialog ID="dialogSearch" runat="server" RestrictionZoneID="html" Size="Small" HeadButtons="Close" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Literal ID="ltrDialogSearchTitle" runat="server" Text="Tìm kiếm ">
            </asp:Literal>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">
                            <asp:Literal ID="ltrSearchTenLoaiMau" runat="server"></asp:Literal>
                        </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenLoaiMau" runat="server">  </SweetSoft:ExtraTextBox>
                    </div>

                    <div class="form-group">
                        <label class="extra-label">Trạng thái  </label>
                        <SweetSoft:ExtraComboBox ID="cbbSearchStatus" EnableLiveSearch="true" ComboStyle="Dropdown_White" runat="server">
                            <asp:ListItem Value="" Text="Tất cả">  </asp:ListItem>
                            <asp:ListItem Value="1" Text="Kích hoạt">  </asp:ListItem>
                            <asp:ListItem Value="0" Text="Không kích hoạt"> </asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
    <script src="/scripts/jquery.light.js"></script>
    <script type="text/javascript">

        $(function () {
            InnitUpload();
            addRequestHanlde(InnitUpload);
        });
        function InnitUpload() {
            $('a[rel=light]').light();
            $("#fileUpload").on("change", function (e) {
                var data = new FormData();
                var files = e.target.files;
              
                var id = $("input[type='hidden'][id$='hiddenEditID']").val();

                // if (files.FileList > 0)
                {
                    data.append("UploadedExcelFile", files[0]);
                    data.append("UploadType", "MauIn");
                    for (var i = 0; i < files.length; i++) {
                        data.append("file_" + i, files[i]);
                    }
                    data.append("MauInID", id);
                }
           
                triggerUpload = true;
                // Make Ajax request with the contentType = false, and procesDate = false
                var ajaxRequest = $.ajax({
                    type: "POST",
                    url: "/UploadHandler.ashx",
                    contentType: false,
                    processData: false,
                    data: data
                });

                ajaxRequest.done(function (xhr, textStatus) {
                    triggerUpload = false;
                   
                    if (textStatus === "success") {
                        var btnReload = $("input[id$='btnReload']");
                        if (btnReload.length > 0) btnReload.click();
                    }
                });
            });
        }

        function RemoveImage(id) {
            var data = { "id": id, "type": "MauIn" };
            var result = AjaxPost(data, "/Default.aspx/RemoveImage");

            var btnReload = $("input[id$='btnReload']");
            if (btnReload.length > 0) btnReload.click();
        }

        
    </script>
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
