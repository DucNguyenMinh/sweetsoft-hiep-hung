﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPages/MainMaster.Master" AutoEventWireup="true" CodeBehind="HTNhanVien.aspx.cs" Inherits="SweetSoft.HiepHung.HTNhanVien" %>

<%@ Register Src="~/Controls/Common_Paging.ascx" TagName="Paging" TagPrefix="SweetSoft" %>
<%@ Register Src="~/Controls/QLTTLT_UCUserInRole.ascx" TagName="UCUserInRole" TagPrefix="SweetSoft1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style type="text/css">
        .textbox-edit-width
        {
            width: 100% !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="breadCrumbsPlaceHolder" runat="server">
    <ul class="breadcrumb red">
        <li>
            <i class="ace-icon fa fa-home home-icon"></i>
            <a href="/">Trang chính</a>
        </li>
        <li>
            <a href="#">Quản lý hệ thống</a>
        </li>
        <li class="active">
            <asp:Literal ID="breadTitle" runat="server"></asp:Literal>
        </li>
    </ul>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="TopPlaceHolder" runat="server">
    <h1>
        <asp:Literal ID="topTitle1" runat="server"></asp:Literal>
        <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Quản lý danh sách
            <asp:Literal ID="topTitle2" runat="server"></asp:Literal>
        </small>
    </h1>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:UpdatePanel ID="updateContent" runat="server">
        <ContentTemplate>
            <div class="widget-box">
                <div class="widget-header">
                    <h5 class="widget-title bigger lighter">Danh sách
                        <asp:Literal ID="headerTitle" runat="server"></asp:Literal></h5>
                    <div class=" widget-toolbar filter no-border">
                        <SweetSoft:ExtraComboBox ID="cbbFilterColumns" runat="server" DisplayItemCount="7" CssClass="filter-columns-right grid-columns-list" SelectionMode="Multiple"
                            EnableDropUp="false" ItemSelectedChangeTextCount="1" Width="170px">
                        </SweetSoft:ExtraComboBox>
                        <a class="white" data-action="fullscreen" href="#"><i class="ace-icon fa fa-expand"></i></a>
                    </div>
                    <div class="widget-toolbar no-border">
                        <SweetSoft:ExtraButton ID="btnAddNew" Transparent="true" runat="server" EIcon="A_Add" Text="Thêm mới" OnClick="btnAddNew_Click">
                        </SweetSoft:ExtraButton>
                    </div>
                </div>
                <div class="widget-body">
                    <div class="widget-main ">
                        <SweetSoft:ExtraGrid Width="100%" ID="grvData" AutoResponsive="true" EnableFixHeader="true" AutoGenerateColumns="false"
                            runat="server" AllowPaging="true" ShowHeaderWhenEmpty="true" AllowSorting="true"
                            OnNeedDataSource="grvData_NeedDataSource" PagingControlName="paging" ColumnVisibleDefault="0,1,2,6,7,8,9,10,11">
                            <EmptyDataTemplate>
                                Hệ thống không trích xuất được dữ liệu, vui lòng lựa chọn tiêu chí tìm kiếm khác!
                            </EmptyDataTemplate>
                            <Columns>
                                <asp:TemplateField HeaderText="STT" AccessibleHeaderText="STT2" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="5%" ItemStyle-Width="5%">
                                    <ItemTemplate>
                                        <%# GetIndex(grvData, Container.DataItemIndex)  %>.
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tên nhân viên" AccessibleHeaderText="Tên nhân viên" HeaderStyle-Width="200px" ItemStyle-Width="200px" SortExpression="nv.FirstName">
                                    <ItemTemplate>

                                        <SweetSoft:ExtraButton Transparent="true" CssClass="edit-link" ID="lnkEdit" runat="server" Text=' <%# Eval("HoVaTen")  %>' OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>' Visible='<%# GetButtonRole("Update") %>'></SweetSoft:ExtraButton>
                                        <asp:Label ID="lbEdit" runat="server" Visible='<%# !GetButtonRole("Update") %>' Text=' <%# Eval("HoVaTen")  %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Giới tính" AccessibleHeaderText="Giới tính" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.GioiTinh">
                                    <ItemTemplate>
                                        <%# Eval("GioiTinh")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Địa chỉ" AccessibleHeaderText="Địa chỉ" HeaderStyle-Width="300px" ItemStyle-Width="300px" SortExpression="nv.DiaChi">
                                    <ItemTemplate>
                                        <%# Eval("DiaChi")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="CMND" AccessibleHeaderText="CMND" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.CMND">
                                    <ItemTemplate>
                                        <%# Eval("CMND")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Điện thoại" AccessibleHeaderText="Điện thoại" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.CMND">
                                    <ItemTemplate>
                                        <%# Eval("SoDienThoai")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ngày sinh" AccessibleHeaderText="Ngày sinh" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.NgaySinh">
                                    <ItemTemplate>
                                        <%# Eval("NgaySinhToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ngày vào công ty" AccessibleHeaderText="Ngày vào công ty" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.NgayVaoCongTy">
                                    <ItemTemplate>
                                        <%# Eval("NgayVaoCongTyToString")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Phòng ban / kho" AccessibleHeaderText="Phòng ban / kho" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="nv.NgayVaoCongTy">
                                    <ItemTemplate>
                                        <%# Eval("TenPhongBan")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Tài khoản hệ thống" AccessibleHeaderText="Tài khoản hệ thống" HeaderStyle-Width="100px" ItemStyle-Width="100px" SortExpression="us.UserName">
                                    <ItemTemplate>
                                        <%# Eval("UserName")  %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Trạng thái" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150px" ItemStyle-Width="150px" SortExpression="TrangThaiSuDung">
                                    <ItemTemplate>
                                        <%# GetStatusStyle(Eval("TrangThaiSuDung")) %>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <SweetSoft:ExtraCheckBox ID="chkEditStatus" runat="server" CheckboxType="Switch7" DataOn="Bật" DataOff="Tắt" Checked="true">
                                        </SweetSoft:ExtraCheckBox>
                                    </EditItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Thao tác" AccessibleHeaderText="" HeaderStyle-Width="150px" ItemStyle-Width="150px" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" SortExpression="">
                                    <HeaderTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnSearch" runat="server" CssClass="btn-xs" EStyle="Warning" EnableSmallSize="true" EIcon="A_Search" ToolTip="Tìm kiếm" OnClientClick='<%# string.Format("{0}return false;",    dialogSearch.GetScriptOpen()) %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnCancel" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Close" ToolTip="Hủy tìm kiếm" CommandName="Cancel_Search" OnClick="btnCancel_Click" Visible='<%# grvData.BindingWithFiltering %>'>
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <div class="hidden-sm hidden-xs btn-group">
                                            <SweetSoft:ExtraButton ID="btnEdit" runat="server" CssClass="btn-xs" EStyle="Primary" EnableSmallSize="true" EIcon="A_Edit" ToolTip="Chỉnh sửa" OnClick="btnEdit_Click" CommandArgument='<%# Eval("ID") %>'>
                                            </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnAuthorization" runat="server" CssClass="btn-xs" EStyle="Violet" EnableSmallSize="true" EIcon="A_Config" ToolTip="Phân quyền chức năng" CommandArgument='<%# Eval("ID") %>' OnClick="btnAuthorization_Click">  </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnResetPass" runat="server" CssClass="btn-xs" EStyle="Success" EnableSmallSize="true" EIcon="A_Refresh" ToolTip="Cấp lại mật khẩu" OnClick="btnResetPass_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm"> </SweetSoft:ExtraButton>
                                            <SweetSoft:ExtraButton ID="btnDelete" runat="server" CssClass="btn-xs" EStyle="Danger" EnableSmallSize="true" EIcon="A_Delete" ToolTip="Xóa" OnClick="btnDelete_Click" CommandArgument='<%# Eval("ID") %>' CommandName="confirm">
                                            </SweetSoft:ExtraButton>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerTemplate>
                                <SweetSoft:Paging ID="paging" runat="server" />
                            </PagerTemplate>
                        </SweetSoft:ExtraGrid>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="DialogPlaceHolder" runat="server">
    <SweetSoft:ExtraDialog ID="dialogRole" runat="server" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Large" CloseText="Đóng">
        <HeaderTemplate>
            Phân vai trò cho người dùng
            <asp:Label runat="server" ID="lblRoleTitle"> </asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <SweetSoft1:UCUserInRole runat="server" ID="ucUserInRole" />
        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnRoleForUser" runat="server" EIcon="A_Save" EStyle="Success_Bold" EnableSnipButton="true" Text="Lưu" OnClick="btnRoleForUser_Click"> </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
    <SweetSoft:ExtraDialog ID="dialogSearch" runat="server" RestrictionZoneID="html" EnableDraggable="false" Size="Normal" HeadButtons="Close" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Literal ID="ltrDialogSearchTitle" runat="server" Text="Tìm kiếm ">
            </asp:Literal>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-12 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Họ tên nhân viên </label>
                        <SweetSoft:ExtraTextBox ID="txtSearchHoTen" runat="server" PlaceHolder="Nhập họ tên nhân viên cần tìm kiếm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Giới tính </label>
                        <SweetSoft:ExtraComboBox ID="cbSearchGioiTinh" runat="server" ComboStyle="Dropdown_White">
                            <asp:ListItem Value="" Text="Tất cả"></asp:ListItem>
                            <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                            <asp:ListItem Value="Nữ" Text="Nữ"></asp:ListItem>
                            <asp:ListItem Value="Không xác định" Text="Không xác định"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Phòng ban / kho</label>
                        <SweetSoft:ExtraComboBox ID="cbSeachPhongBan" runat="server" ComboStyle="Dropdown_White">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số CMND</label>
                        <SweetSoft:ExtraTextBox ID="txtSeachCMND" ValidationType="Number" runat="server" PlaceHolder="Nhập số cmnd nhân viên cần tìm kiếm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số điện thoại</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchSoDienThoai" ValidationType="Phone" runat="server" PlaceHolder="Nhập số điện thoại nhân viên cần tìm kiếm"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày sinh từ</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgaySinhFrom" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">tới</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgaySinhTo" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày vào công ty từ</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayVaoCongTyTu" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">tới</label>
                        <SweetSoft:ExtraDateTimePicker ID="dateSearchNgayVaoCongTyToi" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Có tài khoản đăng nhập</label>
                        <SweetSoft:ExtraComboBox ID="cbSearchTaiKhoanDangNhap" EnableLiveSearch="true" ComboStyle="Dropdown_White" runat="server">
                            <asp:ListItem Value="" Text="Tất cả">  </asp:ListItem>
                            <asp:ListItem Value="is not null" Text="Có tài khoản">  </asp:ListItem>
                            <asp:ListItem Value="is null" Text="Không có tài khoản"> </asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên tài khoản</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchTenTaiKhoan" runat="server"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Trạng thái hoạt động </label>
                        <SweetSoft:ExtraComboBox ID="cbbSearchStatus" EnableLiveSearch="true" ComboStyle="Dropdown_White" runat="server">
                            <asp:ListItem Value="" Text="Tất cả">  </asp:ListItem>
                            <asp:ListItem Value="1" Text="Còn hoạt động">  </asp:ListItem>
                            <asp:ListItem Value="0" Text="Không Còn hoạt động"> </asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Địa chỉ</label>
                        <SweetSoft:ExtraTextBox ID="txtSearchDiaChi" runat="server" TextMode="MultiLine" Height="50px" PlaceHolder="Nhập địa chỉ nhân viên cần tìm kiếm"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSearchNow" EnableRound="false" EStyle="Primary_Bold" runat="server" Text="Lọc dữ liệu" EIcon="A_Filter" OnClick="btnSearchNow_Click" CommandName="Search">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>

    <SweetSoft:ExtraDialog runat="server" ID="dialogDetail" RestrictionZoneID="html" EnableDraggable="true" HeadButtons="Close" Effect="FadeIn" Size="Normal" CloseText="Đóng">
        <HeaderTemplate>
            <asp:Label runat="server" ID="lblDialogDetailTitle"> </asp:Label>
        </HeaderTemplate>
        <ContentTemplate>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Họ * </label>
                        <SweetSoft:ExtraTextBox ID="txtDetailHo" runat="server" Required="true"> </SweetSoft:ExtraTextBox>
                        <asp:HiddenField ID="hdfDetailUserID" runat="server" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên * </label>
                        <SweetSoft:ExtraTextBox ID="txtDetailTen" runat="server" Required="true"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Giới tính </label>
                        <SweetSoft:ExtraComboBox ID="cbEditGioiTinh" runat="server" ComboStyle="Dropdown_White">
                            <asp:ListItem Value="Nam" Text="Nam"></asp:ListItem>
                            <asp:ListItem Value="Nữ" Text="Nữ"></asp:ListItem>
                            <asp:ListItem Value="Không xác định" Text="Không xác định"></asp:ListItem>
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Phòng ban / kho</label>
                        <SweetSoft:ExtraComboBox ID="cbPhongBan" runat="server" ComboStyle="Dropdown_White">
                        </SweetSoft:ExtraComboBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số CMND</label>
                        <SweetSoft:ExtraTextBox ID="txtCMND" runat="server" ValidationType="Number" data-errormessage-custom-error="Sai định dạng số cmnd" ValidationGroup="HRDetail" PlaceHolder="Nhập số CMND"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Số điện thoại</label>
                        <SweetSoft:ExtraTextBox ID="txtSoDienThoai" ValidationType="Number" data-errormessage-custom-error="Sai định dạng số điện thoại"  ValidationGroup="HRDetail" runat="server" PlaceHolder="Nhập số điện thoại"> </SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày sinh </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateEditNgaySinh" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Ngày vào công ty </label>
                        <SweetSoft:ExtraDateTimePicker ID="dateNgayVaoCongTy" DateTimeMask="Date" Width="100%" runat="server" />
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Có tài khoản người dùng</label><br />
                        <SweetSoft:ExtraCheckBox ID="chkEditCoTaiKhoanNguoiDung" OnCheckedChanged="chkEditCoTaiKhoanNguoiDung_CheckedChanged" AutoPostBack="true" runat="server" CheckboxType="Switch7" DataOff="Không" DataOn="Có"></SweetSoft:ExtraCheckBox>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Tên tài khoản</label>
                        <SweetSoft:ExtraTextBox ID="txtTenTaiKhoan" Enabled="false" runat="server"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Trạng thái hoạt động </label>
                        <br />
                        <SweetSoft:ExtraCheckBox ID="chkDetailTrangThaiSuDung" runat="server" CheckboxType="Switch7" DataOn="Bật" DataOff="Khóa" Width="100%"></SweetSoft:ExtraCheckBox>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group">
                        <label class="extra-label">Địa chỉ </label>
                        <br />
                        <SweetSoft:ExtraTextBox ID="txtDiaChi" runat="server" TextMode="MultiLine" Height="50px" PlaceHolder="Nhập địa chỉ nhân viên"></SweetSoft:ExtraTextBox>
                    </div>
                </div>
            </div>

        </ContentTemplate>
        <FooterTemplate>
            <SweetSoft:ExtraButton ID="btnSave" runat="server"  EIcon="A_Save" Text="Lưu thông tin" EStyle="Success_Bold" ESubmitAndCheckValid="true" EnableSnipButton="true" ValidationGroup="HRDetail" OnClick="btnSave_Click">  </SweetSoft:ExtraButton>
        </FooterTemplate>
    </SweetSoft:ExtraDialog>
</asp:Content>
<asp:Content ID="Content6" ContentPlaceHolderID="StyleFScriptPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content7" ContentPlaceHolderID="ConfirmHeaderPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content8" ContentPlaceHolderID="ConfirmContentPlaceHolder" runat="server">
</asp:Content>
<asp:Content ID="Content9" ContentPlaceHolderID="ConfirmFooterPlaceHolder" runat="server">
    <SweetSoft:ExtraButton ID="btnDelete" runat="server" OnClick="btnDelete_Click" CommandName="exec_delete" Text="Xóa" EIcon="A_Delete" EStyle="Danger_Bold"> </SweetSoft:ExtraButton>
</asp:Content>
