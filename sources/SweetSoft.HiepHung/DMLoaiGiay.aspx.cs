﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.HiepHung
{
    public partial class DMLoaiGiay : BaseGridPage
    {
        #region Properties

        public override string FUNCTION_CODE
        {
            get
            {

                return FunctionSystem.Code.Cat_Loai_Giay;
            }
        }

        public override string FUNCTION_NAME
        {
            get
            {

                return FunctionSystem.Name.Cat_Loai_Giay;
            }
        }

        protected string CurrentType
        {
            get
            {
                return "LG";
            }
        }

        protected string CurrentName
        {
            get
            {
                string rs = string.Empty;
                if (CurrentType == "LG") rs = "loại giấy";
                else if (CurrentType == "QC") rs = "quy cách";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;
            if (CurrentType == "LG")
            {
                if (rightName == "Insert") rs = UserHasRight(RightPageName.LG_Insert);
                else if (rightName == "Update") rs = UserHasRight(RightPageName.LG_Update);
                else if (rightName == "Delete") rs = UserHasRight(RightPageName.LG_Delete);
            }

            return rs;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);
            ltrSearchTenLoaiGiay.Text = string.Format("Tên {0}", CurrentName);
            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);
            txtSearchTenLoaiGiay.EnterSubmitClientID =btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");
            if (!IsPostBack)
            {
                grvData.CurrentSortExpression = TblLoaiGiay.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();
                    item.Text = grvData.Columns[i].HeaderText;
                    item.Selected = grvData.CheckColumnVisible(i);
                    if (item.Text == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            grvData.InsertItem();
        }

        protected void grvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvData.CancelAllCommand();
        }

        protected void grvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grvData.Rows[e.RowIndex];
            if (row != null)
            {
                TextBox txtEditTenLoaiGiay = row.FindControl("txtEditTenLoaiGiay") as TextBox;
                ExtraCheckBox chkEditStatus = row.FindControl("chkEditStatus") as ExtraCheckBox;
                HiddenField hdfEditID = row.FindControl("hdfEditID") as HiddenField;

                #region Validation
                if (string.IsNullOrEmpty(txtEditTenLoaiGiay.Text))
                {
                    AddValidPromt(txtEditTenLoaiGiay.ClientID, string.Format("Nhập tên {0}", CurrentName));
                    txtEditTenLoaiGiay.Focus();
                }



                #endregion

                e.Cancel = !PageIsValid;
                if (PageIsValid)
                {
                    if (grvData.IsInsert)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.PB_Insert, string.Format("Thêm mới {1} <b>{0}</b>", txtEditTenLoaiGiay.Text, CurrentName));

                        #region Insert value for LoaiGiay
                        TblLoaiGiay LoaiGiay = new TblLoaiGiay();
                        LoaiGiay.TenLoaiGiay = txtEditTenLoaiGiay.Text.Trim();
                        LoaiGiay.TrangThaiSuDung = chkEditStatus.Checked;
                        LoaiGiay.NguoiTao = LoaiGiay.NguoiCapNhap = ApplicationContext.Current.CommonUser.UserName;
                        LoaiGiay.NgayTao = LoaiGiay.NgayCapNhat = DateTime.Now;
                        #endregion

                        #region Add log value
                        CurrentLog.AddFirstValue(RightPageName.LG_Insert, string.Format("Tên {0}", CurrentName),
                                                 TblLoaiGiay.Columns.TenLoaiGiay,
                                                 txtEditTenLoaiGiay.Text);
                       
                        CurrentLog.AddFirstValue(RightPageName.LG_Insert, "Trạng thái",
                                                 TblLoaiGiay.Columns.TrangThaiSuDung,
                                                 chkEditStatus.Text);
                        #endregion

                        LoaiGiay = CategoryManager.InsertLoaiGiay(LoaiGiay);
                        ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                   string.Format("Thêm mới {1} <b>{0}</b> thành công.",
                                                 LoaiGiay.TenLoaiGiay, CurrentName),
                                   NotifyType.Success);
                        CurrentLog.SaveLog(RightPageName.LG_Insert);
                        grvData.Rebind();
                    }
                    else
                    {
                        TblLoaiGiay LoaiGiay = CategoryManager.GetLoaiGiayByID(hdfEditID.Value);
                        if (LoaiGiay != null)
                        {
                            #region Update value for LoaiGiay
                            LoaiGiay.TenLoaiGiay = txtEditTenLoaiGiay.Text.Trim();
                            LoaiGiay.TrangThaiSuDung = chkEditStatus.Checked;
                            LoaiGiay.NguoiCapNhap = ApplicationContext.Current.CommonUser.UserName;
                            LoaiGiay.NgayCapNhat = DateTime.Now;

                            #endregion

                            #region Add log value
                            CurrentLog.AddLastValue(RightPageName.LG_Update,
                                                    TblLoaiGiay.Columns.TenLoaiGiay,
                                                    txtEditTenLoaiGiay.Text);
                            CurrentLog.AddLastValue(RightPageName.LG_Update,
                                                    TblLoaiGiay.Columns.TrangThaiSuDung,
                                                    chkEditStatus.Text);
                            #endregion

                            LoaiGiay = CategoryManager.UpdateLoaiGiay(LoaiGiay);
                            ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                       string.Format("Cập nhật {1} <b>{0}</b> thành công.",
                                                     LoaiGiay.TenLoaiGiay, CurrentName),
                                       NotifyType.Success);
                            CurrentLog.SaveLog(RightPageName.LG_Update);
                            grvData.Rebind();
                        }
                    }
                    grvData.CancelAllCommand();
                }
                if (!PageIsValid)
                    ShowValidPromt();
            }
        }

        protected void grvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            ExtraButton btnEdit = e.Row.FindControl("btnEdit") as ExtraButton;
            if (btnEdit != null) btnEdit.Visible = GetButtonRole("Update");

            ExtraButton btnDelete = e.Row.FindControl("btnDelete") as ExtraButton;
            if (btnDelete != null) btnDelete.Visible = GetButtonRole("Delete");

            if (grvData.EditIndex == e.Row.RowIndex && !grvData.IsInsert)
            {
                TblLoaiGiay LoaiGiay = e.Row.DataItem as TblLoaiGiay;
                if (LoaiGiay != null)
                {
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.LG_Update,
                                                 string.Format("Cập nhật loại giấy {1} <b>{0}</b>",
                                                                LoaiGiay.TenLoaiGiay, CurrentName));
                    TextBox txtEditTenLoaiGiay
                        = e.Row.FindControl("txtEditTenLoaiGiay") as TextBox;
                    TextBox txtEditTenVietTat
                        = e.Row.FindControl("txtEditTenVietTat") as TextBox;
                    ExtraCheckBox chkEditStatus
                        = e.Row.FindControl("chkEditStatus") as ExtraCheckBox;
                    HiddenField hdfEditID
                        = e.Row.FindControl("hdfEditID") as HiddenField;

                    if (txtEditTenLoaiGiay != null)
                        txtEditTenLoaiGiay.Text = LoaiGiay.TenLoaiGiay;
                   
                    if (chkEditStatus != null)
                        chkEditStatus.Checked = LoaiGiay.TrangThaiSuDung.HasValue && LoaiGiay.TrangThaiSuDung.Value;
                    if (hdfEditID != null)
                        hdfEditID.Value = LoaiGiay.Id.ToString();

                    #region Add log value
                    CurrentLog.AddFirstValue(RightPageName.LG_Update, string.Format("Tên {0}", CurrentName),
                                             TblLoaiGiay.Columns.TenLoaiGiay,
                                             txtEditTenLoaiGiay.Text);
                    CurrentLog.AddFirstValue(RightPageName.LG_Update, "Trạng thái sử dụng",
                                             TblLoaiGiay.Columns.TrangThaiSuDung,
                                             chkEditStatus.Text);
                    #endregion
                }
            }
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string orderClause = string.Format("{0} desc", TblLoaiGiay.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                string column = "*";

                if (!string.IsNullOrEmpty(txtSearchTenLoaiGiay.Text.Trim()))
                    filterClause += string.Format(" {0} like N'%{1}%' AND ",
                                                  TblLoaiGiay.Columns.TenLoaiGiay,
                                                  txtSearchTenLoaiGiay.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(cbbSearchStatus.SelectedValue))
                    filterClause += string.Format(" {0} = {1} AND ",
                                                  TblLoaiGiay.Columns.TrangThaiSuDung,
                                                  cbbSearchStatus.SelectedValue);

                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);

                int total = 0;
                List<TblLoaiGiay> lst = CategoryManager.SearchLoaiGiay(rowIndex, pageSize, column,
                                                                filterClause, orderClause, out total);
                if (grvData.IsInsert)
                    lst.Insert(0, new TblLoaiGiay());
                grv.DataSource = lst;
                List<TblLoaiGiay> lstEmplty = new List<TblLoaiGiay>();
                lstEmplty.Add(new TblLoaiGiay());
                grvData.DataSourceEmpty = lstEmplty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchTenLoaiGiay.Text = string.Empty;
            cbbSearchStatus.SelectedIndex = 0;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_LoaiGiay")
                        {
                            TblLoaiGiay LoaiGiay = CurrentMessageBoxResult.Value as TblLoaiGiay;
                            if (LoaiGiay != null)
                            {
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.LG_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.LG_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblLoaiGiay.Columns.TenLoaiGiay,
                                                             LoaiGiay.TenLoaiGiay);
                                    resultDelete = CategoryManager.DeleteLoaiGiay(LoaiGiay.Id);
                                }
                                catch
                                {
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              LoaiGiay.TenLoaiGiay, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.LG_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa {0} thất bại", CurrentName),
                                                string.Format("Có lỗi xảy ra khi xóa {1} <b>{0}</b>",
                                                               LoaiGiay.TenLoaiGiay, CurrentName),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblLoaiGiay LoaiGiay = CategoryManager.GetLoaiGiayByID(btn.CommandArgument);
                    if (LoaiGiay != null)
                    {
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = LoaiGiay;
                        result.Submit = true;
                        result.CommandName = "delete_LoaiGiay";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     LoaiGiay.TenLoaiGiay, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }
    }
}