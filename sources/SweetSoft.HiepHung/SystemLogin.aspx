﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SystemLogin.aspx.cs" Inherits="SweetSoft.HiepHung.SystemLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Hiệp Hưng | Đăng nhập hệ thống</title>

    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <link rel="icon" href="/images/icon.ico" type="image/x-icon" />

    <!-- bootstrap & fontawesome -->
    <link href="/css/bootstrap.min.css" rel="stylesheet" />
    <link href="/icons/AweIcon/aweicon.css" rel="stylesheet" />
    <link href="/icons/FlatIcon/flaticon.css" rel="stylesheet" />

    <!-- text fonts -->
    <link href="/themes/ace/fonts/fonts.googleapis.com.css" rel="stylesheet" />
    <link href='https://fonts.googleapis.com/css?family=Noto+Sans&subset=wrap-content-loginvietnamese'
        rel='stylesheet' type='text/css' />
    <link href='https://fonts.googleapis.com/css?family=Lobster&subset=vietnamese'
        rel='stylesheet' type='text/css' />

    <!-- ace styles -->
    <link href="/themes/ace/style/ace.min.css" rel="stylesheet" />
    <!-- controls -->
    <link href="/css/textbox/ExtraTextbox.css" rel="stylesheet" />
    <link href="/css/checkbox/checkbox.css" rel="stylesheet" />
    <link href="/css/nprogress/nprogress.css" rel="stylesheet" />
    <link href="/css/pnotify/animate.css" rel="stylesheet" />
    <link href="/css/pnotify/pnotify.core.css" rel="stylesheet" />
    <link href="/css/pnotify/pnotify.custom.min.css" rel="stylesheet" />

    <link href="/css/validation/validationEngine.jquery.css" rel="stylesheet" />
    <link href="/css/ControlStyle.css" rel="stylesheet" />
    <script src="/scripts/jQuery-2.1.4.min.js"></script>

    <script src="/scripts/pnotify/pnotify.custom.min.js"></script>
    <script src="/scripts/pnotify/pnotify.animate.js"></script>
    <script src="/scripts/nprogress/nprogress.js"></script>

    <script src="/scripts/validation/languages/jquery.validationEngine-vi-VN.js"></script>
    <script src="/scripts/validation/jquery.validationEngine.js"></script>
    <script src="/scripts/validation/validationScript.js"></script>
    <style type="text/css" media="screen">
        body {
            font-family: "Roboto",sans-serif;
        }

        .top-login {
            padding: 20px;
        }

        .wrap-content-login {
            min-height: 497px;
        }

        .login-form .position-relative {
            max-width: 400px;
            margin: auto;
            margin-top: 8px;
        }

        body.login-layout {
            background: transparent;
            background-image: url('/images/IMG_4913-Pano.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-position: 40% 35%;
        }

        .footer-auth {
            background: rgba(14, 17, 19, 0.2);
            position: absolute;
            bottom: 0px;
            left: 0px;
            right: 0px;
            padding: 20px;
        }

        .helper {
            z-index: 999;
            text-align: center;
            margin-top: 20px;
            padding-bottom: 100px;
        }

        .mask
        {
        background-color:rgba(3, 3, 3, 0.5);
        position:absolute;
        top:0;
        left:0;
        width:100%;
        height:100%;
        }
    </style>
    <script src="/scripts/Config.js"></script>
    <script src="/scripts/button/ExtraButton.js"></script>
</head>
<body class="login-layout light-login">
    <form id="mainForm" runat="server" defaultbutton="btnLogin">
        <asp:ScriptManager ID="scriptManager" runat="server"></asp:ScriptManager>
        <div class="mask"></div>
        <div class="main-container" style="padding-top: 14px; height: auto;">
            <div class="main-content">
                <div class="row">
                    <div class="top-login">

                        <div class="col-md-6 title-page text-center col-md-offset-3">
                            <%--<h2 style="color: #fff; font-size: 25px; text-align: center;" class="text-shadown">Quản lý sản xuất </h2>--%>
                            <img src="Images/logo.big.png" alt="Công ty TNHH Hiệp Hưng Nha Trang" title="Công ty TNHH Hiệp Hưng Nha Trang" style="width: 210px; height: 50px;" />
                            <h1 style="font-size: 24px; color: #fff; font-weight: 700; text-transform: uppercase; text-align: center;" class="text-shadown">Phầm mềm quản lý sản xuất</h1>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="wrap-content-login">
                        <div class="col-md-12 login-form">
                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-key blue"></i>
                                                Thông tin đăng nhập
                                            </h4>
                                            <div class="space-6"></div>
                                            <div>
                                                <asp:UpdatePanel runat="server" ID="udpLogin">
                                                    <ContentTemplate>
                                                        <fieldset>
                                                            <label class="block clearfix">
                                                                <span class="block input-icon input-icon-right">
                                                                    <SweetSoft:ExtraTextBox runat="server"
                                                                        ID="txtLoginName" TabIndex="1" PlaceHolder="Tên đăng nhập" Required="true" Text="demo">
                                                                    </SweetSoft:ExtraTextBox>
                                                                    <i class="ace-icon fa fa-user"></i>
                                                                </span>
                                                            </label>
                                                            <label class="block clearfix">
                                                                <span class="block input-icon input-icon-right">
                                                                    <SweetSoft:ExtraTextBox runat="server" ID="txtLoginPassword"
                                                                        TabIndex="2" PlaceHolder="Mật khẩu" Required="true" TextMode="Password" Text="123456">
                                                                    </SweetSoft:ExtraTextBox>
                                                                    <i class="ace-icon fa fa-lock"></i>
                                                                </span>
                                                            </label>

                                                            <div class="space"></div>

                                                            <div class="clearfix">
                                                                <label class="inline">
                                                                    <SweetSoft:ExtraCheckBox
                                                                        runat="server"
                                                                        ID="chkLoginRemember"
                                                                        TabIndex="3" Checked="true"
                                                                        CssClass="ace">
                                                                    </SweetSoft:ExtraCheckBox>
                                                                    <span class="lbl">Duy trì đăng nhập</span>
                                                                </label>
                                                                <SweetSoft:ExtraButton
                                                                    runat="server" ID="btnLogin" EIconPosition="Left" TabIndex="4" EIcon="A_Sign_In" EStyle="Info"
                                                                    CssClass="width-40 pull-right btn btn-sm " ValidationGroup="login" ESubmitAndCheckValid="true" EnableSnipButton="true" SnipText="Đang xử lý..." OnClick="btnLogin_Click">
                                                                    <span class="bigger-110">Đăng nhập</span>
                                                                </SweetSoft:ExtraButton>
                                                            </div>
                                                            <div class="space-4"></div>
                                                        </fieldset>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="space-6"></div>
                                        </div>
                                        <!-- /.widget-main -->

                                        <div class="toolbar clearfix" style="display: none;">
                                            <div>
                                                <a href="#" data-target="#forgot-box"
                                                    class="forgot-password-link">
                                                    <i class="ace-icon fa fa-arrow-left"></i>
                                                    Quên mật khẩu
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.widget-body -->
                                </div>
                                <!-- /.login-box -->


                            </div>
                        </div>

                    </div>

                    <div class="footer-auth text-center">
                        <h5 class="white">
                            <%--<img width="20" alt="logo" src="/images/logo-thue.png" />--%>
                            <strong>© 2019 Phát triển bởi <a href="https://www.sweetsoft.vn" target="_blank" style="color: #ceb017;">SweetSoft JSC</a></strong></h5>
                    </div>
                </div>
                <!-- /.row -->
            </div>
            <!-- /.main-content -->
        </div>
        <!-- /.main-container -->
    </form>

    <!-- basic scripts -->

    <script type="text/javascript">
        window.jQuery || document.write("<script src='/scripts/jQuery-2.1.4.min.js'>" + "<" + "/script>");
    </script>

    <%--<script type="text/javascript">
        window.jQuery || document.write("<script src='assets/js/jquery1x.min.js'>" + "<" + "/script>");
    </script>--%>
    <%--<script type="text/javascript">
        if ('ontouchstart' in document.documentElement)
            document.write("<script src='assets/js/jquery.mobile.custom.min.js'>" + "<" + "/script>");
    </script>--%>

    <!-- inline scripts related to this page -->
    <script type="text/javascript">

        jQuery(function ($) {
            $(document).on('click', '.toolbar a[data-target]', function (e) {
                e.preventDefault();
                var target = $(this).data('target');
                $('.widget-box.visible').removeClass('visible');//hide others
                $(target).addClass('visible');//show target
            });
        });

        //used for changing background
        jQuery(function ($) {
            $('#btn-login-dark').on('click', function (e) {
                $('body').attr('class', 'login-layout');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-light').on('click', function (e) {
                $('body').attr('class', 'login-layout light-login');
                $('#id-text2').attr('class', 'grey');
                $('#id-company-text').attr('class', 'blue');

                e.preventDefault();
            });
            $('#btn-login-blur').on('click', function (e) {
                $('body').attr('class', 'login-layout blur-login');
                $('#id-text2').attr('class', 'white');
                $('#id-company-text').attr('class', 'light-blue');

                e.preventDefault();
            });

        });
    </script>
</body>
</html>
