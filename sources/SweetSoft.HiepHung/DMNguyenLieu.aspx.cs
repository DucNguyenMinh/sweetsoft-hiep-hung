﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Core;

namespace SweetSoft.HiepHung
{
    public partial class DMNguyenLieu : BaseGridPage
    {
        #region Properties

        public override string FUNCTION_CODE
        {
            get
            {
                if (CurrentType == "LMI")
                    return FunctionSystem.Code.Cat_Loai_Mau_In;
                return FunctionSystem.Code.Cat_Nguyen_Lieu;
            }
        }

        public override string FUNCTION_NAME
        {
            get
            {

                if (CurrentType == "LMI")
                    return FunctionSystem.Name.Cat_Loai_Mau_In;
                return FunctionSystem.Name.Cat_Nguyen_Lieu;
            }
        }

        protected string CurrentType
        {
            get
            {
                return "NL";
            }
        }

        protected string CurrentName
        {
            get
            {
                string rs = string.Empty;
                if (CurrentType == "NL") rs = "Nguyên liệu/vật tư";
                else if (CurrentType == "Thung") rs = "loại thùng";
                return rs;
            }
        }

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;
            if (CurrentType == "NL")
            {
                if (rightName == "Insert") rs = UserHasRight(RightPageName.NL_Insert);
                else if (rightName == "Update") rs = UserHasRight(RightPageName.NL_Update);
                else if (rightName == "Delete") rs = UserHasRight(RightPageName.NL_Delete);

            }

            return rs;
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            ltrDialogSearchTitle.Text = string.Format("Tìm kiếm {0}", CurrentName);

            topTitle1.Text = breadTitle.Text = string.Format("Danh mục {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = CurrentName;
            grvData.Columns[1].HeaderText = string.Format("Tên {0}", CurrentName);

            txtSearchTenNguyenLieu.EnterSubmitClientID = txtSearchThuocTinh.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");
            if (!IsPostBack)
            {
                grvData.CurrentSortExpression = TblNguyenLieu.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                LoadDonViTinh(cbSearchDonViTinh);
                LoadDonViTinh(cbSearchDonViLuuKho);
                LoadLoaiGiay(cbSearchLoaiGiay, true);

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();
                    item.Text = grvData.Columns[i].HeaderText;
                    item.Selected = grvData.CheckColumnVisible(i);
                    if (item.Text == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            grvData.InsertItem();
        }

        protected void grvData_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
        {
            grvData.CancelAllCommand();
        }

        protected void grvData_RowUpdating(object sender, GridViewUpdateEventArgs e)
        {
            GridViewRow row = grvData.Rows[e.RowIndex];
            if (row != null)
            {
                TextBox txtEditTenNguyenLieu = row.FindControl("txtEditTenNguyenLieu") as TextBox;
                TextBox txtEditThuocTinh = row.FindControl("txtEditThuocTinh") as TextBox;
                //   TextBox txtEditSoLuongTon = row.FindControl("txtEditSoLuongTon") as TextBox;
                ExtraCheckBox chkEditStatus = row.FindControl("chkEditStatus") as ExtraCheckBox;
                ExtraComboBox cbEditDonViTinh = row.FindControl("cbEditDonViTinh") as ExtraComboBox;
                ExtraComboBox cbEditDonViLuuKho = row.FindControl("cbEditDonViLuuKho") as ExtraComboBox;
                ExtraComboBox cbEditLoaiGiay = row.FindControl("cbEditLoaiGiay") as ExtraComboBox;
                HiddenField hdfEditID = row.FindControl("hdfEditID") as HiddenField;

                #region Validation
                if (string.IsNullOrEmpty(txtEditTenNguyenLieu.Text))
                {
                    AddValidPromt(txtEditTenNguyenLieu.ClientID, string.Format("Nhập tên {0}", CurrentName));
                    txtEditTenNguyenLieu.Focus();
                }

                if (!cbEditDonViTinh.HasValue)
                {
                    AddValidPromt(cbEditDonViTinh.ClientID, "Chọn đơn vị tính");
                    cbEditDonViTinh.Focus();
                }

                if (!cbEditDonViLuuKho.HasValue)
                {
                    AddValidPromt(cbEditDonViLuuKho.ClientID, "Chọn đơn vị lưu kho");
                    cbEditDonViLuuKho.Focus();
                }

                decimal soTon = 0;
                //if (string.IsNullOrEmpty(txtEditSoLuongTon.Text))
                //{
                //    AddValidPromt(txtEditSoLuongTon.ClientID, string.Format("Nhập số lượng tồn"));
                //    txtEditSoLuongTon.Focus();
                //}
                //else
                //{
                //    decimal.TryParse(txtEditSoLuongTon.Text.Trim().Replace(".", "").Replace(",", "."), out soTon);
                //    if (soTon < 0)
                //    {
                //        AddValidPromt(txtEditSoLuongTon.ClientID, string.Format("Số lượng tồn không âm"));
                //        txtEditSoLuongTon.Focus();
                //    }
                //}



                #endregion

                e.Cancel = !PageIsValid;
                if (PageIsValid)
                {
                    if (grvData.IsInsert)
                    {
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.PB_Insert, string.Format("Thêm mới {1} <b>{0}</b>", txtEditTenNguyenLieu.Text, CurrentName));

                        #region Insert value for NguyenLieu
                        TblNguyenLieu NguyenLieu = new TblNguyenLieu();
                        NguyenLieu.TenNguyenLieu = txtEditTenNguyenLieu.Text.Trim();
                        NguyenLieu.ThuocTinh = txtEditThuocTinh.Text.Trim();
                        NguyenLieu.TrangThaiSuDung = chkEditStatus.Checked;
                        NguyenLieu.TonDau = NguyenLieu.TonCuoi = soTon;
                        NguyenLieu.IDDonViTinh = int.Parse(cbEditDonViTinh.SelectedValue);
                        NguyenLieu.IDDonViLuuKho = int.Parse(cbEditDonViLuuKho.SelectedValue);
                        if (cbEditLoaiGiay != null && cbEditLoaiGiay.HasValue)
                            NguyenLieu.IDLoaiGiay = int.Parse(cbEditLoaiGiay.SelectedValue);
                        NguyenLieu.NguoiTao = NguyenLieu.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        NguyenLieu.NgayTao = NguyenLieu.NgayCapNhat = DateTime.Now;
                        #endregion

                        #region Add log value
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, string.Format("Tên {0}", CurrentName),
                                                 TblNguyenLieu.Columns.TenNguyenLieu,
                                                 txtEditTenNguyenLieu.Text);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Thuộc tính",
                                                TblNguyenLieu.Columns.ThuocTinh,
                                                txtEditThuocTinh.Text);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Tồn đầu",
                                                TblNguyenLieu.Columns.TonDau,
                                                soTon.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Tồn cuối",
                                               TblNguyenLieu.Columns.TonCuoi,
                                               soTon.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Thuộc tính",
                                                TblNguyenLieu.Columns.ThuocTinh,
                                                txtEditThuocTinh.Text);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Trạng thái",
                                                 TblNguyenLieu.Columns.TrangThaiSuDung,
                                                 chkEditStatus.Text);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Đơn vị tính",
                                                 TblNguyenLieu.Columns.IDDonViTinh,
                                                 cbEditDonViTinh.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Thuộc tính loại giấy",
                                                 TblNguyenLieu.Columns.IDLoaiGiay,
                                                 cbEditLoaiGiay.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NL_Insert, "Đơn vị lưu kho",
                                           TblNguyenLieu.Columns.IDDonViLuuKho,
                                           cbEditDonViLuuKho.DisplayText);
                        #endregion

                        NguyenLieu = NguyenLieuManager.InsertNguyenLieu(NguyenLieu);
                        ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                   string.Format("Thêm mới {1} <b>{0}</b> thành công.",
                                                 NguyenLieu.TenNguyenLieu, CurrentName),
                                   NotifyType.Success);
                        CurrentLog.SaveLog(RightPageName.NL_Insert);
                        grvData.Rebind();
                    }
                    else
                    {
                        TblNguyenLieu NguyenLieu = NguyenLieuManager.GetNguyenLieuByID(hdfEditID.Value);
                        if (NguyenLieu != null)
                        {
                            #region Update value for NguyenLieu
                            NguyenLieu.TenNguyenLieu = txtEditTenNguyenLieu.Text.Trim();
                            NguyenLieu.ThuocTinh = txtEditThuocTinh.Text.Trim();
                            NguyenLieu.TonDau = NguyenLieu.TonCuoi;
                            NguyenLieu.TonCuoi = soTon;
                            NguyenLieu.IDDonViTinh = int.Parse(cbEditDonViTinh.SelectedValue);
                            NguyenLieu.IDDonViLuuKho = int.Parse(cbEditDonViLuuKho.SelectedValue);
                            NguyenLieu.IDLoaiGiay = null;
                            if (cbEditLoaiGiay != null && cbEditLoaiGiay.HasValue)
                                NguyenLieu.IDLoaiGiay = int.Parse(cbEditLoaiGiay.SelectedValue);
                            NguyenLieu.TrangThaiSuDung = chkEditStatus.Checked;
                            NguyenLieu.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                            NguyenLieu.NgayCapNhat = DateTime.Now;

                            #endregion

                            #region Add log value
                            CurrentLog.AddLastValue(RightPageName.NL_Update,
                                                    TblNguyenLieu.Columns.TenNguyenLieu,
                                                    txtEditTenNguyenLieu.Text);
                            CurrentLog.AddLastValue(RightPageName.NL_Update,
                                                    TblNguyenLieu.Columns.TonDau,
                                                    NguyenLieu.TonDau.Value.ExecptionMod().ToStringWithLanguage());
                            CurrentLog.AddLastValue(RightPageName.NL_Update,
                                                    TblNguyenLieu.Columns.TonCuoi,
                                                    NguyenLieu.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                            CurrentLog.AddLastValue(RightPageName.NL_Update,
                                                    TblNguyenLieu.Columns.ThuocTinh,
                                                    txtEditThuocTinh.Text);
                            CurrentLog.AddLastValue(RightPageName.NL_Update,
                                                    TblNguyenLieu.Columns.TrangThaiSuDung,
                                                    chkEditStatus.Text);
                            CurrentLog.AddLastValue(RightPageName.NL_Insert,
                                                 TblNguyenLieu.Columns.IDDonViTinh,
                                                 cbEditDonViTinh.DisplayText);
                            CurrentLog.AddLastValue(RightPageName.NL_Insert,
                                                TblNguyenLieu.Columns.IDLoaiGiay,
                                                cbEditLoaiGiay.DisplayText);
                            CurrentLog.AddLastValue(RightPageName.NL_Insert,
                                             TblNguyenLieu.Columns.IDDonViLuuKho,
                                             cbEditDonViLuuKho.DisplayText);
                            #endregion

                            NguyenLieu = NguyenLieuManager.UpdateNguyenLieu(NguyenLieu);
                            ShowNotify(string.Format("Lưu {0} thành công!", CurrentName),
                                       string.Format("Cập nhật {1} <b>{0}</b> thành công.",
                                                     NguyenLieu.TenNguyenLieu, CurrentName),
                                       NotifyType.Success);
                            CurrentLog.SaveLog(RightPageName.NL_Update);
                            grvData.Rebind();
                        }
                    }
                    grvData.CancelAllCommand();
                }
                if (!PageIsValid)
                    ShowValidPromt();
            }
        }

        protected void grvData_RowCreated(object sender, GridViewRowEventArgs e)
        {
            ExtraButton btnEdit = e.Row.FindControl("btnEdit") as ExtraButton;
            if (btnEdit != null) btnEdit.Visible = GetButtonRole("Update");

            ExtraButton btnDelete = e.Row.FindControl("btnDelete") as ExtraButton;
            if (btnDelete != null) btnDelete.Visible = GetButtonRole("Delete");

            ExtraComboBox cbEditDonViTinh
                 = e.Row.FindControl("cbEditDonViTinh") as ExtraComboBox;
            if (cbEditDonViTinh != null)
            {
                LoadDonViTinh(cbEditDonViTinh, false);
            }

            ExtraComboBox cbEditDonViLuuKho
                = e.Row.FindControl("cbEditDonViLuuKho") as ExtraComboBox;
            if (cbEditDonViLuuKho != null)
            {
                LoadDonViTinh(cbEditDonViLuuKho, false);
            }

            ExtraComboBox cbEditLoaiGiay = e.Row.FindControl("cbEditLoaiGiay") as ExtraComboBox;
            if (cbEditLoaiGiay != null)
            {
                LoadLoaiGiay(cbEditLoaiGiay, false);
            }

            if (grvData.EditIndex == e.Row.RowIndex && !grvData.IsInsert)
            {
                TblNguyenLieu NguyenLieu = e.Row.DataItem as TblNguyenLieu;
                if (NguyenLieu != null)
                {
                    CurrentLog.Clear();
                    CurrentLog.AddNewLogFunction(RightPageName.NL_Update,
                                                 string.Format("Cập nhật danh mục nguyên liệu {1} <b>{0}</b>",
                                                                NguyenLieu.TenNguyenLieu, CurrentName));
                    ExtraTextBox txtEditTenNguyenLieu
                        = e.Row.FindControl("txtEditTenNguyenLieu") as ExtraTextBox;

                    ExtraTextBox txtEditThuocTinh
                     = e.Row.FindControl("txtEditThuocTinh") as ExtraTextBox;

                    TextBox txtEditSoLuongTon
                    = e.Row.FindControl("txtEditSoLuongTon") as TextBox;

                    ExtraCheckBox chkEditStatus
                        = e.Row.FindControl("chkEditStatus") as ExtraCheckBox;

                    HiddenField hdfEditID
                        = e.Row.FindControl("hdfEditID") as HiddenField;

                    if (txtEditTenNguyenLieu != null)
                    {
                        txtEditTenNguyenLieu.MaxTextLength = TblNguyenLieu.TenNguyenLieuColumn.MaxLength;
                        txtEditTenNguyenLieu.Text = NguyenLieu.TenNguyenLieu;
                    }

                    if (txtEditThuocTinh != null)
                    {
                        txtEditThuocTinh.MaxTextLength = TblNguyenLieu.ThuocTinhColumn.MaxLength;
                        txtEditThuocTinh.Text = NguyenLieu.ThuocTinh;
                    }

                    if (cbEditDonViTinh != null && NguyenLieu.IDDonViTinh.HasValue)
                    {
                        ListItem item = cbEditDonViTinh.Items.FindByValue(NguyenLieu.IDDonViTinh.Value.ToString());
                        if (item != null) cbEditDonViTinh.SelectedValue = NguyenLieu.IDDonViTinh.Value.ToString();
                    }

                    if (cbEditLoaiGiay != null && NguyenLieu.IDLoaiGiay.HasValue)
                    {
                        ListItem item = cbEditLoaiGiay.Items.FindByValue(NguyenLieu.IDLoaiGiay.Value.ToString());
                        if (item != null) cbEditLoaiGiay.SelectedValue = NguyenLieu.IDLoaiGiay.Value.ToString();
                    }

                    if (cbEditDonViLuuKho != null && NguyenLieu.IDDonViLuuKho.HasValue)
                    {
                        ListItem item = cbEditDonViLuuKho.Items.FindByValue(NguyenLieu.IDDonViLuuKho.Value.ToString());
                        if (item != null) cbEditDonViLuuKho.SelectedValue = NguyenLieu.IDDonViLuuKho.Value.ToString();
                    }

                    if (txtEditSoLuongTon != null && NguyenLieu.TonCuoi.HasValue)
                        txtEditSoLuongTon.Text = NguyenLieu.TonCuoi.Value.ExecptionMod().ToStringWithLanguage();

                    if (chkEditStatus != null)
                        chkEditStatus.Checked = NguyenLieu.TrangThaiSuDung.HasValue && NguyenLieu.TrangThaiSuDung.Value;
                    if (hdfEditID != null)
                        hdfEditID.Value = NguyenLieu.Id.ToString();

                    #region Add log value
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, string.Format("Tên {0}", CurrentName),
                                             TblNguyenLieu.Columns.TenNguyenLieu,
                                             txtEditTenNguyenLieu.Text);
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Thuộc tính",
                                           TblNguyenLieu.Columns.ThuocTinh,
                                           txtEditThuocTinh.Text);
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Tồn đầu",
                                           TblNguyenLieu.Columns.TonDau,
                                           NguyenLieu.TonDau.Value.ExecptionMod().ToStringWithLanguage());
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Tồn cuối",
                                           TblNguyenLieu.Columns.TonCuoi,
                                           NguyenLieu.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Trạng thái sử dụng",
                                             TblNguyenLieu.Columns.TrangThaiSuDung,
                                             chkEditStatus.Text);
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Đơn vị tính",
                                             TblNguyenLieu.Columns.IDDonViTinh,
                                             cbEditDonViTinh.DisplayText);
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Đơn vị lưu kho",
                                            TblNguyenLieu.Columns.IDDonViLuuKho,
                                            cbEditDonViLuuKho.DisplayText);
                    CurrentLog.AddFirstValue(RightPageName.NL_Update, "Loại giấy",
                                            TblNguyenLieu.Columns.IDLoaiGiay,
                                            cbEditLoaiGiay.DisplayText);
                    #endregion
                }
            }
        }

        private void LoadDonViTinh(ExtraComboBox cb)
        {
            LoadDonViTinh(cb, true);
        }

        private void LoadDonViTinh(ExtraComboBox cb, bool loadEmpty)
        {
            List<TblDonViTinh> donViCol = CategoryManager.GetDonViTinhALL(true);
            if (loadEmpty)
            {
                TblDonViTinh empty = new TblDonViTinh();
                empty.Id = -1;
                empty.TenDonVi = "Tất cả";
                donViCol.Insert(0, empty);
            }
            cb.DataSource = donViCol;
            cb.DataValueField = TblDonViTinh.Columns.Id;
            cb.DataTextField = "DisplayText";
            cb.DataBind();
        }

        private void LoadLoaiGiay(ExtraComboBox cb, bool loadEmpty)
        {
            List<TblLoaiGiay> loaiGiayCol = CategoryManager.GetLoaiGiayALL(true);
            if (loadEmpty)
            {
                TblLoaiGiay empty = new TblLoaiGiay();
                empty.Id = -1;
                empty.TenLoaiGiay = "Tất cả";
                loaiGiayCol.Insert(0, empty);
            }
            cb.DataSource = loaiGiayCol;
            cb.DataValueField = TblLoaiGiay.Columns.Id;
            cb.DataTextField = TblLoaiGiay.Columns.TenLoaiGiay;
            cb.DataBind();
        }



        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string orderClause = string.Format("{0} desc", TblNguyenLieu.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                string column = string.Format(" nl.*, dvt.{0},dvt.{1},dvt2.{0} as TenDonViLuuKho,dvt2.{1} as TenVietTatLuuKho,lg.{2} ", TblDonViTinh.Columns.TenDonVi, TblDonViTinh.Columns.TenVietTat, TblLoaiGiay.Columns.TenLoaiGiay);

                if (!string.IsNullOrEmpty(txtSearchTenNguyenLieu.Text.Trim()))
                    filterClause += string.Format(" nl.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieu.Columns.TenNguyenLieu,
                                                  txtSearchTenNguyenLieu.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTenNguyenLieu.Text.Trim()))
                    filterClause += string.Format(" nl.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieu.Columns.ThuocTinh,
                                                  txtSearchThuocTinh.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(cbbSearchStatus.SelectedValue))
                    filterClause += string.Format(" nl.{0} = {1} AND ",
                                                  TblNguyenLieu.Columns.TrangThaiSuDung,
                                                  cbbSearchStatus.SelectedValue);
                if (cbSearchDonViTinh.HasValue)
                    filterClause += string.Format(" nl.{0} = {1} AND ",
                                                  TblNguyenLieu.Columns.IDDonViTinh,
                                                  cbSearchDonViTinh.SelectedValue);
                if (cbSearchDonViLuuKho.HasValue)
                    filterClause += string.Format(" nl.{0} = {1} AND ",
                                                  TblNguyenLieu.Columns.IDDonViLuuKho,
                                                  cbSearchDonViLuuKho.SelectedValue);
                if (cbSearchLoaiGiay.HasValue)
                    filterClause += string.Format(" nl.{0} = {1} AND ",
                                                  TblNguyenLieu.Columns.IDLoaiGiay,
                                                  cbSearchLoaiGiay.SelectedValue);

                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());

                filterClause = (filterClause1 + filterClause.Trim()).Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);

                int total = 0;
                List<TblNguyenLieu> lst = NguyenLieuManager.SearchNguyenLieu(rowIndex, pageSize, column,
                                                                filterClause, orderClause, out total);
                if (grvData.IsInsert)
                    lst.Insert(0, new TblNguyenLieu());
                grv.DataSource = lst;
                List<TblNguyenLieu> lstEmplty = new List<TblNguyenLieu>();
                lstEmplty.Add(new TblNguyenLieu());
                grvData.DataSourceEmpty = lstEmplty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchTenNguyenLieu.Text = txtSearchThuocTinh.Text = string.Empty;
            cbbSearchStatus.SelectedIndex = 0;
            cbSearchDonViTinh.SelectedIndex = cbSearchDonViLuuKho.SelectedIndex = cbSearchLoaiGiay.SelectedIndex = 0;
            grvData.Rebind();
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {
                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {
                        if (CurrentMessageBoxResult.CommandName == "delete_NguyenLieu")
                        {
                            TblNguyenLieu NguyenLieu = CurrentMessageBoxResult.Value as TblNguyenLieu;
                            if (NguyenLieu != null)
                            {
                                string msg = "";
                                try
                                {
                                    CurrentLog.AddNewLogFunction(RightPageName.NL_Delete,
                                                                 string.Format("Xóa {0}", CurrentName));
                                    CurrentLog.AddFirstValue(RightPageName.NL_Delete, string.Format("Tên {0}", CurrentName),
                                                             TblNguyenLieu.Columns.TenNguyenLieu,
                                                             NguyenLieu.TenNguyenLieu);
                                    resultDelete = NguyenLieuManager.DeleteNguyenLieu(NguyenLieu.Id);
                                }
                                catch (Exception ex)
                                {
                                    if (ex.Message.Contains("FK_TblNguyenLieuChiTiet_TblNguyenLieu"))
                                        msg = " vì đang được sử dụng cho chi tiết nguyên/vật liệu";
                                }
                                if (resultDelete)
                                {
                                    grvData.Rebind();
                                    ShowNotify(string.Format("Xóa {0} thành công", CurrentName),
                                                string.Format("Bạn vừa xóa {1} <b>{0}</b>",
                                                              NguyenLieu.TenNguyenLieu, CurrentName),
                                                NotifyType.Success);
                                    CurrentLog.SaveLog(RightPageName.NL_Delete);
                                }
                                else
                                {
                                    ShowNotify(string.Format("Xóa nguyên liệu chưa thành công", CurrentName),
                                                string.Format("Hệ thống không cho phép xóa nguyên liệu <b>{0}</b> {1}. Vui lòng kiểm tra lại dữ liệu hoặc liên hệ quản trị",
                                                               NguyenLieu.TenNguyenLieu, msg),
                                                NotifyType.Error);
                                }
                                CloseMessageBox();
                            }
                        }
                    }
                }
                else
                {
                    TblNguyenLieu NguyenLieu = NguyenLieuManager.GetNguyenLieuByID(btn.CommandArgument);
                    if (NguyenLieu != null)
                    {
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = NguyenLieu;
                        result.Submit = true;
                        result.CommandName = "delete_NguyenLieu";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa {1} <b>{0}</b> ?",
                                                     NguyenLieu.TenNguyenLieu, CurrentName));
                    }
                }
            }
        }

        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

    }
}