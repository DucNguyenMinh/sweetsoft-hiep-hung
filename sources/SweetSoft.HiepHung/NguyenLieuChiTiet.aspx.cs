﻿using SweetSoft.ExtraControls;
using SweetSoft.HiepHung.Common;
using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.Core.Manager;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.HiepHung.Manager;
using SweetSoft.HiepHung.Core;
namespace SweetSoft.HiepHung
{
    public partial class NguyenLieuChiTiet : BaseGridPage
    {
        #region Properties


        public override string FUNCTION_NAME
        {
            get
            {
                return FunctionSystem.Name.NguyenLieuVatTu;
            }
        }

        public override string FUNCTION_CODE
        {
            get
            {
                return FunctionSystem.Code.NguyenLieuVatTu;
            }
        }

        protected string CurrentName
        {
            get
            {
                string rs = "Nguyên liệu / vật tư";
                return rs;
            }
        }
        #endregion

        protected bool GetButtonRole(string rightName)
        {
            bool rs = false;

            if (rightName == "Insert") rs = UserHasRight(RightPageName.NLCT_Insert);
            else if (rightName == "Update") rs = UserHasRight(RightPageName.NLCT_Update);
            else if (rightName == "Delete") rs = UserHasRight(RightPageName.NLCT_Delete);

            return rs;
        }
        protected void Page_Load(object sender, EventArgs e)
        {


            topTitle1.Text = breadTitle.Text = string.Format("Danh sách {0}", CurrentName);
            topTitle2.Text = headerTitle.Text = " chi tiết " + CurrentName;

            txtSearchGhiChu.EnterSubmitClientID = txtSearchMaCode.EnterSubmitClientID = txtSearchTenChiTiet.EnterSubmitClientID =
            txtSearchTonToi.EnterSubmitClientID = txtSearchTonTu.EnterSubmitClientID =
            txtSearchViTri.EnterSubmitClientID = btnSearchNow.ClientID;

            txtEditGhiChu.EnterSubmitClientID = txtEditMaCode.EnterSubmitClientID =
            txtSearchTenChiTiet.EnterSubmitClientID = txtEditSoLuongTon.EnterSubmitClientID =
            txtEditViTri.EnterSubmitClientID = btnSearchNow.ClientID;
            btnAddNew.Visible = GetButtonRole("Insert");

            txtSearchGhiChu.MaxTextLength = txtEditGhiChu.MaxTextLength = TblNguyenLieuChiTiet.GhiChuColumn.MaxLength;
            txtSearchTenChiTiet.MaxTextLength = txtEditTenNguyenLieu.MaxTextLength = TblNguyenLieuChiTiet.TenChiTietNguyenLieuColumn.MaxLength;
            txtSearchMaCode.MaxTextLength = txtEditMaCode.MaxTextLength = TblNguyenLieuChiTiet.CodeColumn.MaxLength;
            txtSearchViTri.MaxTextLength = txtEditViTri.MaxTextLength = TblNguyenLieuChiTiet.ViTriTrongKhoColumn.MaxLength;

            if (!IsPostBack)
            {
                List<TblPhongBan> phongBanCol = CategoryManager.GetPhongBanAll(true, true);
                cbEditKho.DataSource = phongBanCol;
                cbEditKho.DataValueField = TblPhongBan.Columns.Id;
                cbEditKho.DataTextField = TblPhongBan.Columns.TenPhongBan;
                cbEditKho.DataBind();


                List<TblPhongBan> phongBanCol2 = new List<TblPhongBan>(phongBanCol);
                {
                    TblPhongBan empty = new TblPhongBan();
                    empty.Id = -1;
                    empty.TenPhongBan = "Tất cả";
                    phongBanCol2.Insert(0, empty);
                }
                cbSearchKho.DataSource = phongBanCol2;
                cbSearchKho.DataValueField = TblPhongBan.Columns.Id;
                cbSearchKho.DataTextField = TblPhongBan.Columns.TenPhongBan;
                cbSearchKho.DataBind();

                LoadNguyenLieu();
                grvData.CurrentSortExpression = TblKhachHang.Columns.Id;
                grvData.CurrentSortDerection = "Desc";
                grvData.Rebind();

                cbbFilterColumns.Items.Clear();
                grvData.FilterColumnsControlClientID = cbbFilterColumns.ClientID;
                for (int i = 0; i < grvData.Columns.Count; i++)
                {
                    ListItem item = new ListItem();
                    item.Value = i.ToString();

                    item.Selected = grvData.CheckColumnVisible(i);
                    if (grvData.Columns[i].HeaderText == "Thao tác")
                        grvData.Columns[i].HeaderText = string.Empty;
                    item.Text = grvData.Columns[i].HeaderText;
                    cbbFilterColumns.Items.Add(item);
                }
            }

            cbbFilterColumns.EmptyMessage = "Chọn tất cả";
            cbbFilterColumns.MultipleSelectedTextFormat = "Có {count} cột được chọn";
        }

        private void LoadKho(ExtraComboBox cb, bool isEmpty)
        {

        }

        private void LoadNguyenLieu()
        {
            List<TblNguyenLieu> nguyenLieuCol = NguyenLieuManager.GetNguyenLieuAll(true);
            cbEditNguyenLieu.DataSource = nguyenLieuCol;
            cbEditNguyenLieu.DataValueField = TblNguyenLieu.Columns.Id;
            cbEditNguyenLieu.DataTextField = TblNguyenLieu.Columns.TenNguyenLieu;
            cbEditNguyenLieu.DataBind();

            if (nguyenLieuCol != null && nguyenLieuCol.Count > 0)
            {
                hiddenDonViTinhRoot.Value = nguyenLieuCol[0].TenVietTat;
                hiddenDonViLuuKho.Value = nguyenLieuCol[0].TenVietTatLuuKho;
            }

            List<TblNguyenLieu> nguyenLieuCol2 = new List<TblNguyenLieu>(nguyenLieuCol);
            TblNguyenLieu all = new TblNguyenLieu();
            all.Id = -1;
            all.TenNguyenLieu = "Tất cả";
            nguyenLieuCol2.Insert(0, all);
            cbSearchNguyenLieu.DataSource = nguyenLieuCol2;
            cbSearchNguyenLieu.DataValueField = TblNguyenLieu.Columns.Id;
            cbSearchNguyenLieu.DataTextField = TblNguyenLieu.Columns.TenNguyenLieu;
            cbSearchNguyenLieu.DataBind();
        }

        private void RefreshDetail()
        {
            btnAddNewSecond.Visible = false;
            txtEditGhiChu.Text = txtEditMaCode.Text = txtEditTenNguyenLieu.Text = txtEditViTri.Text = string.Empty;
            txtEditSoLuongTon.Text = "0";
            dateNgayHetHan.DateValue = DateTime.MinValue;
            cbEditNguyenLieu.SelectedIndex = 0;
            hiddenEditID.Value = string.Empty;
            lbDonViTinh.Text = hiddenDonViTinhRoot.Value;
            lbDonViLuuKho.Text = hiddenDonViLuuKho.Value;
        }

        protected void btnAddNew_Click(object sender, EventArgs e)
        {
            RefreshDetail();
            btnAddNewSecond.Visible = true;
            txtEditTenNguyenLieu.Focus();

            lbDetail.Text = "Thêm mới chi tiết nguyên liệu";
            dialogDetail.ShowDialog(true);
        }

        protected void grvData_NeedDataSource(object sender, ExtraControls.ExtraGridEventArg e)
        {
            ExtraGrid grv = sender as ExtraGrid;
            if (grv != null)
            {
                int rowIndex = (grv.CurrentPageIndex - 1) * grv.CurrentPageSize;
                int pageSize = grv.CurrentPageSize;

                int ogr = -1;

                string filterClause1 = "";
                string filterClause = string.Empty;
                string filterClauseExtend = string.Empty;
                string orderClause = string.Format(" chiTiet.{0} desc ", TblNguyenLieuChiTiet.Columns.Id);
                if (!string.IsNullOrEmpty(grv.CurrentSortExpression))
                    orderClause = string.Format("{0} {1}",
                                                grv.CurrentSortExpression,
                                                grv.CurrentSortDerection);
                #region set value for column
                string column = string.Format(" chiTiet.*, nl.{0}, dvt.{1},dvt.{2},dvt2.{1} as TenVietTatLuuKho ,dvt2.{2} as TenDonViLuuKho,kho.TenPhongBan ",
                                                TblNguyenLieu.Columns.TenNguyenLieu,
                                                TblDonViTinh.Columns.TenVietTat,
                                                TblDonViTinh.Columns.TenDonVi);

                #endregion

                #region set value for filter clause
                if (!string.IsNullOrEmpty(txtSearchGhiChu.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieuChiTiet.Columns.GhiChu,
                                                  txtEditGhiChu.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchMaCode.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieuChiTiet.Columns.Code,
                                                  txtEditMaCode.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchTenChiTiet.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieuChiTiet.Columns.TenChiTietNguyenLieu,
                                                  txtSearchTenChiTiet.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (!string.IsNullOrEmpty(txtSearchViTri.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} like N'%{1}%' AND ",
                                                  TblNguyenLieuChiTiet.Columns.ViTriTrongKho,
                                                  txtSearchViTri.Text.Trim().Replace("\'", "\'\'").Replace("%", "[%]"));
                if (cbSearchNguyenLieu.HasValue)
                    filterClause += string.Format(" chiTiet.{0} = {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                                  cbSearchNguyenLieu.SelectedValue);
                if (cbSearchKho.HasValue)
                    filterClause += string.Format(" chiTiet.{0} = {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.IDKho,
                                                  cbSearchKho.SelectedValue);
                if (!string.IsNullOrEmpty(txtSearchSoLuongLuuKhoTu.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} >= {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                                  txtSearchSoLuongLuuKhoTu.Text.Trim().Replace(".", "").Replace(",", "."));
                if (!string.IsNullOrEmpty(txtSearchSoLuongLuuKhoToi.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} <= {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                                  txtSearchSoLuongLuuKhoToi.Text.Trim().Replace(".", "").Replace(",", "."));

                if (!string.IsNullOrEmpty(txtSearchTonTu.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} >= {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.TonCuoi,
                                                  txtSearchTonTu.Text.Trim().Replace(".", "").Replace(",", "."));
                if (!string.IsNullOrEmpty(txtSearchTonToi.Text.Trim()))
                    filterClause += string.Format(" chiTiet.{0} <= {1} AND ",
                                                  TblNguyenLieuChiTiet.Columns.TonCuoi,
                                                  txtSearchTonToi.Text.Trim().Replace(".", "").Replace(",", "."));
                if (dateHetHanTu.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" chiTiet.{0} >='{0}' AND ",
                                                  TblNguyenLieuChiTiet.Columns.NgayHetHan,
                                                  dateHetHanTu.DateValue.ToString("yyyy-MM-dd 00:00:00"));
                if (dateHetHanToi.DateValue != DateTime.MinValue)
                    filterClause += string.Format(" chiTiet.{0} <='{0}' AND ",
                                                  TblNguyenLieuChiTiet.Columns.NgayHetHan,
                                                  dateHetHanToi.DateValue.ToString("yyyy-MM-dd 23:59:59"));
                #endregion

                filterClause = filterClause.Trim();
                if (filterClause.EndsWith("AND"))
                    filterClause = filterClause.Substring(0, filterClause.Length - 3);
                grv.BindingWithFiltering = !string.IsNullOrEmpty(filterClause.Trim());
                filterClause = (filterClause1 + filterClause.Trim()).Trim();

                string tableJoin = string.Format(@" left join {0} as nl on chiTiet.{1} = nl.Id 
                                                    left join {2} as dvt on nl.{3} = dvt.Id 
                                                    left join {2} as dvt2 on nl.{4} = dvt2.Id 
                                                    left join {5} as kho on chiTiet.{6} = kho.Id ",
                                                    TblNguyenLieu.Schema.TableName, TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                                    TblDonViTinh.Schema.TableName, TblNguyenLieu.Columns.IDDonViTinh, TblNguyenLieu.Columns.IDDonViLuuKho,
                                                    TblPhongBan.Schema.TableName, TblNguyenLieuChiTiet.Columns.IDKho);

                int total = 0;
                List<TblNguyenLieuChiTiet> lst = NguyenLieuManager.SearchNguyenLieuChiTiet(rowIndex, pageSize, column, tableJoin,
                                                                       filterClause, orderClause, out total);

                grv.DataSource = lst;
                List<TblNguyenLieuChiTiet> lstEmpty = new List<TblNguyenLieuChiTiet>();
                lstEmpty.Add(new TblNguyenLieuChiTiet());
                grv.DataSourceEmpty = lstEmpty;
                grv.VirtualRecordCount = total;
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            txtSearchGhiChu.Text = txtSearchMaCode.Text = txtSearchTenChiTiet.Text = txtSearchTonToi.Text =
            txtSearchTonTu.Text = txtSearchViTri.Text = txtSearchSoLuongLuuKhoTu.Text = txtSearchSoLuongLuuKhoToi.Text = string.Empty;
            cbSearchNguyenLieu.SelectedIndex = cbSearchKho.SelectedIndex = 0;
            grvData.Rebind();
        }
        protected void btnSearchNow_Click(object sender, EventArgs e)
        {
            grvData.CurrentPageIndex = 1;
            grvData.Rebind();
            dialogSearch.CloseDialog();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmd = btn.CommandArgument;
                if (!string.IsNullOrEmpty(cmd))
                {
                    TblNguyenLieuChiTiet chiTiet = NguyenLieuManager.GetNguyenLieuChiTietByID(cmd);
                    if (chiTiet != null)
                    {
                        hiddenEditID.Value = chiTiet.Id.ToString();
                        txtEditGhiChu.Text = chiTiet.GhiChu;
                        txtEditMaCode.Text = chiTiet.Code;
                        txtEditSoLuongTon.Text = chiTiet.TonCuoi.Value.ExecptionMod().ToStringWithLanguage();
                        txtEditSoLuongLuuKho.Text = chiTiet.SoLuongLuuKho.Value.ExecptionMod().ToStringWithLanguage();
                        txtEditTenNguyenLieu.Text = chiTiet.TenChiTietNguyenLieu;
                        txtEditViTri.Text = chiTiet.ViTriTrongKho;
                        ListItem item = cbEditNguyenLieu.Items.FindByValue(chiTiet.IDNguyenLieu.Value.ToString());
                        if (item != null)
                        {
                            cbEditNguyenLieu.SelectedValue = chiTiet.IDNguyenLieu.Value.ToString();
                            lbDonViTinh.Text = chiTiet.TenVietTat;
                            lbDonViLuuKho.Text = chiTiet.TenVietTatLuuKho;
                        }

                        if (chiTiet.IDKho.HasValue)
                        {
                            ListItem itemKho = cbEditKho.Items.FindByValue(chiTiet.IDKho.Value.ToString());
                            if (itemKho != null) cbEditKho.SelectedValue = chiTiet.IDKho.Value.ToString();
                        }



                        #region
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.NLCT_Update,
                                                     string.Format("Cập nhật chi tiết nguyên liệu vật tư <b>{0} - {1}</b>", chiTiet.Code, chiTiet.TenChiTietNguyenLieu));
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Tên chi tiết",
                                             TblNguyenLieuChiTiet.Columns.TenChiTietNguyenLieu,
                                             chiTiet.TenChiTietNguyenLieu);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Mã code",
                                             TblNguyenLieuChiTiet.Columns.Code,
                                             chiTiet.Code);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Tên nguyên liệu / vật tư",
                                             TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                             cbEditNguyenLieu.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Kho",
                                             TblNguyenLieuChiTiet.Columns.IDKho,
                                             cbEditKho.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Tồn đầu",
                                             TblNguyenLieuChiTiet.Columns.TonDau,
                                             chiTiet.TonDau.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Tồn cuối",
                                             TblNguyenLieuChiTiet.Columns.TonCuoi,
                                             chiTiet.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Số lượng lưu kho",
                                             TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                             chiTiet.SoLuongLuuKho.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Vị trí trong kho",
                                             TblNguyenLieuChiTiet.Columns.ViTriTrongKho,
                                             chiTiet.ViTriTrongKho);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Update, "Ghi chú",
                                             TblNguyenLieuChiTiet.Columns.GhiChu,
                                             chiTiet.GhiChu);
                        #endregion
                        dialogDetail.ShowDialog();
                    }
                    else
                        ShowNotify("Thông báo", "Không tìm thấy thông tin nguyên vật liệu (2)", NotifyType.Error);
                }
                else
                    ShowNotify("Thông báo", "Không tìm thấy thông tin nguyên vật liệu", NotifyType.Error);
            }
        }

        private void Save(bool forceCheckCode)
        {
            #region Validation
            if (string.IsNullOrEmpty(txtEditTenNguyenLieu.Text))
            {
                AddValidPromt(txtEditTenNguyenLieu.ClientID, "Nhập tên chi tiết");
                txtEditTenNguyenLieu.Focus();
            }

            decimal soLuongLuuKho = 0;
            if (string.IsNullOrEmpty(txtEditSoLuongLuuKho.Text))
            {
                AddValidPromt(txtEditSoLuongLuuKho.ClientID, "Nhập số lượng lưu kho");
                txtEditSoLuongLuuKho.Focus();
            }
            else
            {
                if (!decimal.TryParse(txtEditSoLuongLuuKho.Text.Trim(), out soLuongLuuKho) || soLuongLuuKho < 1)
                {
                    AddValidPromt(txtEditSoLuongLuuKho.ClientID, "Số lượng lưu kho phải lớn hơn 0.");
                    txtEditSoLuongLuuKho.Focus();
                }
            }

            decimal soTon = 0;
            if (string.IsNullOrEmpty(txtEditSoLuongTon.Text))
            {
                AddValidPromt(txtEditSoLuongTon.ClientID, "Nhập số lượng tồn");
                txtEditSoLuongTon.Focus();
            }
            else
            {
                if (!decimal.TryParse(txtEditSoLuongTon.Text.Trim(), out soTon) || soTon < 0)
                {
                    AddValidPromt(txtEditSoLuongTon.ClientID, "Số lượng tồn không âm.");
                    txtEditSoLuongTon.Focus();
                }
            }

            if (!cbEditKho.HasValue)
            {
                AddValidPromt(cbEditKho.ClientID, "Chọn kho");
                cbEditKho.Focus();
            }

            if (string.IsNullOrEmpty(txtEditViTri.Text))
            {
                AddValidPromt(txtEditViTri.ClientID, "Nhập thông tin vị trí trong kho");
                txtEditViTri.Focus();
            }

            if (!cbEditNguyenLieu.HasValue)
            {
                AddValidPromt(cbEditNguyenLieu.ClientID, "Chọn loại nguyên liệu");
                cbEditNguyenLieu.Focus();
            }

            if (string.IsNullOrEmpty(txtEditMaCode.Text) && !forceCheckCode)
            {
                btnSubmitSave.Visible = true;
                btnDelete.Visible = false;
                MessageBoxResult result = new MessageBoxResult();
                result.Value = "OK";
                result.Submit = true;

                CurrentMessageBoxResult = result;
                OpenMessageBox(string.Format("Tạo mã nguyên liệu / vật tư"),
                               string.Format("Mã nguyên liệu / vật tư chưa có, bạn muốn hệ thống tự động sinh mã mới ?"));
                return;
            }
            #endregion

            if (PageIsValid)
            {
                if (!string.IsNullOrEmpty(hiddenEditID.Value))
                {
                    #region Update
                    TblNguyenLieuChiTiet chiTiet = NguyenLieuManager.GetNguyenLieuChiTietByID(hiddenEditID.Value);
                    if (chiTiet != null)
                    {
                        chiTiet.TenChiTietNguyenLieu = txtEditTenNguyenLieu.Text;
                        decimal soLuongCachBiet = soTon - chiTiet.TonCuoi.Value;
                        if (soLuongCachBiet != 0)
                        {
                            chiTiet.TonDau = chiTiet.TonCuoi;
                            chiTiet.TonCuoi = soTon;
                        }
                        chiTiet.SoLuongLuuKho = soLuongLuuKho;
                        chiTiet.IDNguyenLieu = int.Parse(cbEditNguyenLieu.SelectedValue);
                        if (dateNgayHetHan.DateValue != DateTime.MinValue)
                            chiTiet.NgayHetHan = dateNgayHetHan.DateValue;
                        chiTiet.ViTriTrongKho = txtEditViTri.Text;
                        chiTiet.GhiChu = txtEditGhiChu.Text;
                        chiTiet.Code = txtEditMaCode.Text;
                        chiTiet.IDKho = int.Parse(cbEditKho.SelectedValue);
                        if (string.IsNullOrEmpty(chiTiet.Code))
                            chiTiet.Code = string.Format("MK-{0}", chiTiet.Id);
                        chiTiet.NgayCapNhat = DateTime.Now;
                        chiTiet.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        chiTiet = NguyenLieuManager.UpdateNguyenLieuChiTiet(chiTiet);

                        #region Log
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                         TblNguyenLieuChiTiet.Columns.TenChiTietNguyenLieu,
                                         txtEditTenNguyenLieu.Text);
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.Code,
                                        txtEditMaCode.Text);
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                        cbEditNguyenLieu.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                       TblNguyenLieuChiTiet.Columns.IDKho,
                                       cbEditKho.DisplayText);
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.TonDau,
                                        chiTiet.TonDau.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.TonCuoi,
                                        chiTiet.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                       TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                       chiTiet.SoLuongLuuKho.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.NgayHetHan,
                                        chiTiet.NgayHetHan.HasValue && chiTiet.NgayHetHan.Value != DateTime.MinValue ? chiTiet.NgayHetHan.Value.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.ViTriTrongKho,
                                        txtEditViTri.Text);
                        CurrentLog.AddLastValue(RightPageName.NLCT_Update,
                                        TblNguyenLieuChiTiet.Columns.GhiChu,
                                        txtEditGhiChu.Text);
                        CurrentLog.SaveLog(RightPageName.NLCT_Update);
                        #endregion

                        ShowNotify("Thông báo", "Cập nhật thông tin chi tiết nguyên liệu / vật tư thành công", NotifyType.Success);
                        RunScript("DialogCloseAll", string.Empty);
                        grvData.Rebind();
                    }
                    #endregion
                }
                else
                {
                    #region Insert
                    TblNguyenLieuChiTiet chiTiet = new TblNguyenLieuChiTiet();
                    if (chiTiet != null)
                    {
                        chiTiet.TenChiTietNguyenLieu = txtEditTenNguyenLieu.Text;
                        chiTiet.TonDau = 0;
                        chiTiet.TonCuoi = soTon;
                        chiTiet.SoLuongLuuKho = soLuongLuuKho;
                        chiTiet.IDNguyenLieu = int.Parse(cbEditNguyenLieu.SelectedValue);
                        chiTiet.IDKho = int.Parse(cbEditKho.SelectedValue);
                        if (dateNgayHetHan.DateValue != DateTime.MinValue)
                            chiTiet.NgayHetHan = dateNgayHetHan.DateValue;
                        chiTiet.ViTriTrongKho = txtEditViTri.Text;
                        chiTiet.GhiChu = txtEditGhiChu.Text;
                        chiTiet.Code = txtEditMaCode.Text;
                        chiTiet.NgayCapNhat = DateTime.Now;
                        chiTiet.NguoiCapNhat = ApplicationContext.Current.CommonUser.UserName;
                        chiTiet = NguyenLieuManager.InsertNguyenLieuChiTiet(chiTiet);
                        if (string.IsNullOrEmpty(chiTiet.Code))
                        {
                            chiTiet.Code = string.Format("MK-{0}", chiTiet.Id);
                            chiTiet = NguyenLieuManager.UpdateNguyenLieuChiTiet(chiTiet);
                        }
                        #region Log
                        CurrentLog.Clear();
                        CurrentLog.AddNewLogFunction(RightPageName.NLCT_Insert,
                                                     string.Format("Thêm mới chi tiết nguyên liệu / vật tư <b>[{0}] - [1]</b>", chiTiet.Code, chiTiet.TenChiTietNguyenLieu));

                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Tên chi tiết nguyên vật liệu",
                                         TblNguyenLieuChiTiet.Columns.TenChiTietNguyenLieu,
                                         txtEditTenNguyenLieu.Text);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Mã",
                                        TblNguyenLieuChiTiet.Columns.Code,
                                        txtEditMaCode.Text);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Tên nguyên liệu",
                                        TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                        cbEditNguyenLieu.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Kho",
                                        TblNguyenLieuChiTiet.Columns.IDKho,
                                        cbEditKho.DisplayText);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Tồn đầu",
                                        TblNguyenLieuChiTiet.Columns.TonDau,
                                        chiTiet.TonDau.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Tồn cuối",
                                        TblNguyenLieuChiTiet.Columns.TonCuoi,
                                        chiTiet.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Số lượng lưu kho",
                                 TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                 chiTiet.SoLuongLuuKho.Value.ExecptionMod().ToStringWithLanguage());
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Ngày hết hạn",
                                        TblNguyenLieuChiTiet.Columns.NgayHetHan,
                                        chiTiet.NgayHetHan.HasValue && chiTiet.NgayHetHan.Value != DateTime.MinValue ? chiTiet.NgayHetHan.Value.ToString("dd/MM/yyyy") : "");
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Vị trí trong kho",
                                        TblNguyenLieuChiTiet.Columns.ViTriTrongKho,
                                        txtEditViTri.Text);
                        CurrentLog.AddFirstValue(RightPageName.NLCT_Insert, "Ghi chú",
                                        TblNguyenLieuChiTiet.Columns.GhiChu,
                                        txtEditGhiChu.Text);
                        CurrentLog.SaveLog(RightPageName.NLCT_Insert);
                        #endregion

                        ShowNotify("Thông báo", "Thêm mới chi tiết nguyên liệu / vật tư thành công", NotifyType.Success);
                        RunScript("DialogCloseAll", string.Empty);
                        grvData.Rebind();
                    }
                    #endregion
                }
            }

            if (!PageIsValid) ShowValidPromt();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            Save(false);
        }

        protected void btnSubmitSave_Click(object sender, EventArgs e)
        {
            Save(true);
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            bool resultDelete = false;
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                string cmdName = btn.CommandName;
                if (cmdName == "exec_delete")
                {

                    if (CurrentMessageBoxResult.Submit && CurrentMessageBoxResult.Value != null)
                    {

                        TblNguyenLieuChiTiet chiTiet = CurrentMessageBoxResult.Value as TblNguyenLieuChiTiet;
                        if (chiTiet != null)
                        {
                            string result = "";
                            try
                            {
                                CurrentLog.AddNewLogFunction(RightPageName.NLCT_Delete,
                                                             string.Format("Xóa chi tiết nguyên vật liệu <b>{0} - {1}</b> ", chiTiet.Code, chiTiet.TenChiTietNguyenLieu));
                                CurrentLog.AddFirstValue(RightPageName.NLCT_Delete, string.Format("Tên chi tiết nguyên vật liệu"),
                                                         TblNguyenLieuChiTiet.Columns.TenChiTietNguyenLieu,
                                                         chiTiet.TenChiTietNguyenLieu);
                                CurrentLog.AddFirstValue(RightPageName.NLCT_Delete, string.Format("Tên nguyên liệu"),
                                                         TblNguyenLieuChiTiet.Columns.IDNguyenLieu,
                                                         chiTiet.TenNguyenLieu);
                                CurrentLog.AddFirstValue(RightPageName.NLCT_Delete, string.Format("Số lượng tồn"),
                                                         TblNguyenLieuChiTiet.Columns.TonCuoi,
                                                         chiTiet.TonCuoi.Value.ExecptionMod().ToStringWithLanguage());
                                CurrentLog.AddFirstValue(RightPageName.NLCT_Delete, "Kho",
                                                        TblNguyenLieuChiTiet.Columns.IDKho,
                                                        chiTiet.TenPhongBan);
                                CurrentLog.AddFirstValue(RightPageName.NLCT_Delete, "Số lượng lưu kho",
                                                        TblNguyenLieuChiTiet.Columns.SoLuongLuuKho,
                                                        chiTiet.SoLuongLuuKho.Value.ExecptionMod().ToStringWithLanguage());
                                NguyenLieuManager.DeleteNguyenLieuChiTiet(chiTiet.Id);
                                resultDelete = true;
                            }
                            catch
                            {
                            }
                            if (resultDelete)
                            {
                                grvData.Rebind();
                                ShowNotify(string.Format("Xóa chi tiết nguyên liệu thành công"),
                                            string.Format("Bạn vừa xóa chi tiết nguyên liệu <b>[{0}] - {1}</b>",
                                                          chiTiet.Code, chiTiet.TenChiTietNguyenLieu),
                                            NotifyType.Success);
                                CurrentLog.SaveLog(RightPageName.NLCT_Delete);
                            }
                            else
                            {
                                ShowNotify(string.Format("Xóa {0} thất bại. {2}", CurrentName),
                                            string.Format("Hệ thống không cho phép xóa chi tiết nguyên/vật liệu {1} <b>{0}</b>. Vui lòng kiểm tra lại dữ liệu hệ thống.",
                                                           chiTiet.TenChiTietNguyenLieu, CurrentName, result),
                                            NotifyType.Error);
                            }
                            CloseMessageBox();
                        }
                    }

                }
                else
                {
                    TblNguyenLieuChiTiet chiTiet = NguyenLieuManager.GetNguyenLieuChiTietByID(btn.CommandArgument);
                    if (chiTiet != null)
                    {
                        btnSubmitSave.Visible = false;
                        btnDelete.Visible = true;
                        MessageBoxResult result = new MessageBoxResult();
                        result.Value = chiTiet;
                        result.Submit = true;
                        result.CommandName = "delete_nguyenlieu";
                        CurrentMessageBoxResult = result;
                        OpenMessageBox(string.Format("Xóa {0}", CurrentName),
                                       string.Format("Bạn thật sự muốn xóa chi tiết nguyên vật liệu {1} <b>[{0}] - {1}</b> ?",
                                                     chiTiet.Code, chiTiet.TenChiTietNguyenLieu, CurrentName));
                    }
                }

            }
        }

        protected void cbEditNguyenLieu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbEditNguyenLieu.HasValue)
            {
                TblNguyenLieu nguyenLieu = NguyenLieuManager.GetNguyenLieuByID(cbEditNguyenLieu.SelectedValue);
                if (nguyenLieu != null)
                {
                    lbDonViTinh.Text = nguyenLieu.TenVietTat;
                    lbDonViLuuKho.Text = nguyenLieu.TenVietTatLuuKho;
                }
            }
        }
    }
}