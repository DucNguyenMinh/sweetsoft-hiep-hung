﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetSoft.ExtraControls.html
{
    internal class HtmlAttribute
    {
        public string Name { get; private set; }
        public string Value { get; private set; }
        private bool? _isRendered;
        public bool IsRendered
        {
            get
            {
                if (_isRendered.HasValue)
                    return _isRendered.Value;

                return !string.IsNullOrEmpty(Value);
            }
        }

        public HtmlAttribute(string name, object value)
        {
            this.Name = name;
            this.Value = value.ToString();
            this._isRendered = true;
        }

        public HtmlAttribute(string name, string value, bool? isRendered)
        {
            this.Name = name;
            this.Value = value;
            this._isRendered = isRendered;
        }

        public HtmlAttribute(string name, object value, bool? isRendered)
            : this(name, (value == null) ? String.Empty : value.ToString().ToLower(), isRendered)
        { }

    }

    internal class HtmlIdAttribute : HtmlAttribute
    {
        public HtmlIdAttribute(string value) : base("id", value, null) { }
        public HtmlIdAttribute(object value) : base("id", (value == null) ? String.Empty : value.ToString().ToLower(), null) { }
    }

    internal class HtmlTypeAttribute : HtmlAttribute
    {
        public HtmlTypeAttribute(string value) : base("type", value, null) { }
        public HtmlTypeAttribute(object value) : base("type", (value == null) ? String.Empty : value.ToString().ToLower(), null) { }
    }

    internal class HtmlNameAttribute : HtmlAttribute
    {
        public HtmlNameAttribute(string value) : base("name", value, null) { }
        public HtmlNameAttribute(object value) : base("name", (value == null) ? String.Empty : value.ToString().ToLower(), null) { }
    }

    internal class HtmlClassAttribute : HtmlAttribute
    {
        public HtmlClassAttribute(string value) : base("class", value, null) { }
        public HtmlClassAttribute(object value) : base("class", (value == null) ? String.Empty : value.ToString(), null) { }
        public HtmlClassAttribute(params object[] values) : base("class", (values.Length == 0) ? String.Empty : BuildClass(values), null) { }

        public static string BuildClass(params object[] classNames)
        {
            StringBuilder classes = new StringBuilder();

            foreach (object o in classNames)
            {
                string str = o.ToString();
                if (!string.IsNullOrEmpty(str))
                    classes.Append(string.Format("{0} ", str));
            }

            if (classes.Length > 0)
                classes.Remove(classes.Length - 1, 1);

            return classes.ToString();
        }
    }

    internal class HtmlHrefAttribute : HtmlAttribute
    {
        public HtmlHrefAttribute(string value) : base("href", value, null) { }
        public HtmlHrefAttribute(object value) : base("href", (value == null) ? String.Empty : value.ToString().ToLower(), null) { }
    }
}
