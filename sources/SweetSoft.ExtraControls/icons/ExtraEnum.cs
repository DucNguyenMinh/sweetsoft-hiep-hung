﻿using SweetSoft.ExtraControls.html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetSoft.ExtraControls.icons
{
    public enum ButtonGroupSize
    {
        [Render("")]
        Default,
        [Render("btn-group-lg")]
        Large,
        [Render("btn-group-sm")]
        Small,
        [Render("btn-group-xs")]
        ExtraSmall
    }

    public enum DateTimeMask {
        [Render("")]
        None,
        [Render("None")]
        Date,
        [Render("Full")]
        DateTimeFull,
        [Render("Half")]
        DateTimeHalf,
    }

    public enum InputStyle
    {
        [Render("")]
        Default,
        [Render("has-success")]
        Success,
        [Render("has-warning")]
        Warning,
        [Render("has-error")]
        Error
    }

    public enum InputSize
    {
        [Render("")]
        Default,
        [Render("input-lg")]
        Large,
        [Render("input-sm")]
        Small
    }

    public enum ButtonSize
    {
        [Render("")]
        Default,
        [Render("btn-xs")]
        ExtraSmall,
        [Render("btn-sm")]
        Small,
        [Render("btn-lg")]
        Large
    }

    public enum IconColor
    {
        [Render("")]
        Default,
        [Render("icon-primary")]
        Primary,
        [Render("icon-success")]
        Success,
        [Render("icon-info")]
        Info,
        [Render("icon-warning")]
        Warning,
        [Render("icon-danger")]
        Danger
    }

    public enum ButtonStyle
    {
        [Render("")]
        None,
        [Render("btn-default")]
        Default,
        [Render("btn-primary")]
        Primary,
        [Render("btn-success")]
        Success,
        [Render("btn-info")]
        Info,
        [Render("btn-warning")]
        Warning,
        [Render("btn-danger")]
        Danger,
        [Render("btn-link")]
        Link,
        [Render("btn-violet")]
        Violet,
        [Render("btn-purple")]
        Purple,
        [Render("btn-default btn-white btn-bold")]
        Default_Bold,
        [Render("btn-primary btn-white btn-bold")]
        Primary_Bold,
        [Render("btn-success btn-white btn-bold")]
        Success_Bold,
        [Render("btn-info btn-white btn-bold")]
        Info_Bold,
        [Render("btn-warning btn-white btn-bold")]
        Warning_Bold,
        [Render("btn-danger btn-white btn-bold")]
        Danger_Bold,
        [Render("btn-link btn-white btn-bold")]
        Link_Bold,
        [Render("btn-violet btn-white btn-bold")]
        Violet_Bold,
        [Render("btn-white btn-purple btn-bold")]
        Purple_Bold,
        [Render("btn btn-primary btn-white dropdown-toggle")]
        Dropdown_White
    }

    public enum IconPosition { Left, Right }

    public enum DialogEffect
    {
        [Render("")]
        None,
        [Render("fade")]
        FadeIn,
        [Render("fade")]
        FadeOut,
    }

    public enum DialogSize
    {
        [Render("modal-fullscreen")]
        FullScreen,
        [Render("modal-sm")]
        Small,
        [Render("modal-lg")]
        Large,
        [Render("modal-normal")]
        Normal
    }

    public enum DialogStyle
    {
        [Render("type-default")]
        Default,
        [Render("type-primary")]
        Primary,
        [Render("type-success")]
        Success,
        [Render("type-info")]
        Info,
        [Render("type-warning")]
        Warning,
        [Render("type-danger")]
        Danger,
        [Render("type-violet")]
        Violet,
    }

    public enum DialogToolButton
    {
        [Render("fa fa-times")]
        Close,
        [Render("flaticon-checkbox3")]
        Maximize,
        [Render("flaticon-copy21")]
        Minimize,
        [Render("fa fa-question")]
        Help
    }

    public enum ExtraStyle
    {
        [Render("default")]
        Default,
        [Render("primary")]
        Primary,
        [Render("success")]
        Success,
        [Render("info")]
        Info,
        [Render("warning")]
        Warning,
        [Render("danger")]
        Danger,
        [Render("violet")]
        Violet,
    }

    public enum CheckBoxSize
    {
        [Render("large")]
        Large,
        [Render("primary")]
        Primary,
        [Render("normal")]
        Normal,
        [Render("small")]
        Small,
    }

    public enum CheckBoxType
    {
        [Render("")]
        Normal,
        [Render("cmn-toggle-round")]
        Toggle,
        [Render("ace ace-switch ace-switch-1")]
        Switch1,
        [Render("ace ace-switch ace-switch-2")]
        Switch2,
        [Render("ace ace-switch ace-switch-3")]
        Switch3,
        [Render("ace ace-switch ace-switch-4")]
        Switch4,
        [Render("ace ace-switch ace-switch-5")]
        Switch5,
        [Render("ace ace-switch ace-switch-6")]
        Switch6,
        [Render("ace ace-switch ace-switch-7")]
        Switch7,
        [Render("ace ace-switch btn-rotate")]
        Switch8
    }



    public enum CheckBoxBorder
    {
        [Render("b-round")]
        Round,
        [Render("b-square")]
        Square,
    }

    public enum SkinName
    {
        [Render("sk-violet")]
        Violet,
        [Render("sk-Success")]
        Green,
        [Render("sk-Success-Bold")]
        Green_Bold,
    }

    public enum DateTimeStyle
    {
        [Render("")]
        Defaut,
        [Render("dt-violet")]
        Violet,
        [Render("dt-success")]
        Success,
        [Render("dt-success-bold")]
        Success_Bold
    }

    public enum TextBoxStyle
    {
        [Render("")]
        Default,
        [Render("text-violet")]
        Violet,
        [Render("text-success")]
        Success
    }

    public enum ValidationStyle
    {
        [Render("tooltip")]
        Tooltip,
        [Render("inline")]
        Inline,
    }

    public enum ExtraTheme
    {
        [Render("ACE")]
        ACE,
        [Render("MaterialLTE")]
        MaterialLTE
    }
}

