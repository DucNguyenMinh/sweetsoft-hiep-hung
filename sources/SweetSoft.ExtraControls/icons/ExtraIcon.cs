﻿using SweetSoft.ExtraControls.html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetSoft.ExtraControls.icons
{
    public enum ExtraIcon
    {

        [Render("")]
        None,
        [Render("fa fa-plus-square-o")]
        A_Add_Square,
        [Render("fa fa-paperclip")]
        A_Attachment,
        [Render("fa fa-plus")]
        A_Add,
        [Render("fa fa-eye")]
        A_Eye,
        [Render("fa fa-barcode")]
        A_Barcode,
        [Render("fa fa-check")]
        A_Check,
        [Render("fa fa-cloud-download")]
        A_Download,
        [Render("fa fa-times")]
        A_Close,
        [Render("fa fa-bar-chart")]
        A_Chart,
        [Render("fa fa-calculator")]
        A_Calculator,
        [Render("fa fa-calendar")]
        A_Calendar,
        [Render("fa fa-clone")]
        A_Clone,
        [Render("fa fa-cog")]
        A_Config,
        [Render("fa a-cogs")]
        A_Configs,
        [Render("flaticon-calendar146")]
        F_Calendar_Date,
        [Render("flaticon-calendar68")]
        F_Calendar_DateTime,
        [Render("flaticon-clock96")]
        F_Calendar_Time,
        [Render("fa fa-ban")]
        A_Cancel,
        [Render("fa fa-trash-o")]
        A_Delete,
        [Render("fa fa-delicious")]
        A_Delicious,
        [Render("fa fa-pencil-square-o")]
        A_Edit,
        [Render("fa fa-exchange")]
        A_Exchange,
        [Render("fa fa-exclamation")]
        A_Exclamation,
        [Render("fa fa-file-excel-o")]
        A_Excel,
        [Render("fa fa-filter")]
        A_Filter,
        [Render("fa fa-file-o")]
        A_File,
        [Render("fa fa-flask")]
        A_Flask,
        [Render("fa fa-history")]
        A_History,
        [Render("fa fa-key")]
        A_Key,
        [Render("fa fa-list-ul")]
        A_List,
        [Render("fa fa-lock")]
        A_Locked,
        [Render("fa fa-envelope-o")]
        A_Mail,
        [Render("fa fa-folder-open-o")]
        A_Open,
        [Render("fa fa-picture-o")]
        A_Picture,
        [Render("fa fa-file-pdf-o")]
        A_Pdf,
        [Render("fa fa-refresh")]
        A_Refresh,
        [Render("fa fa-share")]
        A_Reply,
        [Render("fa fa-reply-all fa-flip-horizontal")]
        A_ReplyAll,
        [Render("fa fa-share-alt")]
        A_Share,
        [Render("fa fa-paper-plane-o")]
        A_Send,
        [Render("fa fa-search")]
        A_Search,
        [Render("fa fa-sign-in")]
        A_Sign_In,
        [Render("fa fa-sign-out")]
        A_Sign_Out,
        [Render("fa fa-check")]
        A_Submit,
        [Render("fa fa-floppy-o")]
        A_Save,
        [Render("fa fa-sign-in")]
        A_SignIn,
        [Render("fa fa-sign-out")]
        A_SignOut,
        [Render("fa fa-thumb-tack")]
        A_Sticked,
        [Render("fa fa-th")]
        A_Th,
        [Render("fa fa-retweet")]
        A_Retweet,
        [Render("fa fa-users")]
        A_Users,
        [Render("fa fa-unlock-alt")]
        A_Unlocked,
        [Render("fa fa-undo")]
        A_Undo,
        [Render("fa fa-print")]
        A_Print,
        [Render("fa fa-cloud-upload")]
        A_Upload,
    }
}
