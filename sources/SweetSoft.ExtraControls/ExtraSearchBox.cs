﻿using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
    [System.Security.Permissions.PermissionSet(System.Security.Permissions.SecurityAction.Demand, Name = "FullTrust")]
    public class ExtraSearchBox : WebControl, INamingContainer
    {
        public event TagCloseEvent TagClosed;
        private ITemplate popupTemplate;

        #region Properties
        public ExtraIcon cr_buttonSearchIcon = ExtraIcon.A_Search;
        public ExtraIcon SearchIcon { get { return cr_buttonSearchIcon; } set { cr_buttonSearchIcon = value; } }

        private ButtonStyle cr_Button_Style = ExtraSkinManager.BUTTON_STYLE;
        public ButtonStyle ButtonStyle { get { return cr_Button_Style; } set { cr_Button_Style = value; } }

        private Unit cr_dropdown_widht = 300;
        public Unit DropDownWidth { get { return cr_dropdown_widht; } set { cr_dropdown_widht = value; } }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate PopupTemplate
        {
            get { return popupTemplate; }
            set { popupTemplate = value; }
        }

        private List<SearchTagItem> cr_items = new List<SearchTagItem>();
        public List<SearchTagItem> TagItems
        {
            get
            {

                if (this.ViewState["TagSearch"] == null) this.ViewState["TagSearch"] = new List<SearchTagItem>();
                return this.ViewState["TagSearch"] as List<SearchTagItem>;
            }
            set
            {
                this.ViewState["TagSearch"] = value;
                onLoad = false;
                CreateChildControls();
            }
        }


        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //     this.Page.RegisterRequiresRaiseEvent(this);
            ExtraScriptRegister.RegisterSearchBox = true;

            //       CreateChildControls();
        }

        private bool onLoad = false;
        protected override void CreateChildControls()
        {
            if (!onLoad)
            {
                Controls.Clear();
                if (TagItems.Count > 0)
                {
                    HtmlGenericControl divRow = new HtmlGenericControl("div");
                    divRow.Attributes.Add("class", "tagbox row");
                    for (int i = 0; i < TagItems.Count; i++)
                    {
                        SearchTagItem item = TagItems[i];
                        if (item.Status)
                        {
                            HtmlGenericControl div = new HtmlGenericControl("div");
                            div.Attributes.Add("class", "search-tag input-group input-group-sm");

                            HtmlGenericControl span = new HtmlGenericControl("span");
                            span.Attributes.Add("class", "form-control");
                            span.InnerHtml = item.Text;
                            div.Controls.Add(span);


                            ExtraButton btn = new ExtraButton();
                            btn.EIcon = ExtraIcon.A_Close;
                            btn.CssClass = "input-group-addon";
                            btn.CommandArgument = i.ToString();

                            if (TagClosed != null)
                                btn.Click += btn_Click;
                            div.Controls.Add(btn);

                            divRow.Controls.Add(div);
                        }

                    }
                    this.Controls.Add(divRow);
                }

            }
            onLoad = true;
        }

        public void Update()
        {
            onLoad = false;
            CreateChildControls();
        }

        void btn_Click(object sender, EventArgs e)
        {
            ExtraButton btn = sender as ExtraButton;
            if (btn != null)
            {
                SearchTagItem tag = TagItems[int.Parse(btn.CommandArgument)];
                if (tag != null && TagClosed != null)
                {
                    TagClosed(this, tag);
                }
            }
        }
    }

    public delegate void TagCloseEvent(object sender, SearchTagItem tag);

    [Serializable]
    public class SearchTagItem
    {
        public object ID { get; set; }
        public string Key { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public object Tag { get; set; }
        private bool cr_status = true;
        public bool Status { get { return cr_status; } set { cr_status = value; } }
        private bool cr_isNew = true;
        public bool IsNew { get { return cr_isNew; } set { cr_isNew = value; } }

        public SearchTagItem(string key, string text) { this.Key = key; this.Text = text; Tag = null; }
        public SearchTagItem(string key, string text, string value, object tag) { this.Key = key; this.Text = text; Value = value; Tag = tag; }
        public SearchTagItem(string key, string text, string value) { this.Key = key; this.Text = text; Value = value; }
    }
}
