﻿using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.ExtraControls;
using SweetSoft.ExtraControls.html;

namespace SweetSoft.ExtraControls
{
    [ToolboxData("<{0}:ExtraButton runat=\"server\"></{0}:ExtraButton>")]
    public class ExtraButton : LinkButton, IPostBackEventHandler
    {
        #region Properties
        private ExtraIcon cr_Icon = ExtraIcon.None;
        /// <summary>
        /// Icon trên button
        /// </summary>
        public ExtraIcon EIcon { get { return cr_Icon; } set { cr_Icon = value; } }

        private IconPosition cr_Icon_Position = IconPosition.Left;
        /// <summary>
        /// Vị trí icon trên button
        /// </summary>
        public IconPosition EIconPosition { get { return cr_Icon_Position; } set { cr_Icon_Position = value; } }

        private ButtonStyle cr_Button_Style = ExtraSkinManager.BUTTON_STYLE;
        /// <summary>
        /// Style button
        /// </summary>
        public ButtonStyle EStyle { get { return cr_Button_Style; } set { cr_Button_Style = value; } }

        private bool cr_SubmitWithCheckValid = false;
        /// <summary>
        /// Kiểm tra page valid khi submit
        /// </summary>
        public bool ESubmitAndCheckValid { get { return cr_SubmitWithCheckValid; } set { cr_SubmitWithCheckValid = value; } }

        private string cr_group_valid = string.Empty;
        /// <summary>
        /// Tên nhóm class các control cần kiểm tra valid
        /// </summary>
        public string EGroupValidName { get { return cr_group_valid; } set { cr_group_valid = value; } }

        private string cr_Text_Class = string.Empty;
        /// <summary>
        /// Class cho text hiển thị
        /// </summary>
        public string ETextClass { get { return cr_Text_Class; } set { cr_Text_Class = value; } }

        private bool cr_snipbutton = true;
        public bool EnableSnipButton { get { return cr_snipbutton; } set { cr_snipbutton = value; } }

        private string cr_sniptext = string.Empty;
        public string SnipText { get { return cr_sniptext; } set { this.cr_sniptext = value; CreateChildControls(); } }

        private bool cr_runLoader = true;
        public bool RunLoaderWhenPostBack { get { return cr_runLoader; } set { cr_runLoader = value; } }

        private bool cr_transparent = false;
        public bool Transparent { get { return cr_transparent; } set { cr_transparent = value; } }

        private bool cr_round = false;
        public bool EnableRound { get { return cr_round; } set { cr_round = value; } }

        private bool cr_small = false;
        public bool EnableSmallSize { get { return cr_small; } set { cr_small = value; } }

        private string navigate_url = string.Empty;
        public string NavigateUrl { get { return navigate_url; } set { navigate_url = value; } }

        private bool cr_postback = true;
        public bool AutoPostback { get { return cr_postback; } set { cr_postback = value; } }

        private string cr_target = "_self";
        public string Target { get { return cr_target; } set { cr_target = value; } }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ExtraScriptRegister.RegisterButton = true;
        }

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            string cssClass = "btn " + this.CssClass + " ";
            if (ExtraSkinManager.DEFAULT_THEME == ExtraTheme.MaterialLTE)
            {
                cssClass += " btn-primary btn-raised ";
            }
            #region Build cssClass
            if (cr_transparent) cssClass += " btn-transparent";
            else
            {
                cssClass += cr_Button_Style.ToRender() + " ";
                if (cr_round) cssClass += " btn-round";
                if (cr_small) cssClass += " smallsize";
            }

            #endregion

            HtmlElement aE = new HtmlElement("a");

            string clickScript = this.OnClientClick;
            #region Build script
            string postbackScript = string.Empty;
            if (page != null && cr_postback)
                postbackScript = ExtraScriptRegister.MergeScript(clickScript, page.ClientScript.GetPostBackClientHyperlink(this, ""));

            if (this.cr_SubmitWithCheckValid)
            {
                string bound = ".vlg-" + ValidationGroup;
                if (string.IsNullOrEmpty(ValidationGroup)) bound = "#" + ExtraScriptRegister.aspnetForm;
                aE.Attributes.Add(new HtmlAttribute("data-group-name", bound));
                string validScript = string.Format("return ButtonCheckValid('{0}');", this.ClientID);
                clickScript = ExtraScriptRegister.MergeScript(validScript, clickScript);
            }

            if (base.HasAttributes)
            {
                string baseClick = base.Attributes["onclick"];
                if (baseClick != null && !string.IsNullOrEmpty(baseClick))
                {
                    clickScript = ExtraScriptRegister.MergeScript(clickScript, baseClick);
                    base.Attributes.Remove("onclick");
                }
            }
            #endregion

            string icon = cr_Icon.ToRender();
            #region Build Icon

            #endregion

            //if (!this.cr_SubmitWithCheckValid && !string.IsNullOrEmpty(postbackScript))
            //    clickScript = ExtraScriptRegister.MergeScript(clickScript, postbackScript);
            if (cr_snipbutton) clickScript = ExtraScriptRegister.MergeScript(string.Format(" ClickSnip('{0}');", this.ClientID), clickScript);

            if (string.IsNullOrEmpty(NavigateUrl))
                aE.Attributes.Add(new HtmlAttribute("data-run-loader", cr_runLoader.ToString().ToLower()));
            aE.Attributes.AddRange(this.GetListAttributes());
            aE.Attributes.Add(new HtmlAttribute("class", cssClass));
            if (string.IsNullOrEmpty(NavigateUrl))
            {
                if (!string.IsNullOrEmpty(clickScript) && this.Enabled)
                    aE.Attributes.Add(new HtmlAttribute("onclick", this.OnClientClick + clickScript, IsEnabled));
            }

            aE.Attributes.Add(new HtmlAttribute("name", this.UniqueID));
            if (string.IsNullOrEmpty(NavigateUrl))
            {
                if (!string.IsNullOrEmpty(postbackScript) && this.Enabled && cr_postback)
                    aE.Attributes.Add(new HtmlAttribute("href", postbackScript));
            }
            else if (this.Enabled)
            {
                aE.Attributes.Add(new HtmlAttribute("href", NavigateUrl));
                aE.Attributes.Add(new HtmlAttribute("target", cr_target));
            }
            aE.Attributes.Add(new HtmlAttribute("id", this.ClientID));
            aE.Attributes.Add(new HtmlAttribute("data-valid-type", "inline"));
            if (string.IsNullOrEmpty(NavigateUrl))
                aE.Attributes.Add(new HtmlAttribute("type", "submit"));
            aE.Attributes.Add(new HtmlAttribute("disabled", "disabled", !IsEnabled));
            string aEStyle = "";
            if (this.Width.Value > 0)
                aEStyle += string.Format("width:{0}", this.Width);
            aE.Attributes.Add(new HtmlAttribute("style", aEStyle));
            if (TabIndex > 0)
                aE.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));

            HtmlElement iE = new HtmlElement("i");
            iE.Attributes.Add(new HtmlAttribute("id", this.ClientID + "_icon"));

            HtmlElement iSnip = new HtmlElement("i");
            iSnip.Attributes.Add(new HtmlAttribute("class", "fa fa-spinner fa-pulse fa-spin-custom"));
            iSnip.Attributes.Add(new HtmlAttribute("id", this.ClientID + "_snip"));
            if (!string.IsNullOrEmpty(this.Text))
                iSnip.Attributes.Add(new HtmlAttribute("style", "width:17px;display:none;margin-right:5px;"));
            else
                iSnip.Attributes.Add(new HtmlAttribute("style", "width:14px;display:none;margin-right:0px;"));

            if (!string.IsNullOrEmpty(this.Text))
            {
                if (cr_Icon_Position == IconPosition.Left) cr_Text_Class += " right";
                HtmlElement text = new HtmlElement("span", string.Format("text {0}", cr_Text_Class),
                                              string.Format("{0}_text", this.ClientID), this.Text,
                                              null, null, true, null);
                if (cr_snipbutton) text.Attributes.Add(new HtmlAttribute("sniptext", !string.IsNullOrEmpty(cr_sniptext) ? cr_sniptext : this.Text));
                aE.Elements.Add(text);
            }

            aE.Elements.Add(iE);
            if (cr_snipbutton) aE.Elements.Add(iSnip);
            iE.Attributes.Add(new HtmlAttribute("class", "icon " + icon));
            writer.WriteHtmlElement(aE);
        }
        #endregion
    }


}
