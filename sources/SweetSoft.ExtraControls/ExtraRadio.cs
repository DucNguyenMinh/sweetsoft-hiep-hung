﻿using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
    [ToolboxData("<{0}:ExtraRadio runat=\"server\" />")]
    [ParseChildren(false)]
    public class ExtraRadio : RadioButton
    {
        #region Properties

        private string cr_text = string.Empty;
        public string Text { get { return cr_text; } set { cr_text = value; } }

        private CheckBoxType cr_cbtype = CheckBoxType.Normal;
        [Category("Appearance"),
        Description("Kiểu checkbox")]
        public CheckBoxType CheckboxType { get { return cr_cbtype; } set { cr_cbtype = value; } }

        private string cr_labelstyle = string.Empty;
        public string LabelStyle { get { return cr_labelstyle; } set { cr_labelstyle = value; } }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            ExtraScriptRegister.RegisterRadio = true;
            base.OnInit(e);
        }

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Visible)
            {
                Page page = this.Page;
                if (page == null)
                    page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

                HtmlElement divSwitch = new HtmlElement("label");
                string cssClass = "ext-control " + this.CssClass;

                if (cr_cbtype == CheckBoxType.Normal)
                {
                    HtmlElement input = new HtmlElement("input");
                    #region Input
                   
                    input.Attributes.Add(new HtmlAttribute("id", this.ClientID));
                    input.Attributes.Add(new HtmlAttribute("type", "radio"));
                    if (!string.IsNullOrEmpty(GroupName))
                    {
                        HtmlAttribute att = input.Attributes.Find(p => p.Name.ToLower() == "name");
                        if (att != null) input.Attributes.Remove(att);
                        input.Attributes.Add(new HtmlAttribute("name", GroupName));
                    }

                    input.Attributes.Add(new HtmlAttribute("class", "ace"));

                    string postbackScript = string.Empty;
                    if (page != null)
                    {
                        postbackScript = page.ClientScript.GetPostBackClientHyperlink(this, "");
                        string onclick = this.Attributes["onclick"];
                        if (this.AutoPostBack)
                        {
                            if (string.IsNullOrEmpty(onclick))
                                input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}", postbackScript)));
                            else
                                input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}{1}setTimeout('__doPostBack(\\'{2}\\',\\'\\')', 0);", onclick.Trim(), onclick.Trim().EndsWith(";") ? string.Empty : ";", this.UniqueID)));

                        }
                    }

                    if (this.Checked)
                        input.Attributes.Add(new HtmlAttribute("checked", "checked"));

                    if (TabIndex > 0)
                        input.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));

                    input.Attributes.AddRange(this.GetListAttributes());
                    #endregion

                    HtmlElement label = new HtmlElement("label");
                    #region Label
                    label.Attributes.Add(new HtmlAttribute("class", "ext-control cb pos-rel "));
                    label.Elements.Add(input);

                    // if (!string.IsNullOrEmpty(cr_text))
                    {
                        HtmlElement labelText = new HtmlElement("label", "lbl " + cr_labelstyle, null, this.cr_text, null, null, true, null);
                        labelText.Attributes.Add(new HtmlAttribute("for", this.ClientID));
                        label.Elements.Add(labelText);
                    }
                    #endregion
                    writer.WriteHtmlElement(label);
                }
            }
        }
        #endregion
    }
}
