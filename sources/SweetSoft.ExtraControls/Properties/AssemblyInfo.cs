﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SweetSoft.ExtraControls")]
[assembly: AssemblyDescription("Project xây dựng các web control")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("SweetSoft")]
[assembly: AssemblyProduct("SweetSoft.ExtraControls")]
[assembly: AssemblyCopyright("Copyright ©  2015")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Custom resource for control
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.datetime.moment.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.searchTagBox.searchTagBox.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.datetime.daterangepicker.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.datetime.datetimeScript.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.jquery_1_11_2_min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.dialog.bootstrap-dialog.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.dialog.run_prettify.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.dialog.ExtraDialog.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.ExtraGrid.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.dataTables.bootstrap.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.dataTables.fixedHeader.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.dataTables.fixedColumns.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.dataTables.responsive.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.responsive.bootstrap.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.grids.jquery.dataTables.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.button.ExtraButton.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.bootstrap.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.jQuery-2.1.4.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.nprogress.nprogress.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.combobox.bootstrap-select.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.switch.ExtraToggleSwitch.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.combobox.ExtraCombobox.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.pnotify.pnotify.custom.min.js", "application/x-javascript")]

[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.validation.jquery.validationEngine.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.validation.validationScript.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.validation.languages.jquery.validationEngine-vi-VN.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.validation.languages.jquery.validationEngine-en-US.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.mainScript.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.Config.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.jquery-ui.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.tab.tabulous.js", "application/x-javascript")]


[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.input_mask.jquery.inputmask.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.input_mask.jquery.inputmask.regex.extensions.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.input_mask.jquery.inputmask.extensions.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.scripts.input_mask.jquery.inputmask.numeric.extensions.js", "application/x-javascript")]


[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.searchTagBox.searchTagBox.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.ControlStyle.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.textbox.ExtraTextbox.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.bootstrap.min.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.dialog.bootstrap-dialog.min.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.pnotify.pnotify.custom.min.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.pnotify.core.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.pnotify.animate.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.nprogress.nprogress.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.combobox.bootstrap-select.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.tab.tabulous.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.checkbox.checkbox.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.grids.dataTables.bootstrap.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.grids.fixedHeader.bootstrap.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.grids.fixedColumns.bootstrap.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.grids.responsive.bootstrap.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.datetime.daterangepicker.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.validation.validationEngine.jquery.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.css.validation.bootstrapValidator.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.FlatIcon.flaticon.css", "text/css", PerformSubstitution = true)]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.css", "text/css", PerformSubstitution = true)]

[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.FlatIcon.flaticon.eot", "application/x-font-eot")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.FlatIcon.flaticon.svg", "application/x-font-svg")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.FlatIcon.flaticon.ttf", "application/x-font-ttf")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.FlatIcon.flaticon.woff", "application/x-font-woff")]

[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.eot", "application/x-font-eot")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.svg", "application/x-font-svg")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.ttf", "application/x-font-ttf")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.woff", "application/x-font-woff")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.otf", "application/x-font-otf")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.icons.AweIcon.aweicon.woff2", "application/x-font-woff2")]

// ACE Theme css
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.style.jquery.gritter.min.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.style.ace.min.css", "text/css")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.style.button.ExtraButton.css", "text/css")]

// ACE Theme script
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.ace-elements.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.ace-extra.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.ace.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.jquery.gritter.min.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.checkbox.ExtraCheckbox.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.radio.ExtraRadio.js", "application/x-javascript")]
[assembly: System.Web.UI.WebResource("SweetSoft.ExtraControls.themes.ace.script.button.ExtraButton.js", "application/x-javascript")]


// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("66e92111-6916-4437-aac5-e11e6fe69891")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
