﻿
using System;
using System.ComponentModel;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Web.Script.Serialization;
using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls.helper;
using SweetSoft.ExtraControls.icons;
using SweetSoft.ExtraControls;

namespace SweetSoft.ExtraControls
{

    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ExtraDateTimePicker runat=\"server\"></{0}:ExtraDateTimePicker>")]
    public class ExtraDateTimePicker : WebControl, INamingContainer, IPostBackDataHandler, IPostBackEventHandler
    {


        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return Inline == true ? HtmlTextWriterTag.Div : HtmlTextWriterTag.Input;
            }
        }

        #region field

        private bool autoPostBack = false;
        JavaScriptSerializer jss = new JavaScriptSerializer();

        string hdfId = "_hdfDTPValue";
        string _text = "";

        [Description("Fires when the date has been changed and AutoPostBack is set to 'true'.")]
        public event EventHandler<DateChangedEventArgs> DateChanged;

        //CultureInfo ciLanguage = CultureInfo.GetCultureInfo("vi-VN");

        #endregion

        #region base

        string _sCssClass = "";

        /// <summary>
        /// Adds the CSS class.
        /// </summary>
        /// <param name="cssClass">The CSS class.</param>
        private void AddCssClass(string cssClass)
        {
            if (String.IsNullOrEmpty(this._sCssClass))
            {
                this._sCssClass = cssClass;
            }
            else
            {
                this._sCssClass += " " + cssClass;
            }
        }

        [Bindable(true)]
        [DefaultValue(false)]
        [Themeable(false)]
        public virtual bool ReadOnly
        {
            get
            {
                object obj = ViewState["ReadOnly"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["ReadOnly"] = value;
            }
        }

        [Browsable(true)]
        [DefaultValue(false)]
        public bool AutoPostBack
        {
            get { return autoPostBack; }
            set { autoPostBack = value; }
        }

        public string CssPlaceHolder
        {
            get
            {
                object obj = ViewState["CssPlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CssPlaceHolder"] = value; }
        }

        public string JsPlaceHolder
        {
            get
            {
                object obj = ViewState["JsPlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["JsPlaceHolder"] = value; }
        }

        public bool Required
        {
            get
            {
                object obj = ViewState["Required"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["Required"] = value; }
        }

        public string RequiredText
        {
            get
            {
                object obj = ViewState["RequiredText"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["RequiredText"] = value; }
        }

        public string PlaceHolder
        {
            get
            {
                object obj = ViewState["PlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["PlaceHolder"] = value; }
        }

        public bool ForceCheckOnSubmit
        {
            get
            {
                object obj = ViewState["ForceCheckOnSubmit"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ForceCheckOnSubmit"] = value; }
        }

        public bool InitAfterLoad
        {
            get
            {
                object obj = ViewState["InitAfterLoad"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["InitAfterLoad"] = value; }
        }

        public bool SimpleInit
        {
            get
            {
                object obj = ViewState["SimpleInit"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["SimpleInit"] = value;

                CssPlaceHolder = "cpVenderCSS";
                JsPlaceHolder = "cpVenderScript";

                RenderFull = true;
                AllowInputToggle = true;
                AddonIconAfter = "fa fa-calendar";
                CssClass = "form-control";
                Language = "vi-VN";
                Format = "DD/MM/YYYY";
                UseMomentShortDay = true;
            }
        }

        #endregion

        #region Range date

        /// <summary>
        /// Mark as Start date in range
        /// </summary>
        public bool LinkDateIsStartDate
        {
            get
            {
                object obj = ViewState["LinkDateIsStartDate"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["LinkDateIsStartDate"] = value; }
        }

        /// <summary>
        /// Group of two datetimepicker must be unique
        /// </summary>
        public string LinkDateGroup
        {
            get
            {
                object obj = ViewState["LinkDateGroup"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["LinkDateGroup"] = value; }
        }

        #endregion

        #region properties

        bool valid_inline = true;
        public bool ValidationInline { get { return valid_inline; } set { valid_inline = value; } }

        public string ValidationClientID { get { return "dtpwrap_" + this.ClientID; } }

        private DateTimeMask cr_datetimemask = DateTimeMask.DateTimeFull;
        public DateTimeMask DateTimeMask
        {
            get
            {
                return cr_datetimemask;
            }
            set { cr_datetimemask = value; }
        }

        public bool RenderFull
        {
            get
            {
                object obj = ViewState["RenderFull"];
                return (obj == null) ? true : (bool)obj;
            }
            set { ViewState["RenderFull"] = value; }
        }

        public string AddonIconBefore
        {
            get
            {
                object obj = ViewState["AddonIconBefore"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AddonIconBefore"] = value; }
        }

        public string AddonIconAfter
        {
            get
            {
                object obj = ViewState["AddonIconAfter"];
                return (obj == null) ? "glyphicon glyphicon-calendar" : (string)obj;
            }
            set { ViewState["AddonIconAfter"] = value; }
        }

        /// <summary>
        /// More info about Timezone: http://momentjs.com/timezone/
        /// </summary>
        public string TimeZone
        {
            get
            {
                object obj = ViewState["TimeZone"];
                return (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
                    ? DateTimeHelper.GetMomentTimezone(obj.ToString()) : string.Empty;
            }
            set { ViewState["TimeZone"] = DateTimeHelper.GetMomentTimezone(value); }
        }

        public string Format
        {
            get
            {
                object obj = ViewState["Format"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Format"] = value; }
        }

        public string DayViewHeaderFormat
        {
            get
            {
                object obj = ViewState["DayViewHeaderFormat"];
                return (obj == null) ? "MMMM YYYY" : (string)obj;
            }
            set { ViewState["DayViewHeaderFormat"] = value; }
        }

        public string[] ExtraFormats
        {
            get
            {
                object obj = ViewState["ExtraFormats"];
                return (obj == null) ? null : (string[])obj;
            }
            set { ViewState["ExtraFormats"] = value; }
        }

        public int Stepping
        {
            get
            {
                object obj = ViewState["Stepping"];
                return (obj == null) ? 1 : (int)obj;
            }
            set { ViewState["Stepping"] = value; }
        }

        public DateTime MinDate
        {
            get
            {
                object obj = ViewState["MinDate"];
                return (obj == null) ? DateTime.MinValue : (DateTime)obj;
            }
            set { ViewState["MinDate"] = value; }
        }

        public DateTime MaxDate
        {
            get
            {
                object obj = ViewState["MaxDate"];
                return (obj == null) ? DateTime.MinValue : ((DateTime)obj);
            }
            set { ViewState["MaxDate"] = value; }
        }

        public bool UseCurrent
        {
            get
            {
                object obj = ViewState["UseCurrent"];
                return (obj == null) ? true : (bool)obj;
            }
            set { ViewState["UseCurrent"] = value; }
        }

        public bool Collapse
        {
            get
            {
                object obj = ViewState["Collapse"];
                return (obj == null) ? true : (bool)obj;
            }
            set { ViewState["Collapse"] = value; }
        }

        public string Language
        {
            get
            {
                object obj = ViewState["Language"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Language"] = value; }
        }

        /*
        /// <summary>
        /// must be one of [string, moment or Date]
        /// </summary>
        public object DefaultDate
        {
            get
            {
                object obj = ViewState["DefaultDate"];
                return (obj == null) ? false : obj;
            }
            set { ViewState["DefaultDate"] = value; }
        }
        */

        public DateTime DateValue
        {
            get
            {
                object obj = ViewState["DateValue"];
                return (obj == null) ? DateTime.MinValue : (DateTime)obj;
            }
            set
            {
                ViewState["DateValue"] = value;
            }
        }

        public DateTime[] DisabledDates
        {
            get
            {
                object obj = ViewState["DisabledDates"];
                return (obj == null) ? null : (DateTime[])obj;
            }
            set { ViewState["DisabledDates"] = value; }
        }

        public DateTime[] EnabledDates
        {
            get
            {
                object obj = ViewState["EnabledDates"];
                return (obj == null) ? null : (DateTime[])obj;
            }
            set { ViewState["EnabledDates"] = value; }
        }

        #region icon

        public string Icons_Time
        {
            get
            {
                object obj = ViewState["Icons_Time"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Time"] = value; }
        }

        public string Icons_Date
        {
            get
            {
                object obj = ViewState["Icons_Date"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Date"] = value; }
        }

        public string Icons_Up
        {
            get
            {
                object obj = ViewState["Icons_Up"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Up"] = value; }
        }

        public string Icons_Down
        {
            get
            {
                object obj = ViewState["Icons_Down"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Down"] = value; }
        }

        public string Icons_Previous
        {
            get
            {
                object obj = ViewState["Icons_Previous"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Previous"] = value; }
        }

        public string Icons_Next
        {
            get
            {
                object obj = ViewState["Icons_Next"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Next"] = value; }
        }

        public string Icons_Today
        {
            get
            {
                object obj = ViewState["Icons_Today"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Today"] = value; }
        }

        public string Icons_Clear
        {
            get
            {
                object obj = ViewState["Icons_Clear"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Clear"] = value; }
        }

        public string Icons_Close
        {
            get
            {
                object obj = ViewState["Icons_Close"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Icons_Close"] = value; }
        }

        #endregion

        public bool UseStrict
        {
            get
            {
                object obj = ViewState["UseStrict"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["UseStrict"] = value; }
        }

        public bool SideBySide
        {
            get
            {
                object obj = ViewState["SideBySide"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SideBySide"] = value; }
        }

        public int[] DaysOfWeekDisabled
        {
            get
            {
                object obj = ViewState["DaysOfWeekDisabled"];
                return (obj == null) ? null : (int[])obj;
            }
            set { ViewState["DaysOfWeekDisabled"] = value; }
        }

        public bool CalendarWeeks
        {
            get
            {
                object obj = ViewState["CalendarWeeks"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["CalendarWeeks"] = value; }
        }

        public DTPViewMode ViewMode
        {
            get
            {
                object obj = ViewState["ViewMode"];
                return (obj == null) ? DTPViewMode.Days : (DTPViewMode)Enum.Parse(typeof(DTPViewMode), obj.ToString());
            }
            set { ViewState["ViewMode"] = value; }
        }

        public DTPToolbarPlacement ToolbarPlacement
        {
            get
            {
                object obj = ViewState["ToolbarPlacement"];
                return (obj == null) ? DTPToolbarPlacement.Default : (DTPToolbarPlacement)Enum.Parse(typeof(DTPToolbarPlacement), obj.ToString());
            }
            set { ViewState["ToolbarPlacement"] = value; }
        }

        public bool ShowTodayButton
        {
            get
            {
                object obj = ViewState["ShowTodayButton"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowTodayButton"] = value; }
        }

        public bool ShowClear
        {
            get
            {
                object obj = ViewState["ShowClear"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowClear"] = value; }
        }

        public bool ShowClose
        {
            get
            {
                object obj = ViewState["ShowClose"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowClose"] = value; }
        }

        public DTPWidgetPositioningHorizontal WidgetPositioning_Horizontal
        {
            get
            {
                object obj = ViewState["WidgetPositioning_Horizontal"];
                return (obj == null) ? DTPWidgetPositioningHorizontal.Auto : (DTPWidgetPositioningHorizontal)Enum.Parse(typeof(DTPWidgetPositioningHorizontal), obj.ToString());
            }
            set { ViewState["WidgetPositioning_Horizontal"] = value; }
        }

        public DTPWidgetPositioningVertical WidgetPositioning_Vertical
        {
            get
            {
                object obj = ViewState["WidgetPositioning_Vertical"];
                return (obj == null) ? DTPWidgetPositioningVertical.Auto : (DTPWidgetPositioningVertical)Enum.Parse(typeof(DTPWidgetPositioningVertical), obj.ToString());
            }
            set { ViewState["WidgetPositioning_Vertical"] = value; }
        }

        public string WidgetParent
        {
            get
            {
                object obj = ViewState["WidgetParent"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["WidgetParent"] = value; }
        }

        public string Text
        {
            get
            {
                return _text;
            }
        }

        public bool IgnoreReadonly
        {
            get
            {
                object obj = ViewState["IgnoreReadonly"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["IgnoreReadonly"] = value; }
        }

        public bool KeepOpen
        {
            get
            {
                object obj = ViewState["KeepOpen"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["KeepOpen"] = value; }
        }

        public bool FocusOnShow
        {
            get
            {
                object obj = ViewState["FocusOnShow"];
                return (obj == null) ? true : (bool)obj;
            }
            set { ViewState["FocusOnShow"] = value; }
        }

        public bool Inline
        {
            get
            {
                object obj = ViewState["Inline"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["Inline"] = value; }
        }

        public bool KeepInvalid
        {
            get
            {
                object obj = ViewState["KeepInvalid"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["KeepInvalid"] = value; }
        }

        public string DatepickerInputClass
        {
            get
            {
                object obj = ViewState["DatepickerInputClass"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["DatepickerInputClass"] = value; }
        }

        public string KeyBindsObject
        {
            get
            {
                object obj = ViewState["KeyBindsObject"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["KeyBindsObject"] = value; }
        }

        public bool Debug
        {
            get
            {
                object obj = ViewState["Debug"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["Debug"] = value; }
        }

        public bool AllowInputToggle
        {
            get
            {
                object obj = ViewState["AllowInputToggle"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["AllowInputToggle"] = value; }
        }

        public bool UseMomentShortDay
        {
            get
            {
                object obj = ViewState["UseMomentShortDay"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                Format = string.Empty;
                ViewState["UseMomentShortDay"] = value;
            }
        }

        public string DisabledTimeIntervalsObject
        {
            get
            {
                object obj = ViewState["DisabledTimeIntervalsObject"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["DisabledTimeIntervalsObject"] = value; }
        }

        public string ToolTipObject
        {
            get
            {
                object obj = ViewState["ToolTipObject"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ToolTipObject"] = value; }
        }

        #region event binding

        public string OnClientChangedFunc
        {
            get
            {
                object obj = ViewState["OnClientChangedFunc"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClientChangedFunc"] = value; }
        }

        public string OnClientOpenedFunc
        {
            get
            {
                object obj = ViewState["OnClientOpenedFunc"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClientOpenedFunc"] = value; }
        }

        public string OnClientClosedFunc
        {
            get
            {
                object obj = ViewState["OnClientClosedFunc"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClientClosedFunc"] = value; }
        }

        public string OnClientErrorFunc
        {
            get
            {
                object obj = ViewState["OnClientErrorFunc"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClientErrorFunc"] = value; }
        }

        public string OnClientUpdateFunc
        {
            get
            {
                object obj = ViewState["OnClientUpdateFunc"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClientUpdateFunc"] = value; }
        }

        #endregion

        public int[] DisabledHours
        {
            get
            {
                object obj = ViewState["DisabledHours"];
                return (obj == null) ? null : (int[])obj;
            }
            set { ViewState["DisabledHours"] = value; }
        }

        public int[] EnabledHours
        {
            get
            {
                object obj = ViewState["EnabledHours"];
                return (obj == null) ? null : (int[])obj;
            }
            set { ViewState["EnabledHours"] = value; }
        }

        /*
        //later use
        /// <summary>
        /// must be one of [string, moment or Date]
        /// </summary>
        public object ViewDate
        {
            get
            {
                object obj = ViewState["ViewDate"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ViewDate"] = value; }
        }
        */
        #endregion

        static CultureInfo GetLanguage(string lang)
        {
            CultureInfo ci = null;
            if (string.IsNullOrEmpty(lang) == false)
            {
                try
                {
                    ci = CultureInfo.GetCultureInfo(lang);
                }
                catch (Exception ex)
                {
                    ci = CultureInfo.GetCultureInfo("vi-VN");
                }
            }
            else
                ci = CultureInfo.GetCultureInfo("vi-VN");
            return ci;
        }

        DateTime GetDate(object obj)
        {
            DateTime dt = DateTime.MinValue;

            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                CultureInfo ci = GetLanguage(Language);
                if (ci != null)
                {
                    try
                    {
                        dt = DateTime.Parse(obj.ToString(), ci);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            return dt;
        }

        public double GetDTime(string yourTimeZoneId)
        {
            try
            {
                var yourTimeZone = TimeZoneInfo.FindSystemTimeZoneById(yourTimeZoneId);
                var serverTimeZone = TimeZoneInfo.Local;
                var now = DateTimeOffset.UtcNow;
                TimeSpan yourOffset = yourTimeZone.GetUtcOffset(now);
                TimeSpan serverOffset = serverTimeZone.GetUtcOffset(now);
                TimeSpan different = yourOffset - serverOffset;
                return different.TotalHours;
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }
        private DateTime GetCTime(DateTime yourDateTime)
        {
            try
            {
                if (string.IsNullOrEmpty(yourDateTime.ToString()) || yourDateTime == DateTime.MinValue)
                    return yourDateTime.ToLocalTime();
                String timeZoneId = "SE Asia Standard Time";
                double differentTime = GetDTime(timeZoneId);
                if (TimeZoneInfo.Local.Id != "SE Asia Standard Time")
                {
                    if (yourDateTime.Month <= 3)
                    {
                        if (yourDateTime.Month == 3)
                        {
                            return yourDateTime.AddDays(1).ToLocalTime();
                        }
                        else
                            return yourDateTime.AddDays(1).AddHours(differentTime).ToLocalTime();
                    }
                    else
                        return yourDateTime.AddHours(differentTime).ToLocalTime();
                }
                else
                    return yourDateTime.AddHours(differentTime).ToLocalTime();
            }
            catch
            {
                return yourDateTime.ToLocalTime();
            }
            return yourDateTime.ToLocalTime();
        }

        protected override void OnPreRender(EventArgs e)
        {
            //   RegisterScript(CssPlaceHolder, JsPlaceHolder, GetLanguage(Language).Name);

            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            //Need to write this, so that LoadPostData() gets called.
            if (page != null)
                page.RegisterRequiresPostBack(this);

            base.OnPreRender(e);
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ExtraScriptRegister.RegisterDateTime = true;
        }

        public static void RegisterScript(string cssPlaceHolder, string jsPlaceHolder, string LanguageCode)
        {
            Page page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;
            if (page != null)
            {
                // We need <head runat="server"> for this code to work
                if (page.Header == null)
                    throw new NotSupportedException("No <head runat=\"server\"> control found in page.");

                // Define the resource name and type.
                Type thisT = typeof(ExtraDateTimePicker);
                if (thisT != typeof(ExtraDateTimePicker))
                    thisT = thisT.BaseType;

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = page.ClientScript;
                // Check to see if the startup script is already registered.
                if (cs.IsStartupScriptRegistered(thisT, "SweetSoft.ExtraControls.setScriptExtraDateTimePicker") == false)
                {
                    #region css

                    string baseCSS = page.ResolveClientUrl("~/css/datetime/bootstrap-datetimepicker.css");

                    string strCss = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + baseCSS + "\" />";

                    // If not, register it
                    LiteralControl link = new LiteralControl();
                    link.EnableViewState = false;
                    link.Text = strCss;

                    try
                    {
                        if (string.IsNullOrEmpty(cssPlaceHolder))
                            page.Header.Controls.Add(link);
                        else
                        {
                            ContentPlaceHolder cpCss = page.Header.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                            if (cpCss == null)
                                cpCss = page.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                            if (cpCss != null)
                                cpCss.Controls.Add(link);
                            else
                                page.Header.Controls.Add(link);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    #endregion

                    #region js

                    String str = "<script src='" + page.ResolveClientUrl("~/scripts/moment/moment-with-locales.min.js") + "'></script> " +
                                 "<script src='" + page.ResolveClientUrl("~/scripts/datetime/bootstrap-datetimepicker.js") + "'></script> " +
                                 "<script src='" + page.ResolveClientUrl("~/scripts/datetime/locales/" + LanguageCode + ".js") + "'></script>" +
                                 "<script src='" + page.ResolveClientUrl("~/scripts/datetime/ExtraDateTimePicker.js") + "'></script>";

                    // Register the client resource with the page.
                    if (string.IsNullOrEmpty(jsPlaceHolder))
                    {
                        ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateTimePicker", str, false);
                    }
                    else
                    {
                        ContentPlaceHolder cpJs = page.Header.FindControl(jsPlaceHolder) as ContentPlaceHolder;
                        if (cpJs == null)
                            cpJs = page.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                        if (cpJs != null)
                        {
                            try
                            {
                                cpJs.Controls.Add(new LiteralControl(str));
                                ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateTimePicker", "", true);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateTimePicker", str, false);
                        }
                    }

                    #endregion
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            #region RegionName

            bool renderFull = RenderFull;

            if (this.cr_datetimemask != icons.DateTimeMask.None)
            {
                CssClass += " MaskDate ";
                this.Attributes.Add("TimeFormat", this.cr_datetimemask.ToRender());
            }

            this.AddCssClass(CssClass);
            if (renderFull == false)
                writer.AddAttribute("data-datetimepicker", "true");
            else
                AddCssClass("datepickerinput");
            CssClass = _sCssClass;

            if (Required)
            {
                writer.AddAttribute("required", "required");
                if (RequiredText != null && RequiredText.Length > 0)
                    writer.AddAttribute("data-msg-required", RequiredText);
            }

            if (InitAfterLoad == true)
                writer.AddAttribute("data-initAfterLoad", "true");

            #region RegionName

            if (UseCurrent == false)
                writer.AddAttribute("data-useCurrent", "false");
            if (Collapse == false)
                writer.AddAttribute("data-collapse", "false");
            if (UseStrict == true)
                writer.AddAttribute("data-useStrict", "true");
            if (SideBySide == true)
                writer.AddAttribute("data-sideBySide", "true");
            if (CalendarWeeks == true)
                writer.AddAttribute("data-calendarWeeks", "true");
            if (ShowTodayButton == true)
                writer.AddAttribute("data-showTodayButton", "true");
            if (ShowClear == true)
                writer.AddAttribute("data-showClear", "true");
            if (ShowClose == true)
                writer.AddAttribute("data-showClose", "true");
            if (IgnoreReadonly == true)
                writer.AddAttribute("data-ignoreReadonly", "true");
            if (KeepOpen == true)
                writer.AddAttribute("data-keepOpen", "true");
            if (FocusOnShow == false)
                writer.AddAttribute("data-focusOnShow", "false");
            if (Inline == true)
                writer.AddAttribute("data-inline", "true");
            if (KeepInvalid == true)
                writer.AddAttribute("data-keepInvalid", "true");
            if (Debug == true)
                writer.AddAttribute("data-debug", "true");
            if (AllowInputToggle == true)
                writer.AddAttribute("data-allowInputToggle", "true");
            if (UseMomentShortDay == true)
                writer.AddAttribute("data-useMomentShortDay", "true");

            //Range date
            if (LinkDateIsStartDate == true)
                writer.AddAttribute("data-linkDateIsStartDate", "true");

            if (string.IsNullOrEmpty(LinkDateGroup) == false)
                writer.AddAttribute("data-linkDateGroup", LinkDateGroup);
            //Range date

            if (string.IsNullOrEmpty(PlaceHolder) == false)
                writer.AddAttribute("placeHolder", PlaceHolder);
            if (string.IsNullOrEmpty(TimeZone) == false)
                writer.AddAttribute("data-timeZone", TimeZone);
            if (string.IsNullOrEmpty(Format) == false)
                writer.AddAttribute("data-format", Format);
            if (string.IsNullOrEmpty(DayViewHeaderFormat) == false && DayViewHeaderFormat != "MMMM YYYY")
                writer.AddAttribute("data-dayViewHeaderFormat", DayViewHeaderFormat);
            if (Stepping != 1)
                writer.AddAttribute("data-stepping", Stepping.ToString());
            if (string.IsNullOrEmpty(Language) == false)
                writer.AddAttribute("data-locale", Language);
            if (string.IsNullOrEmpty(Icons_Time) == false)
                writer.AddAttribute("data-icons_Time", Icons_Time);
            if (string.IsNullOrEmpty(Icons_Date) == false)
                writer.AddAttribute("data-icons_Date", Icons_Date);
            if (string.IsNullOrEmpty(Icons_Up) == false)
                writer.AddAttribute("data-icons_Up", Icons_Up);
            if (string.IsNullOrEmpty(Icons_Down) == false)
                writer.AddAttribute("data-icons_Down", Icons_Down);
            if (string.IsNullOrEmpty(Icons_Previous) == false)
                writer.AddAttribute("data-icons_Previous", Icons_Previous);
            if (string.IsNullOrEmpty(Icons_Next) == false)
                writer.AddAttribute("data-icons_Next", Icons_Next);
            if (string.IsNullOrEmpty(Icons_Today) == false)
                writer.AddAttribute("data-icons_Today", Icons_Today);
            if (string.IsNullOrEmpty(Icons_Clear) == false)
                writer.AddAttribute("data-icons_Clear", Icons_Clear);
            if (string.IsNullOrEmpty(Icons_Close) == false)
                writer.AddAttribute("data-icons_Close", Icons_Close);
            if (string.IsNullOrEmpty(WidgetParent) == false)
                writer.AddAttribute("data-widgetParent", WidgetParent);
            if (string.IsNullOrEmpty(DatepickerInputClass) == false)
                writer.AddAttribute("data-datepickerInputClass", DatepickerInputClass);
            if (string.IsNullOrEmpty(KeyBindsObject) == false)
                writer.AddAttribute("data-keyBindsObject", KeyBindsObject);
            if (string.IsNullOrEmpty(DisabledTimeIntervalsObject) == false)
                writer.AddAttribute("data-disabledTimeIntervalsObject", DisabledTimeIntervalsObject);
            if (string.IsNullOrEmpty(ToolTipObject) == false)
                writer.AddAttribute("data-toolTipObject", ToolTipObject);

            if (string.IsNullOrEmpty(OnClientChangedFunc) == false)
                writer.AddAttribute("data-onClientChangedFunc", OnClientChangedFunc);
            if (string.IsNullOrEmpty(OnClientClosedFunc) == false)
                writer.AddAttribute("data-onClientClosedFunc", OnClientClosedFunc);
            if (string.IsNullOrEmpty(OnClientErrorFunc) == false)
                writer.AddAttribute("data-onClientErrorFunc", OnClientErrorFunc);
            if (string.IsNullOrEmpty(OnClientOpenedFunc) == false)
                writer.AddAttribute("data-onClientOpenedFunc", OnClientOpenedFunc);
            if (string.IsNullOrEmpty(OnClientUpdateFunc) == false)
                writer.AddAttribute("data-onClientUpdateFunc", OnClientUpdateFunc);

            //if (string.IsNullOrEmpty(ToolTip) == false)
            //    writer.AddAttribute("data-toolTip", ToolTip);

            if (MinDate != DateTime.MinValue)
                writer.AddAttribute("data-minDate", jss.Serialize(MinDate));
            if (MaxDate != DateTime.MinValue)
                writer.AddAttribute("data-maxDate", jss.Serialize(MaxDate));
            if (ViewMode != DTPViewMode.Days)
                writer.AddAttribute("data-viewMode", ViewMode.ToRender());
            if (ToolbarPlacement != DTPToolbarPlacement.Default)
                writer.AddAttribute("data-toolbarPlacement", ToolbarPlacement.ToRender());
            if (WidgetPositioning_Horizontal != DTPWidgetPositioningHorizontal.Auto)
                writer.AddAttribute("data-widgetPositioning_Horizontal", WidgetPositioning_Horizontal.ToRender());
            if (WidgetPositioning_Vertical != DTPWidgetPositioningVertical.Auto)
                writer.AddAttribute("data-widgetPositioning_Vertical", WidgetPositioning_Vertical.ToRender());


            if (ExtraFormats != null && ExtraFormats.Length > 0)
                writer.AddAttribute("data-extraFormats", jss.Serialize(ExtraFormats));

            if (DisabledDates != null && DisabledDates.Length > 0)
                writer.AddAttribute("data-disabledDates", jss.Serialize(DisabledDates));
            if (EnabledDates != null && EnabledDates.Length > 0)
                writer.AddAttribute("data-enabledDates", jss.Serialize(EnabledDates));
            if (DaysOfWeekDisabled != null && DaysOfWeekDisabled.Length > 0)
                writer.AddAttribute("data-daysOfWeekDisabled", jss.Serialize(DaysOfWeekDisabled));
            if (DisabledHours != null && DisabledHours.Length > 0)
                writer.AddAttribute("data-disabledHours", jss.Serialize(DisabledHours));
            if (EnabledHours != null && EnabledHours.Length > 0)
                writer.AddAttribute("data-enabledHours", jss.Serialize(EnabledHours));

            #endregion

            Page page = Page;
            if (page != null)
                page.VerifyRenderingInServerForm(this);

            if (AutoPostBack)
            {
                //writer.AddAttribute("data-onchange", "true");
                if (page != null)
                {
                    string onchange = page.ClientScript.GetPostBackEventReference(GetPostBackOptions(), true);
                    onchange = String.Concat("setTimeout('", onchange.Replace("\\", "\\\\").Replace("'", "\\'"), "', 0)");
                    writer.AddAttribute("data-onchange", onchange);
                }
            }
            else if (page != null)
                page.ClientScript.RegisterForEventValidation(UniqueID, String.Empty);

            if (ReadOnly)
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "ReadOnly", false);

            if (Inline == false)
                writer.AddAttribute("type", "text");
            else
                writer.AddAttribute("data-datetimepicker", "true");

            writer.AddAttribute("data-hdf", ClientID + hdfId);

            #endregion

            /*using JsonConvert
            string date1 = Date1 == DateTime.MinValue ? "" : JsonConvert.SerializeObject(Date1);
            string date2 = Date2 == DateTime.MinValue ? "" : JsonConvert.SerializeObject(Date2);
            */

            //using JavaScriptSerializer
            string date = DateValue != DateTime.MinValue ? jss.Serialize(DateValue) : string.Empty;

            if (Inline == false)
            {
                string addonIconAfter = AddonIconAfter;
                string addonIconBefore = AddonIconBefore;
                if (renderFull == true)
                {
                    if (!valid_inline)
                        writer.Write("<div class='input-group datetimepicker' data-datetimepicker='true' id='dtpwrap_" + this.ClientID + "'>");
                    else
                        writer.Write("<div class='input-group datetimepicker' data-valid-type='inline' data-datetimepicker='true' id='dtpwrap_" + this.ClientID + "'>");

                    if (string.IsNullOrEmpty(addonIconBefore) == false)
                        writer.Write("<span class='input-group-addon'>" +
                                         "<span class='" + addonIconBefore + "'></span>" +
                                     "</span>");
                }

                base.Render(writer);

                writer.WriteHtmlElement(new HtmlElement(string.Format("{0}", HtmlTextWriterTag.Input), "",
                    ClientID + hdfId, null, null,
                  new HtmlAttribute[] { 
                    new HtmlAttribute("type", "hidden", null), 
                    new HtmlAttribute("value", date, null),
                    new HtmlAttribute("name", UniqueID + hdfId, null) }, true, null), true);

                if (renderFull == true)
                {
                    if (string.IsNullOrEmpty(addonIconAfter) == false)
                        writer.Write("<span class='input-group-addon'>" +
                                         "<span class='" + addonIconAfter + "'></span>" +
                                     "</span>");

                    writer.Write("</div>");
                }
            }
            else
            {
                base.Render(writer);
                writer.WriteHtmlElement(new HtmlElement(string.Format("{0}", HtmlTextWriterTag.Input), "",
                    ClientID + hdfId, null, null,
                  new HtmlAttribute[] { 
                    new HtmlAttribute("type", "hidden", null), 
                    new HtmlAttribute("value", date, null),
                    new HtmlAttribute("name", UniqueID + hdfId, null) }, true, null), true);
            }
        }


        /// <summary>
        /// Raise the DateChanged event.
        /// </summary>
        /// <param name="eventArgument"></param>
        public void RaisePostBackEvent(string eventArgument)
        {
            if (DateChanged != null)
                DateChanged(this, new DateChangedEventArgs(DateValue));
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        public virtual bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            bool isChange = false;
            string postedValue = postCollection[postDataKey + hdfId];
            if (postedValue != null && string.IsNullOrEmpty(postedValue) == false)
            {

                string[] data = postedValue.Split('|');
                string text = string.Empty;
                string dataDate = string.Empty;
                if (data.Length > 0)
                {
                    dataDate = data[0];
                    if (data.Length > 1)
                        text = data[1];
                    if (data.Length > 2)
                    {
                        for (int i = 2; i < data.Length; i++)
                            text += "|" + data[i];
                    }

                    if (text != _text)
                        _text = text;

                    string dateOld = DateValue.ToString();

                    if (dataDate != null && dataDate.Length > 0
                        && dataDate.ToLower() != "invalid date")
                    {
                        DateTime temp = DateTime.MinValue;
                        try
                        {
                            temp = jss.Deserialize<DateTime>("\"" + dataDate + "\"");
                        }
                        catch (Exception ex)
                        {
                            temp = DateTime.MinValue;
                        }

                        string dateNew = temp.ToString();
                        if (dateNew != dateOld)
                        {
                            isChange = true;
                            DateValue = temp;
                        }
                    }
                    else
                    {
                        if ((dataDate == null || dataDate.Length == 0 || dataDate.ToLower() == "invalid date")
                            && dateOld.Length > 0)
                        {
                            isChange = true;
                            DateValue = DateTime.MinValue;
                        }
                    }
                }

            }

            return isChange;
        }

        /*
         bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
         {
             return LoadPostData(postDataKey, postCollection);
         }
        */

        /*good text changed
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }
        
        protected virtual void RaisePostDataChangedEvent()
        {
            OnTextChanged(EventArgs.Empty);
        }

        public event EventHandler TextChanged;

        protected virtual void OnTextChanged(EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
        }
        */

        PostBackOptions GetPostBackOptions()
        {
            PostBackOptions options = new PostBackOptions(this);
            options.ActionUrl = null;
            options.ValidationGroup = null;
            options.Argument = String.Empty;
            options.RequiresJavaScriptProtocol = false;
            options.ClientSubmit = true;

            return options;
        }

    }

    /// <summary>
    /// Arguments of the DateChanged event.
    /// </summary>
    public class DateChangedEventArgs : EventArgs
    {
        private DateTime _date;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="date"></param>
        public DateChangedEventArgs(DateTime date)
        {
            this._date = date;
        }

        /// <summary>
        /// Get the date.
        /// </summary>
        public DateTime Date
        {
            get { return _date; }
        }

    }


    public enum DTPViewMode
    {
        [Render("months")]
        Months,
        [Render("years")]
        Years,
        [Render("decades")]
        Decades,
        [Render("days")]
        Days
    }

    public enum DTPToolbarPlacement
    {
        [Render("top")]
        Top,
        [Render("bottom")]
        Bottom,

        //[Render("default")]
        [Render("")]
        Default
    }

    public enum DTPWidgetPositioningHorizontal
    {
        [Render("left")]
        Left,
        [Render("right")]
        Right,
        //[Render("auto")]
        [Render("")]
        Auto
    }

    public enum DTPWidgetPositioningVertical
    {
        [Render("top")]
        Top,
        [Render("bottom")]
        Bottom,
        //[Render("auto")]
        [Render("")]
        Auto
    }

}