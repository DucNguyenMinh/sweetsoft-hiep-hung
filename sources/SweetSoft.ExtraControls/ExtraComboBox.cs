﻿using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI.WebControls;
using SweetSoft.ExtraControls;
using System.Web.UI.HtmlControls;
using System.Web;
using System.Web.UI;

namespace SweetSoft.ExtraControls
{
    public class ExtraComboBox : ListBox, IPostBackDataHandler
    {
        #region Properties
        private string cr_groupfield = "";
        public string DataGroupField { get { return cr_groupfield; } set { cr_groupfield = value; } }

        private int cr_displaycount = 0;
        [Category("Data")]
        public int DisplayItemCount { get { return cr_displaycount; } set { cr_displaycount = value; } }

        private bool cr_showarrowmenu = false;
        [Category("Data")]
        [Description("Hiển thị mũi tên khi buông popup")]
        public bool ShowArrowPopup { get { return cr_showarrowmenu; } set { cr_showarrowmenu = value; } }

        private bool cr_showtick = false;
        [Category("Data")]
        [Description("Hiển thị ký tự check item")]
        public bool ShowTick { get { return cr_showtick; } set { cr_showtick = value; } }

        private string cr_emptymessage = "";
        [Category("Data")]
        [Description("Thông báo khi chưa chọn")]
        public string EmptyMessage { get { return cr_emptymessage; } set { cr_emptymessage = value; } }

        private bool cr_livesearch = true;
        [Category("Data")]
        [Description("Kích hoạt tìm kiếm theo từ khóa")]
        public bool EnableLiveSearch { get { return cr_livesearch; } set { cr_livesearch = value; } }

        private ButtonStyle cr_style = ButtonStyle.Primary;
        [Category("Data")]
        [Description("Kiểu mẫu của combobox")]
        public ButtonStyle ComboStyle { get { return cr_style; } set { cr_style = value; } }

        private int cr_countTextLimit = 3;
        [Category("Data")]
        [Description("Đổi text hiển thị với số lượng > giá trị")]
        public int ItemSelectedChangeTextCount { get { return cr_countTextLimit; } set { cr_countTextLimit = value; } }

        [Category("Data")]
        [Description("Text hiển thị khi chọn 1 phần tử")]
        private string cr_singleTextFormat = "{0} item selected";
        public string SingleSelectedTextFormat { get { return cr_singleTextFormat; } set { cr_singleTextFormat = value; } }

        [Category("Data")]
        [Description("Text hiển thị khi chọn nhiều phần tử")]
        private string cr_multipleTextFormat = "{0} items selected";
        public string MultipleSelectedTextFormat { get { return cr_multipleTextFormat; } set { cr_multipleTextFormat = value; } }

        private bool cr_dropup = true;
        [Category("Data")]
        [Description("Tự động dropup khi scroll")]
        public bool EnableDropUp { get { return cr_dropup; } set { cr_dropup = value; } }

        private bool cr_runLoader = true;
        public bool RunLoaderWhenPostBack { get { return cr_runLoader; } set { cr_runLoader = value; } }

        public string DisplayText
        {
            get
            {
                if (string.IsNullOrEmpty(this.SelectedValue) || (!string.IsNullOrEmpty(this.SelectedValue) && this.SelectedValue == "-1")) return "";
                return this.SelectedItem != null  ? this.SelectedItem.Text : this.Text;
            }
        }

        public List<ListItem> SelectedItems
        {
            set
            {
                foreach (ListItem item in value)
                {
                    ListItem it = this.Items.FindByValue(item.Value);
                    if (it != null) it.Selected = true;
                }
            }
            get { return this.GetSelectedItems(); }
        }

        public bool HasValue
        {
            get
            {
                return !string.IsNullOrEmpty(SelectedValue) && SelectedValue != "-1" && SelectedValue != "0";
            }
        }

        private ITemplate itemTemPlate;

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate ItemsTemplate
        {
            get { return itemTemPlate; }
            set { itemTemPlate = value; }
        }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            ExtraScriptRegister.RegisterCombobox = true;
            base.OnInit(e);
            CreateChildControls();
        }

        protected override object SaveViewState()
        {
            // create object array for Item count + 1
            object[] allStates = new object[this.Items.Count + 1];

            // the +1 is to hold the base info
            object baseState = base.SaveViewState();
            allStates[0] = baseState;

            Int32 i = 1;
            // now loop through and save each Style attribute for the List
            foreach (ListItem li in this.Items)
            {
                Int32 j = 0;
                string[][] attributes = new string[li.Attributes.Count][];
                foreach (string attribute in li.Attributes.Keys)
                {
                    attributes[j++] = new string[] { attribute, li.Attributes[attribute] };
                }
                allStates[i++] = attributes;
            }
            return allStates;
        }

        protected override void LoadViewState(object savedState)
        {
            if (savedState != null)
            {
                object[] myState = (object[])savedState;

                // restore base first
                if (myState[0] != null)
                    base.LoadViewState(myState[0]);

                Int32 i = 1;
                foreach (ListItem li in this.Items)
                {
                    // loop through and restore each style attribute
                    foreach (string[] attribute in (string[][])myState[i++])
                    {
                        li.Attributes[attribute[0]] = attribute[1];
                    }
                }
            }
        }
        protected override void CreateChildControls()
        {
            base.CreateChildControls();
        }

        protected override void RenderContents(System.Web.UI.HtmlTextWriter writer)
        {
            ListItemCollection items = Items;
        }

        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;


            string cr_class = "selectpicker form-control ext-control ";
            if (cr_dropup) cr_class += " dropup";
            SweetSoft.ExtraControls.html.HtmlElement select = new SweetSoft.ExtraControls.html.HtmlElement("select");
            select.Attributes.AddRange(this.GetListAttributes());
            select.Attributes.Add(new HtmlAttribute("id", this.ClientID));
            select.Attributes.Add(new HtmlAttribute("name", this.UniqueID));
            select.Attributes.Add(new HtmlAttribute("data-width", this.Width));
            select.Attributes.Add(new HtmlAttribute("data-valid-type", "inline"));
            select.Attributes.Add(new HtmlAttribute("data-run-loader", cr_runLoader.ToString().ToLower()));
            select.Attributes.Add(new HtmlAttribute("title", cr_emptymessage));
            if (TabIndex > 0)
                select.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));
            if (cr_livesearch)
                select.Attributes.Add(new HtmlAttribute("data-live-search", "true"));
            if (cr_displaycount > 0)
                select.Attributes.Add(new HtmlAttribute("data-size", cr_displaycount));
            else
                select.Attributes.Add(new HtmlAttribute("data-size", "auto"));
            if (!this.Enabled)
                select.Attributes.Add(new HtmlAttribute("disabled", "disabled"));
            if (cr_style != ButtonStyle.None)
                select.Attributes.Add(new HtmlAttribute("data-style", cr_style.ToRender()));
            if (cr_countTextLimit == -1)
                select.Attributes.Add(new HtmlAttribute("data-selected-text-format", "count"));
            else
                select.Attributes.Add(new HtmlAttribute("data-selected-text-format", string.Format("count>{0}", cr_countTextLimit)));

            if (!string.IsNullOrEmpty(cr_singleTextFormat))
                select.Attributes.Add(new HtmlAttribute("data-single-text-format", cr_singleTextFormat));

            if (!string.IsNullOrEmpty(cr_multipleTextFormat))
                select.Attributes.Add(new HtmlAttribute("data-mutiple-text-format", cr_multipleTextFormat));

            if (page != null)
            {
                string postbackScript = page.ClientScript.GetPostBackClientHyperlink(this, "");
                if (this.AutoPostBack) select.Attributes.Add(new HtmlAttribute("onchange", string.Format("javascript:setTimeout('__doPostBack(\\'{0}\\',\\'\\')', 0)", this.UniqueID)));
            }

            #region Build Multiple
            if (this.SelectionMode == ListSelectionMode.Multiple)
                select.Attributes.Add(new HtmlAttribute("multiple", "multiple"));
            #endregion

            #region Build Class
            if (cr_showarrowmenu) cr_class += " show-menu-arrow ";
            if (cr_showtick) cr_class += " show-tick ";
            cr_class += string.Format(" {0}", this.CssClass);
            if (!string.IsNullOrEmpty(ValidationGroup)) cr_class += string.Format(" vlg-{0}", ValidationGroup);
            select.Attributes.Add(new HtmlAttribute("class", cr_class));
            #endregion

            #region Render Item collection
            ListItemCollection items = this.Items;
            if (items != null)
            {
                foreach (ListItem it in items)
                {

                    SweetSoft.ExtraControls.html.HtmlElement element = new SweetSoft.ExtraControls.html.HtmlElement("option", null, null, HttpUtility.HtmlEncode(it.Text), null, null, true, null);
                    foreach (string attribute in it.Attributes.Keys)
                    {
                        element.Attributes.Add(new HtmlAttribute(attribute, it.Attributes[attribute]));
                    }

                    element.Attributes.Add(new HtmlAttribute("value", it.Value));
                    element.Attributes.Add(new HtmlAttribute("class", "opt"));
                    if (it.Selected) element.Attributes.Add(new HtmlAttribute("selected", "selected"));
                    if (!it.Enabled) element.Attributes.Add(new HtmlAttribute("disabled", "disabled"));

                    select.Elements.Add(element);
                }
            }
            #endregion

            writer.WriteHtmlElement(select);
        }

        public string GetValidPromt(string message)
        {
            return string.Format("$('button[data-id={0}]').validationEngine('showPrompt', '{1}', 'error', true);", this.ClientID, message);
        }
    }

    public class ExtraListItem
    {
        public string GroupName { get; set; }
        public string Text { get; set; }
        public string Value { get; set; }
        public string SubText { get; set; }
        public bool Enable { get; set; }
        public string CssClass { get; set; }
    }
}
