﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
   

    public class BaseUserControl : WebControl
    {
        #region Properties
        /// <summary>
        /// Placeholder chứa các script
        /// </summary>
        public string ScriptPlaceHolder
        {
            get
            {
                string result = "JSPlaceHolder";
                if (ConfigurationManager.AppSettings["JSPlaceHolder"] != null)
                    result = ConfigurationManager.AppSettings["JSPlaceHolder"];
                return result;
            }
        }
        /// <summary>
        /// Placeholder chứa các style
        /// </summary>
        public string StylePlaceHolder
        {
            get
            {
                string result = "StylePlaceHolder";
                if (ConfigurationManager.AppSettings["StylePlaceHolder"] != null)
                    result = ConfigurationManager.AppSettings["StylePlaceHolder"];
                return result;
            }
        }
        #endregion

     
    }
}
