﻿using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
    [DefaultProperty("Text")]
    [ParseChildren(false)]
    [ToolboxData("<{0}:ExtraTextBox runat='server' />")]
    public class ExtraTextBox : TextBox
    {
        private bool rebuildControl = false;//test123
        #region Properties

        public string TextValue
        {
            get
            {
                return base.Text.Replace("\'", "\'\'").Replace("%", "[%]");
            }
            set
            {
                base.Text = value;
            }
        }
        HtmlElement input = new HtmlElement("input");
        private bool cr_required = false;
        public bool Required { get { return cr_required; } set { cr_required = value; } }

        private int cr_maxlength = -1;
        public int MaxTextLength { get { return cr_maxlength; } set { cr_maxlength = value; } }

        private TextBoxStyle cr_style = ExtraSkinManager.TEXTBOX_STYLE;
        public TextBoxStyle SkinStyle { get { return cr_style; } set { cr_style = value; } }

        private TextBoxValidateType cr_customValidation = TextBoxValidateType.None;
        public TextBoxValidateType ValidationType { get { return cr_customValidation; } set { cr_customValidation = value; } }

        private string cr_description = string.Empty;
        public string Description { get { return cr_description; } set { cr_description = value; rebuildControl = true; } }

        private ValidationStyle cr_valid = ExtraSkinManager.DEFAULT_VALIDATIONTYPE;
        public ValidationStyle ValidStyle { get { return cr_valid; } set { cr_valid = value; } }

        private bool cr_validByKey = false;
        public bool ValidByKeypress { get { return cr_validByKey; } set { cr_validByKey = true; } }

        public string cr_valid_regex = string.Empty;
        public string ExtenRegex { get { return cr_valid_regex; } set { cr_valid_regex = value; } }

        public string cr_valid_regex_mesg = "";
        public string ValidRegexMessage { get { return cr_valid_regex_mesg; } set { cr_valid_regex_mesg = value; } }

        private string cr_placeholder = string.Empty;
        public string PlaceHolder { get { return cr_placeholder; } set { cr_placeholder = value; } }

        public string cr_enter_input = string.Empty;
        /// <summary>
        /// ID control sẽ click khi textbox nhấn enter
        /// </summary>
        public string EnterSubmitClientID
        {
            get { return cr_enter_input; }
            set
            {
                cr_enter_input = value;
                input.Attributes.Add(new HtmlAttribute("data-input-enter", "true"));
                input.Attributes.Add(new HtmlAttribute("data-enter-id", cr_enter_input));
            }
        }

        public string ValidRequiredMessage { get; set; }

        private HorizontalAlign cr_iconAlign = HorizontalAlign.NotSet;
        public HorizontalAlign IconAlign { get { return cr_iconAlign; } set { cr_iconAlign = value; } }

        private string cr_icon = string.Empty;
        public string IconClass { get { return cr_icon; } set { cr_icon = value; } }

        private string cr_icon_color = "#478fca";
        public string IconColor { get { return cr_icon_color; } set { cr_icon_color = value; } }

        private string cr_icon_background = "transparent";
        public string IconBackground { get { return cr_icon_background; } set { cr_icon_background = value; } }

        private string cr_valid_target = "";
        public string ValidTarget { get { return cr_valid_target; } set { cr_valid_target = value; } }
        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ExtraScriptRegister.RegisterTextbox = true;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

        }

        protected override void Render(HtmlTextWriter writer)
        {
            string cssClass = string.Format("ext-control form-control {0}", this.CssClass);
            if (!string.IsNullOrEmpty(ValidationGroup)) cssClass += string.Format(" vlg-{0}", ValidationGroup);
            cssClass += " " + cr_style.ToRender();
            string valid = string.Empty;
            #region Validation
            if (cr_required) valid += "required,";
            if (cr_maxlength > 0) valid += string.Format("maxSize[{0}],", cr_maxlength);
            if (cr_customValidation != TextBoxValidateType.None) valid += string.Format("custom[{0}],", cr_customValidation.ToRender());
            if (cr_customValidation != TextBoxValidateType.None) cssClass += string.Format("mask-{0}", cr_customValidation.ToRender());
            if (!string.IsNullOrEmpty(cr_valid_regex)) valid += "custom[extregex],";

            valid = valid.Trim();
            if (!string.IsNullOrEmpty(valid))
            {
                if (valid.EndsWith(",")) valid = valid.Substring(0, valid.Length - 1);
                //  cssClass += string.Format("validate[{0}]", valid);
            }
            #endregion


            if (this.Enabled == false)
            {
                input.Attributes.Add(new HtmlAttribute("disabled", "disabled"));
            }

            if (this.ReadOnly == true)
            {
                input.Attributes.Add(new HtmlAttribute("readonly", "readonly"));
            }

            if (!string.IsNullOrEmpty(cr_valid_target)) input.Attributes.Add(new HtmlAttribute("data-valid-target", cr_valid_target));
            if (!string.IsNullOrEmpty(cr_placeholder)) input.Attributes.Add(new HtmlAttribute("placeholder", cr_placeholder));
            if (this.TextMode != TextBoxMode.MultiLine)
            {

                input.Attributes.Add(new HtmlAttribute("type", this.TextMode == TextBoxMode.SingleLine ? "text" : "password"));
                input.Attributes.Add(new HtmlAttribute("class", cssClass));
                input.Attributes.Add(new HtmlAttribute("id", this.ClientID));
                input.Attributes.Add(new HtmlAttribute("name", this.UniqueID));
                input.Attributes.Add(new HtmlAttribute("width", this.Width));

                if (!string.IsNullOrEmpty(this.Text))
                    input.Attributes.Add(new HtmlAttribute("value", this.Text));
                if (!string.IsNullOrEmpty(valid))
                    input.Attributes.Add(new HtmlAttribute("data-validation-engine", string.Format("validate[{0}]", valid)));

            }
            else if (this.TextMode == TextBoxMode.MultiLine)
            {
                input = new HtmlElement("textarea", "", "", this.Text, null, null, true, null);
                if (!string.IsNullOrEmpty(cr_placeholder)) input.Attributes.Add(new HtmlAttribute("placeholder", cr_placeholder));
                input.Attributes.Add(new HtmlAttribute("class", string.Format("ext-control form-control {0}", this.CssClass)));
                input.Attributes.Add(new HtmlAttribute("id", this.ClientID));
                input.Attributes.Add(new HtmlAttribute("name", this.UniqueID));
                input.Attributes.Add(new HtmlAttribute("width", this.Width));
                input.Attributes.Add(new HtmlAttribute("height", this.Width));
                if (!string.IsNullOrEmpty(this.Text))
                    input.Attributes.Add(new HtmlAttribute("value", this.Text));
                if (!string.IsNullOrEmpty(valid))
                    input.Attributes.Add(new HtmlAttribute("data-validation-engine", string.Format("validate[{0}]", valid)));

            }

            if (cr_validByKey) input.Attributes.Add(new HtmlAttribute("valid-keypress", "true"));
            input.Attributes.Add(new HtmlAttribute("data-valid-type", cr_valid.ToRender()));
            if (!string.IsNullOrEmpty(cr_valid_regex) &&
                !string.IsNullOrEmpty(cr_valid_regex_mesg))
            {
                input.Attributes.Add(new HtmlAttribute("data-regex", cr_valid_regex));
                input.Attributes.Add(new HtmlAttribute("data-message-extregex", cr_valid_regex_mesg));
            }
            if (!string.IsNullOrEmpty(cr_enter_input))
            {
                input.Attributes.Add(new HtmlAttribute("data-input-enter", "true"));
                input.Attributes.Add(new HtmlAttribute("data-enter-id", cr_enter_input));
            }
            input.Attributes.AddRange(this.GetListAttributes());
            if (TabIndex > 0)
                input.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));

            if (cr_iconAlign != HorizontalAlign.NotSet &&
                !string.IsNullOrEmpty(cr_icon) &&
                !string.IsNullOrEmpty(cr_icon_color) &&
                !string.IsNullOrEmpty(cr_icon_background))
            {
                HtmlElement spanIcon = new HtmlElement("span", "input-icon" + (cr_iconAlign == HorizontalAlign.Right ? " input-icon-right" : string.Empty), string.Empty, null, null, null, true, null);
                if (!this.Width.IsEmpty)
                    spanIcon.Attributes.Add(new HtmlAttribute("style", "width:" + this.Width));
                else
                    spanIcon.Attributes.Add(new HtmlAttribute("style", "width:100%"));
                spanIcon.Elements.Add(input);
                HtmlElement icon = new HtmlElement("i");
                icon.Attributes.Add(new HtmlAttribute("class", "ace-icon " + cr_icon));
                icon.Attributes.Add(new HtmlAttribute("style", string.Format("color:{0};background-color:{1};", cr_icon_color, cr_icon_background)));
                spanIcon.Elements.Add(icon);
                writer.WriteHtmlElement(spanIcon);
            }
            else
                writer.WriteHtmlElement(input);
            if (!string.IsNullOrEmpty(cr_description))
            {
                HtmlElement span = new HtmlElement("span", "sp-title", null, this.cr_description, null, null, true, null);
                writer.WriteHtmlElement(span);
            }
        }
    }

    public enum TextBoxValidateType
    {
        [Render("")]
        None,
        [Render("email")]
        Email,
        [Render("phone")]
        Phone,
        [Render("onlyLetterSp")]
        Letter,
        [Render("integer")]
        Number,
        [Render("onlyNumberSp")]
        OnlyNumber,
        [Render("onlyLetterNumber")]
        LetterAndNumber,
        [Render("currency")]
        Currency,
        [Render("date")]
        Date,
        [Render("time")]
        Time,
        [Render("datetime")]
        DateTime,
    }
}
