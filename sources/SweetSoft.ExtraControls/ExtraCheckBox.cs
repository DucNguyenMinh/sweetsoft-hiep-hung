﻿using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using SweetSoft.ExtraControls;
using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls;

namespace SweetSoft.ExtraControls
{
    [ToolboxData("<{0}:ExtraCheckBox runat=\"server\" />")]
    [ParseChildren(false)]
    public class ExtraCheckBox : CheckBox
    {
        #region Properties
        private string cr_ontext = "Bật";
        [Category("Description"),
        Description("Text trạng thái on")]
        public string DataOn { get { return cr_ontext; } set { cr_ontext = value; } }

        private string cr_offtext = "Tắt";
        [Category("Appearance"),
        Description("Text trạng thái off")]
        public string DataOff { get { return cr_offtext; } set { cr_offtext = value; } }

        private ExtraStyle cr_OnStyle = ExtraSkinManager.CHECKBOX_STYLE;
        [Category("Appearance"),
        Description("Style trạng thái on")]
        public ExtraStyle StyleOn { get { return cr_OnStyle; } set { cr_OnStyle = value; } }

        private ExtraStyle cr_OffStyle = ExtraStyle.Default;
        [Category("Appearance"),
        Description("Style trạng thái off")]
        public ExtraStyle StyleOff { get { return cr_OffStyle; } set { cr_OffStyle = value; } }

        private CheckBoxSize cr_Size = CheckBoxSize.Small;
        [Category("Appearance"),
        Description("Kích thước")]
        public CheckBoxSize Size { get { return cr_Size; } set { cr_Size = value; } }

        private CheckBoxType cr_cbtype = CheckBoxType.Normal;
        [Category("Appearance"),
        Description("Kiểu checkbox")]
        public CheckBoxType CheckboxType { get { return cr_cbtype; } set { cr_cbtype = value; } }

        private CheckBoxBorder cr_cbborder = CheckBoxBorder.Round;
        public CheckBoxBorder CheckboxBorder { get { return cr_cbborder; } set { cr_cbborder = value; } }

        private string cr_labelstyle = string.Empty;
        public string LabelStyle { get { return cr_labelstyle; } set { cr_labelstyle = value; } }

        private Unit cr_width = Unit.Empty;
        public Unit Width { get { return cr_width; } set { cr_width = value; } }

        private string cr_text = string.Empty;
        public string Text { get { return cr_text; } set { cr_text = value; } }

        private string cr_valid_target = string.Empty;
        public string ValidTargetID { get { return cr_valid_target; } set { cr_valid_target = value; } }

        private ValidationStyle cr_valid = ExtraSkinManager.DEFAULT_VALIDATIONTYPE;
        public ValidationStyle ValidStyle { get { return cr_valid; } set { cr_valid = value; } }

        private bool cr_runLoader = true;
        public bool RunLoaderWhenPostBack { get { return cr_runLoader; } set { cr_runLoader = value; } }

        private bool cr_enableTriggerGroup = false;
        public bool EnableClickGroup { get { return cr_enableTriggerGroup; } set { cr_enableTriggerGroup = value; } }

        private string cr_groupName = string.Empty;
        public string GroupName { get { return cr_groupName; } set { cr_groupName = value; } }
        #endregion

        #region Register Script

        protected override void OnInit(EventArgs e)
        {
            ExtraScriptRegister.RegisterCheckbox = true;
            base.OnInit(e);
        }
        #endregion

        #region Render
        protected override void Render(HtmlTextWriter writer)
        {
            if (this.Visible)
            {
                Page page = this.Page;
                if (page == null)
                    page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

                HtmlElement divSwitch = new HtmlElement("label");
                string cssClass = "ext-control " + this.CssClass;
                cssClass += " " + cr_OnStyle.ToRender();
                #region Theme Ace
                if (ExtraSkinManager.DEFAULT_THEME == ExtraTheme.ACE)
                {

                    if (cr_cbtype != CheckBoxType.Toggle)
                    {
                        HtmlElement input = new HtmlElement("input");
                        #region Input
                        input.Attributes.Add(new HtmlAttribute("id", this.ClientID));
                        input.Attributes.Add(new HtmlAttribute("type", "checkbox"));
                        input.Attributes.Add(new HtmlAttribute("name", this.UniqueID));
                        string inputClass = "ace";
                        if (cr_cbtype != CheckBoxType.Normal)
                        {
                            string defaultSwitch = cr_cbtype.ToRender();

                            inputClass += " " + defaultSwitch;
                        }
                        inputClass += " " + this.CssClass;
                        input.Attributes.Add(new HtmlAttribute("class", inputClass));

                        if (this.Checked)
                            input.Attributes.Add(new HtmlAttribute("checked", "checked"));

                        if (TabIndex > 0)
                            input.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));

                        if (!string.IsNullOrEmpty(cr_groupName))
                            input.Attributes.Add(new HtmlAttribute("data-group-name", cr_groupName));
                        if (EnableClickGroup)
                            input.Attributes.Add(new HtmlAttribute("data-click-group", EnableClickGroup.ToString().ToLower()));

                        string postbackScript = string.Empty;
                        if (page != null)
                        {
                            postbackScript = page.ClientScript.GetPostBackClientHyperlink(this, "");
                            string onclick = this.Attributes["onclick"];
                            if (this.AutoPostBack)
                            {
                                if (string.IsNullOrEmpty(onclick))
                                    input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}", postbackScript)));
                                else
                                    input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}{1}setTimeout('__doPostBack(\\'{2}\\',\\'\\')', 0);", onclick.Trim(), onclick.Trim().EndsWith(";") ? string.Empty : ";", this.UniqueID)));

                            }
                        }
                        input.Attributes.AddRange(this.GetListAttributes());
                        #endregion

                        HtmlElement label = new HtmlElement("label");
                        #region Label
                        label.Attributes.Add(new HtmlAttribute("class", "ext-control cb " + cr_Size.ToRender() + " pos-rel "));
                        label.Elements.Add(input);

                        // if (!string.IsNullOrEmpty(cr_text))
                        {
                            HtmlElement labelText = new HtmlElement("label", "lbl " + cr_labelstyle, null, this.cr_text, null, null, true, null);
                            labelText.Attributes.Add(new HtmlAttribute("for", this.ClientID));
                            if (cr_cbtype != CheckBoxType.Normal)
                            {
                                labelText.Attributes.Add(new HtmlAttribute("data-off", cr_offtext));
                                labelText.Attributes.Add(new HtmlAttribute("data-on", cr_ontext));
                            }

                            label.Elements.Add(labelText);
                        }
                        #endregion
                        writer.WriteHtmlElement(label);
                    }

                }
                else if (ExtraSkinManager.DEFAULT_THEME == ExtraTheme.MaterialLTE)
                {
                    HtmlElement div = new HtmlElement("div");
                    div.Attributes.Add(new HtmlAttribute("class", "checkbox"));

                    HtmlElement label = new HtmlElement("label", " ", null, "  " + this.cr_text, null, null, true, null);

                    HtmlElement input = new HtmlElement("input");
                    #region Input
                    input.Attributes.Add(new HtmlAttribute("id", this.ClientID));
                    input.Attributes.Add(new HtmlAttribute("type", "checkbox"));
                    input.Attributes.Add(new HtmlAttribute("name", this.UniqueID));


                    if (this.Checked)
                        input.Attributes.Add(new HtmlAttribute("checked", "checked"));

                    if (TabIndex > 0)
                        input.Attributes.Add(new HtmlAttribute("tabindex", TabIndex));

                    if (!string.IsNullOrEmpty(cr_groupName))
                        input.Attributes.Add(new HtmlAttribute("data-group-name", cr_groupName));
                    if (EnableClickGroup)
                        input.Attributes.Add(new HtmlAttribute("data-click-group", EnableClickGroup.ToString().ToLower()));

                    string postbackScript = string.Empty;
                    if (page != null)
                    {
                        postbackScript = page.ClientScript.GetPostBackClientHyperlink(this, "");
                        string onclick = this.Attributes["onclick"];
                        if (this.AutoPostBack)
                        {
                            if (string.IsNullOrEmpty(onclick))
                                input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}", postbackScript)));
                            else
                                input.Attributes.Add(new HtmlAttribute("onclick", string.Format("{0}{1}setTimeout('__doPostBack(\\'{2}\\',\\'\\')', 0);", onclick.Trim(), onclick.Trim().EndsWith(";") ? string.Empty : ";", this.UniqueID)));

                        }
                    }
                    input.Attributes.AddRange(this.GetListAttributes());
                    label.Elements.Add(input);
                    #endregion
                    div.Elements.Add(label);
                    writer.WriteHtmlElement(div);
                }
                #endregion


                #region Old Code
                if (false)
                {

                    //input.Attributes.Add(new HtmlAttribute("data-run-loader", cr_runLoader.ToString().ToLower()));

                    //if (!string.IsNullOrEmpty(ValidationGroup))
                    //    input.Attributes.Add(new HtmlAttribute("data-group-name", ValidationGroup));
                    //if (EnableClickGroup)
                    //    input.Attributes.Add(new HtmlAttribute("data-change-group", EnableClickGroup));

                    //if (cr_cbtype != CheckBoxType.Normal)
                    //    divClass = "switch";
                    //divClass += " " + cr_OnStyle.ToRender();

                    //divSwitch.Attributes.Add(new HtmlAttribute("class", divClass));
                    //string divStyle = string.Format("width:{0}", cr_width.IsEmpty ? "100px" : cr_width.ToString());
                    //if (cr_cbtype == CheckBoxType.Normal)
                    //{
                    //    divStyle = "height:16px;";
                    //    if (string.IsNullOrEmpty(cr_text) || cr_width.IsEmpty) divStyle += "width:16px;";
                    //    else divStyle += string.Format("width:{0};", cr_width.ToString());
                    //}
                    //divStyle += " display:inline-flex;";
                    //divSwitch.Attributes.Add(new HtmlAttribute("style", divStyle));

                    //cssClass = cr_cbtype.ToRender() + " " + cssClass;
                    //divSwitch.Elements.Add(input);


                    //label.Attributes.Add(new HtmlAttribute("for", this.ClientID));
                    //label.Attributes.Add(new HtmlAttribute("class", "cb " + cr_Size.ToRender() + " " + cr_cbborder.ToRender()));
                    //divSwitch.Elements.Add(label);

                    //if (cr_cbtype == CheckBoxType.YesNo)
                    //{
                    //    label.Attributes.Add(new HtmlAttribute("data-off", cr_offtext));
                    //    label.Attributes.Add(new HtmlAttribute("data-on", cr_ontext));
                    //    cr_labelstyle = string.Format("width:{0} {1}", cr_width.IsEmpty ? "120px" : cr_width.ToString(), cr_labelstyle);
                    //    label.Attributes.Add(new HtmlAttribute("style", cr_labelstyle));
                    //}

                    //if (!string.IsNullOrEmpty(cr_text))
                    //{
                    //    HtmlElement labelText = new HtmlElement("label", "text", null, this.cr_text, null, null, true, null);
                    //    divSwitch.Elements.Add(labelText);
                    //}

                    //if (!this.Enabled)
                    //{
                    //    input.Attributes.Add(new HtmlAttribute("disabled", "disabled"));
                    //    divSwitch.Attributes.Add(new HtmlAttribute("disabled", "disabled"));
                    //}
                    //if (this.Checked)
                    //    input.Attributes.Add(new HtmlAttribute("checked", "checked"));

                    //if (!string.IsNullOrEmpty(ValidationGroup)) cssClass += string.Format(" vlg-{0}", ValidationGroup);
                    //input.Attributes.Add(new HtmlAttribute("class", cssClass));
                    //input.Attributes.AddRange(this.GetListAttributes());
                    //input.Attributes.Add(new HtmlAttribute("data-valid-type", cr_valid.ToRender()));
                    //if (!string.IsNullOrEmpty(cr_valid_target)) input.Attributes.Add(new HtmlAttribute("data-valid-target", cr_valid_target));

                    //if (page != null)
                    //{
                    //    postbackScript = page.ClientScript.GetPostBackClientHyperlink(this, "");
                    //    if (this.AutoPostBack) input.Attributes.Add(new HtmlAttribute("onclick", postbackScript));
                    //}
                }
                #endregion



            }

        }
        #endregion
    }
}
