﻿using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
    public class ExtraGrid : GridView
    {
        public event GridNeedDataSourceHandler NeedDataSource;

        #region Properties

        private const string _GridSortExpression = "GridSortExpression";
        private const string _GridSortDerection = "GridSortDerection";
        private const string _GridVirtualCount = "GridVirtualCount";
        private const string _GridPagingControlName = "GridPagingControlName";
        private const string _GridPageIndex = "GridPageIndex";
        private const string _GridPageSize = "GridPageSize";
        private const string _GridInsert = "GridInsert";
        private const string _GridBindingWithFiltering = "GridBindingWithFiltering";

        public bool BindingWithFiltering
        {
            get
            {
                bool vl = false;
                if (this.ViewState[string.Format("{0}_{1}", _GridBindingWithFiltering, this.ClientID)] != null)
                {
                    bool u = false;
                    if (bool.TryParse(this.ViewState[string.Format("{0}_{1}", _GridBindingWithFiltering, this.ClientID)].ToString(), out u))
                        vl = u;
                }
                return vl;
            }
            set
            {
                this.ViewState[string.Format("{0}_{1}", _GridBindingWithFiltering, this.ClientID)] = value;
            }
        }

        private bool showHeader = true;

        public string CurrentSortExpression
        {
            get
            {
                if (this.ViewState[string.Format("{0}_{1}", _GridSortExpression, this.ClientID)] != null) return this.ViewState[string.Format("{0}_{1}", _GridSortExpression, this.ClientID)].ToString();
                return string.Empty;
            }
            set
            {
                if (this.ViewState[string.Format("{0}_{1}", _GridSortExpression, this.ClientID)] != null &&
                   this.ViewState[string.Format("{0}_{1}", _GridSortExpression, this.ClientID)].ToString() != value) CurrentSortDerection = "ASC";
                else
                {
                    if (CurrentSortDerection == "ASC") CurrentSortDerection = "DESC";
                    else CurrentSortDerection = "ASC";
                }
                this.ViewState[string.Format("{0}_{1}", _GridSortExpression, this.ClientID)] = value;
            }
        }

        public string CurrentSortDerection
        {
            get
            {
                if (this.ViewState[string.Format("{0}_{1}", _GridSortDerection, this.ClientID)] == null) this.ViewState[string.Format("{0}_{1}", _GridSortDerection, this.ClientID)] = "DESC";
                return this.ViewState[string.Format("{0}_{1}", _GridSortDerection, this.ClientID)].ToString();
            }
            set { this.ViewState[string.Format("{0}_{1}", _GridSortDerection, this.ClientID)] = value; }
        }


        public bool AutoResponsive
        {
            get
            {
                if (this.ViewState[this.ID + "_IsResponsive"] == null)
                    this.ViewState[this.ID + "_IsResponsive"] = true;
                return (bool)this.ViewState[this.ID + "_IsResponsive"];
            }
            set
            {
                bool _IsResponsive = false;
                if (bool.TryParse(value.ToString(), out _IsResponsive))
                    _IsResponsive = true;
                this.ViewState[this.ID + "_IsResponsive"] = _IsResponsive;
            }
        }
        /// <summary>
        /// Biến số dòng ảo
        /// </summary>
        public int? VirtualRecordCount
        {
            get
            {
                int? vl = 0;
                if (this.ViewState[string.Format("{0}_{1}", _GridVirtualCount, this.ClientID)] != null)
                {
                    int u = 0;
                    if (int.TryParse(this.ViewState[string.Format("{0}_{1}", _GridVirtualCount, this.ClientID)].ToString(), out u))
                        vl = u;
                }
                return vl;
            }
            set
            {
                this.ViewState[string.Format("{0}_{1}", _GridVirtualCount, this.ClientID)] = value;
            }
        }
        private string cr_pagingName = "paging";
        public string PagingControlName
        {
            get { return cr_pagingName; }

            set { cr_pagingName = value; }

        }

        private string cr_columnVisible = string.Empty;
        public string ColumnVisibleDefault { get { return cr_columnVisible; } set { cr_columnVisible = value; } }


        private string cr_filterColumnControlClientId = string.Empty;
        public string FilterColumnsControlClientID
        {
            get { return cr_filterColumnControlClientId; }
            set
            {
                cr_filterColumnControlClientId = value;
                this.Attributes.Add("filterClientId", cr_filterColumnControlClientId);
            }
        }
        public int CurrentPageIndex
        {
            get
            {

           
                int vl = 1;
                if (this.ViewState[string.Format("{0}_{1}", _GridPageIndex, this.ClientID)] != null)
                {
                    int u = 1;
                    if (int.TryParse(this.ViewState[string.Format("{0}_{1}", _GridPageIndex, this.ClientID)].ToString(), out u))
                        vl = u;
                }
                return vl;
            }
            set
            {
                this.ViewState[string.Format("{0}_{1}", _GridPageIndex, this.ClientID)] = value;
            }
        }

        public int CurrentPageSize
        {
            get
            {
                int vl = 15;
                if (this.ViewState[string.Format("{0}_{1}", _GridPageSize, this.ClientID)] != null)
                {
                    int u = 15;
                    if (int.TryParse(this.ViewState[string.Format("{0}_{1}", _GridPageSize, this.ClientID)].ToString(), out u))
                        vl = u;
                }
                PageSize = vl;
                return vl;
            }
            set
            {
                this.ViewState[string.Format("{0}_{1}", _GridPageSize, this.ClientID)] = value;
                PageSize = value;
            }
        }
        public bool IsInsert
        {
            get
            {
                bool vl = false;
                if (this.ViewState[string.Format("{0}_{1}", _GridInsert, this.ClientID)] != null)
                {
                    bool u = false;
                    if (bool.TryParse(this.ViewState[string.Format("{0}_{1}", _GridInsert, this.ClientID)].ToString(), out u))
                        vl = u;
                }
                return vl;
            }
        }
        /// <summary>
        /// Control paging 
        /// </summary>
        public ExtraGridPaging PagingControl
        {
            get
            {
                ExtraGridPaging pageControl = null;
                if (BottomPagerRow != null && BottomPagerRow.Controls.Count > 0 && !string.IsNullOrEmpty(PagingControlName))
                {
                    BottomPagerRow.Visible = true;
                    //Control ctl = ;
                    //if (ctl != null && ctl.GetType().Equals(typeof(ExtraGridPaging)))
                    pageControl = BottomPagerRow.FindControl(PagingControlName) as ExtraGridPaging;
                }
                return pageControl;
            }
        }

        private bool cr_alterRowStyle = true;
        public bool AlterningRowStyle { get { return cr_alterRowStyle; } set { cr_alterRowStyle = value; } }

        private bool cr_fixHeader = true;
        public bool EnableFixHeader { get { return cr_fixHeader; } set { cr_fixHeader = value; } }

        private int cr_fixLeftColumn = 0;
        public int FixColumnLeft { get { return cr_fixLeftColumn; } set { cr_fixLeftColumn = value; } }

        private int cr_fixRightColumn = 0;
        public int FixColumnRight { get { return cr_fixRightColumn; } set { cr_fixRightColumn = value; } }
        #endregion

        public int GetColumnSort()
        {

            int i = -1;
            for (int j = 0; j < this.Columns.Count; j++)
            {
                try
                {
                    if (!AllowSorting || string.IsNullOrEmpty(this.Columns[j].SortExpression))
                    {
                        this.HeaderRow.Cells[j].CssClass = "sorting_disabled";
                        this.HeaderRow.Cells[j].Attributes.Add("data-sorting-sw", this.HeaderRow.Cells[j].CssClass);
                    }

                    if (this.Columns[j].SortExpression.ToLower() == CurrentSortExpression.ToLower() && !string.IsNullOrEmpty(CurrentSortExpression))
                    {
                        i = j;
                    }
                }
                catch (Exception)
                {
                }
            }
            return i;
        }

        public ExtraStyle cr_style = ExtraSkinManager.EXTRA_STYLE;
        public ExtraStyle EStyle { get { return cr_style; } set { cr_style = value; } }

        public void UpdateColumnSorting()
        {
            // if (AllowSorting)
            {
                int column = GetColumnSort();
                if (column > -1)
                {
                    try
                    {
                        this.HeaderRow.Cells[column].CssClass = CurrentSortDerection == "DESC" ? "sorting_desc" : "sorting_asc";
                        this.HeaderRow.Cells[column].Attributes.Add("data-sorting-sw", this.HeaderRow.Cells[column].CssClass);
                    }
                    catch (Exception ex) { }
                }
            }
        }

        #region Public function created by duc.nguyen in 23/07/2015
        public void RenderPaging()
        {
            if (AllowPaging && PagerSettings.Position == PagerPosition.Bottom)
            {
                if (VirtualRecordCount.HasValue)
                {
                    ExtraGridPaging pageControl = PagingControl;
                    if (pageControl != null)
                    {
                        pageControl.BindingWithFiltering = this.BindingWithFiltering;
                        pageControl.RenderButtonControl(CurrentPageIndex, CurrentPageSize, pageControl.PageCount, VirtualRecordCount.Value);
                        pageControl.SetMessage(string.Format(" Hiển thị <b>{0}</b> đến <b>{1}</b> trên tổng số <b>{2}</b> kết quả.", ((CurrentPageIndex - 1) * CurrentPageSize + 1), ((CurrentPageIndex - 1) * CurrentPageSize + CurrentPageSize), this.VirtualRecordCount));
                    }
                }
            }
        }

        public object DataSourceEmpty
        {
            get;
            set;
        }

        /// <summary>
        /// Hàm thêm dòng insert trên lưới: duc.nguyen 24/07/2015
        /// </summary>
        public void InsertItem()
        {
            this.ViewState[string.Format("{0}_{1}", _GridInsert, this.ClientID)] = true;
            EditIndex = 0;
            AllowSorting = false;
            Rebind();
        }
        /// <summary>
        /// Hàm clear các thuộc tính Edit trên lưới: duc.nguyen 24/07/2015
        /// </summary>
        public void CancelAllCommand()
        {
            this.ViewState[string.Format("{0}_{1}", _GridInsert, this.ClientID)] = false;
            EditIndex = -1;
            Rebind();
        }

        public void CancelInsertCommand() {
            this.ViewState[string.Format("{0}_{1}", _GridInsert, this.ClientID)] = false;
            EditIndex = -1;
        }
        /// <summary>
        /// Hàm Rebind lại dữ liệu trên lưới duc.nguyen 24/07/2015
        /// </summary>
        public void Rebind()
        {
            string id = this.ID;
            LoadDataSource();
        }
        /// <summary>
        /// Hàm Rebind lại dữ liệu trên lưới duc.nguyen 28/07/2015
        /// </summary>
        /// <param name="filter">Có đang filter hay không?</param>
        public void Rebind(bool filter)
        {
            string id = this.ID;
            BindingWithFiltering = filter;
            LoadDataSource();
        }

        private void LoadDataSource()
        {
            if (NeedDataSource != null)
            {
                ExtraGridEventArg e = new ExtraGridEventArg();
                e.IsInsert = IsInsert;
                NeedDataSource(this, e);
            }

            if (DataSource != null)
                this.DataBind();

            if (this.Rows.Count == 0 && DataSourceEmpty != null)
            {
                this.DataSource = DataSourceEmpty;
                this.DataBind();
                this.Rows[0].Visible = false;
            }



            this.RenderPaging();
            this.UpdateColumnSorting();

        }

        public bool CheckColumnVisible(int index)
        {
            if (!string.IsNullOrEmpty(ColumnVisibleDefault))
            {
                return !string.IsNullOrEmpty(ColumnVisibleDefault.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault(p => p == index.ToString()));
            }
            return true;
        }
        #endregion

        #region Custom Event created by duc.nguyen in 23/07/2015
        #endregion

        #region Override Function
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.CssClass = "table dataTable extra-grid table-bordered table-hover  " + this.CssClass;
            if (AutoResponsive) this.CssClass += " dt-responsive responsive ";
            if (AlterningRowStyle) this.CssClass += " table-striped ";
            if (cr_fixHeader)
                this.Attributes.Add("data-fix-header", "true");
           
            if (!AllowSorting) this.Attributes.Add("data-sorting", "false");

            ExtraScriptRegister.RegisterGrid_FixHeader = cr_fixHeader;
            ExtraScriptRegister.RegisterGrid_FixColumn = cr_fixLeftColumn > 0 || cr_fixRightColumn > 0;
            ExtraScriptRegister.RegisterGrid = true;
        }
        protected override void Render(System.Web.UI.HtmlTextWriter writer)
        {

            if (!string.IsNullOrEmpty(cr_filterColumnControlClientId))
            {
                this.Attributes.Add("data-hide", "all");
                this.Attributes.Add("filterClientId", cr_filterColumnControlClientId);
            }
            int row = this.Rows.Count;
            if (this.Rows.Count == 1 && !this.Rows[0].Visible) row = 0;
            this.Attributes.Add("data-rows", row.ToString());
            StringBuilder sb = new StringBuilder();
            HtmlTextWriter tw = new HtmlTextWriter(new System.IO.StringWriter(sb));
            //Render the page to the new HtmlTextWriter which actually writes to the stringbuilder
            base.Render(tw);

            //Get the rendered content
            string sContent = sb.ToString();


            sContent = sContent.Substring(5);//"<div>
            string css = "table-responsive no-top-bottom " + cr_style.ToRender();
            string fixLeft = string.Format("data-fix-left-cl='{0}'", cr_fixLeftColumn);
            string fixRight = string.Format("data-fix-right-cl='{0}'", cr_fixRightColumn);
            sContent = string.Format("<div class='{0}' {1} {2}>", css, fixLeft, fixRight) + sContent;
            sContent = string.Format("<div id='loading_{0}' class='grid-loading'><i class=' fa fa-spinner fa-pulse' style='font-size: 30px;'></i></div>{1}", this.ClientID, sContent);
            writer.Write(sContent);
        }

        protected override void OnRowEditing(GridViewEditEventArgs e)
        {
            EditIndex = e.NewEditIndex;
            AllowSorting = false;
            Rebind();
        }
        protected override void OnRowCancelingEdit(GridViewCancelEditEventArgs e)
        {
            AllowSorting = true;
            CancelAllCommand();
        }

        protected override void OnSorting(GridViewSortEventArgs e)
        {
            CurrentSortExpression = e.SortExpression;
            Rebind();
        }

        protected override void OnLoad(EventArgs e)
        {
            this.PagerStyle.BackColor = Color.FromArgb(239, 243, 248);
            if (PagingControl != null)
            {
                PagingControl.PageChanged += PagingControl_PageChanged;
                PagingControl.PageSizeChanged += PagingControl_PageSizeChanged;
            }
        }

        void PagingControl_PageSizeChanged(object sender, ExtraPagingEventArg e)
        {
            this.CurrentPageSize = e.PageSize;
            this.Rebind();
        }

        void PagingControl_PageChanged(object sender, ExtraPagingEventArg e)
        {
            this.CurrentPageIndex = e.PageIndex;
            this.Rebind();
        }
        #endregion
    }

    public delegate void GridNeedDataSourceHandler(object sender, ExtraGridEventArg e);

    public class ExtraGridEventArg
    {
        public bool IsInsert { get; set; }
    }
}
