﻿using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SweetSoft.ExtraControls
{
    public class ExtraSkinManager : HiddenField
    {
        public static ButtonStyle BUTTON_STYLE = ButtonStyle.Default;
        public static ExtraStyle CHECKBOX_STYLE = ExtraStyle.Default;
        public static DialogStyle DIALOG_STYLE = DialogStyle.Default;
        public static DateTimeStyle DATETIME_STYLE = DateTimeStyle.Defaut;
        public static TextBoxStyle TEXTBOX_STYLE = TextBoxStyle.Default;
        public static ValidationStyle DEFAULT_VALIDATIONTYPE = ValidationStyle.Inline;
        public static ExtraStyle EXTRA_STYLE = ExtraStyle.Default;
        public static string DEFAULT_LANGUAGE = "vi-VN";

        public static ExtraTheme DEFAULT_THEME = ExtraTheme.ACE;
       
        public ExtraTheme ControlTheme
        {
            get { return DEFAULT_THEME; }
            set
            {
                DEFAULT_THEME = value;
            }
        }

        private SkinName cr_skin = SkinName.Violet;
        public SkinName Skin
        {
            get { return this.cr_skin; }
            set
            {
                this.cr_skin = value;
                switch (this.cr_skin)
                {
                    //case SkinName.Green:
                    //    BUTTON_STYLE = ButtonStyle.Success;
                    //    CHECKBOX_STYLE = ExtraStyle.Success;
                    //    DIALOG_STYLE = DialogStyle.Success;
                    //    DATETIME_STYLE = DateTimeStyle.Success;
                    //    TEXTBOX_STYLE = TextBoxStyle.Success;
                    //    break;
                    //case SkinName.Green_Bold:
                    //    BUTTON_STYLE = ButtonStyle.Success_Bold;
                    //    CHECKBOX_STYLE = ExtraStyle.Success;
                    //    DIALOG_STYLE = DialogStyle.Success;
                    //    DATETIME_STYLE = DateTimeStyle.Success;
                    //    TEXTBOX_STYLE = TextBoxStyle.Success;
                    //    break;
                }
            }
        }

        public ButtonStyle ButtonStyle
        {
            get { return ExtraSkinManager.BUTTON_STYLE; }
            set { ExtraSkinManager.BUTTON_STYLE = value; }
        }

        public ExtraStyle CheckBoxStyle
        {
            get { return ExtraSkinManager.CHECKBOX_STYLE; }
            set { ExtraSkinManager.CHECKBOX_STYLE = value; }
        }

        protected override void OnLoad(EventArgs e)
        {
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            if (page != null)
            {
                ClientScriptManager cs = page.ClientScript;
                Type thisT = this.GetType();
                if (thisT != typeof(ExtraScriptRegister))
                    thisT = thisT.BaseType;

                string script = @"  ";

                //if (!SweetSoft_Settings.Properties.Postback) {
                //    var progressPage = $(""<div class='goto-progress'><div class='progress-box'><img class='image' src='/images/loading-gears-animation-3.gif' /></div></div>"");
                //    progressPage.appendTo($('body'));
                //}";



                // cs.RegisterStartupScript(thisT, "RegisterSystemModal", script, false);
            }
        }

        private ValidationStyle cr_valid = ValidationStyle.Tooltip;
        public ValidationStyle ValidationStyle { get { return cr_valid; } set { cr_valid = value; DEFAULT_VALIDATIONTYPE = value; } }
        private string cr_lag = "";
        public string Language { get { return cr_lag; } set { this.cr_lag = value; DEFAULT_LANGUAGE = value; } }
        private string cr_progressPageHtml = "";
        public string ProgressPageInnerHtml { get; set; }
    }
}
