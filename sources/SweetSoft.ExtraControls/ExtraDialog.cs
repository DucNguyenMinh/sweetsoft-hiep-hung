﻿using SweetSoft.ExtraControls.html;
using SweetSoft.ExtraControls.icons;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Security.Permissions;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using SweetSoft.ExtraControls;

namespace SweetSoft.ExtraControls
{
    [ParseChildren(true), PersistChildren(false)]
    [ToolboxData("<{0}:ExtraDialog runat=\"server\"></{0}:ExtraDialog>")]
    [AspNetHostingPermission(SecurityAction.Demand, Level = AspNetHostingPermissionLevel.Minimal)]
    public class ExtraDialog : WebControl, INamingContainer
    {
        private ITemplate headerTemPlate;
        private ITemplate containerTemPlate;
        private ITemplate footerTemPlate;

        private Control headerControl;
        private Control containerControl;
        private Control footerControl;

        private UpdatePanel panelUpdateHeader;
        private UpdatePanel panelUpdateContainer;
        private UpdatePanel panelUpdateFooter;
        private UpdatePanel panelModal;

        #region Properties
        private bool cr_click_close = false;
        public bool CloseOutSide
        {
            get { return cr_click_close; }
            set { cr_click_close = value; }
        }
        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate ContentTemplate
        {
            get { return containerTemPlate; }
            set { containerTemPlate = value; }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate HeaderTemplate
        {
            get { return headerTemPlate; }
            set { headerTemPlate = value; }
        }

        [Browsable(false)]
        [PersistenceMode(PersistenceMode.InnerProperty)]
        [TemplateInstance(TemplateInstance.Single)]
        public ITemplate FooterTemplate
        {
            get { return footerTemPlate; }
            set { footerTemPlate = value; }
        }

        private string cr_url = string.Empty;
        [Category("Appearance"),
        Description("Url Content")]
        public string UrlContent { get { return cr_url; } set { cr_url = value; } }

        private string cr_class = string.Empty;
        [Category("Appearance"),
        Description("Style css class dialog")]
        public string CssClass { get { return cr_class; } set { cr_class = value; } }

        private string cr_header_class = string.Empty;
        [Category("Appearance"),
        Description("Style css class header")]
        public string Header_CssClass { get { return cr_header_class; } set { cr_header_class = value; } }

        private string cr_restriction = "html";
        [Category("Appearance"),
        Description("Style css class body")]
        public string RestrictionZoneID { get { return cr_restriction; } set { cr_restriction = value; } }

        private string cr_body_class = string.Empty;
        [Category("Appearance"),
        Description("Style css class body")]
        public string Body_CssClass { get { return cr_body_class; } set { cr_body_class = value; } }

        private string cr_footer_class = string.Empty;
        [Category("Appearance"),
        Description("Style css class footer")]
        public string Footer_CssClass { get { return cr_footer_class; } set { cr_footer_class = value; } }

        private DialogEffect cr_effect = DialogEffect.FadeIn;
        [Category("Appearance"),
        Description("Hiệu ứng mở dialog")]
        public DialogEffect Effect { get { return cr_effect; } set { cr_effect = value; } }

        private DialogSize cr_size = DialogSize.Normal;
        [Category("Appearance"),
        Description("Kích thước của dialog ")]
        public DialogSize Size { get { return cr_size; } set { cr_size = value; } }

        private Unit cr_width = 0;
        [Category("Appearance"),
        Description("Kích thước bề rộng của dialog ")]
        public Unit Width { get { return cr_width; } set { cr_width = value; } }

        private bool cr_header_showclose = true;
        [Category("Appearance"),
        Description("Hiển thị button close trên header")]
        public bool ShowHeaderButtonClose { get { return cr_header_showclose; } set { cr_header_showclose = value; } }

        private bool cr_footer_showclose = true;
        [Category("Appearance"),
        Description("Hiển thị button close trên footer")]
        public bool ShowFooterButtonClose
        {
            get { return cr_footer_showclose; }
            set
            {
                cr_footer_showclose = value;
            }
        }

        private bool cr_draggeble = true;
        [Category("Appearance"),
        Description("Kích hoạt kéo thả dialog")]
        public bool EnableDraggable { get { return cr_draggeble; } set { cr_draggeble = value; } }

        public string cr_toolbar = string.Empty;
        [Category("Appearance"),
        Description("Button trên header")]
        public string HeadButtons { get { return cr_toolbar; } set { cr_toolbar = value; } }

        [Category("Appearance"),
        Description("Tiêu đề dialog")]
        public string Title
        {
            get
            {
                if (ViewState[this.ClientID + "_Title"] == null)
                    return "";
                return ViewState[this.ClientID + "_Title"].ToString();
            }
            set
            {
                ViewState[this.ClientID + "_Title"] = value;
                if (ltrHeaderText != null) ltrHeaderText.Text = value;
            }
        }

        private DialogStyle cr_style = ExtraSkinManager.DIALOG_STYLE;
        [Category("Appearance"),
        Description("Style dialog")]
        public DialogStyle Style
        {
            get { return cr_style; }
            set
            {
                cr_style = value;
                //if (divModal != null)
                //{
                //    divModal.Attributes.Remove("class");
                //    divModal.Attributes.Add("class", string.Format("modal bootstrap-dialog {0} {1} {2}", this.cr_class, Style.ToRender(), Effect.ToRender()));
                //}
            }
        }

        private string cr_close_text = "Close";
        [Category("Appearance"),
        Description("Text hiển thị cho button Close")]
        public string CloseText
        {
            get { return cr_close_text; }
            set
            {
                cr_close_text = value;
                if (footerButtonClose != null)
                    footerButtonClose.InnerHtml = string.Format("<i  class='fa fa-close' style='margin-right:4px;'></i>{0}", value); ;
            }
        }

        private bool cr_renderUpdatePanel = true;
        public bool AppendUpdatePanel { get { return cr_renderUpdatePanel; } set { cr_renderUpdatePanel = value; } }

        private string cr_validationgroup = string.Empty;
        [Category("Appearance"),
        Description("Text hiển thị cho button Close")]
        public string ValidationGroupBound { get { return cr_validationgroup; } set { cr_validationgroup = value; } }


        public Control ContentControls
        {
            get
            {
                if (containerControl == null) containerControl = new Panel();
                return containerControl;
            }
        }

        public Control HeaderControls
        {
            get
            {
                if (headerControl == null) headerControl = new Panel();
                return headerControl;
            }
        }

        public Control FooterControls
        {
            get
            {
                if (footerControl == null) footerControl = new Panel();
                return footerControl;
            }
        }

        private bool cr_showonload = false;
        public bool ShowOnLoad { get { return cr_showonload; } set { cr_showonload = value; } }
        #endregion

        #region Events
        public event DialogCloseEvent DialogClosed;
        public event DialogOpenEvent DialogOpen;
        #endregion

        protected override void OnInit(EventArgs e)
        {
            ExtraScriptRegister.RegisterDialog = true;
            //  EnsureChildControls();
            base.OnInit(e);
            CreateChildControls();
            //      Page.RegisterRequiresControlState(this);
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //   CreateChildControls();
        }

        public string GetScriptClose() { return string.Format("CloseDialogClient('{0}');", this.ClientID); }

        public string GetScriptOpen() { return string.Format("OpenDialogClient('{0}');", this.ClientID); }

        protected override void OnUnload(EventArgs e)
        {
            base.OnUnload(e);
        }

        HtmlGenericControl divModal = null;
        LiteralControl ltrHeaderText = null;
        HtmlGenericControl footerButtonClose = null;
        private bool onLoad = false;
        protected override void CreateChildControls()
        {
            if (!onLoad)
            {
                Controls.Clear();
                divModal = new HtmlGenericControl("div");
                ltrHeaderText = new LiteralControl("<span class='title'>" + Title + "</span>");
                string closeScript = string.Format("CloseDialogClient('{0}');", this.ClientID);


                #region Build divModal
                divModal.Attributes.Add("id", this.ClientID);
                //    divModal.Attributes.Add("runat", "server");
                string modalClass = this.cr_class;
                if (cr_showonload) divModal.Attributes.Add("data-showonload", "true");
                divModal.Attributes.Add("class", string.Format("modal bootstrap-dialog {0} {1} {2}", modalClass, Style.ToRender(), Effect.ToRender()));
                divModal.Attributes.Add("role", "dialog");
                divModal.Attributes.Add("aria-hidden", "true");
                if (!cr_click_close)
                {
                    divModal.Attributes.Add("data-backdrop", "static");
                    divModal.Attributes.Add("data-keyboard", "false");
                }
                if (!string.IsNullOrEmpty(cr_validationgroup))
                    divModal.Attributes.Add("validation-group-bound", cr_validationgroup);
                List<HtmlAttribute> attrCol = this.GetListAttributes();
                if (attrCol.Count > 0)
                {
                    foreach (HtmlAttribute attr in attrCol)
                        divModal.Attributes.Add(attr.Name, attr.Value);
                }
                #endregion

                HtmlGenericControl divDialog = new HtmlGenericControl("div");
                #region Build div Dialog
                string divDialogStyle = "";
                divDialog.Attributes.Add("class", string.Format("modal-dialog {0}", Size.ToRender()));
                if (Width.Value > 0)
                    divDialogStyle += string.Format("width:{0};", Width);
                if (Height.Value > 0)
                    divDialogStyle += string.Format("height:{0};", Height);
                if (!string.IsNullOrEmpty(divDialogStyle))
                    divDialog.Attributes.Add("style", divDialogStyle);
                #endregion

                HtmlGenericControl divContent = new HtmlGenericControl("div");
                #region Build div Content
                divContent.Attributes.Add("class", "modal-content");
                #endregion


                HtmlGenericControl divHeader = new HtmlGenericControl("div");
                if (cr_draggeble)
                {
                    ExtraScriptRegister.RegisterDraggable = true;
                    cr_header_class = "bootstrap-dialog-draggable " + cr_header_class;
                }
                divHeader.Attributes.Add("class", string.Format("modal-header {0}", cr_header_class));
                divHeader.Attributes.Add("id", string.Format("{0}_header", this.ClientID));
                if (!string.IsNullOrEmpty(cr_restriction))
                    divHeader.Attributes.Add("restrictionzone", RestrictionZoneID);
                #region Build div Header Content
                HtmlGenericControl divHeaderContent = new HtmlGenericControl("div");
                divHeaderContent.Attributes.Add("class", "bootstrap-dialog-header");
                headerControl = new Panel();
                #region button header
                if (!string.IsNullOrEmpty(cr_toolbar))
                {
                    string str = cr_toolbar + ",";
                    str = str.Replace(" ", string.Empty);

                    if (str.Contains("Close,"))
                    {
                        HtmlGenericControl headerButtonClose = new HtmlGenericControl("button");
                        headerButtonClose.Attributes.Add("type", "button");
                        headerButtonClose.Attributes.Add("class", "close");
                        headerButtonClose.Attributes.Add("onclick", closeScript);
                        headerButtonClose.Attributes.Add("aria-hidden", "true");
                        headerButtonClose.Attributes.Add("title", "Close");
                        headerButtonClose.Attributes.Add("style", "margin-top: -5px;margin-left: 8px");
                        headerButtonClose.InnerHtml = "&times;";
                        headerControl.Controls.Add(headerButtonClose);
                    }

                    if (str.Contains("Maximize,"))
                    {
                        HtmlGenericControl headerButtonMaximize = new HtmlGenericControl("button");
                        headerButtonMaximize.Attributes.Add("type", "button");
                        headerButtonMaximize.Attributes.Add("class", "close maximize");
                        headerButtonMaximize.Attributes.Add("aria-hidden", "true");
                        headerButtonMaximize.Attributes.Add("title", cr_close_text);
                        headerButtonMaximize.InnerHtml = "<i class='fa fa-square-o'></i>";
                        headerControl.Controls.Add(headerButtonMaximize);
                    }

                }
                #endregion
                #region Custom control
                if (false) // (cr_renderUpdatePanel)
                {
                    panelUpdateHeader = new UpdatePanel();
                    panelUpdateHeader.UpdateMode = UpdatePanelUpdateMode.Conditional;
                    Panel panelInsideHeaderPanel = new Panel();
                    panelInsideHeaderPanel.Controls.Add(ltrHeaderText);
                    if (HeaderTemplate != null) HeaderTemplate.InstantiateIn(panelInsideHeaderPanel);
                    panelUpdateHeader.ContentTemplateContainer.Controls.Add(panelInsideHeaderPanel);
                    headerControl.Controls.Add(panelUpdateHeader);
                }
                else
                {
                    if (!string.IsNullOrEmpty(Title))
                        headerControl.Controls.Add(ltrHeaderText);
                    else headerControl.Controls.Add(new LiteralControl("&nbsp;"));
                    if (HeaderTemplate != null)
                        HeaderTemplate.InstantiateIn(headerControl);
                }
                divHeaderContent.Controls.Add(headerControl);
                divHeader.Controls.Add(divHeaderContent);
                #endregion
                divContent.Controls.Add(divHeader);
                #endregion

                HtmlGenericControl divBody = new HtmlGenericControl("div");
                #region Build div Body
                divBody.Attributes.Add("class", string.Format("modal-body {0}", cr_body_class));
                containerControl = new Panel();
                if (false)// (cr_renderUpdatePanel)
                {
                    panelUpdateContainer = new UpdatePanel();
                    panelUpdateContainer.UpdateMode = UpdatePanelUpdateMode.Conditional;
                    Panel panelInsideContainerPanel = new Panel();
                    if (ContentTemplate != null) ContentTemplate.InstantiateIn(panelInsideContainerPanel);
                    panelUpdateContainer.ContentTemplateContainer.Controls.Add(panelInsideContainerPanel);
                    containerControl.Controls.Add(panelUpdateContainer);
                }
                else
                    if (ContentTemplate != null) ContentTemplate.InstantiateIn(containerControl);
                HtmlGenericControl divBodyContent = new HtmlGenericControl("div");
                divBodyContent.Attributes.Add("class", "bootstrap-dialog-body");
                divBodyContent.Controls.Add(containerControl);
                divBody.Controls.Add(divBodyContent);
                divContent.Controls.Add(divBody);
                #endregion

                HtmlGenericControl divFooter = new HtmlGenericControl("div");
                #region Build div Footer
                divFooter.Attributes.Add("class", string.Format("modal-footer {0}", cr_footer_class));
                #region Custom control
                footerControl = new Panel();
                Panel panelInsideFooterPanel = null;
                if (false) //(cr_renderUpdatePanel)
                {
                    panelUpdateFooter = new UpdatePanel();
                    panelUpdateFooter.UpdateMode = UpdatePanelUpdateMode.Conditional;
                    panelUpdateFooter.ID = this.ID + "UpdatePanelFooter";
                    panelInsideFooterPanel = new Panel();
                    if (FooterTemplate != null)
                        FooterTemplate.InstantiateIn(panelInsideFooterPanel);
                    panelUpdateFooter.ContentTemplateContainer.Controls.Add(panelInsideFooterPanel);
                    footerControl.Controls.Add(panelUpdateFooter);
                }
                else
                    if (FooterTemplate != null)
                        FooterTemplate.InstantiateIn(footerControl);
                HtmlGenericControl divFooterContent = new HtmlGenericControl("div");
                divFooterContent.Attributes.Add("class", "bootstrap-dialog-footer");
                divFooterContent.Controls.Add(footerControl);
                divFooter.Controls.Add(divFooterContent);
                #endregion
                #region button close
                if (cr_footer_showclose)
                {

                    footerButtonClose = new HtmlGenericControl("a");
                    // footerButtonClose.Attributes.Add("type", "button");
                    footerButtonClose.Attributes.Add("id", string.Format("button_close_dialog_{0}", this.ClientID));
                    footerButtonClose.Attributes.Add("class", "btn btn-default btn-white btn-bold");
                    footerButtonClose.Attributes.Add("onclick", closeScript);
                    footerButtonClose.Attributes.Add("title", cr_close_text);
                    footerButtonClose.InnerHtml = string.Format("<i class='fa fa-close' style='margin-right:4px;'></i>{0}", cr_close_text);
                    if (false)//(cr_renderUpdatePanel && panelUpdateFooter != null)
                        panelInsideFooterPanel.Controls.Add(footerButtonClose);
                    else
                        footerControl.Controls.Add(footerButtonClose);
                }
                #endregion

                divContent.Controls.Add(divFooter);
                #endregion

                if (cr_renderUpdatePanel)
                {
                    panelModal = new UpdatePanel();
                    panelModal.UpdateMode = UpdatePanelUpdateMode.Always;
                    panelModal.ID = this.ID + "UpdatePanelModal";
                    panelModal.ContentTemplateContainer.Controls.Add(divContent);
                    divDialog.Controls.Add(panelModal);
                }
                else
                    divDialog.Controls.Add(divContent);
                divModal.Controls.Add(divDialog);
                Controls.Add(divModal);

            }
            onLoad = true;

        }

        public override void DataBind()
        {
            base.DataBind();
        }

        #region function
        /// <summary>
        /// Hàm đóng Dialog
        /// </summary>
        public void CloseDialog()
        {
            CloseDialog(true);
        }

        public void CloseDialog(bool update)
        {
            string script = string.Format("CloseDialogServer('{0}');", this.ClientID);
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            if (page != null)
            {
                Type thisT = this.GetType();
                if (thisT != typeof(ExtraScriptRegister))
                    thisT = thisT.BaseType;

                ClientScriptManager cs = page.ClientScript;
                ScriptManager.RegisterStartupScript(Page, thisT, "RunScriptModal", script, true);
            }

            if (DialogClosed != null)
            {
                ExtraControlEventArg e = new ExtraControlEventArg();
                DialogClosed(this, e);
            }

            if (update) UpdateContentDialog();

        }

        public void ShowDialog()
        {
            ShowDialog(true);
        }

        /// <summary>
        /// Mở Dialog đi kèm việc updatepanel các control
        /// </summary>
        /// <param name="update"></param>
        public void ShowDialog(bool update)
        {
            if (update) UpdateContentDialog();

            string script = string.Format("OpenDialogClient('{0}');", this.ClientID);
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            if (page != null)
            {
                Type thisT = this.GetType();
                if (thisT != typeof(ExtraScriptRegister))
                    thisT = thisT.BaseType;

                ScriptManager.RegisterStartupScript(page, thisT, "RunScriptModal", script, true);
            }

            if (DialogOpen != null)
            {
                ExtraControlEventArg e = new ExtraControlEventArg();
                DialogOpen(this, e);
            }

        }
        /// <summary>
        /// Làm mới nội dung dialog khi có postback
        /// </summary>
        public void UpdateContentDialog()
        {
            if (panelModal != null)
            {
                panelModal.UpdateMode = UpdatePanelUpdateMode.Conditional;
                panelModal.Update();
                panelModal.UpdateMode = UpdatePanelUpdateMode.Always;
            }
        }
        #endregion
    }


    public class DialogContentTemplate2 : ITemplate
    {
        public void InstantiateIn(Control container)
        {

        }
    }

    public delegate void DialogCloseEvent(object obj, ExtraControlEventArg e);
    public delegate void DialogOpenEvent(object obj, ExtraControlEventArg e);
}
