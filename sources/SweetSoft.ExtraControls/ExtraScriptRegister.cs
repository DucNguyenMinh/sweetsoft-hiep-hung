﻿using SweetSoft.ExtraControls.html;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


namespace SweetSoft.ExtraControls
{
    public class ExtraScriptRegister : HiddenField
    {
        public static bool RegisterGrid = false;
        public static bool RegisterGrid_FixHeader = false;
        public static bool RegisterGrid_FixColumn = false;
        public static bool RegisterCheckbox = false;
        public static bool RegisterRadio = true;
        public static bool RegisterDialog = false;
        public static bool RegisterCombobox = false;
        public static bool RegisterButton = false;
        public static bool RegisterDraggable = false;
        public static bool RegisterTextbox = false;
        public static bool RegisterDateTime = false;
        public static bool RegisterSearchBox = false;
        public static string JSPlaceHolder = "JSPlaceHolder";
        public static string CssPlaceHolder = "StylePlaceHolder";
        public static string aspnetForm = "aspnetForm";
        public static HiddenField hiddenControlProperties = null;
        public static string ScriptRunStartUp = string.Empty;
        public static string CssPath = "/";
        public static string ScriptPath = "/";
        public static string ThemePath = "/";
        public static string IconPath = "/";

        #region Properties

        public string AspnetFormClientID { get { return aspnetForm; } set { aspnetForm = value; } }

        public string StyleFolderPath
        {
            set { CssPath = value; }
        }

        public string ScriptFolderPath
        {
            set { ScriptPath = value; }
        }

        public string ThemeFolderPath
        {
            set { ThemePath = value; }
        }

        public string IconFolderPath
        {
            set { IconPath = value; }
        }

        public ExtraScriptRegister()
            : base()
        {
            if (hiddenControlProperties == null)
                hiddenControlProperties = this;
            else
            {

            }
        }
        private bool cr_enablebootstrapstyle = true;
        public bool EnableBootstrapStyle
        {
            get { return cr_enablebootstrapstyle; }
            set { cr_enablebootstrapstyle = value; }
        }

        private bool cr_enablebootstrapscript = true;
        public bool EnableBootstrapScript
        {
            get { return cr_enablebootstrapscript; }
            set { cr_enablebootstrapscript = value; }
        }

        private bool cr_enableJqueryMinscript = true;
        public bool EnableJqueryMinScript
        {
            get { return cr_enableJqueryMinscript; }
            set { cr_enableJqueryMinscript = value; }
        }
        /// <summary>
        /// Placeholder chứa các script
        /// </summary>
        public string ScriptPlaceHolder
        {
            get
            {
                return JSPlaceHolder;
            }
            set { JSPlaceHolder = value; }
        }
        /// <summary>
        /// Placeholder chứa các style
        /// </summary>
        public string StylePlaceHolder
        {
            get
            {
                return CssPlaceHolder;
            }
            set { CssPlaceHolder = value; }
        }

        private bool cr_addGridStyle = false;
        private bool cr_GridStyleExits = false;
        /// <summary>
        /// Cho phép chèn style của lưới
        /// </summary>
        public bool AllowEmbedGridStyle
        {
            get { return cr_addGridStyle; }
            set
            {
                cr_addGridStyle = value;
                if (value && !cr_GridStyleExits)
                {
                    Page page = this.Page;
                    if (page == null)
                        page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

                    if (page != null)
                    {
                        ClientScriptManager cs = page.ClientScript;
                        Type thisT = this.GetType();
                        if (thisT != typeof(ExtraScriptRegister))
                            thisT = thisT.BaseType;

                        string templatestyle = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />";
                        string gridStyle = string.Format(templatestyle,
                                                           cs.GetWebResourceUrl(thisT, "SweetSoft.ExtraControls.css.dataTables.bootstrap.css"));
                        cr_GridStyleExits = true;

                        LiteralControl link = new LiteralControl(gridStyle);
                        link.EnableViewState = false;


                        if (string.IsNullOrEmpty(StylePlaceHolder))
                            page.Header.Controls.Add(link);
                        else
                        {
                            ContentPlaceHolder cpCss = page.FindControl(StylePlaceHolder) as ContentPlaceHolder;
                            if (cpCss != null)
                                cpCss.Controls.Add(link);
                            else
                                page.Header.Controls.Add(link);
                        }
                    }
                }
            }
        }

        private string cr_Language = "vi-VN";
        /// <summary>
        /// Mã ngôn ngữ mặc định
        /// </summary>
        public string DefaultLanguage { get { return cr_Language; } set { cr_Language = value; } }
        #endregion

        #region On Event
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);
            RegisterScriptAndStyle();
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            //   RegisterScriptAndStyle();
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            //   RegisterScriptAndStyle();
        }
        #endregion

        #region Function
        public void RegisterScriptAndStyle()
        {
            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            if (page != null)
            {
                ClientScriptManager cs = page.ClientScript;

                // Define the resource name and type.
                Type thisT = this.GetType();
                if (thisT != typeof(ExtraScriptRegister))
                    thisT = thisT.BaseType;

                // Check to see if the startup script is already registered.
                if (!cs.IsStartupScriptRegistered(thisT, "RegisterBaseScript"))
                {
                    string templatescript = "<script type=\"text/javascript\" src=\"{0}\"></script> ";
                    string templatestyle = "<link rel=\"stylesheet\" type=\"text/css\" href=\"{0}\" />";
                    if (ExtraSkinManager.DEFAULT_THEME == icons.ExtraTheme.ACE ||
                        ExtraSkinManager.DEFAULT_THEME == icons.ExtraTheme.MaterialLTE)
                    {
                        #region Register Style
                        string cr_style = "";
                        #region Grid
                        string gridStyle = string.Empty;
                        if (RegisterGrid)
                        {
                            gridStyle += string.Format(templatestyle,
                                                                     (CssPath + "css/grids/dataTables.bootstrap.css"));
                            gridStyle += string.Format(templatestyle,
                                                                    (CssPath + "css/grids/responsive.bootstrap.css"));
                            if (RegisterGrid_FixHeader)
                                gridStyle += string.Format(templatestyle,
                                                                     (CssPath + "css/grids/fixedHeader.bootstrap.css"));
                            cr_style += gridStyle;
                        }
                        #endregion

                        if (ExtraSkinManager.DEFAULT_THEME == icons.ExtraTheme.ACE)
                        {
                            


                            string buttonStyle = string.Empty;
                            #region Button Style
                            if (RegisterButton)
                            {
                                buttonStyle += string.Format(templatestyle,
                                                                   (ThemePath + "themes/ace/style/button/ExtraButton.css"));
                            }
                            #endregion

                            string dialogStyle = string.Empty;
                            #region Dialog Style
                            if (RegisterDialog)
                                dialogStyle += string.Format(templatestyle,
                                                                  (CssPath + "css/dialog/bootstrap-dialog.min.css"));
                            #endregion

                            string loaderStyle = string.Empty;
                            #region Loader Style
                            loaderStyle = string.Format(templatestyle,
                                                                      (CssPath + "css/nprogress/nprogress.css"));
                            #endregion

                            string textboxStyle = string.Empty;
                            #region Textbox Style
                            if (RegisterTextbox)
                                textboxStyle = string.Format(templatestyle,
                                                               (CssPath + "css/textbox/Extratextbox.css"));
                            #endregion

                            #region Validation Style
                            string validationStyle = string.Format(templatestyle,
                                                                      (CssPath + "css/validation/validationEngine.jquery.css"));
                            validationStyle += string.Format(templatestyle,
                                                                     (CssPath + "css/validation/bootstrapValidator.css"));
                            #endregion

                            #region Font Style
                            string fontStyle = string.Empty;
                            //fontStyle += string.Format(templatestyle,
                            //                                      (IconPath + "icons/FlatIcon/flaticon.css"));
                            fontStyle += string.Format(templatestyle,
                                                                  (IconPath + "icons/AweIcon/aweicon.css"));
                            #endregion

                            #region ACE Style
                            string aceStyle = string.Empty;
                            if (cr_enablebootstrapstyle)
                                aceStyle += string.Format(templatestyle,
                                                                (CssPath + "css/bootstrap.min.css"));
                            aceStyle += string.Format(templatestyle,
                                                               (ThemePath + "themes/ace/style/jquery.gritter.min.css"));
                            aceStyle += string.Format(templatestyle,
                                                               (ThemePath + "themes/ace/style/ace.min.css"));

                            #endregion

                            string datetimeStyle = string.Empty;
                            if (RegisterDateTime)
                                datetimeStyle = string.Format(templatestyle,
                                                                         (CssPath + "css/datetime/bootstrap-datetimepicker.css"));
                            #region Combobox Style
                            string comboboxStyle = string.Empty;
                            if (ExtraScriptRegister.RegisterCombobox)
                                comboboxStyle += string.Format(templatestyle,
                                                                  (CssPath + "css/combobox/bootstrap-select.css"));
                            #endregion


                            string controlStyle = string.Format(templatestyle,
                                                                  (CssPath + "css/ControlStyle.css"));
                             cr_style = comboboxStyle + fontStyle + aceStyle + buttonStyle + gridStyle + dialogStyle + loaderStyle + validationStyle + datetimeStyle;
                            cr_style += controlStyle;
                          


                           
                        }

                        LiteralControl link = new LiteralControl(cr_style);
                        link.EnableViewState = false;
                        if (string.IsNullOrEmpty(StylePlaceHolder))
                            page.Header.Controls.Add(link);
                        else
                        {
                            ContentPlaceHolder cpCss = page.FindControl(StylePlaceHolder) as ContentPlaceHolder;
                            if (cpCss != null)
                                cpCss.Controls.Add(link);
                            else
                                page.Header.Controls.Add(link);
                        }
                        #endregion

                        #region Register Script
                        string registerScript = string.Empty;
                        //registerScript += string.Format(templatescript,
                        //                                  (ScriptPath + "scripts/Config.js"));

                        if (ExtraSkinManager.DEFAULT_THEME == icons.ExtraTheme.ACE)
                        {
                            string aceScript = string.Empty;
                            #region Ace Script



                            aceScript += string.Format(templatescript,
                                                             (ThemePath + "themes/ace/script/jquery.gritter.min.js"));
                            if (cr_enablebootstrapscript)
                            {
                                aceScript += string.Format(templatescript,
                                                                    (ThemePath + "themes/ace/script/ace-elements.min.js"));
                                aceScript += string.Format(templatescript,
                                                                  (ThemePath + "themes/ace/script/ace.min.js"));
                            }

                            #endregion

                            string checkboxScript = string.Empty;
                            #region Checkbox Script
                            if (RegisterCheckbox)
                            {
                                checkboxScript += string.Format(templatescript,
                                                              (ThemePath + "themes/ace/script/checkbox/ExtraCheckbox.js"));
                            }
                            #endregion

                            string radioScript = string.Empty;
                            #region Radio Script
                            if (RegisterRadio)
                            {
                                radioScript += string.Format(templatescript,
                                                              (ThemePath + "themes/ace/script/radio/ExtraRadio.js"));
                            }
                            #endregion

                            registerScript += aceScript + checkboxScript + radioScript;
                        }
                        string comboboxScript = string.Empty;
                        #region Combobox Script
                        if (ExtraScriptRegister.RegisterCombobox)
                        {
                            comboboxScript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/combobox/bootstrap-select.js"));
                            //comboboxScript += string.Format(templatescript,
                            //                                  (ScriptPath + "scripts/combobox/select2.full.js"));
                            comboboxScript += string.Format(templatescript,
                                                             (ScriptPath + "scripts/combobox/ExtraCombobox.js"));
                        }
                        #endregion

                        string buttonSCript = string.Empty;
                        #region Button Script
                        if (RegisterButton)
                            buttonSCript = string.Format(templatescript,
                                                            (ThemePath + "scripts/button/ExtraButton.js"));
                        #endregion

                        string gridScript = string.Empty;
                        #region Grid Script
                        if (RegisterGrid)
                        {
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/jquery.dataTables.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.bootstrap.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.responsive.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/responsive.bootstrap.js"));

                            if (RegisterGrid_FixHeader)
                                gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.fixedHeader.js"));
                            //if (RegisterGrid_FixColumn)
                            //    gridScript += string.Format(templatescript,
                            //                                  (ScriptPath + "scripts/grids/dataTables.fixedColumns.js"));

                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/ExtraGrid.js"));

                        }
                        #endregion

                        string dialogScript = string.Empty;
                        #region Dialog Script
                        if (RegisterDialog)
                        {
                            //dialogScript += string.Format(templatescript,
                            //                                        (ScriptPath + "scripts/dialog/run_prettify.min.js"));
                            dialogScript += string.Format(templatescript,
                                                                    (ScriptPath + "scripts/dialog/bootstrap-dialog.min.js"));
                            dialogScript += string.Format(templatescript,
                                                                    (ScriptPath + "scripts/dialog/ExtraDialog.js"));
                        }
                        #endregion

                        #region Validation Script
                        string validationScript = string.Format(templatescript,
                                                           (ScriptPath + "scripts/validation/jquery.validationEngine.js"));
                        validationScript += string.Format(templatescript,
                                                          string.Format(ScriptPath + "scripts/validation/languages/jquery.validationEngine-{0}.js", ExtraSkinManager.DEFAULT_LANGUAGE));
                        validationScript += string.Format(templatescript,
                                                          (ScriptPath + "scripts/validation/validationScript.js"));
                        #endregion

                        #region Loader Script
                        string loaderScript = string.Empty;
                        loaderScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/nprogress/nprogress.js"));
                        #endregion


                        string notifyScript = string.Empty;
                        //#region Notification Sctipt
                        //notifyScript += string.Format(templatescript,
                        //                                  (ScriptPath + "scripts/pnotify/pnotify.animate.js"));
                        //notifyScript += string.Format(templatescript,
                        //                                  (ScriptPath + "scripts/pnotify/pnotify.custom.min.js"));
                        //#endregion

                        string inputMaskScript = string.Empty;
                        #region input mask
                        if (RegisterDateTime || RegisterTextbox)
                        {
                            inputMaskScript += string.Format(templatescript,
                                                               (ScriptPath + "scripts/input_mask/jquery.inputmask.js"));
                            inputMaskScript += string.Format(templatescript,
                                                               (ScriptPath + "scripts/input_mask/jquery.inputmask.date.extensions.js"));
                            inputMaskScript += string.Format(templatescript,
                                                               (ScriptPath + "scripts/input_mask/jquery.inputmask.extensions.js"));
                            inputMaskScript += string.Format(templatescript,
                                                               (ScriptPath + "scripts/input_mask/jquery.inputmask.numeric.extensions.js"));
                            inputMaskScript += string.Format(templatescript,
                                                               (ScriptPath + "scripts/input_mask/jquery.inputmask.regex.extensions.js"));
                        }
                        #endregion

                        #region DateTime
                        string datetimescript = string.Empty;
                        if (RegisterDateTime)
                        {
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/moment/moment-with-locales.min.js"));
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/datetime/bootstrap-datetimepicker.js"));
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/datetime/locales/vi-VN.js"));
                            datetimescript += string.Format(templatescript,
                                                                   (ScriptPath + "scripts/datetime/ExtraDateTimePicker.js"));
                        }
                        #endregion

                        string mainscript = string.Format(templatescript,
                                                          (ScriptPath + "scripts/mainScript.js"));

                        // Get a ClientScriptManager reference from the Page class.


                        //registerScript += string.Format(templatescript,
                        //                                  (ScriptPath + "scripts/tab.tabulous.js"));

                       

                        registerScript += loaderScript + buttonSCript + gridScript + validationScript + dialogScript + inputMaskScript + comboboxScript + notifyScript + datetimescript;
                        registerScript += mainscript;

                        //  cs.RegisterStartupScript(thisT, "RegisterBaseScript", registerScript, false);

                        #region Register Header
                        if (ExtraSkinManager.DEFAULT_THEME == icons.ExtraTheme.ACE)
                        {
                            string aceExtraScript = string.Format(templatescript,
                                                            (ThemePath + "themes/ace/script/ace-extra.min.js"));

                            page.Header.Controls.Add(new LiteralControl(aceExtraScript));
                        }


                        if (cr_enableJqueryMinscript)
                        {
                            string minScript = string.Format(templatescript,
                                                                   (ScriptPath + "scripts/jQuery-2.1.4.min.js"));

                            page.Header.Controls.Add(new LiteralControl(minScript));
                        }

                        if (cr_enablebootstrapscript)
                        {
                            string bootstrapScript = string.Format(templatescript,
                                                               (ScriptPath + "scripts/bootstrap.min.js"));

                            page.Header.Controls.Add(new LiteralControl(bootstrapScript));
                        }

                        if (!string.IsNullOrEmpty(ScriptPlaceHolder) && Page.Master != null)
                        {
                            ContentPlaceHolder holder = Page.Master.FindControl(ScriptPlaceHolder) as ContentPlaceHolder;
                            if (holder != null)
                            {
                                holder.Controls.Add(new LiteralControl(registerScript));
                            }
                            else cs.RegisterStartupScript(thisT, "RegisterBaseScript", registerScript, false);

                        }
                        else cs.RegisterStartupScript(thisT, "RegisterBaseScript", registerScript, false);

                        string scriptHiddenPropertiesID = string.Format("SweetSoft_Settings.Properties.hiddenPropertiesClientID = '{0}';", this.ClientID);
                        //if (panelHiddenProperties != null)
                        //    scriptHiddenPropertiesID += string.Format("SweetSoft_Settings.Properties.updatePanelPropertiesID = '{0}';", panelHiddenProperties.ID);
                        if (!string.IsNullOrEmpty(ExtraScriptRegister.ScriptRunStartUp))
                            scriptHiddenPropertiesID += string.Format("setTimeout(function(){{{0}}},800);", ExtraScriptRegister.ScriptRunStartUp);
                        ScriptManager.RegisterStartupScript(this.Page, thisT, "RegisterHiddenPropertiesID", scriptHiddenPropertiesID, true);
                        ExtraScriptRegister.ScriptRunStartUp = string.Empty;
                        #endregion

                        #endregion
                    }
                    else
                    {

                        #region Register Script

                        #region DateTime
                        string datetimescript = string.Empty;
                        if (RegisterDateTime)
                        {
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/moment/moment-with-locales.min.js"));
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/datetime/bootstrap-datetimepicker.js"));
                            datetimescript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/datetime/locales/vi-VN.js"));
                            datetimescript += string.Format(templatescript,
                                                                   (ScriptPath + "scripts/datetime/ExtraDateTimePicker.js"));
                        }
                        #endregion

                        string draggableScript = string.Empty;
                        if (RegisterDraggable)
                            draggableScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/jquery-ui.js"));
                        string checkboxScript = string.Empty;
                        if (RegisterCheckbox)
                            checkboxScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/switch/ExtraToggleSwitch.js"));

                        #region Grid Script
                        string gridScript = string.Empty;
                        if (RegisterGrid)
                        {
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/jquery.dataTables.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.bootstrap.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.responsive.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/responsive.bootstrap.js"));

                            if (RegisterGrid_FixHeader)
                                gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/dataTables.fixedHeader.js"));
                            gridScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/grids/ExtraGrid.js"));

                        }
                        #endregion

                        #region Dialog Script
                        string dialogScript = string.Empty;
                        if (RegisterDialog)
                        {
                            dialogScript += string.Format(templatescript,
                                                                    (ScriptPath + "scripts/dialog/run_prettify.min.js"));
                            dialogScript += string.Format(templatescript,
                                                                    (ScriptPath + "scripts/dialog/bootstrap-dialog.min.js"));
                            dialogScript += string.Format(templatescript,
                                                                    (ScriptPath + "scripts/dialog/ExtraDialog.js"));
                        }
                        #endregion

                        #region Combobox Script
                        string comboboxScript = string.Empty;
                        if (ExtraScriptRegister.RegisterCombobox)
                        {
                            comboboxScript += string.Format(templatescript,
                                                              (ScriptPath + "scripts/combobox/bootstrap-select.js"));
                            comboboxScript += string.Format(templatescript,
                                                             (ScriptPath + "scripts/combobox/ExtraCombobox.js"));
                        }
                        #endregion

                        #region Button Script
                        string buttonSCript = string.Empty;
                        if (RegisterButton)
                            buttonSCript = string.Format(templatescript,
                                                            (ScriptPath + "scripts/button/ExtraButton.js"));
                        #endregion

                        #region Validation Script
                        string validationScript = string.Format(templatescript,
                                                           (ScriptPath + "scripts/validation/jquery.validationEngine.js"));
                        validationScript += string.Format(templatescript,
                                                          cs.GetWebResourceUrl(thisT, string.Format("SweetSoft.ExtraControls.scripts.validation/languages.jquery.validationEngine-{0}.js", ExtraSkinManager.DEFAULT_LANGUAGE)));
                        validationScript += string.Format(templatescript,
                                                          (ScriptPath + "scripts/validation/validationScript.js"));
                        #endregion

                        #region Notification Sctipt
                        string notifyScript = string.Empty;
                        notifyScript += string.Format(templatescript,
                                                          (ScriptPath + "scripts/pnotify/pnotify.animate.js"));
                        notifyScript += string.Format(templatescript,
                                                          (ScriptPath + "scripts/pnotify/pnotify.custom.min.js"));
                        #endregion

                        #region Loader Script
                        string loaderScript = string.Empty;
                        loaderScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/nprogress/nprogress.js"));
                        #endregion

                        #region Search Box
                        string searchBoxScript = string.Empty;
                        searchBoxScript += string.Format(templatescript,
                                                           (ScriptPath + "scripts/searchTagBox.searchTagBox.js"));
                        #endregion

                        string mainscript = string.Format(templatescript,
                                                           (ScriptPath + "scripts/mainScript.js"));

                        // Get a ClientScriptManager reference from the Page class.
                        string registerScript = string.Format(templatescript,
                                                           (ScriptPath + "scripts/bootstrap.min.js"));
                        //registerScript += string.Format(templatescript,
                        //                                   (ScriptPath + "scripts/Config.js"));
                        registerScript += notifyScript + checkboxScript + loaderScript + validationScript + searchBoxScript + datetimescript + draggableScript + dialogScript + comboboxScript + buttonSCript + gridScript;
                        registerScript += mainscript;

                        cs.RegisterStartupScript(thisT, "RegisterBaseScript", registerScript, false);

                        string scriptHiddenPropertiesID = string.Format("SweetSoft_Settings.Properties.hiddenPropertiesClientID = '{0}';", this.ClientID);
                        //if (panelHiddenProperties != null)
                        //    scriptHiddenPropertiesID += string.Format("SweetSoft_Settings.Properties.updatePanelPropertiesID = '{0}';", panelHiddenProperties.ID);
                        if (!string.IsNullOrEmpty(ExtraScriptRegister.ScriptRunStartUp))
                            scriptHiddenPropertiesID += string.Format("setTimeout(function(){{{0}}},800);", ExtraScriptRegister.ScriptRunStartUp);
                        ScriptManager.RegisterStartupScript(this.Page, thisT, "RegisterHiddenPropertiesID", scriptHiddenPropertiesID, true);
                        ExtraScriptRegister.ScriptRunStartUp = string.Empty;
                        #endregion

                        #region Register Style

                        #region Grid
                        string gridStyle = string.Empty;
                        if (RegisterGrid)
                        {
                            gridStyle += string.Format(templatestyle,
                                                                     (CssPath + "css/grids/dataTables.bootstrap.css"));
                            gridStyle += string.Format(templatestyle,
                                                                    (CssPath + "css/grids/responsive.bootstrap.css"));
                            if (RegisterGrid_FixHeader)
                                gridStyle += string.Format(templatestyle,
                                                                     (CssPath + "css/grids/fixedHeader.bootstrap.css"));

                        }
                        #endregion

                        #region DateTime
                        string datetimeStyle = string.Empty;
                        if (RegisterDateTime)
                            datetimeStyle = string.Format(templatestyle,
                                                                     (CssPath + "css/datetime.daterangepicker.css"));
                        #endregion

                        #region Font Style
                        string fontStyle = string.Empty;
                        fontStyle += string.Format(templatestyle,
                                                              (IconPath + "icons/FlatIcon.flaticon.css"));
                        fontStyle += string.Format(templatestyle,
                                                              (IconPath + "icons/AweIcon.aweicon.css"));
                        #endregion

                        string controlStyle = string.Empty;

                        #region Dialog Style
                        string dialogStyle = string.Empty;
                        if (RegisterDialog)
                            dialogStyle += string.Format(templatestyle,
                                                              (CssPath + "css/dialog.bootstrap-dialog.min.css"));
                        #endregion

                        #region CheckBox Style
                        string toggleStyle = string.Empty;
                        if (RegisterCheckbox)
                            toggleStyle += string.Format(templatestyle,
                                                                  (CssPath + "css/checkbox.checkbox.css"));
                        #endregion

                        #region Combobox Style
                        string comboboxStyle = string.Empty;
                        if (ExtraScriptRegister.RegisterCombobox)
                            comboboxStyle += string.Format(templatestyle,
                                                              (CssPath + "css/combobox.bootstrap-select.css"));
                        #endregion

                        #region Notification Style
                        string notifyStyle = string.Empty;
                        notifyStyle = string.Format(templatestyle,
                                                                  (CssPath + "css/pnotify/pnotify.custom.min.css"));
                        notifyStyle += string.Format(templatestyle,
                                                                  (CssPath + "css/pnotify/pnotify.core.css"));
                        notifyStyle += string.Format(templatestyle,
                                                                  (CssPath + "css/pnotify/animate.css"));
                        #endregion

                        #region Validation Style
                        string validationStyle = string.Format(templatestyle,
                                                                  (CssPath + "css/validation/validationEngine.jquery.css"));
                        validationStyle += string.Format(templatestyle,
                                                                 (CssPath + "css/validation/bootstrapValidator.css"));
                        #endregion

                        #region Search Tag Box
                        string searchTagStyle = string.Empty;
                        if (RegisterSearchBox)
                            searchTagStyle = string.Format(templatestyle,
                                                                      (CssPath + "css/searchTagBox.searchTagBox.css"));
                        #endregion

                        #region Textbox Style
                        string textboxStyle = string.Empty;
                        if (RegisterTextbox)
                            textboxStyle = string.Format(templatestyle,
                                                           (CssPath + "css/textbox/Extratextbox/css"));
                        #endregion

                        controlStyle = string.Format(templatestyle,
                                                           (CssPath + "css/ControlStyle.css"));
                        if (RegisterDialog || RegisterButton || RegisterCombobox)
                            controlStyle += string.Format(templatestyle,
                                                            (CssPath + "css/bootstrap.min.css"));

                        string registerStyle = gridStyle + datetimeStyle + searchTagStyle + fontStyle + dialogStyle + toggleStyle + comboboxStyle + controlStyle + textboxStyle + notifyStyle;


                        LiteralControl link = new LiteralControl(registerStyle);
                        link.EnableViewState = false;


                        if (string.IsNullOrEmpty(StylePlaceHolder))
                            page.Header.Controls.Add(link);
                        else
                        {
                            ContentPlaceHolder cpCss = page.FindControl(StylePlaceHolder) as ContentPlaceHolder;
                            if (cpCss != null)
                                cpCss.Controls.Add(link);
                            else
                                page.Header.Controls.Add(link);
                        }
                        #endregion
                    }
                }
            }
        }

        public static string MergeScript(string firstScript, string secondScript)
        {
            if (string.IsNullOrEmpty(firstScript))
            {
                if (secondScript.Length > 0)
                {
                    if (secondScript.StartsWith("javascript:"))
                        return secondScript;
                    else
                        return ("javascript:" + secondScript);
                }
            }

            if (!firstScript.StartsWith("javascript:"))
                firstScript = "javascript:" + firstScript;

            if (secondScript.StartsWith("javascript:"))
                secondScript = secondScript.Substring("javascript:".Length);

            if (!string.IsNullOrEmpty(firstScript)) return (firstScript + (firstScript.EndsWith(";") ? "" : ";") + secondScript);
            if (secondScript.TrimStart(new char[0]).StartsWith("javascript:", StringComparison.Ordinal))
            {
                return secondScript + ";";
            }
            if (!secondScript.EndsWith(";"))
                secondScript = secondScript + ";";
            return string.Empty;
        }
        #endregion

        #region Notification
        public static void ShowNotify(string title, string message, NotifyType Type, AnimationType aIn, AnimationType aOut)
        {
            string script = string.Format("SPnotify.Show('{0}','{1}','{2}','{3}','{4}');", Type.ToRender(), title, message, aIn.ToRender(), aOut.ToRender());
            RunSCript(script);
        }
        #endregion

        private static void RunSCript(string script)
        {

            Page page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            if (page != null)
            {
                ClientScriptManager cs = page.ClientScript;

                // Define the resource name and type.
                Type thisT = page.GetType();
                if (thisT != typeof(ExtraScriptRegister))
                    thisT = thisT.BaseType;
                cs.RegisterStartupScript(thisT, "runScript", script);
            }
        }
    }
    public class ExtraControlEventArg
    {
        public bool IsControl { get; set; }
        public string CommandName { get; set; }
        public string ControlClientID { get; set; }
        public Type SourceType { get; set; }
        public object Value { get; set; }
        public object Tag { get; set; }
        public ExtraControlEventArg() { }
    }

    public enum NofifyPosition
    {
        [Render("stack_bar_bottom")]
        Bottom,
        [Render("stack_bar_top")]
        Top
    }



    public enum NotifyType
    {
        [Render("success")]
        [Description("success")]
        Success,
        [Render("error")]
        [Description("error")]
        Error,
        [Render("warning")]
        [Description("warning")]
        Warning,
        [Render("info")]
        [Description("info")]
        Info
    }

    public enum AnimationType
    {
        [Render("slideInRight")]
        [Description("slideInRight")]
        SlideInRight,
        [Render("slideOutRight")]
        [Description("slideOutRight")]
        SlideOutRight,
    }
}
