﻿
using System;
using System.ComponentModel;
using System.Web.UI;
using SweetSoft.ExtraControls.html;
using System.Web.UI.WebControls;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
//using Newtonsoft.Json;
using System.Web.Script.Serialization;


namespace SweetSoft.ExtraControls
{
    [DefaultProperty("Text")]
    [ToolboxData("<{0}:ExtraDateRangePicker runat=\"server\"></{0}:ExtraDateRangePicker>")]
    public class ExtraDateRangePicker : WebControl, INamingContainer, IPostBackDataHandler, IPostBackEventHandler
    {
        protected override HtmlTextWriterTag TagKey
        {
            get
            {
                return RenderAsLabel == true ? HtmlTextWriterTag.Span : HtmlTextWriterTag.Input;
            }
        }

        #region field

        private bool autoPostBack = false;
        private bool allowType = true;
        JavaScriptSerializer jss = new JavaScriptSerializer();

        string hdfId = "_hdfDRPValue";

        [Description("Fires when the date has been changed and AutoPostBack is set to 'true'.")]
        public event EventHandler<DateRangeChangedEventArgs> DateChanged;

        //CultureInfo ciLanguage = CultureInfo.GetCultureInfo("vi-VN");

        #endregion

        #region CssClass method

        string _sCssClass = "";

        public ExtraDateRangePicker()
        {
            SimpleInit = true;
        }

        public ExtraDateRangePicker(HtmlTextWriterTag tag)
            : base(tag)
        {
            
        }
        protected ExtraDateRangePicker(string tag)
            : base(tag)
        {
            
        }

        /// <summary>
        /// Adds the CSS class.
        /// </summary>
        /// <param name="cssClass">The CSS class.</param>
        private void AddCssClass(string cssClass)
        {
            if (String.IsNullOrEmpty(this._sCssClass))
            {
                this._sCssClass = cssClass;
            }
            else
            {
                this._sCssClass += " " + cssClass;
            }
        }

        #endregion

        #region RegionName

        #region default

        public bool AllowType
        {
            get { return allowType; }
            set { allowType = value; }
        }

        public bool SimpleInit
        {
            get
            {
                object obj = ViewState["SimpleInit"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["SimpleInit"] = value;

                Language = "vi-VN";
                //Format = "DD/MM/YYYY";
                UseMomentShortDay = true;

                if (Theme == DRPTheme.None)
                    Theme = DRPTheme.Latoja;

                SingleDate = true;
                ShowDropdownMonth = true;
                ShowDropdownYear = true;
                ShowMonthShort = true;
                ShowTopbar = false;
                AutoClose = true;
                CssClass += " form-control";
                Separator = " - ";
            }
        }

        public bool ShowClearButton
        {
            get
            {
                object obj = ViewState["ShowClearButton"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["ShowClearButton"] = value;
            }
        }

        [Browsable(true)]
        [DefaultValue(false)]
        public bool AutoPostBack
        {
            get { return autoPostBack; }
            set { autoPostBack = value; }
        }

        public string CssPlaceHolder
        {
            get
            {
                object obj = ViewState["CssPlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CssPlaceHolder"] = value; }
        }

        public string JsPlaceHolder
        {
            get
            {
                object obj = ViewState["JsPlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["JsPlaceHolder"] = value; }
        }

        public bool Required
        {
            get
            {
                object obj = ViewState["Required"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["Required"] = value; }
        }

        public string RequiredText
        {
            get
            {
                object obj = ViewState["RequiredText"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["RequiredText"] = value; }
        }

        public string PlaceHolder
        {
            get
            {
                object obj = ViewState["PlaceHolder"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["PlaceHolder"] = value; }
        }

        public bool ForceCheckOnSubmit
        {
            get
            {
                object obj = ViewState["ForceCheckOnSubmit"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ForceCheckOnSubmit"] = value; }
        }

        #endregion

        #region RegionName

        public string ExtraClass
        {
            get
            {
                object obj = ViewState["ExtraClass"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ExtraClass"] = value; }
        }

        #region Custom Values

        public string CustomValueLabel
        {
            get
            {
                object obj = ViewState["CustomValueLabel"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CustomValueLabel"] = value; }
        }

        public bool ShowCustomValues
        {
            get
            {
                object obj = ViewState["ShowCustomValues"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowCustomValues"] = value; }
        }

        public string CustomValuesData
        {
            get
            {
                object obj = ViewState["CustomValuesData"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CustomValuesData"] = value; }
        }

        #endregion

        public bool RenderAsLabel
        {
            get
            {
                object obj = ViewState["RenderAsLabel"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["RenderAsLabel"] = value; }
        }

        public string DayDivAttrs
        {
            get
            {
                object obj = ViewState["DayDivAttrs"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["DayDivAttrs"] = value; }
        }

        public string DayTdAttrs
        {
            get
            {
                object obj = ViewState["DayTdAttrs"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["DayTdAttrs"] = value; }
        }

        public string CustomShortcuts
        {
            get
            {
                object obj = ViewState["CustomShortcuts"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CustomShortcuts"] = value; }
        }

        public bool HideGap
        {
            get
            {
                object obj = ViewState["HideGap"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["HideGap"] = value; }
        }

        public bool InitAfterLoad
        {
            get
            {
                object obj = ViewState["InitAfterLoad"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["InitAfterLoad"] = value; }
        }

        public bool AlwaysOpen
        {
            get
            {
                object obj = ViewState["AlwaysOpen"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["AlwaysOpen"] = value; }
        }

        public DRPBatchMode BatchMode
        {
            get
            {
                object obj = ViewState["BatchMode"];
                return (obj == null) ? DRPBatchMode.None : (DRPBatchMode)Enum.Parse(typeof(DRPBatchMode), obj.ToString());
            }
            set { ViewState["BatchMode"] = value; }
        }

        public bool UseMomentShortDay
        {
            get
            {
                object obj = ViewState["UseMomentShortDay"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["UseMomentShortDay"] = value; }
        }

        public bool Inline
        {
            get
            {
                object obj = ViewState["Inline"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["Inline"] = value; }
        }

        public bool SelectBackward
        {
            get
            {
                object obj = ViewState["SelectBackward"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SelectBackward"] = value; }
        }

        public bool SelectForward
        {
            get
            {
                object obj = ViewState["SelectForward"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SelectForward"] = value; }
        }

        public bool OverrideDataTooltip
        {
            get
            {
                object obj = ViewState["OverrideDataTooltip"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["OverrideDataTooltip"] = value; }
        }

        public bool ShowWeekNumbers
        {
            get
            {
                object obj = ViewState["ShowWeekNumbers"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowWeekNumbers"] = value; }
        }

        public bool ShowDropdownMonth
        {
            get
            {
                object obj = ViewState["ShowDropdownMonth"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowDropdownMonth"] = value; }
        }

        public bool ShowMonthShort
        {
            get
            {
                object obj = ViewState["ShowMonthShort"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowMonthShort"] = value; }
        }

        public bool ShowDropdownYear
        {
            get
            {
                object obj = ViewState["ShowDropdownYear"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowDropdownYear"] = value; }
        }

        public bool ShowTopbar
        {
            get
            {
                object obj = ViewState["ShowTopbar"];
                return (obj == null) ? true : (bool)obj;
            }
            set { ViewState["ShowTopbar"] = value; }
        }

        public bool ShowShortcuts
        {
            get
            {
                object obj = ViewState["ShowShortcuts"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowShortcuts"] = value; }
        }

        public bool SwapTime
        {
            get
            {
                object obj = ViewState["SwapTime"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SwapTime"] = value; }
        }

        public string StartDate
        {
            get
            {
                object obj = ViewState["StartDate"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["StartDate"] = value; }
        }

        public string EndDate
        {
            get
            {
                object obj = ViewState["EndDate"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["EndDate"] = value; }
        }

        public bool ShowOtherMonth
        {
            get
            {
                object obj = ViewState["ShowOtherMonth"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["ShowOtherMonth"] = value; }
        }

        public bool SingleDate
        {
            get
            {
                object obj = ViewState["SingleDate"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SingleDate"] = value; }
        }

        public bool StickyMonths
        {
            get
            {
                object obj = ViewState["StickyMonths"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["StickyMonths"] = value; }
        }

        public bool SingleMonth
        {
            get
            {
                object obj = ViewState["SingleMonth"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["SingleMonth"] = value; }
        }

        public bool AutoClose
        {
            get
            {
                object obj = ViewState["AutoClose"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["AutoClose"] = value; }
        }

        public bool LookBehind
        {
            get
            {
                object obj = ViewState["LookBehind"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["LookBehind"] = value; }
        }

        public string DefaultDate
        {
            get
            {
                object obj = ViewState["DefaultDate"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["DefaultDate"] = value; }
        }

        public bool CalculateWraperWith
        {
            get
            {
                object obj = ViewState["CalculateWraperWith"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["CalculateWraperWith"] = value; }
        }

        public string GetWeekNumberFunction
        {
            get
            {
                object obj = ViewState["GetWeekNumberFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["GetWeekNumberFunction"] = value; }
        }

        public string BeforeShowDayFunction
        {
            get
            {
                object obj = ViewState["BeforeShowDayFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["BeforeShowDayFunction"] = value; }
        }

        public string ShowDifferenceFunction
        {
            get
            {
                object obj = ViewState["ShowDifferenceFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ShowDifferenceFunction"] = value; }
        }

        public string ShowDateFilterFunction
        {
            get
            {
                object obj = ViewState["ShowDateFilterFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ShowDateFilterFunction"] = value; }
        }

        public string SetValueFunction
        {
            get
            {
                object obj = ViewState["SetValueFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["SetValueFunction"] = value; }
        }

        public string GetValueFunction
        {
            get
            {
                object obj = ViewState["GetValueFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["GetValueFunction"] = value; }
        }

        public string HoveringTooltipFunction
        {
            get
            {
                object obj = ViewState["HoveringTooltipFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["HoveringTooltipFunction"] = value; }
        }

        public string OnChangeMonthYearFunction
        {
            get
            {
                object obj = ViewState["OnChangeMonthYearFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnChangeMonthYearFunction"] = value; }
        }

        public string FirstDateSelectedFunction
        {
            get
            {
                object obj = ViewState["FirstDateSelectedFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["FirstDateSelectedFunction"] = value; }
        }

        public string LastDateSelectedFunction
        {
            get
            {
                object obj = ViewState["LastDateSelectedFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["LastDateSelectedFunction"] = value; }
        }

        public string OnClosedFunction
        {
            get
            {
                object obj = ViewState["OnClosedFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnClosedFunction"] = value; }
        }

        public string OnOpenedFunction
        {
            get
            {
                object obj = ViewState["OnOpenedFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["OnOpenedFunction"] = value; }
        }


        public string MaxDays
        {
            get
            {
                object obj = ViewState["MaxDays"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["MaxDays"] = value; }
        }

        public string Duration
        {
            get
            {
                object obj = ViewState["Duration"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Duration"] = value; }
        }

        public string MinDays
        {
            get
            {
                object obj = ViewState["MinDays"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["MinDays"] = value; }
        }

        public string ShortcutsObject
        {
            get
            {
                object obj = ViewState["ShortcutsObject"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ShortcutsObject"] = value; }
        }

        public bool EnableTime
        {
            get
            {
                object obj = ViewState["EnableTime"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["EnableTime"] = value; }
        }

        public DRPStartOfWeek StartOfWeek
        {
            get
            {
                object obj = ViewState["StartOfWeek"];
                return (obj == null) ? DRPStartOfWeek.None : (DRPStartOfWeek)Enum.Parse(typeof(DRPStartOfWeek), obj.ToString());
            }
            set { ViewState["StartOfWeek"] = value; }
        }

        public string Language
        {
            get
            {
                object obj = ViewState["Language"];
                return (obj == null) ? "vi-VN" : (string)obj;
            }
            set
            {
                ViewState["Language"] = value;
            }
        }

        CultureInfo GetLanguage()
        {
            CultureInfo ci = null;
            if (string.IsNullOrEmpty(Language) == false)
            {
                try
                {
                    ci = CultureInfo.GetCultureInfo(Language);
                }
                catch (Exception ex)
                {
                    ci = null;
                }
            }
            return ci;
        }

        public string Separator
        {
            get
            {
                object obj = ViewState["Separator"];
                return (obj == null) ? " to " : (string)obj;
            }
            set { ViewState["Separator"] = value; }
        }

        public string Format
        {
            get
            {
                object obj = ViewState["Format"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Format"] = value; }
        }

        public string LoadingTemplateFunction
        {
            get
            {
                object obj = ViewState["LoadingTemplateFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["LoadingTemplateFunction"] = value; }
        }

        public DRPDirection Direction
        {
            get
            {
                object obj = ViewState["Direction"];
                return (obj == null) ? DRPDirection.None : (DRPDirection)Enum.Parse(typeof(DRPDirection), obj.ToString());
            }
            set { ViewState["Direction"] = value; }
        }

        public string Collision
        {
            get
            {
                object obj = ViewState["Collision"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Collision"] = value; }
        }

        public DRPOpenAt OpenAt
        {
            get
            {
                object obj = ViewState["OpenAt"];
                return (obj == null) ? DRPOpenAt.None : (DRPOpenAt)Enum.Parse(typeof(DRPOpenAt), obj.ToString());
            }
            set { ViewState["OpenAt"] = value; }
        }

        public string CustomTopBar
        {
            get
            {
                object obj = ViewState["CustomTopBar"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["CustomTopBar"] = value; }
        }

        public string Container
        {
            get
            {
                object obj = ViewState["Container"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["Container"] = value; }
        }

        public string BtnPrev
        {
            get
            {
                object obj = ViewState["BtnPrev"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["BtnPrev"] = value; }
        }

        public string BtnNext
        {
            get
            {
                object obj = ViewState["BtnNext"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["BtnNext"] = value; }
        }

        public string ApplyBtnClass
        {
            get
            {
                object obj = ViewState["ApplyBtnClass"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["ApplyBtnClass"] = value; }
        }

        [Bindable(true)]
        [DefaultValue(false)]
        [Themeable(false)]
        public virtual bool ReadOnly
        {
            get
            {
                object obj = ViewState["ReadOnly"];
                return (obj == null) ? false : (bool)obj;
            }
            set
            {
                ViewState["ReadOnly"] = value;
            }
        }

        #endregion

        #region Ajax setting

        public string AjaxDataPath
        {
            get
            {
                object obj = ViewState["AjaxDataPath"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxDataPath"] = value; }
        }

        public string AjaxCacheNumerKey
        {
            get
            {
                object obj = ViewState["AjaxCacheNumerKey"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxCacheNumerKey"] = value; }
        }

        public bool AjaxCacheOnLoad
        {
            get
            {
                object obj = ViewState["AjaxCacheOnLoad"];
                return (obj == null) ? false : (bool)obj;
            }
            set { ViewState["AjaxCacheOnLoad"] = value; }
        }

        public string AjaxDataType
        {
            get
            {
                object obj = ViewState["AjaxDataType"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxDataType"] = value; }
        }

        public string AjaxMethodType
        {
            get
            {
                object obj = ViewState["AjaxMethodType"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxMethodType"] = value; }
        }

        public string AjaxContentType
        {
            get
            {
                object obj = ViewState["AjaxContentType"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxContentType"] = value; }
        }

        public string AjaxDataRequest
        {
            get
            {
                object obj = ViewState["AjaxDataRequest"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxDataRequest"] = value; }
        }

        public string AjaxBeforeSendFunction
        {
            get
            {
                object obj = ViewState["AjaxBeforeSendFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxBeforeSendFunction"] = value; }
        }

        public string AjaxErrorFunction
        {
            get
            {
                object obj = ViewState["AjaxErrorFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxErrorFunction"] = value; }
        }

        public string AjaxSuccessFunction
        {
            get
            {
                object obj = ViewState["AjaxSuccessFunction"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxSuccessFunction"] = value; }
        }

        public string AjaxObjectSetting
        {
            get
            {
                object obj = ViewState["AjaxObjectSetting"];
                return (obj == null) ? "" : (string)obj;
            }
            set { ViewState["AjaxObjectSetting"] = value; }
        }

        #endregion

        public DRPTheme Theme
        {
            get
            {
                object obj = ViewState["Theme"];
                return (obj == null) ? DRPTheme.None : (DRPTheme)Enum.Parse(typeof(DRPTheme), obj.ToString());
            }
            set { ViewState["Theme"] = value; }
        }

        public string Text
        {
            get
            {
                object obj = ViewState["Text"];
                return (obj == null) ? "" : (string)obj;
            }
            set
            {
                ViewState["Text"] = value;
            }
        }

        DateTime GetDate(object obj)
        {
            DateTime dt = DateTime.MinValue;

            if (obj != null && string.IsNullOrEmpty(obj.ToString()) == false)
            {
                CultureInfo ci = GetLanguage();
                if (ci == null)
                    ci = CultureInfo.GetCultureInfo("vi-VN");
                if (ci != null)
                {
                    try
                    {
                        dt = DateTime.Parse(obj.ToString(), ci);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }

            return dt;
        }

        public string Date1
        {
            get
            {
                object obj = ViewState["Date1"];
                return (obj == null) ? "" : (string)obj;
            }
            set
            {
                ViewState["Date1"] = value;
            }
        }

        public DateTime GetDate1()
        {
            return GetDate(Date1);
        }

        public void SetDate1(DateTime date)
        {
            CultureInfo ci = GetLanguage();
            if (ci == null)
                ci = CultureInfo.GetCultureInfo("vi-VN");
            if (ci != null)
            {
                try
                {
                    Text = string.Empty;
                    ViewState["Date1"] = date.ToString(ci);
                }
                catch (Exception ex)
                {

                }
            }
            else
                ViewState["Date1"] = string.Empty;
        }

        public string Date2
        {
            get
            {
                object obj = ViewState["Date2"];
                return (obj == null) ? "" : (string)obj;
            }
            set
            {
                ViewState["Date2"] = value;
            }
        }

        public DateTime GetDate2()
        {
            return GetDate(Date2);
        }

        public void SetDate2(DateTime date)
        {
            if (date != DateTime.MinValue)
            {
                CultureInfo ci = GetLanguage();
                if (ci == null)
                    ci = CultureInfo.GetCultureInfo("vi-VN");
                if (ci != null)
                {
                    try
                    {
                        Text = string.Empty;
                        ViewState["Date2"] = date.ToString(ci);
                    }
                    catch (Exception ex)
                    {

                    }
                }
            }
            else
                ViewState["Date2"] = string.Empty;
        }

        public List<DateTime> GetRangeDates()
        {
            return new List<DateTime>() { GetDate1(), GetDate2() };
        }

        #endregion

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            ExtraScriptRegister.RegisterDateTime = true;
        }

        protected override void OnPreRender(EventArgs e)
        {
            if (SimpleInit == true)
                CssPlaceHolder = "cpVenderCSS";

            RegisterScript(CssPlaceHolder, JsPlaceHolder);

            Page page = this.Page;
            if (page == null)
                page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;

            //Need to write this, so that LoadPostData() gets called.
            if (page != null)
                page.RegisterRequiresPostBack(this);

            base.OnPreRender(e);
        }

        public static void RegisterScript(string cssPlaceHolder, string jsPlaceHolder)
        {
            Page page = (System.Web.UI.Page)System.Web.HttpContext.Current.Handler;
            if (page != null)
            {
                // We need <head runat="server"> for this code to work
                if (page.Header == null)
                    throw new NotSupportedException("No <head runat=\"server\"> control found in page.");

                // Define the resource name and type.
                Type thisT = typeof(ExtraDateRangePicker);
                if (thisT != typeof(ExtraDateRangePicker))
                    thisT = thisT.BaseType;

                // Get a ClientScriptManager reference from the Page class.
                ClientScriptManager cs = page.ClientScript;
                // Check to see if the startup script is already registered.
                if (cs.IsStartupScriptRegistered(thisT, "SweetSoft.ExtraControls.setScriptExtraDateRangePicker") == false)
                {
                    #region RegionName
                    /*
                    string baseCSS = cs.GetWebResourceUrl(thisT, "Bootstrap.NET.Source.js.select2.css");

                    // If not, register it
                    LiteralControl link = new LiteralControl();
                    link.EnableViewState = false;
                    link.Text = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + baseCSS + "\" />";

                    if (string.IsNullOrEmpty(CssPlaceHolder))
                        page.Header.Controls.Add(link);
                    else
                    {
                        ContentPlaceHolder cpCss = page.Header.FindControl(CssPlaceHolder) as ContentPlaceHolder;
                        if (cpCss != null)
                            cpCss.Controls.Add(link);
                        else
                            page.Header.Controls.Add(link);
                    }

                    
                    string thisN = "Bootstrap.NET.Source.js.Dropdown.js";
                    string baseJS = "Bootstrap.NET.Source.js.select2.js";
                    string template = "<script type='text/javascript' src='{0}'></script>";


                    String str = string.Format(template, cs.GetWebResourceUrl(thisT, baseJS)) +
                        string.Format(template, cs.GetWebResourceUrl(thisT, thisN));
                    // Register the client resource with the page.
                    if (string.IsNullOrEmpty(JsPlaceHolder))
                        cs.RegisterStartupScript(thisT, "Bootstrap.NET.setScriptExtraDateRangePicker", str, false);
                    else
                    {
                        ContentPlaceHolder cpJs = page.Header.FindControl(JsPlaceHolder) as ContentPlaceHolder;
                        if (cpJs != null)
                            cpJs.Controls.Add(new LiteralControl(str));
                        else
                            cs.RegisterStartupScript(thisT, "Bootstrap.NET.setScriptExtraDateRangePicker", str, false);
                    }
                    */
                    #endregion

                    #region css

                    string baseCSS = "/css/daterangepicker/daterangepicker.full.css";
                    string customCSS = "/css/daterangepicker/custom.css";
                    string strCss = "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + baseCSS + "\" />";
                    strCss += "<link rel=\"stylesheet\" type=\"text/css\" href=\"" + customCSS + "\" />";
                    // If not, register it
                    LiteralControl link = new LiteralControl();
                    link.EnableViewState = false;
                    link.Text = strCss;

                    try
                    {
                        if (string.IsNullOrEmpty(cssPlaceHolder))
                            page.Header.Controls.Add(link);
                        else
                        {
                            ContentPlaceHolder cpCss = page.Header.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                            if (cpCss == null)
                                cpCss = page.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                            if (cpCss != null)
                                cpCss.Controls.Add(link);
                            else
                                page.Header.Controls.Add(link);
                        }
                    }
                    catch (Exception ex)
                    {

                    }

                    #endregion

                    #region js

                    String str = "<script src='/scripts/moment/moment-with-locales.min.js'></script> " +
                                 "<script src='/scripts/daterangepicker/jquery.daterangepicker.js'></script> " +
                                 "<script src='/scripts/daterangepicker/jquery.daterangepicker.languagetexts.js'></script>" +
                                 "<script src='/scripts/daterangepicker/ui.version.js'></script>" +
                                 "<script src='/scripts/daterangepicker/ui.position.js'></script>" +
                                 "<script src='/scripts/daterangepicker/jquery.data-selector.js'></script>" +
                                 "<script src='/scripts/daterangepicker/ExtraDateRangePicker.js'></script>";
                    // Register the client resource with the page.
                    if (string.IsNullOrEmpty(jsPlaceHolder))
                    {
                        ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateRangePicker", str, false);
                    }
                    else
                    {
                        ContentPlaceHolder cpJs = page.Header.FindControl(jsPlaceHolder) as ContentPlaceHolder;
                        if (cpJs == null)
                            cpJs = page.FindControl(cssPlaceHolder) as ContentPlaceHolder;
                        if (cpJs != null)
                        {
                            try
                            {
                                cpJs.Controls.Add(new LiteralControl(str));
                                ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateRangePicker", "", true);
                            }
                            catch (Exception ex)
                            {

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(page, thisT, "SweetSoft.ExtraControls.setScriptExtraDateRangePicker", str, false);
                        }
                    }

                    #endregion
                }
            }
        }

        protected override void Render(HtmlTextWriter writer)
        {
            #region RegionName

            this.AddCssClass(CssClass);
            this.AddCssClass("daterange-picker");
            CssClass = _sCssClass;

            if (Required)
            {
                writer.AddAttribute("required", "required");
                if (RequiredText != null && RequiredText.Length > 0)
                    writer.AddAttribute("data-msg-required", RequiredText);
            }

            if (HideGap == true)
                writer.AddAttribute("data-hideGap", "true");
            if (InitAfterLoad == true)
                writer.AddAttribute("data-hideGap", "true");
            if (AlwaysOpen == true)
                writer.AddAttribute("data-alwaysOpen", "true");
            if (UseMomentShortDay == true)
                writer.AddAttribute("data-useMomentShortDay", "true");
            if (Inline == true)
                writer.AddAttribute("data-inline", "true");
            if (SelectBackward == true)
                writer.AddAttribute("data-selectBackward", "true");
            if (SelectForward == true)
                writer.AddAttribute("data-selectForward", "true");
            if (OverrideDataTooltip == true)
                writer.AddAttribute("data-overrideDataTooltip", "true");
            if (ShowWeekNumbers == true)
                writer.AddAttribute("data-showWeekNumbers", "true");
            if (ShowDropdownMonth == true)
                writer.AddAttribute("data-showDropdownMonth", "true");
            if (ShowMonthShort == true)
                writer.AddAttribute("data-showMonthShort", "true");
            if (ShowDropdownYear == true)
                writer.AddAttribute("data-showDropdownYear", "true");
            if (ShowTopbar == false)
                writer.AddAttribute("data-showTopbar", "false");
            if (ShowShortcuts == true)
                writer.AddAttribute("data-showShortcuts", "true");
            if (SwapTime == true)
                writer.AddAttribute("data-swapTime", "true");
            if (ShowClearButton == true)
                writer.AddAttribute("data-showClearButton", "true");
            if (ShowOtherMonth == true)
                writer.AddAttribute("data-showOtherMonth", "true");
            if (SingleDate == true)
                writer.AddAttribute("data-singleDate", "true");
            if (StickyMonths == true)
                writer.AddAttribute("data-stickyMonths", "true");
            if (SingleMonth == true)
                writer.AddAttribute("data-singleMonth", "true");
            if (AutoClose == true)
                writer.AddAttribute("data-autoClose", "true");
            if (LookBehind == true)
                writer.AddAttribute("data-lookBehind", "true");
            if (CalculateWraperWith == true)
                writer.AddAttribute("data-calculateWraperWith", "true");
            if (EnableTime == true)
                writer.AddAttribute("data-enableTime", "true");
            if (ShowCustomValues == true)
                writer.AddAttribute("data-showCustomValues", "true");
            if (allowType == false)
                writer.AddAttribute("OnKeyPress", "return false;");

            if (StartOfWeek != DRPStartOfWeek.None)
                writer.AddAttribute("data-startOfWeek", StartOfWeek.ToRender());
            if (Direction != DRPDirection.None)
                writer.AddAttribute("data-direction", Direction.ToRender());
            if (string.IsNullOrEmpty(Collision) == false)
                writer.AddAttribute("data-collision", Collision);
            if (OpenAt != DRPOpenAt.None)
                writer.AddAttribute("data-openAt", OpenAt.ToRender());
            if (BatchMode != DRPBatchMode.None)
                writer.AddAttribute("data-batchMode", BatchMode.ToRender());

            if (string.IsNullOrEmpty(PlaceHolder) == false)
                writer.AddAttribute("placeholder", PlaceHolder);
            if (string.IsNullOrEmpty(ShortcutsObject) == false)
                writer.AddAttribute("data-shortcuts", ShortcutsObject);
            if (string.IsNullOrEmpty(CustomValueLabel) == false)
                writer.AddAttribute("data-customValueLabel", CustomValueLabel);
            if (string.IsNullOrEmpty(CustomValuesData) == false)
                writer.AddAttribute("data-customValues", CustomValuesData);

            if (string.IsNullOrEmpty(FirstDateSelectedFunction) == false)
                writer.AddAttribute("data-firstDateSelectedFunc", FirstDateSelectedFunction);
            if (string.IsNullOrEmpty(LastDateSelectedFunction) == false)
                writer.AddAttribute("data-lastDateSelectedFunc", LastDateSelectedFunction);
            if (string.IsNullOrEmpty(OnOpenedFunction) == false)
                writer.AddAttribute("data-onOpenedFunc", OnOpenedFunction);
            if (string.IsNullOrEmpty(OnClosedFunction) == false)
                writer.AddAttribute("data-onClosedFunc", OnClosedFunction);

            if (string.IsNullOrEmpty(ShowDifferenceFunction) == false)
                writer.AddAttribute("data-showDifferenceFunc", ShowDifferenceFunction);
            if (string.IsNullOrEmpty(BeforeShowDayFunction) == false)
                writer.AddAttribute("data-beforeShowDayFunc", BeforeShowDayFunction);
            if (string.IsNullOrEmpty(ShowDateFilterFunction) == false)
                writer.AddAttribute("data-showDateFilterFunc", ShowDateFilterFunction);
            if (string.IsNullOrEmpty(GetWeekNumberFunction) == false)
                writer.AddAttribute("data-getWeekNumber", GetWeekNumberFunction);
            if (string.IsNullOrEmpty(SetValueFunction) == false)
                writer.AddAttribute("data-setValue", SetValueFunction);
            if (string.IsNullOrEmpty(GetValueFunction) == false)
                writer.AddAttribute("data-getValue", GetValueFunction);
            if (string.IsNullOrEmpty(HoveringTooltipFunction) == false)
                writer.AddAttribute("data-hoveringTooltip", HoveringTooltipFunction);
            if (string.IsNullOrEmpty(OnChangeMonthYearFunction) == false)
                writer.AddAttribute("data-onChangeMonthYear", OnChangeMonthYearFunction);

            string extraClass = ExtraClass;
            if (Theme != DRPTheme.None)
                extraClass += " " + Theme.ToRender();

            if (string.IsNullOrEmpty(extraClass) == false)
                writer.AddAttribute("data-extraClass", extraClass);

            if (string.IsNullOrEmpty(DayDivAttrs) == false)
                writer.AddAttribute("data-dayDivAttrs", DayDivAttrs);
            if (string.IsNullOrEmpty(DayTdAttrs) == false)
                writer.AddAttribute("data-dayTdAttrs", DayTdAttrs);
            if (string.IsNullOrEmpty(CustomShortcuts) == false)
                writer.AddAttribute("data-customShortcuts", CustomShortcuts);
            if (string.IsNullOrEmpty(StartDate) == false)
                writer.AddAttribute("data-startDate", StartDate);
            if (string.IsNullOrEmpty(EndDate) == false)
                writer.AddAttribute("data-endDate", EndDate);
            if (string.IsNullOrEmpty(DefaultDate) == false)
                writer.AddAttribute("data-defaultDate", DefaultDate);
            if (string.IsNullOrEmpty(MaxDays) == false)
                writer.AddAttribute("data-maxDays", MaxDays);
            if (string.IsNullOrEmpty(Duration) == false)
                writer.AddAttribute("data-duration", Duration);
            if (string.IsNullOrEmpty(MinDays) == false)
                writer.AddAttribute("data-minDays", MinDays);
            if (string.IsNullOrEmpty(Language) == false)
                writer.AddAttribute("data-language", Language);
            if (string.IsNullOrEmpty(Separator) == false)
                writer.AddAttribute("data-separator", Separator);
            if (string.IsNullOrEmpty(Format) == false)
                writer.AddAttribute("data-format", Format);
            if (string.IsNullOrEmpty(LoadingTemplateFunction) == false)
                writer.AddAttribute("data-loadingTemplate", LoadingTemplateFunction);
            if (string.IsNullOrEmpty(CustomTopBar) == false)
                writer.AddAttribute("data-customTopBar", CustomTopBar);
            if (string.IsNullOrEmpty(Container) == false)
                writer.AddAttribute("data-container", Container);
            if (string.IsNullOrEmpty(BtnPrev) == false)
                writer.AddAttribute("data-btnPrev", BtnPrev);
            if (string.IsNullOrEmpty(BtnNext) == false)
                writer.AddAttribute("data-btnNext", BtnNext);
            if (string.IsNullOrEmpty(ApplyBtnClass) == false)
                writer.AddAttribute("data-applyBtnClass", ApplyBtnClass);

            if (string.IsNullOrEmpty(ApplyBtnClass) == false)
                writer.AddAttribute("data-applyBtnClass", ApplyBtnClass);

            #region ajax

            if (string.IsNullOrEmpty(AjaxDataPath) == false)
            {
                writer.AddAttribute("data-ajaxDataPath", AjaxDataPath);
                if (string.IsNullOrEmpty(AjaxDataType) == false)
                    writer.AddAttribute("data-ajaxDataType", AjaxDataType);
                if (string.IsNullOrEmpty(AjaxCacheNumerKey) == false)
                    writer.AddAttribute("data-ajaxCacheNumerKey", AjaxCacheNumerKey);
                if (string.IsNullOrEmpty(AjaxMethodType) == false)
                    writer.AddAttribute("data-ajaxMethodType", AjaxMethodType);
                if (string.IsNullOrEmpty(AjaxContentType) == false)
                    writer.AddAttribute("data-ajaxContentType", AjaxContentType);
                if (string.IsNullOrEmpty(AjaxDataRequest) == false)
                    writer.AddAttribute("data-AjaxDataRequest", AjaxDataRequest);
                if (string.IsNullOrEmpty(AjaxBeforeSendFunction) == false)
                    writer.AddAttribute("data-ajaxBeforeSendFunction", AjaxBeforeSendFunction);
                if (string.IsNullOrEmpty(AjaxErrorFunction) == false)
                    writer.AddAttribute("data-ajaxErrorFunction", AjaxErrorFunction);
                if (string.IsNullOrEmpty(AjaxSuccessFunction) == false)
                    writer.AddAttribute("data-ajaxSuccessFunction", AjaxSuccessFunction);
                if (AjaxCacheOnLoad == true)
                    writer.AddAttribute("data-ajaxCacheOnLoad", "true");
            }
            if (string.IsNullOrEmpty(AjaxObjectSetting) == false)
                writer.AddAttribute("data-ajaxObjectSetting", AjaxObjectSetting);

            #endregion

            Page page = Page;
            if (page != null)
                page.VerifyRenderingInServerForm(this);

            if (AutoPostBack)
            {
                //writer.AddAttribute("data-onchange", "true");
                if (page != null)
                {
                    string onchange = page.ClientScript.GetPostBackEventReference(GetPostBackOptions(), true);
                    onchange = String.Concat("setTimeout('", onchange.Replace("\\", "\\\\").Replace("'", "\\'"), "', 0)");
                    writer.AddAttribute("data-onchange", onchange);
                }
            }
            else if (page != null)
                page.ClientScript.RegisterForEventValidation(UniqueID, String.Empty);

            if (ReadOnly)
                writer.AddAttribute(HtmlTextWriterAttribute.ReadOnly, "ReadOnly", false);

            writer.AddAttribute("data-hdf", ClientID + hdfId);

            #endregion

            /*using JsonConvert
            string date1 = Date1 == DateTime.MinValue ? "" : JsonConvert.SerializeObject(Date1);
            string date2 = Date2 == DateTime.MinValue ? "" : JsonConvert.SerializeObject(Date2);
            */

            //using JavaScriptSerializer
            DateTime d1 = GetDate(Date1);
            DateTime d2 = GetDate(Date2);
            string date1 = (d1 == DateTime.MinValue) ? "" : jss.Serialize(d1);
            string date2 = (d2 == DateTime.MinValue) ? "" : jss.Serialize(d2);

            /*set text, can not set
            if (SingleDate)
            {
                if (string.IsNullOrEmpty(date1) == false)
                {
                    string fm = Format;
                    if (string.IsNullOrEmpty(fm))
                    {
                        CultureInfo ci = GetLanguage();
                        if (ci != null)
                            fm = ci.DateTimeFormat.ShortDatePattern;
                    }
                    Text = Date1.ToString(string.IsNullOrEmpty(fm) ? "dd/MM/yyyy" : fm);
                }
            }
            else
            {
                if (string.IsNullOrEmpty(date1) == false && string.IsNullOrEmpty(date2) == false)
                {
                    string fm = Format;
                    if (string.IsNullOrEmpty(fm))
                    {
                        CultureInfo ci = GetLanguage();
                        if (ci != null)
                            fm = ci.DateTimeFormat.ShortDatePattern;
                    }
                    Text = Date1.ToString(string.IsNullOrEmpty(fm) ? "dd/MM/yyyy" : fm)
                        + Separator + Date2.ToString(string.IsNullOrEmpty(fm) ? "dd/MM/yyyy" : fm);
                }
            }
            */
            base.Render(writer);

            writer.WriteHtmlElement(new HtmlElement(string.Format("{0}", HtmlTextWriterTag.Input), "",
                ClientID + hdfId, null, null,
              new HtmlAttribute[] { 
                    new HtmlAttribute("type", "hidden", null), 
                    //new HtmlAttribute("value", date1 + "," + date2, null),
                    new HtmlAttribute("value", date1 + "," + date2 + "|" + Text, null),
                    new HtmlAttribute("name", UniqueID + hdfId, null) }, true, null), true);
        }

        protected override void RenderChildren(HtmlTextWriter writer)
        {
            if (RenderAsLabel)
                writer.Write(Text);
        }

        /// <summary>
        /// Raise the DateChanged event.
        /// </summary>
        /// <param name="eventArgument"></param>
        public void RaisePostBackEvent(string eventArgument)
        {
            if (DateChanged != null)
            {
                if (SingleDate == true)
                    DateChanged(this, new DateRangeChangedEventArgs(GetDate(Date1)));
                else
                    DateChanged(this, new DateRangeChangedEventArgs(GetDate(Date1), GetDate(Date2)));
            }
        }

        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
        }

        public virtual bool LoadPostData(string postDataKey, NameValueCollection postCollection)
        {
            bool isChange = false;
            string postedValue = postCollection[postDataKey + hdfId];
            if (postedValue != null && string.IsNullOrEmpty(postedValue) == false)
            {
                string[] data = postedValue.Split('|');
                string text = string.Empty;
                string dataDate = string.Empty;
                if (data.Length > 0)
                {
                    dataDate = data[0];
                    if (data.Length > 1)
                        text = data[1];
                    if (data.Length > 2)
                    {
                        for (int i = 2; i < data.Length; i++)
                            text += "|" + data[i];
                    }

                    if (text != Text)
                    {
                        Text = text;
                        isChange = true;
                    }

                    CultureInfo ci = GetLanguage();

                    string[] dataPosted = dataDate.Split(',');
                    if (dataPosted.Length == 2)
                    {
                        //string date1Old = JsonConvert.SerializeObject(Date1);
                        string date1Old = jss.Serialize(Date1);

                        if (dataPosted[0] != null && dataPosted[0].Length > 0
                            && dataPosted[0].ToLower() != "invalid date" &&
                                dataPosted[0] != date1Old)
                        {
                            isChange = true;
                            //long.TryParse(dataPosted[0], out date1);
                            try
                            {
                                //Date1 = JsonConvert.DeserializeObject<DateTime>("\"" + dataPosted[0] + "\"");
                                Date1 = jss.Deserialize<DateTime>("\"" + dataPosted[0] + "\"").ToString(ci);
                            }
                            catch (Exception ex)
                            {
                                Date1 = string.Empty;
                            }
                        }
                        else
                        {
                            if ((dataPosted[0] == null || dataPosted[0].Length == 0 || dataPosted[0].ToLower() == "invalid date") && date1Old.Length > 0)
                            {
                                isChange = true;
                                Date1 = string.Empty;
                            }
                        }

                        //string date2Old = JsonConvert.SerializeObject(Date2);
                        string date2Old = jss.Serialize(Date2);
                        if (dataPosted[1] != null && dataPosted[1].Length > 0
                            && dataPosted[1].ToLower() != "invalid date" &&
                               dataPosted[1] != date2Old)
                        {
                            isChange = true;
                            //long.TryParse(dataPosted[1], out date2);
                            try
                            {
                                //Date2 = JsonConvert.DeserializeObject<DateTime>("\"" + dataPosted[1] + "\"");
                                Date2 = jss.Deserialize<DateTime>("\"" + dataPosted[1] + "\"").ToString(ci);
                            }
                            catch (Exception ex)
                            {
                                Date2 = string.Empty;
                            }
                        }
                        else
                        {
                            if ((dataPosted[1] == null || dataPosted[1].Length == 0 || dataPosted[1].ToLower() == "invalid date") && date2Old.Length > 0)
                            {
                                isChange = true;
                                Date2 = string.Empty;
                            }
                        }
                    }
                    else if (dataPosted.Length == 1)
                    {
                        //string date1Old = JsonConvert.SerializeObject(Date1);
                        string date1Old = jss.Serialize(Date1);
                        if (dataPosted[0] != null && dataPosted[0].Length > 0
                            && dataPosted[0].ToLower() != "invalid date" &&
                            dataPosted[0] != date1Old)
                        {
                            isChange = true;
                            //long.TryParse(dataPosted[0], out date1);
                            try
                            {
                                //Date1 = JsonConvert.DeserializeObject<DateTime>("\"" + dataPosted[0] + "\"");
                                Date1 = jss.Deserialize<DateTime>("\"" + dataPosted[0] + "\"").ToString(ci);
                            }
                            catch (Exception ex)
                            {
                                Date1 = string.Empty;
                            }
                        }
                        else
                        {
                            if ((dataPosted[0] == null || dataPosted[0].Length == 0 || dataPosted[0].ToLower() == "invalid date") && date1Old.Length > 0)
                            {
                                isChange = true;
                                Date1 = string.Empty;
                            }
                        }
                    }
                }
            }

            return isChange;
        }

        /*
         bool IPostBackDataHandler.LoadPostData(string postDataKey, NameValueCollection postCollection)
         {
             return LoadPostData(postDataKey, postCollection);
         }
        */

        /*good text changed
        void IPostBackDataHandler.RaisePostDataChangedEvent()
        {
            RaisePostDataChangedEvent();
        }
        
        protected virtual void RaisePostDataChangedEvent()
        {
            OnTextChanged(EventArgs.Empty);
        }

        public event EventHandler TextChanged;

        protected virtual void OnTextChanged(EventArgs e)
        {
            if (TextChanged != null)
                TextChanged(this, e);
        }
        */

        PostBackOptions GetPostBackOptions()
        {
            PostBackOptions options = new PostBackOptions(this);
            options.ActionUrl = null;
            options.ValidationGroup = null;
            options.Argument = String.Empty;
            options.RequiresJavaScriptProtocol = false;
            options.ClientSubmit = true;

            return options;
        }

    }

    /// <summary>
    /// Arguments of the DateChanged event.
    /// </summary>
    public class DateRangeChangedEventArgs : EventArgs
    {
        private DateTime _date1;
        private DateTime _date2;

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="date"></param>
        public DateRangeChangedEventArgs(DateTime date1, DateTime date2)
        {
            this._date1 = date1;
            this._date2 = date2;
        }

        /// <summary>
        /// Create a new instance.
        /// </summary>
        /// <param name="date"></param>
        public DateRangeChangedEventArgs(DateTime date1)
        {
            this._date1 = date1;
            this._date2 = DateTime.MinValue;
        }

        /// <summary>
        /// Get the date 1.
        /// </summary>
        public DateTime Date1
        {
            get { return _date1; }
        }

        /// <summary>
        /// Get the date 2.
        /// </summary>
        public DateTime Date2
        {
            get { return _date2; }
        }
    }


    public enum DRPBatchMode
    {
        [Render("week-range")]
        WeekRange,
        [Render("month-range")]
        MonthRange,
        [Render("workweek")]
        WorkWeek,
        [Render("weekend")]
        Weekend,
        [Render("week")]
        Week,
        [Render("month")]
        Month,
        [Render("")]
        None
    }

    public enum DRPStartOfWeek
    {
        [Render("sunday")]
        Sunday,
        [Render("monday")]
        Monday,
        [Render("")]
        None
    }

    public enum DRPTheme
    {
        [Render("ll-skin-cangas")]
        Cangas,
        [Render("ll-skin-nigran")]
        Nigran,
        [Render("ll-skin-lugo")]
        Lugo,
        [Render("ll-skin-latoja")]
        Latoja,
        [Render("")]
        None
    }

    public enum DRPDirection
    {
        [Render("left")]
        Left,
        [Render("right")]
        Right,
        [Render("center")]
        Center,
        [Render("")]
        None
    }

    public enum DRPOpenAt
    {
        [Render("top")]
        Top,
        [Render("bottom")]
        Bottom,
        [Render("center")]
        Center,
        [Render("")]
        None
    }

    public enum DRPCollision
    {
        [Render("flip")]
        Flip,
        [Render("fit")]
        Fit,
        [Render("flipfit")]
        Flipfit,
        [Render("none")]
        None
    }

}