using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace SweetSoft.HiepHung.DataAccess
{
    /// <summary>
    /// Controller class for TblKhachHang
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class TblKhachHangController
    {
        // Preload our schema..
        TblKhachHang thisSchemaLoad = new TblKhachHang();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public TblKhachHangCollection FetchAll()
        {
            TblKhachHangCollection coll = new TblKhachHangCollection();
            Query qry = new Query(TblKhachHang.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public TblKhachHangCollection FetchByID(object Id)
        {
            TblKhachHangCollection coll = new TblKhachHangCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public TblKhachHangCollection FetchByQuery(Query qry)
        {
            TblKhachHangCollection coll = new TblKhachHangCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (TblKhachHang.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (TblKhachHang.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
        public TblKhachHang Insert(TblKhachHang item)
	    {
		 
		    item.Save(UserName);
            return item;
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
        public TblKhachHang Update(TblKhachHang khachHang)
	    {
		    TblKhachHang item = new TblKhachHang();
	        item.MarkOld();
	        item.IsLoaded = true;

            item.Id = khachHang.Id;

            item.TenKhachHang = khachHang.TenKhachHang;

            item.SoDienThoai = khachHang.SoDienThoai;

            item.MaSoThue = khachHang.MaSoThue;

            item.TaiKhoanNganHang = khachHang.TaiKhoanNganHang;

            item.DiaChi = khachHang.DiaChi;

            item.CreatedBy = khachHang.CreatedBy;

            item.CreatedDate = khachHang.CreatedDate;

            item.UpdatedBy = khachHang.UpdatedBy;

            item.UpdatedDate = khachHang.UpdatedDate;
				
	        item.Save(UserName);

            khachHang.MarkOld();
            khachHang.IsLoaded = true;
            khachHang.Save(UserName);
            return khachHang;
	    }
    }
}
