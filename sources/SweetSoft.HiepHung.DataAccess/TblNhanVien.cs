using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
// <auto-generated />
namespace SweetSoft.HiepHung.DataAccess
{
	/// <summary>
	/// Strongly-typed collection for the TblNhanVien class.
	/// </summary>
    [Serializable]
	public partial class TblNhanVienCollection : ActiveList<TblNhanVien, TblNhanVienCollection>
	{	   
		public TblNhanVienCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>TblNhanVienCollection</returns>
		public TblNhanVienCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                TblNhanVien o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the TblNhanVien table.
	/// </summary>
	[Serializable]
	public partial class TblNhanVien : ActiveRecord<TblNhanVien>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public TblNhanVien()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public TblNhanVien(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public TblNhanVien(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public TblNhanVien(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("TblNhanVien", TableType.Table, DataService.GetInstance("DataAcessProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarFirstName = new TableSchema.TableColumn(schema);
				colvarFirstName.ColumnName = "FirstName";
				colvarFirstName.DataType = DbType.String;
				colvarFirstName.MaxLength = 50;
				colvarFirstName.AutoIncrement = false;
				colvarFirstName.IsNullable = true;
				colvarFirstName.IsPrimaryKey = false;
				colvarFirstName.IsForeignKey = false;
				colvarFirstName.IsReadOnly = false;
				colvarFirstName.DefaultSetting = @"";
				colvarFirstName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFirstName);
				
				TableSchema.TableColumn colvarLastName = new TableSchema.TableColumn(schema);
				colvarLastName.ColumnName = "LastName";
				colvarLastName.DataType = DbType.String;
				colvarLastName.MaxLength = 50;
				colvarLastName.AutoIncrement = false;
				colvarLastName.IsNullable = true;
				colvarLastName.IsPrimaryKey = false;
				colvarLastName.IsForeignKey = false;
				colvarLastName.IsReadOnly = false;
				colvarLastName.DefaultSetting = @"";
				colvarLastName.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLastName);
				
				TableSchema.TableColumn colvarGioiTinh = new TableSchema.TableColumn(schema);
				colvarGioiTinh.ColumnName = "GioiTinh";
				colvarGioiTinh.DataType = DbType.String;
				colvarGioiTinh.MaxLength = 10;
				colvarGioiTinh.AutoIncrement = false;
				colvarGioiTinh.IsNullable = true;
				colvarGioiTinh.IsPrimaryKey = false;
				colvarGioiTinh.IsForeignKey = false;
				colvarGioiTinh.IsReadOnly = false;
				colvarGioiTinh.DefaultSetting = @"";
				colvarGioiTinh.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGioiTinh);
				
				TableSchema.TableColumn colvarDiaChi = new TableSchema.TableColumn(schema);
				colvarDiaChi.ColumnName = "DiaChi";
				colvarDiaChi.DataType = DbType.String;
				colvarDiaChi.MaxLength = 450;
				colvarDiaChi.AutoIncrement = false;
				colvarDiaChi.IsNullable = true;
				colvarDiaChi.IsPrimaryKey = false;
				colvarDiaChi.IsForeignKey = false;
				colvarDiaChi.IsReadOnly = false;
				colvarDiaChi.DefaultSetting = @"";
				colvarDiaChi.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDiaChi);
				
				TableSchema.TableColumn colvarSoDienThoai = new TableSchema.TableColumn(schema);
				colvarSoDienThoai.ColumnName = "SoDienThoai";
				colvarSoDienThoai.DataType = DbType.String;
				colvarSoDienThoai.MaxLength = 20;
				colvarSoDienThoai.AutoIncrement = false;
				colvarSoDienThoai.IsNullable = true;
				colvarSoDienThoai.IsPrimaryKey = false;
				colvarSoDienThoai.IsForeignKey = false;
				colvarSoDienThoai.IsReadOnly = false;
				colvarSoDienThoai.DefaultSetting = @"";
				colvarSoDienThoai.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSoDienThoai);
				
				TableSchema.TableColumn colvarCmnd = new TableSchema.TableColumn(schema);
				colvarCmnd.ColumnName = "CMND";
				colvarCmnd.DataType = DbType.String;
				colvarCmnd.MaxLength = 20;
				colvarCmnd.AutoIncrement = false;
				colvarCmnd.IsNullable = true;
				colvarCmnd.IsPrimaryKey = false;
				colvarCmnd.IsForeignKey = false;
				colvarCmnd.IsReadOnly = false;
				colvarCmnd.DefaultSetting = @"";
				colvarCmnd.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCmnd);
				
				TableSchema.TableColumn colvarNgaySinh = new TableSchema.TableColumn(schema);
				colvarNgaySinh.ColumnName = "NgaySinh";
				colvarNgaySinh.DataType = DbType.DateTime;
				colvarNgaySinh.MaxLength = 0;
				colvarNgaySinh.AutoIncrement = false;
				colvarNgaySinh.IsNullable = true;
				colvarNgaySinh.IsPrimaryKey = false;
				colvarNgaySinh.IsForeignKey = false;
				colvarNgaySinh.IsReadOnly = false;
				colvarNgaySinh.DefaultSetting = @"";
				colvarNgaySinh.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNgaySinh);
				
				TableSchema.TableColumn colvarNgayVaoCongTy = new TableSchema.TableColumn(schema);
				colvarNgayVaoCongTy.ColumnName = "NgayVaoCongTy";
				colvarNgayVaoCongTy.DataType = DbType.DateTime;
				colvarNgayVaoCongTy.MaxLength = 0;
				colvarNgayVaoCongTy.AutoIncrement = false;
				colvarNgayVaoCongTy.IsNullable = true;
				colvarNgayVaoCongTy.IsPrimaryKey = false;
				colvarNgayVaoCongTy.IsForeignKey = false;
				colvarNgayVaoCongTy.IsReadOnly = false;
				colvarNgayVaoCongTy.DefaultSetting = @"";
				colvarNgayVaoCongTy.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNgayVaoCongTy);
				
				TableSchema.TableColumn colvarIDPhongBan = new TableSchema.TableColumn(schema);
				colvarIDPhongBan.ColumnName = "IDPhongBan";
				colvarIDPhongBan.DataType = DbType.Int32;
				colvarIDPhongBan.MaxLength = 0;
				colvarIDPhongBan.AutoIncrement = false;
				colvarIDPhongBan.IsNullable = true;
				colvarIDPhongBan.IsPrimaryKey = false;
				colvarIDPhongBan.IsForeignKey = false;
				colvarIDPhongBan.IsReadOnly = false;
				colvarIDPhongBan.DefaultSetting = @"";
				colvarIDPhongBan.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIDPhongBan);
				
				TableSchema.TableColumn colvarIDUser = new TableSchema.TableColumn(schema);
				colvarIDUser.ColumnName = "IDUser";
				colvarIDUser.DataType = DbType.Int32;
				colvarIDUser.MaxLength = 0;
				colvarIDUser.AutoIncrement = false;
				colvarIDUser.IsNullable = true;
				colvarIDUser.IsPrimaryKey = false;
				colvarIDUser.IsForeignKey = false;
				colvarIDUser.IsReadOnly = false;
				colvarIDUser.DefaultSetting = @"";
				colvarIDUser.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIDUser);
				
				TableSchema.TableColumn colvarTrangThaiSuDung = new TableSchema.TableColumn(schema);
				colvarTrangThaiSuDung.ColumnName = "TrangThaiSuDung";
				colvarTrangThaiSuDung.DataType = DbType.Boolean;
				colvarTrangThaiSuDung.MaxLength = 0;
				colvarTrangThaiSuDung.AutoIncrement = false;
				colvarTrangThaiSuDung.IsNullable = true;
				colvarTrangThaiSuDung.IsPrimaryKey = false;
				colvarTrangThaiSuDung.IsForeignKey = false;
				colvarTrangThaiSuDung.IsReadOnly = false;
				colvarTrangThaiSuDung.DefaultSetting = @"";
				colvarTrangThaiSuDung.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTrangThaiSuDung);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["DataAcessProvider"].AddSchema("TblNhanVien",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("FirstName")]
		[Bindable(true)]
		public string FirstName 
		{
			get { return GetColumnValue<string>(Columns.FirstName); }
			set { SetColumnValue(Columns.FirstName, value); }
		}
		  
		[XmlAttribute("LastName")]
		[Bindable(true)]
		public string LastName 
		{
			get { return GetColumnValue<string>(Columns.LastName); }
			set { SetColumnValue(Columns.LastName, value); }
		}
		  
		[XmlAttribute("GioiTinh")]
		[Bindable(true)]
		public string GioiTinh 
		{
			get { return GetColumnValue<string>(Columns.GioiTinh); }
			set { SetColumnValue(Columns.GioiTinh, value); }
		}
		  
		[XmlAttribute("DiaChi")]
		[Bindable(true)]
		public string DiaChi 
		{
			get { return GetColumnValue<string>(Columns.DiaChi); }
			set { SetColumnValue(Columns.DiaChi, value); }
		}
		  
		[XmlAttribute("SoDienThoai")]
		[Bindable(true)]
		public string SoDienThoai 
		{
			get { return GetColumnValue<string>(Columns.SoDienThoai); }
			set { SetColumnValue(Columns.SoDienThoai, value); }
		}
		  
		[XmlAttribute("Cmnd")]
		[Bindable(true)]
		public string Cmnd 
		{
			get { return GetColumnValue<string>(Columns.Cmnd); }
			set { SetColumnValue(Columns.Cmnd, value); }
		}
		  
		[XmlAttribute("NgaySinh")]
		[Bindable(true)]
		public DateTime? NgaySinh 
		{
			get { return GetColumnValue<DateTime?>(Columns.NgaySinh); }
			set { SetColumnValue(Columns.NgaySinh, value); }
		}
		  
		[XmlAttribute("NgayVaoCongTy")]
		[Bindable(true)]
		public DateTime? NgayVaoCongTy 
		{
			get { return GetColumnValue<DateTime?>(Columns.NgayVaoCongTy); }
			set { SetColumnValue(Columns.NgayVaoCongTy, value); }
		}
		  
		[XmlAttribute("IDPhongBan")]
		[Bindable(true)]
		public int? IDPhongBan 
		{
			get { return GetColumnValue<int?>(Columns.IDPhongBan); }
			set { SetColumnValue(Columns.IDPhongBan, value); }
		}
		  
		[XmlAttribute("IDUser")]
		[Bindable(true)]
		public int? IDUser 
		{
			get { return GetColumnValue<int?>(Columns.IDUser); }
			set { SetColumnValue(Columns.IDUser, value); }
		}
		  
		[XmlAttribute("TrangThaiSuDung")]
		[Bindable(true)]
		public bool? TrangThaiSuDung 
		{
			get { return GetColumnValue<bool?>(Columns.TrangThaiSuDung); }
			set { SetColumnValue(Columns.TrangThaiSuDung, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varFirstName,string varLastName,string varGioiTinh,string varDiaChi,string varSoDienThoai,string varCmnd,DateTime? varNgaySinh,DateTime? varNgayVaoCongTy,int? varIDPhongBan,int? varIDUser,bool? varTrangThaiSuDung)
		{
			TblNhanVien item = new TblNhanVien();
			
			item.FirstName = varFirstName;
			
			item.LastName = varLastName;
			
			item.GioiTinh = varGioiTinh;
			
			item.DiaChi = varDiaChi;
			
			item.SoDienThoai = varSoDienThoai;
			
			item.Cmnd = varCmnd;
			
			item.NgaySinh = varNgaySinh;
			
			item.NgayVaoCongTy = varNgayVaoCongTy;
			
			item.IDPhongBan = varIDPhongBan;
			
			item.IDUser = varIDUser;
			
			item.TrangThaiSuDung = varTrangThaiSuDung;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varFirstName,string varLastName,string varGioiTinh,string varDiaChi,string varSoDienThoai,string varCmnd,DateTime? varNgaySinh,DateTime? varNgayVaoCongTy,int? varIDPhongBan,int? varIDUser,bool? varTrangThaiSuDung)
		{
			TblNhanVien item = new TblNhanVien();
			
				item.Id = varId;
			
				item.FirstName = varFirstName;
			
				item.LastName = varLastName;
			
				item.GioiTinh = varGioiTinh;
			
				item.DiaChi = varDiaChi;
			
				item.SoDienThoai = varSoDienThoai;
			
				item.Cmnd = varCmnd;
			
				item.NgaySinh = varNgaySinh;
			
				item.NgayVaoCongTy = varNgayVaoCongTy;
			
				item.IDPhongBan = varIDPhongBan;
			
				item.IDUser = varIDUser;
			
				item.TrangThaiSuDung = varTrangThaiSuDung;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn FirstNameColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn LastNameColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn GioiTinhColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DiaChiColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn SoDienThoaiColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CmndColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn NgaySinhColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn NgayVaoCongTyColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IDPhongBanColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IDUserColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn TrangThaiSuDungColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string FirstName = @"FirstName";
			 public static string LastName = @"LastName";
			 public static string GioiTinh = @"GioiTinh";
			 public static string DiaChi = @"DiaChi";
			 public static string SoDienThoai = @"SoDienThoai";
			 public static string Cmnd = @"CMND";
			 public static string NgaySinh = @"NgaySinh";
			 public static string NgayVaoCongTy = @"NgayVaoCongTy";
			 public static string IDPhongBan = @"IDPhongBan";
			 public static string IDUser = @"IDUser";
			 public static string TrangThaiSuDung = @"TrangThaiSuDung";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
