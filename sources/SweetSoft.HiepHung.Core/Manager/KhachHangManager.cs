﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class KhachHangManager
    {
        #region Khách hàng
        public static TblKhachHang InsertKhachHang(TblKhachHang dvt)
        {
            return new TblKhachHangController().Insert(dvt);
        }
        public static TblKhachHang UpdateKhachHang(TblKhachHang dvt)
        {
            return new TblKhachHangController().Update(dvt);
        }
        public static string DeleteKhachHang(object id)
        {
            string result = string.Empty;
            if (new TblKhachHangController().Delete(id))
                result = "success";
            return result;
        }

        public static List<TblKhachHang> GetKhachHangAll(bool? isWarHourse)
        {
            Select select = new Select(

                );
            select.From(TblKhachHang.Schema.TableName);

            select.Where(TblKhachHang.Columns.Id).IsNotNull();

            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblKhachHang>();
        }

        public static List<TblKhachHang> SearchKhachHangByText(string key)
        {
            Select select = new Select();
            select.From(TblKhachHang.Schema);
            select.Where(TblKhachHang.Columns.TenKhachHang).Like(string.Format("%{0}%", key));
            select.Or(TblKhachHang.Columns.DiaChi).Like(string.Format("%{0}%", key));
            select.Or(TblKhachHang.Columns.SoDienThoai).Like(string.Format("%{0}%", key));
            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblKhachHang>();
        }

        public static TblKhachHang GetKhachHangByID(object id)
        {
            return new Select().From(TblKhachHang.Schema).Where(TblKhachHang.Columns.Id).IsEqualTo(id).ExecuteSingle<TblKhachHang>();
        }

        public static List<TblKhachHang> SearchKhachHang(int rowIndex, int pageSize, string column, string tableJoin, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           string.Format("{0} as kh", TblKhachHang.Schema.TableName), tableJoin,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblKhachHang> KhachHang = sp.GetDataSet().ToList<TblKhachHang>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return KhachHang;
        }
        #endregion


        #region Khách hàng
        public static TblKhachHangLienHe InsertKhachHangLienHe(TblKhachHangLienHe dvt)
        {
            return new TblKhachHangLienHeController().Insert(dvt);
        }
        public static TblKhachHangLienHe UpdateKhachHangLienHe(TblKhachHangLienHe dvt)
        {
            return new TblKhachHangLienHeController().Update(dvt);
        }
        public static bool DeleteKhachHangLienHe(object id)
        {
            return new TblKhachHangLienHeController().Delete(id);
        }

        public static List<TblKhachHangLienHe> GetKhachHangLienHeAll(bool? isWarHourse)
        {
            Select select = new Select(

                );
            select.From(TblKhachHangLienHe.Schema.TableName);

            select.Where(TblKhachHangLienHe.Columns.Id).IsNotNull();

            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblKhachHangLienHe>();
        }

        public static TblKhachHangLienHe GetKhachHangLienHeByID(object id)
        {
            return new Select().From(TblKhachHangLienHe.Schema).Where(TblKhachHangLienHe.Columns.Id).IsEqualTo(id).ExecuteSingle<TblKhachHangLienHe>();
        }

        public static List<TblKhachHangLienHe> GetKhachHangLienHeByKhachHangID(object id)
        {
            return new Select().From(TblKhachHangLienHe.Schema).Where(TblKhachHangLienHe.IDKhachHangColumn).IsEqualTo(id).ExecuteTypedList<TblKhachHangLienHe>();
        }

        public static List<TblKhachHangLienHe> SearchKhachHangLienHe(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           string.Format(" {0} as khlh ", TblKhachHangLienHe.Schema.TableName), string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblKhachHangLienHe> KhachHangLienHe = sp.GetDataSet().ToList<TblKhachHangLienHe>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return KhachHangLienHe;
        }
        #endregion
    }
}
