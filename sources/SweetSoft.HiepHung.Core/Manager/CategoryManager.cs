﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class CategoryManager
    {
        #region Phòng ban
        public static TblPhongBan InsertPhongBan(TblPhongBan dvt)
        {
            return new TblPhongBanController().Insert(dvt);
        }
        public static TblPhongBan UpdatePhongBan(TblPhongBan dvt)
        {
            return new TblPhongBanController().Update(dvt);
        }
        public static bool DeletePhongBan(object id)
        {
            return new TblPhongBanController().Delete(id);
        }

        public static List<TblPhongBan> GetPhongBanAll(bool? status)
        {
            return GetPhongBanAll(status, null);
        }

        public static List<TblPhongBan> GetPhongBanAll(bool? status, bool? isWarHourse)
        {
            Select select = new Select(

                );
            select.From(TblPhongBan.Schema.TableName);

            select.Where(TblPhongBan.Columns.Id).IsNotNull();
            if (status.HasValue)
                select.And(TblPhongBan.Columns.TrangThaiSuDung).IsEqualTo(status.Value);
            if (isWarHourse.HasValue)
                select.And(TblPhongBan.Columns.IsWarehourse).IsEqualTo(isWarHourse.Value);
            select.OrderAsc(TblPhongBan.Columns.IsWarehourse);
            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblPhongBan>();
        }

        public static TblPhongBan GetPhongBanByID(object id)
        {
            return new Select().From(TblPhongBan.Schema).Where(TblPhongBan.Columns.Id).IsEqualTo(id).ExecuteSingle<TblPhongBan>();
        }

        public static List<TblPhongBan> SearchPhongBan(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           TblPhongBan.Schema.TableName, string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblPhongBan> PhongBan = sp.GetDataSet().ToList<TblPhongBan>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return PhongBan;
        }
        #endregion

        #region Đơn vị tính
        public static TblDonViTinh InsertDonViTinh(TblDonViTinh dvt)
        {
            return new TblDonViTinhController().Insert(dvt);
        }
        public static TblDonViTinh UpdateDonViTinh(TblDonViTinh dvt)
        {
            return new TblDonViTinhController().Update(dvt);
        }
        public static bool DeleteDonViTinh(object id)
        {
            return new TblDonViTinhController().Delete(id);
        }

        public static TblDonViTinh GetDonViTinhByID(object id)
        {
            return new Select().From(TblDonViTinh.Schema).Where(TblDonViTinh.Columns.Id).IsEqualTo(id).ExecuteSingle<TblDonViTinh>();
        }

        public static List<TblDonViTinh> GetDonViTinhALL(bool? status)
        {
            Select select = new Select();
            select.From(TblDonViTinh.Schema);
            select.Where(TblDonViTinh.Columns.Id).IsNotNull();
            if (status.HasValue)
                select.And(TblDonViTinh.Columns.TrangThaiSuDung).IsEqualTo(status.Value);
            return select.ExecuteTypedList<TblDonViTinh>();
        }



        public static List<TblDonViTinh> SearchDonViTinh(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           TblDonViTinh.Schema.TableName, string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblDonViTinh> donvitinh = sp.GetDataSet().ToList<TblDonViTinh>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return donvitinh;
        }
        #endregion

        #region Công việc
        public static TblCongViec InsertCongViec(TblCongViec dvt)
        {
            return new TblCongViecController().Insert(dvt);
        }
        public static TblCongViec UpdateCongViec(TblCongViec dvt)
        {
            return new TblCongViecController().Update(dvt);
        }
        public static bool DeleteCongViec(object id)
        {
            return new TblCongViecController().Delete(id);
        }

        public static TblCongViec GetCongViecByID(object id)
        {
            return new Select().From(TblCongViec.Schema).Where(TblCongViec.Columns.Id).IsEqualTo(id).ExecuteSingle<TblCongViec>();
        }



        public static List<TblCongViec> GetCongViecALL(bool? status)
        {
            Select select = new Select();
            select.From(TblCongViec.Schema);
            select.Where(TblCongViec.Columns.Id).IsNotNull();
            if (status.HasValue)
                select.And(TblCongViec.Columns.TrangThaiSuDung).IsEqualTo(status.Value);
            return select.ExecuteTypedList<TblCongViec>();
        }



        public static List<TblCongViec> SearchCongViec(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           TblCongViec.Schema.TableName, string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblCongViec> CongViec = sp.GetDataSet().ToList<TblCongViec>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return CongViec;
        }
        #endregion

        #region Loại giấy
        public static TblLoaiGiay InsertLoaiGiay(TblLoaiGiay dvt)
        {
            return new TblLoaiGiayController().Insert(dvt);
        }
        public static TblLoaiGiay UpdateLoaiGiay(TblLoaiGiay dvt)
        {
            return new TblLoaiGiayController().Update(dvt);
        }
        public static bool DeleteLoaiGiay(object id)
        {
            return new TblLoaiGiayController().Delete(id);
        }

        public static TblLoaiGiay GetLoaiGiayByID(object id)
        {
            return new Select().From(TblLoaiGiay.Schema).Where(TblLoaiGiay.Columns.Id).IsEqualTo(id).ExecuteSingle<TblLoaiGiay>();
        }

        public static List<TblLoaiGiay> GetLoaiGiayALL(bool? status)
        {
            Select select = new Select();
            select.From(TblLoaiGiay.Schema);
            select.Where(TblLoaiGiay.Columns.Id).IsNotNull();
            if (status.HasValue)
                select.And(TblLoaiGiay.Columns.TrangThaiSuDung).IsEqualTo(status.Value);
            return select.ExecuteTypedList<TblLoaiGiay>();
        }



        public static List<TblLoaiGiay> SearchLoaiGiay(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           TblLoaiGiay.Schema.TableName, string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblLoaiGiay> LoaiGiay = sp.GetDataSet().ToList<TblLoaiGiay>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return LoaiGiay;
        }
        #endregion

        #region Loại mẫu in
        public static TblLoaiMauIn InsertLoaiMauIn(TblLoaiMauIn dvt)
        {
            return new TblLoaiMauInController().Insert(dvt);
        }
        public static TblLoaiMauIn UpdateLoaiMauIn(TblLoaiMauIn dvt)
        {
            return new TblLoaiMauInController().Update(dvt);
        }
        public static bool DeleteLoaiMauIn(object id)
        {
            return new TblLoaiMauInController().Delete(id);
        }

        public static TblLoaiMauIn GetLoaiMauInByID(object id)
        {
            return new Select().From(TblLoaiMauIn.Schema).Where(TblLoaiMauIn.Columns.Id).IsEqualTo(id).ExecuteSingle<TblLoaiMauIn>();
        }

        public static List<TblLoaiMauIn> GetLoaiMauInALL(bool? status, string Ctype = "LMI")
        {
            Select select = new Select();
            select.From(TblLoaiMauIn.Schema);
            select.Where(TblLoaiMauIn.Columns.Id).IsNotNull();
            if (status.HasValue)
                select.And(TblLoaiMauIn.Columns.TrangThaiSuDung).IsEqualTo(status.Value);
            if (!string.IsNullOrEmpty(Ctype))
                select.And(TblLoaiMauIn.Columns.CType).IsEqualTo(Ctype);
            return select.ExecuteTypedList<TblLoaiMauIn>();
        }



        public static List<TblLoaiMauIn> SearchLoaiMauIn(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           TblLoaiMauIn.Schema.TableName, string.Empty,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblLoaiMauIn> LoaiMauIn = sp.GetDataSet().ToList<TblLoaiMauIn>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return LoaiMauIn;
        }
        #endregion

        #region Image
        public static TblImage InsertImage(TblImage img)
        {
            return new TblImageController().Insert(img.Id, img.ImagePath);
        }

        public static bool DeleteImage(object id)
        {
            return new TblImageController().Delete(id);
        }

        public static TblLoaiMauImage InsertLoaiMauImage(TblLoaiMauImage img)
        {
            return new TblLoaiMauImageController().Insert(img.IDLoaiMau, img.Img);
        }

        public static bool DeleteLoaiMauImage(object id)
        {
            return new TblLoaiMauImageController().Delete(id);
        }

        public static TblImage GetImageByID(object id)
        {
            return new Select().From(TblImage.Schema).Where(TblImage.Columns.Id).IsEqualTo(id).ExecuteSingle<TblImage>();
        }

        public static TblLoaiMauImage GetLoaiMauImageByID(object id)
        {
            return new Select().From(TblLoaiMauImage.Schema).Where(TblLoaiMauImage.Columns.Id).IsEqualTo(id).ExecuteSingle<TblLoaiMauImage>();
        }

        public static List<TblImage> GetLoaiMauImage(object idLoaiMau)
        {
            Select select = new Select();
            string sql = string.Format(@"select img.*,mii.ID as LoaiMauIMGID from {0} as img left join {1} as mii on img.ID = mii.{2} where mii.{3} = {4}",
                TblImage.Schema.TableName, TblLoaiMauImage.Schema.TableName, TblLoaiMauImage.Columns.Img, TblLoaiMauImage.Columns.IDLoaiMau,
                idLoaiMau);
            if (idLoaiMau == null)
                sql = string.Format(@" select img.* from {0} as img ", TblImage.Schema.TableName);
            DataTable ds = new DataTable();
            ds.Load(new InlineQuery().ExecuteReader(sql));
            return ds.ToList<TblImage>();
        }
        #endregion
    }
}
