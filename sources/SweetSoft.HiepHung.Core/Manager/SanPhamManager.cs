﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class SanPhamManager
    {
        #region Sản phẩm
        public static TblSanPham InsertSanPham(TblSanPham dvt)
        {
            return new TblSanPhamController().Insert(dvt);
        }
        public static TblSanPham UpdateSanPham(TblSanPham sp)
        {
            string update = string.Empty;
            if (!sp.IDKhachHangRieng.HasValue)
                update += " IDKhachHangRieng = null,";
            if (!sp.IDKieuThung.HasValue)
                update += " IDKieuThung = null, ";
            if (!sp.IDLoaiMauIn.HasValue)
                update += " IDLoaiMauIn = null, ";
            if (!string.IsNullOrEmpty(update))
            {
                update += string.Format(" MaSanPham = '{0}' ", sp.MaSanPham);
                new InlineQuery().Execute(string.Format("update {0} set {1} where Id = {2};", TblSanPham.Schema.TableName, update, sp.Id));
            }
            return new TblSanPhamController().Update(sp);
        }
        public static bool DeleteSanPham(object id)
        {

            return new TblSanPhamController().Delete(id);
        }

        public static TblSanPham GetSanPhamByID(object id)
        {
            string sql = string.Format(@"
                          select
                          sp.*, kh.TenKhachHang, lgm.TenLoaiGiay as LoaiGiayMat,dvt.TenDonVi,
                          lgs.TenLoaiGiay as LoaiGiaySong, lgd.TenLoaiGiay as LoaiGiayDay,
                          lmi.TenMau as TenMauIn,lmt.TenMau as TenMauThung
                          from TblSanPham sp 
                            left join TblKhachHang kh on sp.IDKhachHangRieng = kh.Id 
                            left join TblLoaiGiay lgm on sp.IDLoaiGiayMat = lgm.Id 
                            left join TblLoaiGiay lgs on sp.IDLoaiGiaySong = lgs.Id 
                            left join TblLoaiGiay lgd on sp.IDLoaiGiayDay = lgd.Id 
                            left join TblLoaiMauIn lmt on sp.IDKieuThung = lmt.Id 
                            left join TblLoaiMauIn lmi on sp.IDLoaiMauIn = lmi.Id 
                            left join TblDonViTinh dvt on sp.IDDonViTinh = dvt.Id 
                            where 1 = 1 and sp.Id= {0}
                        ", id);

            DataTable tab = new DataTable();
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblSanPham>()[0];
            return null;
        }

        public static TblSanPham GetSanPhamByCode(string code)
        {
            string sql = string.Format(@"
                          select
                          sp.*, kh.TenKhachHang, lgm.TenLoaiGiay as LoaiGiayMat,dvt.TenDonVi,
                          lgs.TenLoaiGiay as LoaiGiaySong, lgd.TenLoaiGiay as LoaiGiayDay,
                          lmi.TenMau as TenMauIn,lmt.TenMau as TenMauThung
                          from TblSanPham sp 
                            left join TblKhachHang kh on sp.IDKhachHangRieng = kh.Id 
                            left join TblLoaiGiay lgm on sp.IDLoaiGiayMat = lgm.Id 
                            left join TblLoaiGiay lgs on sp.IDLoaiGiaySong = lgs.Id 
                            left join TblLoaiGiay lgd on sp.IDLoaiGiayDay = lgd.Id 
                            left join TblLoaiMauIn lmt on sp.IDKieuThung = lmt.Id 
                            left join TblLoaiMauIn lmi on sp.IDLoaiMauIn = lmi.Id 
                            left join TblDonViTinh dvt on sp.IDDonViTinh = dvt.Id 
                            where 1 = 1 and sp.MaSanPham= '{0}'
                        ", code);

            DataTable tab = new DataTable();
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblSanPham>()[0];
            return null;
        }

        public static List<TblSanPham> SearchSanPham(int rowIndex, int pageSize, string table, string column, string tableJoin, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           table, tableJoin,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblSanPham> SanPham = sp.GetDataSet().ToList<TblSanPham>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return SanPham;
        }
        #endregion

        #region Đơn hàng
        public static TblDonHang InsertDonHang(TblDonHang dvt)
        {
            return new TblDonHangController().Insert(dvt);
        }
        public static TblDonHang UpdateDonHang(TblDonHang dvt)
        {
            return new TblDonHangController().Update(dvt);
        }
        public static bool DeleteDonHang(object id)
        {

            return new TblDonHangController().Delete(id);
        }

        public static TblDonHang GetDonHangByID(object id)
        {
            string sql = string.Format(@"
                          select
                          dh.*, kh.TenKhachHang
                          from TblDonHang dh 
                          left join TblKhachHang kh on kh.IDKhachHang = kh.Id 
                            where 1 = 1 and dh.Id= {0}
                        ", id);

            DataTable tab = new DataTable();
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblDonHang>()[0];
            return null;
        }

        public static TblDonHang GetDonHangByCode(string code)
        {
            string sql = string.Format(@"
                          select
                          dh.*, kh.TenKhachHang
                          from TblDonHang dh 
                            left join TblKhachHang kh on dh.IDKhachHang = kh.Id 
                            where 1 = 1 and dh.MaDonHang= '{0}'
                        ", code);

            DataTable tab = new DataTable();
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblDonHang>()[0];
            return null;
        }

        public static List<TblDonHang> SearchDonHang(int rowIndex, int pageSize, string table, string column, string tableJoin, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           table, tableJoin,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblDonHang> DonHang = sp.GetDataSet().ToList<TblDonHang>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return DonHang;
        }
        #endregion

        #region Sản phẩm Image
        public static TblSanPhamImage InsertSanPhamImage(TblSanPhamImage img)
        {
            return new TblSanPhamImageController().Insert(img);
        }

        public static TblSanPhamImage UpdateSanPhamImage(TblSanPhamImage img)
        {
            return new TblSanPhamImageController().Update(img);
        }

        public static bool DeleteSanPhamImage(object id)
        {
            return new TblSanPhamImageController().Delete(id);
        }

        public static TblSanPhamImage GetSanPhamImageByID(object id)
        {
            return new Select().From(TblSanPhamImage.Schema).Where(TblSanPhamImage.Columns.Id).IsEqualTo(id).ExecuteSingle<TblSanPhamImage>();
        }

        public static List<TblSanPhamImage> GetSanPhamImageBySPID(object id, string imageType = "")
        {
            Select select = new Select(
            string.Format("{0}.*", TblSanPhamImage.Schema.TableName),
            string.Format("{0}.{1}", TblImage.Schema.TableName, TblImage.Columns.ImagePath)
            );
            select.From(TblSanPhamImage.Schema);
            select.LeftOuterJoin(TblImage.Schema.TableName, TblImage.Columns.Id, TblSanPhamImage.Schema.TableName, TblSanPhamImage.Columns.Img);
            select.Where(TblSanPhamImage.Columns.IDSanPham).IsEqualTo(id);
            if (!string.IsNullOrEmpty(imageType) && imageType != "both")
                select.And(TblSanPhamImage.Columns.ImgType).IsEqualTo(imageType);
            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblSanPhamImage>();
        }
        #endregion
    }
}
