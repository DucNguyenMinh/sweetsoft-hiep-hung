﻿using SubSonic;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.HiepHung.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SweetSoft.HiepHung.Manager
{
    public class UserManager
    {
        #region PoliceUserManager

        public static TblCommonUser InsertCommonUser(TblCommonUser user)
        {
            return new TblCommonUserController().Insert(user);
        }

        public static TblCommonUser UpdateCommonUser(TblCommonUser user)
        {
            return new TblCommonUserController().Update(user);
        }

        public static string DeleteCommonUser(object id)
        {
            string result = string.Empty;
            new Delete().From(TblCommonUserInRole.Schema)
                        .Where(TblCommonUserInRole.IDUserColumn).IsEqualTo(id)
                        .Execute();
            new Delete().From(TblCommonUser.Schema)
                        .Where(TblCommonUser.IdColumn).IsEqualTo(id)
                        .Execute();



            result = "success";
            return result;
        }

        public static string DeleteNhanVien(object id)
        {
            string result = string.Empty;
            new Delete().From(TblNhanVien.Schema).Where(TblNhanVien.IdColumn).IsEqualTo(id).Execute();
            result = "success";
            return result;
        }

        public static List<TblNhanVien> GetNhanVienAll(bool? status)
        {
            Select select = new Select();
            select.From(TblNhanVien.Schema);
            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblNhanVien>();
        }



        public static TblCommonUser GetUserByUserName(object userName)
        {
            if (userName != null)
            {
                string sql = string.Format(@"
                select us.*
                from {0} as us
                where us.UserName = '{1}' ",
                    TblCommonUser.Schema.TableName,
                    userName.ToString());
                DataTable table = new DataTable();
                table.Load(new InlineQuery().ExecuteReader(sql));
                if (table != null && table.Rows.Count > 0)
                    return table.ToList<TblCommonUser>()[0];
            }
            return null;
        }

        public static TblCommonUser GetUserByEmail(object email)
        {
            if (email != null && email.ToString() != "-1")
            {
                string sql = string.Format(@"
                select us.*
                from {0} as us
                where us.Email = '{1}' ",
                    TblCommonUser.Schema.TableName,
                    email);
                DataTable table = new DataTable();
                table.Load(new InlineQuery().ExecuteReader(sql));
                if (table != null && table.Rows.Count > 0)
                    return table.ToList<TblCommonUser>()[0];
            }
            return null;
        }

        public static List<TblCommonUser> SearchUser(int rowIndex, int pageSize, string table, string tableJoin,
                                                              string column, string filterClause,
                                                              string orderClause, out int total)
        {
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           table, tableJoin,
                                                           column, filterClause, string.Empty,
                                                           orderClause, out total);
            List<TblCommonUser> user = sp.GetDataSet().ToList<TblCommonUser>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return user;
        }

        public static List<TblNhanVien> SearchNhanVien(int rowIndex, int pageSize, string table, string tableJoin,
                                                              string column, string filterClause,
                                                              string orderClause, out int total)
        {
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           table, tableJoin,
                                                           column, filterClause, string.Empty,
                                                           orderClause, out total);
            List<TblNhanVien> user = sp.GetDataSet().ToList<TblNhanVien>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return user;
        }
        #endregion

        #region Nhân Viên

        public static TblNhanVien InsertNhanVien(TblNhanVien nv)
        {
            return new TblNhanVienController().Insert(nv);
        }

        public static TblNhanVien UpdateNhanVien(TblNhanVien nv)
        {
            string update = string.Empty;
            if (!nv.NgaySinh.HasValue) update += string.Format("{0} = null,", TblNhanVien.Columns.NgaySinh);
            if (!nv.NgayVaoCongTy.HasValue) update += string.Format("{0} = null,", TblNhanVien.Columns.NgayVaoCongTy);
            if (!nv.IDUser.HasValue) update += string.Format("{0} = null,", TblNhanVien.Columns.IDUser);
            update = update.Trim();
            if (update.EndsWith(",")) update = update.Substring(0, update.Length - 1);
            if (!string.IsNullOrEmpty(update))
                new InlineQuery().Execute(string.Format("update {0} set {1} where {2} = {3}", TblNhanVien.Schema.TableName, update, TblNhanVien.Columns.Id, nv.Id));
            return new TblNhanVienController().Update(nv);
        }

        public static Select GetNhanVienSelect()
        {
            Select select = new Select(
            string.Format("{0}.*", TblNhanVien.Schema.TableName),
            string.Format("{0}.{1}", TblCommonUser.Schema.TableName, TblCommonUser.Columns.UserName),
            string.Format("{0}.{1}", TblCommonUser.Schema.TableName, TblCommonUser.Columns.Password),
            string.Format("{0}.{1} as IDUser", TblCommonUser.Schema.TableName, TblCommonUser.Columns.Id)
            );
            select.From(TblNhanVien.Schema);
            select.LeftOuterJoin(TblCommonUser.Schema.TableName, TblCommonUser.Columns.Id, TblNhanVien.Schema.TableName, TblNhanVien.Columns.IDUser);
            select.Where(TblNhanVien.IdColumn).IsNotNull();
            return select;
        }

        public static TblNhanVien GetNhanVienByID(object id)
        {
            Select select = GetNhanVienSelect();
            select.And(TblNhanVien.IdColumn).IsEqualTo(id);
            DataSet ds = select.ExecuteDataSet();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return ds.ToList<TblNhanVien>()[0];
            return null;
        }

        public static TblNhanVien GetNhanVienByUserName(string userName)
        {
            Select select = GetNhanVienSelect();
            select.And(TblCommonUser.UserNameColumn).IsEqualTo(userName);
            DataSet ds = select.ExecuteDataSet();
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return ds.ToList<TblNhanVien>()[0];
            return null;
        }

        public static TblCommonUser GetUserByID(object id)
        {
            return new Select().From(TblCommonUser.Schema).Where(TblCommonUser.IdColumn).IsEqualTo(id).ExecuteSingle<TblCommonUser>();
        }

        #endregion
    }
}