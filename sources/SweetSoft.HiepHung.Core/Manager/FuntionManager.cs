﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class FuntionManager
    {
        public static List<TblCommonFunction> GetAllFunction()
        {
            return new Select().From(TblCommonFunction.Schema)
                               .Where(TblCommonFunction.IsActiveColumn).IsEqualTo(true)
                               .OrderAsc(TblCommonFunction.Columns.DisplayOrder)
                               .ExecuteTypedList<TblCommonFunction>();
        }

        public static List<TblCommonFunction> GetUserFunction(object userId)
        {
            Select select = new Select();
            select.From(TblCommonFunction.Schema);
            return select.ExecuteTypedList<TblCommonFunction>();
        }
    }

    public class FunctionSystem
    {
        public struct Code
        {
            public static string Base_Code = "SweetSoft_HiepHung";
            public static string Trang_Chu = "trang_chu";
            public static string QLNhanVien = "nhan_vien";
            public static string User_Role = "vai_tro";
            public static string QLPhongBan = "phong_ban";
            public static string Cat_Don_Vi_Tinh = "don_vi_tinh";
            public static string Cat_Cong_Viec = "cong_viec";
            public static string Cat_May_Moc = "may_moc";
            public static string Cat_Loai_Giay = "loai_giay";
            public static string Cat_Loai_Mau_In = "loai_mau_in";
            public static string Cat_Loai_Thung = "loai_thung";
            public static string Cat_Nguyen_Lieu = "dm_nguyenlieu";
            public static string Cat_KhachHang = "khach_hang";
            public static string Cat_SanPham = "dm_sanpham";
            public static string NguyenLieuVatTu = "nguyen_lieu";
            public static string DonHang = "don_hang";
        }

        public struct Name
        {
            public static string Base_Name = "Quản lý sản xuất Hiệp Hưng";
            public static string Trang_Chu = "Trang điều khiển";
            public static string QLNhanVien = "Quản lý nhân viên";
            public static string User_Role = "Quản lý vai trò";
            public static string QLPhongBan = "Quản lý phòng ban / kho";
            public static string Cat_Don_Vi_Tinh = "Danh mục đơn vị tính";
            public static string Cat_Cong_Viec = "Danh sách công việc";
            public static string Cat_May_Moc = "Danh sách máy móc";
            public static string Cat_Loai_Giay = "Danh mục loại giấy";
            public static string Cat_Loai_Mau_In = "Danh mục loại mẫu in";
            public static string Cat_Loai_Thung = "Danh mục loại thùng";
            public static string Cat_Nguyen_Lieu = "Danh mục nguyên liệu";
            public static string Cat_KhachHang = "Quản lý khách hàng";
            public static string NguyenLieuVatTu = "Nguyên Liệu Vật Tư";
            public static string Cat_SanPham = "Danh mục sản phẩm";
            public static string DonHang = "Quản lý đơn hàng";
        }
    }
}
