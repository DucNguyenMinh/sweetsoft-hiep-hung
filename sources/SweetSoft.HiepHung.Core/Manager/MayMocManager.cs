﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class MayMocManager
    {
        #region Máy móc
        public static TblMayMoc InsertMayMoc(TblMayMoc dvt)
        {
            return new TblMayMocController().Insert(dvt);
        }
        public static TblMayMoc UpdateMayMoc(TblMayMoc dvt)
        {
            return new TblMayMocController().Update(dvt);
        }
        public static bool DeleteMayMoc(object id)
        {

            return new TblMayMocController().Delete(id);
        }

        public static TblMayMoc GetMayMocByID(object id)
        {
            return new Select().From(TblMayMoc.Schema).Where(TblMayMoc.Columns.Id).IsEqualTo(id).ExecuteSingle<TblMayMoc>();
        }

        public static List<TblMayMoc> GetMayMocALL(bool? status)
        {
            Select select = new Select();
            select.From(TblMayMoc.Schema);
            select.Where(TblMayMoc.Columns.Id).IsNotNull();
            if (status.HasValue && status.Value)
                select.And(TblMayMoc.Columns.TinhTrangSuDung).IsEqualTo("DangSuDung");
            return select.ExecuteTypedList<TblMayMoc>();
        }



        public static List<TblMayMoc> SearchMayMoc(int rowIndex, int pageSize, string table, string column, string tableJoin, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           table, tableJoin,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblMayMoc> MayMoc = sp.GetDataSet().ToList<TblMayMoc>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return MayMoc;
        }
        #endregion


        #region Máy móc công việc
        public static TblMayMocCongViec InsertMayMocCongViec(TblMayMocCongViec dvt)
        {
            return new TblMayMocCongViecController().Insert(dvt);
        }
        public static TblMayMocCongViec UpdateMayMocCongViec(TblMayMocCongViec dvt)
        {
            return new TblMayMocCongViecController().Update(dvt);
        }
        public static bool DeleteMayMocCongViec(object id)
        {

            return new TblMayMocCongViecController().Delete(id);
        }

        public static TblMayMocCongViec GetMayMocCongViecByID(object id)
        {
            return new Select().From(TblMayMocCongViec.Schema).Where(TblMayMocCongViec.Columns.Id).IsEqualTo(id).ExecuteSingle<TblMayMocCongViec>();
        }

        public static List<TblMayMocCongViec> GetMayMocCongViecByMayMocID(object mayMocId)
        {
            Select select = new Select();
            select.From(TblMayMocCongViec.Schema);
            select.Where(TblMayMocCongViec.Columns.IDMayMoc).IsEqualTo(mayMocId);
            return select.ExecuteTypedList<TblMayMocCongViec>();
        }

        public static TblMayMocCongViec GetMayMocCongViec(object idMayMoc, object idCongViec)
        {
            Select select = new Select();
            select.From(TblMayMocCongViec.Schema);
            select.Where(TblMayMocCongViec.IDCongViecColumn).IsEqualTo(idCongViec);
            select.And(TblMayMocCongViec.IDMayMocColumn).IsEqualTo(idMayMoc);
            return select.ExecuteSingle<TblMayMocCongViec>();
        }


        public static List<TblCongViec> GetCongViecByMayMocID(object mayMocId)
        {
            Select select = new Select(
                string.Format("{0}.*", TblCongViec.Schema.TableName)
                );
            select.From(TblCongViec.Schema);
            select.LeftOuterJoin(TblMayMocCongViec.Schema.TableName, TblMayMocCongViec.Columns.IDCongViec, TblCongViec.Schema.TableName, TblCongViec.Columns.Id);
            select.Where(TblMayMocCongViec.IDMayMocColumn).IsEqualTo(mayMocId);
            DataSet ds = select.ExecuteDataSet();
            return ds.ToList<TblCongViec>();

        }
        #endregion
    }
}
