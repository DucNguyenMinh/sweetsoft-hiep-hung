﻿using SweetSoft.HiepHung.Core.Helper;
using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SubSonic;
using System.Data;
using SweetSoft.HiepHung.Core;


namespace SweetSoft.HiepHung.Manager
{
    public class LogManager
    {
        public static TblCommonLogging AddNewLog(TblCommonLogging log)
        {
            return new TblCommonLoggingController().Insert(log);
        }

        public static TblCommonLogging GetLogByID(string id)
        {
            return new Select().From(TblCommonLogging.Schema)
                               .Where(TblCommonLogging.IdColumn)
                               .IsEqualTo(id)
                               .ExecuteSingle<TblCommonLogging>();
        }


        public static List<TblCommonLogging> SearchLog(int rowIndex, int pageSize,
                                                         string column, string filterClause,
                                                         string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize, TblCommonLogging.Schema.TableName,
                                                        string.Empty, column, filterClause, string.Empty,
                                                        orderClause, out total);
            DataSet ds = sp.GetDataSet();
            List<TblCommonLogging> log = ds.ToList<TblCommonLogging>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return log;
        }


        public static bool DeleteLog(object id)
        {
            return new TblCommonLoggingController().Delete(id);
        }


    }

    public class LogItem
    {
        public object Code { get; set; }
        public object FValue { get; set; }
        public object LValue { get; set; }
        public string TableName { get; set; }
        public string PropertiesName { get; set; }
        public bool IsKey { get; set; }

        public bool Active
        {
            get
            {
                return IsKey || (FValue != null && !string.IsNullOrEmpty(FValue.ToString()) && LValue == null)
                             || (LValue != null && FValue != null &&
                                 LValue.ToString().Trim() != FValue.ToString().Trim());
            }
        }

        public LogItem(object fValue, object lValue)
        {
            if (fValue != null)
                FValue = fValue.ToString();
            if (lValue != null)
                LValue = lValue.ToString();
            IsKey = false;
        }

        public LogItem(object fValue, object lValue, string tableName, string propertiesName)
        {
            if (fValue != null)
                FValue = fValue.ToString();
            if (lValue != null)
                LValue = lValue.ToString();
            TableName = tableName;
            PropertiesName = propertiesName;
            IsKey = false;
        }

        public LogItem(object fValue, object lValue, string tableName, string propertiesName, bool isKey)
        {
            if (fValue != null)
                FValue = fValue.ToString();
            if (lValue != null)
                LValue = lValue.ToString();
            TableName = tableName;
            PropertiesName = propertiesName;
            IsKey = isKey;
        }

        public string GetLog()
        {
            return GetLog(true);
        }

        public string GetLog(bool _renew)
        {
            string result = string.Empty;
            if (Active)
                result = string.Format("{0} : {1} {2}",
                                        PropertiesName, FValue.ToString(),
                                        LValue != null &&
                                        LValue.ToString().Trim() != FValue.ToString().Trim() ?
                                        string.Format(" -> {0}", LValue.ToString()) : string.Empty);
            if (_renew && LValue != null)
            {
                FValue = LValue;
                LValue = null;
            }
            return result;
        }
    }

    public class LogFunctionItem
    {
        /// <summary>
        /// Mã chức năng
        /// </summary>
        public string RightFunctionCode { get; set; }
        public string FunctionCode { get; set; }

        /// <summary>
        /// Tiêu đề log
        /// </summary>
        public string Title { get; set; }
        public string UserName
        {
            get
            {
                return
                       ApplicationContext.Current.CommonUser.UserName;
            }
        }
        public DateTime? ActionDate { get; set; }
        public string UserIP
        {
            get
            {
                return ApplicationContext.Current.CurrentUserIp;
            }
        }
        public bool IsSystem { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsHotel { get; set; }
        public int Count
        {
            get
            {
                return logDescriptionItem.Count;
            }
        }
        private Dictionary<string, LogItem> logDescriptionItem = new Dictionary<string, LogItem>();

        public LogFunctionItem(string rFunctionCode, string title, string functionCode)
        {
            RightFunctionCode = rFunctionCode; Title = title; FunctionCode = functionCode;
        }

        public LogFunctionItem()
        { }

        public void AddLastValue(string key, string value)
        {
            if (logDescriptionItem.ContainsKey(key.Trim()))
                logDescriptionItem[key].LValue = value;
            else
                logDescriptionItem.Add(key, new LogItem(null, value));
        }

        public void AddFirstValue(string tableName, string propertiesName, string key, string value)
        {
            AddFirstValue(tableName, propertiesName, key, value, false);
        }

        public void AddFirstValue(string tableName, string propertiesName, string key,
                                  string value, bool primaryKey)
        {
            if (logDescriptionItem.ContainsKey(key.Trim()))
                logDescriptionItem[key].FValue = value;
            else
                logDescriptionItem.Add(key, new LogItem(value, null, tableName, propertiesName, primaryKey));
        }

        public LogItem GetLogByKey(string key)
        {
            return logDescriptionItem.ContainsKey(key) ? logDescriptionItem[key] : null;
        }

        public string GetLog()
        {
            string result = string.Empty;
            int total = 0;
            foreach (KeyValuePair<string, LogItem> item in logDescriptionItem)
            {
                if (item.Value.Active)
                {
                    total += 1;
                    result += string.Format("<li>{0}</li>", item.Value.GetLog());
                }
            }

            if (total > 0)
                result = "<ul>" + result + "</ul>";
            return result;
        }

        public void SaveLog()
        {
            if (ApplicationContext.Current.CommonUser != null)
            {
                TblCommonLogging log = new TblCommonLogging();
                log.Ip = UserIP;
                log.UserName = UserName;
                log.FunctionCode = FunctionCode;
                log.CreatedDate = DateTime.Now;
                log.LogContent = GetLog();
                log.LogTitle = Title;
                log.IsAdmin = IsAdmin;
                log.IsSystem = IsSystem;
                log.IsHotel = IsHotel;

                // log.IDUser = ApplicationContext.Current.User.Id;
                LogManager.AddNewLog(log);
            }
        }

        public void Clear()
        {
            logDescriptionItem.Clear();
        }
    }



    public class LogPage
    {
        private List<LogFunctionItem> logItem = new List<LogFunctionItem>();
        public LogPage()
        {

        }

        public int ID { get; set; }
        public string UserIP { get; set; }
        public string Description { get; set; }
        public string UserName { get; set; }
        public DateTime? ActionDate { get; set; }
        public string FunctionCode { get; set; }
        public string RightFunctionCode { get; set; }
        public string FunctionName { get; set; }
        public string RightName { get; set; }
        public bool IsSystem { get; set; }
        public bool IsAdmin
        {
            get
            {
                return ApplicationContext.Current.CommonUser != null &&
                       ApplicationContext.Current.CommonUser.IsAdmin.HasValue &&
                       ApplicationContext.Current.CommonUser.IsAdmin.Value;
            }
        }


        public LogFunctionItem AddNewLogFunction(string rightFunctionCode, string title)
        {
            LogFunctionItem item = logItem.FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
            if (item == null)
            {
                item = new LogFunctionItem(rightFunctionCode, title, FunctionCode);
                logItem.Add(item);
            }
            else
                item.Title = title;
            return item;
        }
        /// <summary>
        /// Lấy log bằng key
        /// </summary>
        /// <param name="rightFunctionCode"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public LogItem GetLogByKey(string rightFunctionCode, string key)
        {
            LogFunctionItem item = logItem.FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
            if (item != null)
                return item.GetLogByKey(key);
            return null;

        }
        /// <summary>
        /// Hàm thêm log đầu vào
        /// </summary>
        /// <param name="rightFunctionCode">Tên thao tác log</param>
        /// <param name="title">Tiêu đề thao tác log</param>
        /// <param name="tableName">Tên bảng ghi log log</param>
        /// <param name="propertiesName">Tên thuộc tính ghi log</param>
        /// <param name="key">Thuộc tính</param>
        /// <param name="value">Giá trị</param>
        public void AddFirstValue(string rightFunctionCode, string propertiesName, string key, string value)
        {
            AddFirstValue(rightFunctionCode, propertiesName, key, value, false);
        }
        /// <summary>
        /// Hàm thêm log đầu vào
        /// </summary>
        /// <param name="rightFunctionCode">Tên thao tác log</param>
        /// <param name="title">Tiêu đề thao tác log</param>
        /// <param name="tableName">Tên bảng ghi log log</param>
        /// <param name="propertiesName">Tên thuộc tính ghi log</param>
        /// <param name="key">Thuộc tính</param>
        /// <param name="value">Giá trị</param>
        /// <param name="primarykey">Thuộc tính khóa, luôn đưa vào log</param>
        public void AddFirstValue(string rightFunctionCode, string propertiesName, string key,
                                  string value, bool primarykey)
        {
            LogFunctionItem item = logItem.FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
            if (item == null)
                item = AddNewLogFunction(rightFunctionCode, "");
            item.AddFirstValue("", propertiesName, key, value, primarykey);
        }

        public void AddLastValue(string rightFunctionCode, string key, string value)
        {
            LogFunctionItem item = logItem.FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
            if (item != null)
                item.AddLastValue(key, value);
        }

        public string GetLog()
        {
            string result = string.Empty;
            foreach (LogFunctionItem item in logItem)
                result += item.GetLog();
            return result;
        }

        public void Clear() { logItem.Clear(); }
        public void Clear(string rightFunctionCode)
        {
            LogFunctionItem item = logItem.FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
            if (item != null)
                logItem.Remove(item);
        }
        /// <summary>
        /// Lưu toàn bộ log
        /// </summary>
        public void SaveLog()
        {
            SaveLog("all");
        }

        public void SaveLog(string rightFunctionCode)
        {
            if (!string.IsNullOrEmpty(rightFunctionCode))
            {
                if (rightFunctionCode == "all")
                {
                    foreach (LogFunctionItem item in logItem)
                    {
                        item.IsSystem = IsSystem;
                        item.IsAdmin = IsAdmin;

                        item.SaveLog();
                    }
                }
                else
                {
                    LogFunctionItem item = logItem
                                          .FirstOrDefault(p => p.RightFunctionCode == rightFunctionCode);
                    if (item != null)
                    {
                        item.IsSystem = IsSystem;
                        item.IsAdmin = IsAdmin;

                        item.SaveLog();
                    }
                }
            }
        }

    }
}
