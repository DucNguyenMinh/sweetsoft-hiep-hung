﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SubSonic;
using SweetSoft.HiepHung.DataAccess;

namespace SweetSoft.HiepHung.Core.Manager
{
    public class NguyenLieuManager
    {
        #region Nguyên liệu danh mục
        public static TblNguyenLieu InsertNguyenLieu(TblNguyenLieu dvt)
        {
            return new TblNguyenLieuController().Insert(dvt);
        }
        public static TblNguyenLieu UpdateNguyenLieu(TblNguyenLieu dvt)
        {
            return new TblNguyenLieuController().Update(dvt);
        }
        public static bool DeleteNguyenLieu(object id)
        {
            return new TblNguyenLieuController().Delete(id);
        }


        public static List<TblNguyenLieu> GetNguyenLieuAll(bool? status)
        {
            DataTable tab = new DataTable();
            string sql = string.Format(@"
                    select nl.*,dvt.TenDonVi,dvt.TenVietTat,dvt2.TenDonVi as TenDonViLuuKho,dvt2.TenVietTat as TenVietTatLuuKho
                    from 
                    TblNguyenLieu nl
                    left join TblDonViTinh dvt on dvt.Id = nl.IDDonViTinh 
                    left join TblDonViTinh dvt2 on dvt.Id = nl.IDDonViLuuKho
                    where 1 = 1
                ");
            if (status.HasValue)
                sql += string.Format(" and nl.{0} = {1} ", TblNguyenLieu.Columns.TrangThaiSuDung, status.HasValue && status.Value ? "1" : "0");
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblNguyenLieu>();
            return null;
        }

        public static TblNguyenLieu GetNguyenLieuByID(object id)
        {
            DataTable tab = new DataTable();
            string sql = string.Format(@"
                    select nl.*,dvt.TenDonVi,dvt.TenVietTat,dvt2.TenDonVi as TenDonViLuuKho,dvt2.TenVietTat as TenVietTatLuuKho
                    from 
                    TblNguyenLieu nl
                    left join TblDonViTinh dvt on dvt.Id = nl.IDDonViTinh 
                    left join TblDonViTinh dvt2 on dvt.Id = nl.IDDonViLuuKho
                    where 1 = 1 and nl.Id = {0} 
                ", id);

            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblNguyenLieu>()[0];
            return null;
        }

        public static List<TblNguyenLieu> SearchNguyenLieu(int rowIndex, int pageSize, string column, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                          string.Format(" {0} as nl ", TblNguyenLieu.Schema.TableName),
                                                          string.Format(" left join {0} as dvt on nl.{1} = dvt.Id  left join {0} as dvt2 on nl.{2} = dvt2.Id left join {3} as lg on nl.{4} = lg.Id ",
                                                          TblDonViTinh.Schema.TableName, TblNguyenLieu.Columns.IDDonViTinh, TblNguyenLieu.Columns.IDDonViLuuKho,
                                                          TblLoaiGiay.Schema.TableName, TblNguyenLieu.Columns.IDLoaiGiay),
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblNguyenLieu> NguyenLieu = sp.GetDataSet().ToList<TblNguyenLieu>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return NguyenLieu;
        }
        #endregion

        #region Nguyên liệu chi tiết
        public static TblNguyenLieuChiTiet InsertNguyenLieuChiTiet(TblNguyenLieuChiTiet dvt)
        {
            return new TblNguyenLieuChiTietController().Insert(dvt);
        }
        public static TblNguyenLieuChiTiet UpdateNguyenLieuChiTiet(TblNguyenLieuChiTiet dvt)
        {
            return new TblNguyenLieuChiTietController().Update(dvt);
        }
        public static bool DeleteNguyenLieuChiTiet(object id)
        {
            return new TblNguyenLieuChiTietController().Delete(id);
        }

        private static Select GetNguyenLieuChiTietSelect()
        {
            Select select = new Select(
               string.Format("{0}.*", TblNguyenLieuChiTiet.Schema.TableName),
               string.Format("{0}.{1}", TblNguyenLieu.Schema.TableName, TblNguyenLieu.Columns.TenNguyenLieu),
               string.Format("{0}.{1}", TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.TenDonVi),
               string.Format("{0}.{1}", TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.TenVietTat),
               string.Format("{0}.{1} as TenDonViLuuKho", TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.TenDonVi),
               string.Format("{0}.{1} as TenVietTatLuuKho", TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.TenVietTat)
                );
            select.From(TblNguyenLieuChiTiet.Schema);
            select.LeftOuterJoin(TblNguyenLieu.Schema.TableName, TblNguyenLieu.Columns.Id, TblNguyenLieuChiTiet.Schema.TableName, TblNguyenLieuChiTiet.Columns.IDNguyenLieu);
            select.LeftOuterJoin(TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.Id, TblNguyenLieu.Schema.TableName, TblNguyenLieu.Columns.IDDonViTinh);
            select.LeftOuterJoin(TblDonViTinh.Schema.TableName, TblDonViTinh.Columns.Id, TblNguyenLieu.Schema.TableName, TblNguyenLieu.Columns.IDDonViLuuKho);
            return select;
        }


        public static TblNguyenLieuChiTiet GetNguyenLieuChiTietByID(object id)
        {
            DataTable tab = new DataTable();
            string sql = string.Format(@"
                    select chiTiet.*,nl.TenNguyenLieu,dvt.TenDonVi,dvt.TenVietTat,dvt2.TenDonVi as TenDonViLuuKho,dvt2.TenVietTat as TenVietTatLuuKho, kho.TenPhongBan
                    from 
                    TblNguyenLieuChiTiet chiTiet
                    left join TblNguyenLieu nl on nl.Id = chiTiet.IDNguyenLieu 
                    left join TblDonViTinh dvt on dvt.Id = nl.IDDonViTinh 
                    left join TblDonViTinh dvt2 on dvt2.Id = nl.IDDonViLuuKho 
                    left join TblPhongBan kho on kho.Id = chiTiet.IDKho
                    where 1 = 1 and chiTiet.Id = {0}
                ", id);
            tab.Load(new InlineQuery().ExecuteReader(sql));
            if (tab.Rows.Count > 0) return tab.ToList<TblNguyenLieuChiTiet>()[0];
            return null;
        }

        public static List<TblNguyenLieuChiTiet> SearchNguyenLieuChiTiet(int rowIndex, int pageSize, string column, string talbeJoin, string filterClause, string orderClause, out int total)
        {
            total = 0;
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                          string.Format(" {0} as chiTiet ", TblNguyenLieuChiTiet.Schema.TableName),
                                                          talbeJoin,
                                                           column, filterClause, string.Empty, orderClause, out total);
            List<TblNguyenLieuChiTiet> NguyenLieuChiTiet = sp.GetDataSet().ToList<TblNguyenLieuChiTiet>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return NguyenLieuChiTiet;
        }
        #endregion
    }
}
