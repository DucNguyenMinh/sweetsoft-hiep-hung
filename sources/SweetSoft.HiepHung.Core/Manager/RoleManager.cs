﻿using SubSonic;
using SweetSoft.HiepHung.DataAccess;
using SweetSoft.HiepHung.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace SweetSoft.HiepHung.Manager
{
    public class RoleManager
    {
        #region Role
        public static TblCommonRole InsertRole(TblCommonRole role)
        {
            return new TblCommonRoleController().Insert(role);
        }

        public static TblCommonRole UpdateRole(TblCommonRole role)
        {
            return new TblCommonRoleController().Update(role);
        }

        public static bool DeleteRole(object id)
        {
            return new TblCommonRoleController().Delete(id);
        }

        public static TblCommonRole GetRoleByID(object id)
        {
            return new Select().From(TblCommonRole.Schema)
                               .Where(TblCommonRole.IdColumn).IsEqualTo(id)
                               .ExecuteSingle<TblCommonRole>();
        }

        public static List<TblCommonRole> SearchRole(int rowIndex, int pageSize,
                                                       string column, string filterClause,
                                                       string orderClause, out int total)
        {
            StoredProcedure sp = SPs.SearchAndPaging(rowIndex, pageSize,
                                                           string.Format(" {0} as ro ", TblCommonRole.Schema.TableName), "",
                                                           column, filterClause, string.Empty,
                                                           orderClause, out total);
            List<TblCommonRole> role = sp.GetDataSet().ToList<TblCommonRole>();
            if (sp.OutputValues.Count > 0)
            {
                int u = -1;
                if (int.TryParse(sp.OutputValues[0].ToString(), out u))
                    total = u;
            }
            return role;
        }

        public static List<ApplicationUserRight> GetRightsOfRole(int? roleId, bool? getAllRight)
        {
            StoredProcedure sp = SweetSoft.HiepHung.DataAccess
                                .SPs.SpGetRightsOfRole(roleId, getAllRight);
            DataSet ds = sp.GetDataSet();
            List<ApplicationUserRight> role = ds.ToList<ApplicationUserRight>();
            return role;
        }

        public static List<ApplicationUserRight> GetRoleOfUser(long userId, bool? isAdmin, bool? fetchAll)
        {
            StoredProcedure sp = SweetSoft.HiepHung.DataAccess
                                .SPs.SpGetRoleOfUser(userId.ToString(),isAdmin,fetchAll);
            DataSet ds = sp.GetDataSet();
            List<ApplicationUserRight> role = ds.ToList<ApplicationUserRight>();
            return role;
        }

        public static List<ApplicationUserRight> GetRightOfUser(long? userId)
        {
            StoredProcedure sp = SweetSoft.HiepHung.DataAccess
                                .SPs.SpGetRightOfUser(userId);
            DataSet ds = sp.GetDataSet();
            List<ApplicationUserRight> role = ds.ToList<ApplicationUserRight>();
            return role;
        }

      
        #endregion

        #region UserInRole

        public static bool DeleteUserInRole(object id)
        {
            return new TblCommonUserInRoleController().Delete(id);
        }

        public static TblCommonUserInRole InsertUserInRole(TblCommonUserInRole ur)
        {
            return new TblCommonUserInRoleController().Insert(ur);
        }

     

        #endregion

        #region RightInRole
        public static TblCommonRightInRole InsertRightInRole(TblCommonRightInRole rr)
        {
            return new TblCommonRightInRoleController().Insert(rr);
        }

        public static bool DeleteRightInRole(object id)
        {
            return new TblCommonRightInRoleController().Delete(id);
        }
        #endregion

    }
    public class ApplicationUserRight
    {
        public ApplicationUserRight() { }
        public string RightCode { get; set; }
        public string RightDescription { get; set; }
        public string RightName { get; set; }
        public int? RightID { get; set; }
        public string FunctionCode { get; set; }
        public string FunctionName { get; set; }
        public int? FunctionID { get; set; }
        public Guid? UserID { get; set; }
        public long? RightInRoleID { get; set; }
    }
}
