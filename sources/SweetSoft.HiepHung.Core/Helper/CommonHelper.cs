﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Net.Mail;
using System.IO;
using SweetSoft.HiepHung.Manager;
using SweetSoft.HiepHung.Core.Helper;
using System.Globalization;

namespace SweetSoft.HiepHung.Core.Helper
{
    public class CommonHelper
    {
        public static bool IsValidEmail(string Email)
        {
            return Regex.IsMatch(Email, @"^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");
        }

        /// <summary>
        /// Gets query string value by name
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <returns>Query string value</returns>
        public static string QueryString(string Name)
        {
            string result = string.Empty;
            if (HttpContext.Current != null && HttpContext.Current.Request.QueryString[Name] != null)
                result = HttpContext.Current.Request.QueryString[Name].ToString();
            return result;
        }

        /// <summary>
        /// Gets boolean value from query string 
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <returns>Query string value</returns>
        public static bool QueryStringBool(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            return (resultStr == "YES" || resultStr == "TRUE" || resultStr == "1");
        }

        /// <summary>
        /// Gets integer value from query string 
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <returns>Query string value</returns>
        public static int QueryStringInt(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            int result = -1;
            Int32.TryParse(resultStr, out result);
            return result;
        }

        public static short QueryStringShort(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            short result = -1;
            short.TryParse(resultStr, out result);
            return result;
        }
        /// <summary>
        /// Gets integer value from query string 
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <param name="DefaultValue">Default value</param>
        /// <returns>Query string value</returns>
        public static int QueryStringInt(string queryName, int defaultValue)
        {
            string resultStr = QueryString(queryName).ToUpperInvariant();
            if (resultStr.Length > 0)
            {
                return Int32.Parse(resultStr);
            }
            return defaultValue;
        }

        public static byte QueryStringByte(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            byte result = byte.MaxValue;
            if (!string.IsNullOrEmpty(resultStr))
                result = byte.Parse(resultStr);
            return result;
        }

        /// <summary>
        /// Gets integer value from query string 
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <returns>Query string value</returns>
        public static long QueryStringLong(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            long result;
            Int64.TryParse(resultStr, out result);
            return result;
        }

        /// <summary>
        /// Gets GUID value from query string 
        /// </summary>
        /// <param name="Name">Parameter name</param>
        /// <returns>Query string value</returns>
        public static Guid? QueryStringGUID(string Name)
        {
            string resultStr = QueryString(Name).ToUpperInvariant();
            Guid? result = null;
            try
            {
                result = new Guid(resultStr);
            }
            catch
            {
            }
            return result;
        }

        public static bool SendEmailSystem(string subject, string content, string emailToAccount)
        {
            return false;
            ////string emailSenderName = SettingManager.GetSettingValue(SettingCode.Email_SenderName);
            ////if (string.IsNullOrEmpty(emailSenderName)) emailSenderName = "SweetSoft";

            ////string emailSenderAccount = SettingManager.GetSettingValue(SettingCode.Email_SenderAccount);
            ////if (string.IsNullOrEmpty(emailSenderAccount)) emailSenderAccount = "mailer@sweetsoft.vn";

            ////string emailSenderPassword = SettingManager.GetSettingValue(SettingCode.Email_SenderPassword);
            ////if (string.IsNullOrEmpty(emailSenderPassword)) emailSenderPassword = "sweet2012";
            ////else emailSenderPassword = SecurityHelper.Decrypt128(emailSenderPassword);

            ////string smtpHost = SettingManager.GetSettingValue(SettingCode.Email_SMTPHost);
            ////if (string.IsNullOrEmpty(smtpHost)) smtpHost = "mail.sweetsoft.vn";

            ////string port = SettingManager.GetSettingValue(SettingCode.Email_SMTPPort);
            ////int smtpPort = 587;
            ////int.TryParse(port, out smtpPort);

            ////bool useSSL = false;
            ////string ssl = SettingManager.GetSettingValue(SettingCode.Email_SSL);
            ////bool.TryParse(ssl, out useSSL);

            //return SendEmail(emailSenderName, emailSenderAccount, emailSenderPassword, emailToAccount, false, useSSL, smtpHost, smtpPort, subject, content, true, string.Empty);
        }

        public static bool SendEmailError(string errorMessage, Exception ex)
        {
            string emailSenderName = "SweetSoft";

            string emailSenderAccount = "mailer@sweetsoft.vn";

            string emailSenderPassword = "sweet2012";

            string smtpHost = "mail.sweetsoft.vn";

            int smtpPort = 587;

            string subject = string.Format("- Lỗi hệ thống [QLTTLT]: {0}",
               errorMessage).Replace("\t", " ").Replace("\r", "").Replace("\n", " ");

            StringBuilder strMessage = new StringBuilder();
            strMessage.AppendLine(string.Format("Có lỗi trên hệ thống [QLTTLT]: {0}", errorMessage));
            strMessage.AppendLine();
            strMessage.AppendLine("-------------------Chi tiết lỗi-------------------");
            strMessage.AppendLine();
            strMessage.AppendLine(string.Format("Tên chương trình: {0}", SecurityHelper.APPLICATION_NAME));
            strMessage.AppendLine(string.Format("Người dùng: {0}", ApplicationContext.Current.CommonUser.UserName));
            strMessage.AppendLine(string.Format("Địa chỉ IP: {0}", ApplicationContext.Current.CurrentUserIp));
            strMessage.AppendLine(string.Format("Ngày ghi nhận: {0}", DateTime.Now.ToString()));
            strMessage.AppendLine(string.Format("Địa chỉ web: {0}", ApplicationContext.Current.CurrentUri.AbsoluteUri));
            strMessage.AppendLine(string.Format("Chi tiết lỗi: {0}", errorMessage));
            strMessage.AppendLine("STACK TRACE:");
            strMessage.AppendLine(ex.StackTrace);

            bool result = false;
            string[] strArr = "duc.nguyen@sweetsoft.com".Split(new string[] { ";" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string str in strArr)
                result |= SendEmail(emailSenderName, emailSenderAccount, emailSenderPassword, str, false, false, smtpHost, smtpPort, subject, strMessage.ToString(), true, string.Empty);
            return result;
        }

        /// <summary>
        /// Hàm Send Email
        /// </summary>
        /// <param name="emailSenderName">Tên người gửi</param>
        /// <param name="emailSenderAccount">Tài khoản người gửi</param>
        /// <param name="emailSenderPassword">Mật khẩu tài khoản người gửi</param>
        /// <param name="emailToAccount">Tài khoản người nhận</param>
        /// <param name="useDefaultCredentials">Đánh dấu (false)</param>
        /// <param name="useSSL">Sử dụng SSL (false)</param>
        /// <param name="smtpHost">Địa chỉ server SMTP (gmail.smtp.com)</param>
        /// <param name="smtpPort">Cổng SMTP (587)</param>
        /// <param name="subject">Tiêu đề email</param>
        /// <param name="bodyMessage">Nội dung email</param>
        /// <param name="isHtml">Đánh dấu có sử dụng thẻ HMTL</param>
        /// <param name="attachmentPath">đường dẫn file attactment</param>
        /// <returns></returns>
        public static bool SendEmail(string emailSenderName, string emailSenderAccount, string emailSenderPassword, string emailToAccount, bool useDefaultCredentials, bool useSSL, string smtpHost, int smtpPort, string subject, string bodyMessage, bool isHtml, string attachmentPath)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage message = new MailMessage();
                MailAddress fromAddress = new MailAddress(emailSenderAccount, emailSenderName);

                System.Net.NetworkCredential credential = new System.Net.NetworkCredential(
                                emailSenderAccount,
                                emailSenderPassword);
                smtpClient.UseDefaultCredentials = useDefaultCredentials;
                smtpClient.EnableSsl = useSSL;
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.Credentials = credential;
                smtpClient.Host = smtpHost;
                smtpClient.Port = smtpPort; //587

                message.From = fromAddress;
                message.To.Add(emailToAccount);
                message.ReplyTo = new MailAddress(emailSenderAccount, emailSenderName);
                message.Subject = subject;
                message.IsBodyHtml = isHtml;
                message.Body = bodyMessage;

                if (isHtml)
                {
                    AlternateView htmlView = AlternateView.CreateAlternateViewFromString(message.Body, new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Text.Html));

                    htmlView.TransferEncoding = System.Net.Mime.TransferEncoding.SevenBit;
                    message.AlternateViews.Add(htmlView);
                }


                //attach file
                bool isExists = false;
                if (!string.IsNullOrEmpty(attachmentPath))
                {
                    isExists = File.Exists(attachmentPath);
                    if (isExists)
                    {
                        Attachment attach = new Attachment(attachmentPath);
                        message.Attachments.Add(attach);
                    }
                }

                // Send SMTP mail
                smtpClient.Send(message);

                //delete file if it exists
                if (isExists)
                {
                    message.Dispose();
                    if (!FileIsOpen(attachmentPath))
                        File.Delete(attachmentPath);
                }
                return true;
            }
            catch (Exception ex) { }
            return false;
        }

        /// <summary>
        /// Check file is opened, return true if file is opened 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static bool FileIsOpen(string filePath)
        {
            bool results = false;
            try
            {
                using (FileStream stream = new FileStream(filePath, FileMode.Open, FileAccess.ReadWrite, FileShare.None))
                {
                    try
                    {
                        stream.ReadByte();
                    }
                    catch (IOException)
                    {
                        results = true;
                    }
                    finally
                    {
                        stream.Close();
                        stream.Dispose();
                    }
                }
            }
            catch (IOException)
            {
                results = true;  //file is opened at another location
            }

            return results;
        }

        /// <summary>
        /// Status of object in Project
        /// </summary>
        /// <param name="objStatus"></param>
        /// <returns></returns>

        public static string GetFullApplicationPath()
        {
            return HttpContext.Current.Request.Url.Scheme + "://" + HttpContext.Current.Request.Url.Authority + HttpContext.Current.Request.ApplicationPath;
        }

        public static string GetThuNgay(DateTime date)
        {
            string result = string.Empty;
            switch (date.DayOfWeek)
            {
                case DayOfWeek.Monday:
                    result = "Thứ hai";
                    break;
                case DayOfWeek.Tuesday:
                    result = "Thứ ba";
                    break;
                case DayOfWeek.Wednesday:
                    result = "Thứ tư";
                    break;
                case DayOfWeek.Thursday:
                    result = "Thứ năm";
                    break;
                case DayOfWeek.Friday:
                    result = "Thứ sáu";
                    break;
                case DayOfWeek.Saturday:
                    result = "Thứ bảy";
                    break;
                case DayOfWeek.Sunday:
                    result = "Chủ nhật";
                    break;
            }
            return result;
        }

        public static decimal ParseDecimal(string value)
        {

            decimal vl = 0;
            decimal rs = -1;
            if (decimal.TryParse(value, NumberStyles.Any, new CultureInfo("vi-VN"), out rs)) vl = rs;
            return vl;
        }

        public static decimal ParseDecimal(string value, string groupSeparator = ".", string separator = ",")
        {
            decimal vl = 0;
            decimal rs = -1;
            if (decimal.TryParse(value.Replace(separator, groupSeparator), NumberStyles.Any, new CultureInfo("vi-VN"), out rs)) vl = rs;
            return vl;
        }

        public static string ToRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ToRoman(number - 1000);
            if (number >= 900) return "CM" + ToRoman(number - 900);
            if (number >= 500) return "D" + ToRoman(number - 500);
            if (number >= 400) return "CD" + ToRoman(number - 400);
            if (number >= 100) return "C" + ToRoman(number - 100);
            if (number >= 90) return "XC" + ToRoman(number - 90);
            if (number >= 50) return "L" + ToRoman(number - 50);
            if (number >= 40) return "XL" + ToRoman(number - 40);
            if (number >= 10) return "X" + ToRoman(number - 10);
            if (number >= 9) return "IX" + ToRoman(number - 9);
            if (number >= 5) return "V" + ToRoman(number - 5);
            if (number >= 4) return "IV" + ToRoman(number - 4);
            if (number >= 1) return "I" + ToRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }
    }



}
