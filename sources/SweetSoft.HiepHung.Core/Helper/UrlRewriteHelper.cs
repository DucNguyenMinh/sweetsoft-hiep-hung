﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Text.RegularExpressions;
using SweetSoft.HiepHung.Core.Manager;

namespace SweetSoft.HiepHung.Core.Helper
{
    public class UrlRewriteHelper
    {
        private static string ReplaceUnicodeCharactersByRegex(string unicodeValue)
        {
            Regex regex = new Regex("\\p{IsCombiningDiacriticalMarks}+");
            string temp = unicodeValue.Normalize(NormalizationForm.FormD);
            return regex.Replace(temp, String.Empty).Replace('\u0111', 'd').Replace('\u0110', 'D');
        }

        public static string ConvertToSEOFriendly(string title, int maxLength, bool noneUnicode)
        {
            string seoTitle = title;
            if (noneUnicode)
                seoTitle = ReplaceUnicodeCharactersByRegex(title);
            var match = Regex.Match(seoTitle.ToLower(), "[\\w]+");
            StringBuilder result = new StringBuilder("");
            bool maxLengthHit = false;
            while (match.Success && !maxLengthHit)
            {
                if (result.Length + match.Value.Length <= maxLength)
                {
                    result.Append(match.Value + "-");
                }
                else
                {
                    maxLengthHit = true;
                    // Handle a situation where there is only one word and it is greater than the max length.
                    if (result.Length == 0) result.Append(match.Value.Substring(0, maxLength));
                }
                match = match.NextMatch();
            }
            // Remove trailing '-'
            if (result[result.Length - 1] == '-') result.Remove(result.Length - 1, 1);
            return result.ToString();
        }

    }
}
