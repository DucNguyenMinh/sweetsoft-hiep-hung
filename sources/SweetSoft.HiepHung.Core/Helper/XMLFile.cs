﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;

namespace SweetSoft.HiepHung.Core.Helper.XMLDefinition
{
    /// <summary>
    /// Summary description for XMLFile
    /// </summary>
    public class XMLFile
    {
        public XMLFile()
        {
            //
            // TODO: Add constructor logic here
            //
        }
        public struct Columns
        {
            public static string Name = "Name";
            public static string Id = "Id";
        }

        public string Name { get; set; }
        public string Id { get; set; }


        public virtual string FileName
        {
            get
            {
                return "";
            }
        }


        protected internal static XmlNode FetchById(string strFileName, string columnName, string value)
        {
            if (string.IsNullOrEmpty(strFileName) == false)
            {
                if (Path.GetExtension(strFileName).Length == 0)
                    strFileName = strFileName + ".xml";
                string fullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/app_data/"), strFileName);
                if (File.Exists(fullPath))
                {
                    XmlHelper xdoc = new XmlHelper(fullPath, XmlHelper.LoadType.FromLocalFile);
                    if (xdoc.Document != null)
                    {
                        try
                        {
                            return xdoc.Document.DocumentElement.SelectSingleNode("//*[@" + columnName + "=" + value + "]");
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            return null;
        }

        protected internal XmlNode FetchById(string columnName, string value)
        {
            return FetchById(FileName, columnName, value);
        }

        protected internal static XmlNodeList FetchAll(string strFileName)
        {
            return FetchAll(strFileName, strFileName);
        }

        protected internal static XmlNodeList FetchAll(string strFileName, string nodeName)
        {
            if (string.IsNullOrEmpty(strFileName) == false)
            {
                if (Path.GetExtension(strFileName).Length == 0)
                    strFileName = strFileName + ".xml";
                string fullPath = Path.Combine(HttpContext.Current.Server.MapPath("~/app_data/"), strFileName);
                if (File.Exists(fullPath))
                {
                    XmlHelper xdoc = new XmlHelper(fullPath, XmlHelper.LoadType.FromLocalFile);
                    if (xdoc.Document != null)
                    {
                        try
                        {
                            return xdoc.Document.DocumentElement.SelectNodes("//" + nodeName);
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
            return null;
        }

    }
}