﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace SweetSoft.HiepHung.Core.Helper
{
    public class SecurityHelper
    {
        public const string APPLICATION_NAME = "DTNT";
        public const string ACCESS_DENIED = "ACCESS DENIED!!!";
        public const string ROLES_OF_FUNCTION_CACHE_KEY = "ROLES_OF_FUNCTION_CACHE_KEY";
        public static string BCA_EncryptionPrivateKey = "j+zqNUpaAm/Psqz0o77Gyg==";
        public static string BCA_EncryptionLoginKey = "";
        // 128bit(16byte)IV and Key
        private const string AesIV = @"!QAZ2WSX#EDC4RFV";
        private const string AesKey = @"5TGB&YHN7UJM(IK<";
        //ducnm fix 25.11
        public static string CONTAINER_PLACE = "container";
        //end fix
        /// <summary>
        /// Decrypts text
        /// </summary>
        /// <param name="CipherText">Cipher text</param>
        /// <returns>Decrypted string</returns>
        public static string Decrypt(string cipherText)
        {
            return Decrypt(cipherText, BCA_EncryptionPrivateKey);
        }

        public static string Decrypt(string cipherText, string key)
        {
            if (String.IsNullOrEmpty(cipherText))
                return cipherText;

            TripleDESCryptoServiceProvider tDESalg = new TripleDESCryptoServiceProvider();
            tDESalg.Key = new ASCIIEncoding().GetBytes(key.Substring(0, 16));
            tDESalg.IV = new ASCIIEncoding().GetBytes(key.Substring(8, 8));

            byte[] buffer = Convert.FromBase64String(cipherText);
            string result = DecryptTextFromMemory(buffer, tDESalg.Key, tDESalg.IV);
            return result;
        }

        /// <summary>
        /// Encrypts text
        /// </summary>
        /// <param name="PlainText">Plaint text</param>        
        /// <returns>Encrypted string</returns>
        public static string Encrypt(string plainText)
        {
            return Encrypt(plainText, BCA_EncryptionPrivateKey);
        }

        public static string Encrypt(string plainText, string key)
        {
            if (String.IsNullOrEmpty(plainText))
                return plainText;

            TripleDESCryptoServiceProvider tDESalg = new TripleDESCryptoServiceProvider();
            tDESalg.Key = new ASCIIEncoding().GetBytes(key.Substring(0, 16));
            tDESalg.IV = new ASCIIEncoding().GetBytes(key.Substring(8, 8));

            byte[] encryptedBinary = EncryptTextToMemory(plainText, tDESalg.Key, tDESalg.IV);
            string result = Convert.ToBase64String(encryptedBinary);
            return result;
        }


        private static byte[] EncryptTextToMemory(string Data, byte[] Key, byte[] IV)
        {
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, new TripleDESCryptoServiceProvider().CreateEncryptor(Key, IV), CryptoStreamMode.Write);
            byte[] toEncrypt = new UnicodeEncoding().GetBytes(Data);
            cStream.Write(toEncrypt, 0, toEncrypt.Length);
            cStream.FlushFinalBlock();
            byte[] ret = mStream.ToArray();
            cStream.Close();
            mStream.Close();
            return ret;
        }

        private static string DecryptTextFromMemory(byte[] Data, byte[] Key, byte[] IV)
        {
            MemoryStream msDecrypt = new MemoryStream(Data);
            CryptoStream csDecrypt = new CryptoStream(msDecrypt, new TripleDESCryptoServiceProvider().CreateDecryptor(Key, IV), CryptoStreamMode.Read);
            StreamReader sReader = new StreamReader(csDecrypt, new UnicodeEncoding());
            return sReader.ReadLine();
        }

        /// <summary>
        /// AES Encryption
        /// </summary>
        public static string Encrypt128(string text)
        {
            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = Encoding.UTF8.GetBytes(AesIV);
            aes.Key = Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert string to byte array
            byte[] src = Encoding.Unicode.GetBytes(text);

            // encryption
            using (ICryptoTransform encrypt = aes.CreateEncryptor())
            {
                byte[] dest = encrypt.TransformFinalBlock(src, 0, src.Length);

                // Convert byte array to Base64 strings
                return Convert.ToBase64String(dest);
            }
        }

        /// <summary>
        /// AES decryption
        /// </summary>
        public static string Decrypt128(string text)
        {
            // AesCryptoServiceProvider
            AesCryptoServiceProvider aes = new AesCryptoServiceProvider();
            aes.BlockSize = 128;
            aes.KeySize = 128;
            aes.IV = Encoding.UTF8.GetBytes(AesIV);
            aes.Key = Encoding.UTF8.GetBytes(AesKey);
            aes.Mode = CipherMode.CBC;
            aes.Padding = PaddingMode.PKCS7;

            // Convert Base64 strings to byte array
            byte[] src = System.Convert.FromBase64String(text);

            // decryption
            using (ICryptoTransform decrypt = aes.CreateDecryptor())
            {
                byte[] dest = decrypt.TransformFinalBlock(src, 0, src.Length);
                return Encoding.Unicode.GetString(dest);
            }
        }

        public static string Encrypt64(string text)
        {
            return Convert.ToBase64String(ASCIIEncoding.ASCII.GetBytes("5TGBYHN7UJMQvnAZ2WSX#EDC4RFV" + text + "Psqz0o77Gyg"));
        }

        public static string Decrypt64(string text)
        {
            return ASCIIEncoding.ASCII.GetString(Convert.FromBase64String(text))
                                      .Replace("5TGBYHN7UJMQvnAZ2WSX#EDC4RFV", "").Replace("Psqz0o77Gyg", "");
        }

        static bool IsLetter(char c)
        {
            return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
        }

        static bool IsDigit(char c)
        {
            return c >= '0' && c <= '9';
        }

        static bool IsSymbol(char c)
        {
            return c > 32 && c < 127 && !IsDigit(c) && !IsLetter(c);
        }

        public static bool IsValidPassword(string password)
        {
            password = password.Trim();
            foreach (char c in password)
                if (!IsLetter(c) && !IsDigit(c) && !IsSymbol(c)) return false;
            return true;
            //return
            //   password.Any(c => IsLetter(c)) &&
            //   password.Any(c => IsDigit(c)) &&
            //   password.Any(c => IsSymbol(c));
        }

        //public static string GenerateDefaultPassword(string sEmail)
        //{
        //    string encryptPass = SecurityHelper.Encrypt128(sEmail);
        //    return encryptPass.Remove(8);
        //}

        public static string CreateRandomPassword(int PasswordLength)
        {
            string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ!@#$%^&*()?";
            Random randNum = new Random();
            char[] chars = new char[PasswordLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < PasswordLength; i++)
            {
                chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(chars);
        }
    }
}
