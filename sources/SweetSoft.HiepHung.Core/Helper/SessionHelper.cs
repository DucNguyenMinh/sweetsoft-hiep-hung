﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SweetSoft.HiepHung.Core.Helper
{
    class SessionHelper
    {
    }

    public class SessionName
    {
        public const string Session_Prefix = "QLTTLT";
        public static string CurrentConfirmResult = string.Format("{0}_CurrentConfirmResult", Session_Prefix);
        public static string CurrentFunctionName = string.Format("{0}_CurrentFunctionName", Session_Prefix);
        /// <summary>
        /// Session lưu thông tin người dùng công an sau khi đăng nhập
        /// </summary>
        public static string CurrentPoliceInfo = string.Format("{0}_CurrentPoliceInfo", Session_Prefix);

        #region Right/Permission
        public static string Right_CurrentUserRight = string.Format("{0}_CurrentUserRight", Session_Prefix);
        public static string Right_CurrentUserHotelRight = string.Format("{0}_CurrentUserHotelRight", Session_Prefix);
        public static string RightInRole_CurrentRole = string.Format("{0}_Rightinrole_CurrentRole", Session_Prefix);
        #endregion

        #region User
        public static string User_CurrentUser = string.Format("{0}_CurrentUser", Session_Prefix);
        public static string User_CurrentIP = string.Format("{0}_CurrentUserIP", Session_Prefix);
        public static string User_Flag = string.Format("{0}_CurrentUserFlag", Session_Prefix);
        #endregion

        #region Hotel
        public static string Hotel_TaxStatus = string.Format("{0}_CurrentHotelTaxStatus", Session_Prefix);
        #endregion

        #region Employer
        public static string EmployerLogo = string.Format("{0}_EmployerLogo", Session_Prefix);
        #endregion

        #region Login
        /// <summary>
        /// Session lưu thông tin tài khoản đang xử lý đăng ký
        /// </summary>
        public static string Login_Register_Progress = string.Format("{0}_Login_Register_Progress", Session_Prefix);
        /// <summary>
        /// Session lưu thông tin mã lỗi đăng nhập
        /// </summary>
        public static string Login_Error_Code = string.Format("{0}_Login_Error_Code", Session_Prefix);
        /// <summary>
        /// Session lưu trang mà user muốn vào đầu tiên
        /// </summary>
        public static string Login_Return_URL = string.Format("{0}_Login_Return_URL", Session_Prefix);
        #endregion

        #region MessageBox
        public static string ConfirmYes = string.Format("{0}_Admin_ConfirmYes", Session_Prefix);
        public static string ConfirmNo = string.Format("{0}_Admin_ConfirmNo", Session_Prefix);
        #endregion
    }
}
