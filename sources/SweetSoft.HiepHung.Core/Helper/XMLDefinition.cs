﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;

namespace SweetSoft.HiepHung.Core.Helper.XMLDefinition
{
    /// <summary>
    /// Summary description for XMLLocation
    /// </summary>
    public class XMLLocation : XMLFile
    {
        public XMLLocation()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public override string FileName
        {
            get
            {
                return "Location";
            }
        }



        #region Object

        static XMLLocation ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLLocation xml = new XMLLocation();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLLocation FetchByColumn(string column, string value)
        {
            return new XMLLocation().GetByColumn(column, value);
        }

        public static XMLLocation FetchById(object id)
        {
            return new XMLLocation().GetById(id);
        }

        public XMLLocation GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLLocation GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLLocation> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLLocation> lst = new List<XMLLocation>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLLocation> FetchAll()
        {
            return new XMLLocation().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLLocation().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion

    }


    /// <summary>
    /// Summary description for XMLPosition
    /// </summary>
    public class XMLPosition : XMLFile
    {
        public XMLPosition()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public override string FileName
        {
            get
            {
                return "Position";
            }
        }


        #region Object

        static XMLPosition ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLPosition xml = new XMLPosition();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLPosition FetchByColumn(string column, string value)
        {
            return new XMLPosition().GetByColumn(column, value);
        }

        public static XMLPosition FetchById(object id)
        {
            return new XMLPosition().GetById(id);
        }

        public XMLPosition GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLPosition GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLPosition> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLPosition> lst = new List<XMLPosition>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLPosition> FetchAll()
        {
            return new XMLPosition().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLPosition().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion
    }

    public class XMLJobExperience : XMLFile
    {
        public XMLJobExperience()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public struct Columns
        {
            public static string Id = "Id";
            public static string Name = "Name";
        }

        public override string FileName
        {
            get
            {
                return "JobExperience";
            }
        }


        #region Object

        static XMLJobExperience ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLJobExperience xml = new XMLJobExperience();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLJobExperience FetchByColumn(string column, string value)
        {
            return new XMLJobExperience().GetByColumn(column, value);
        }

        public static XMLJobExperience FetchById(object id)
        {
            return new XMLJobExperience().GetById(id);
        }

        public XMLJobExperience GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLJobExperience GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLJobExperience> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLJobExperience> lst = new List<XMLJobExperience>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLPosition> FetchAll()
        {
            return new XMLPosition().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLPosition().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion
    }

    public class XMLJobLevel : XMLFile
    {
        public XMLJobLevel()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public struct Columns
        {
            public static string Id = "Id";
            public static string Name = "Name";
        }

        public override string FileName
        {
            get
            {
                return "JobLevel";
            }
        }


        #region Object

        static XMLJobLevel ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLJobLevel xml = new XMLJobLevel();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLJobLevel FetchByColumn(string column, string value)
        {
            return new XMLJobLevel().GetByColumn(column, value);
        }

        public static XMLJobLevel FetchById(object id)
        {
            return new XMLJobLevel().GetById(id);
        }

        public XMLJobLevel GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLJobLevel GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLJobLevel> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLJobLevel> lst = new List<XMLJobLevel>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLJobLevel> FetchAll()
        {
            return new XMLJobLevel().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLPosition().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion
    }

    public class XMLJobVacancy : XMLFile
    {
        public XMLJobVacancy()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public struct Columns
        {
            public static string Id = "Id";
            public static string Name = "Name";
        }

        public override string FileName
        {
            get
            {
                return "JobVacancy";
            }
        }


        #region Object

        static XMLJobVacancy ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLJobVacancy xml = new XMLJobVacancy();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLJobVacancy FetchByColumn(string column, string value)
        {
            return new XMLJobVacancy().GetByColumn(column, value);
        }

        public static XMLJobVacancy FetchById(object id)
        {
            return new XMLJobVacancy().GetById(id);
        }

        public XMLJobVacancy GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLJobVacancy GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLJobVacancy> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLJobVacancy> lst = new List<XMLJobVacancy>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLJobVacancy> FetchAll()
        {
            return new XMLJobVacancy().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLPosition().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion
    }

    public class XMLJobEducationLevel : XMLFile
    {
        public XMLJobEducationLevel()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        public struct Columns
        {
            public static string Id = "Id";
            public static string Name = "Name";
        }

        public override string FileName
        {
            get
            {
                return "JobEducationLevel";
            }
        }


        #region Object

        static XMLJobEducationLevel ConvertTo(XmlNode node)
        {
            if (node != null)
            {
                XMLJobEducationLevel xml = new XMLJobEducationLevel();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    var found = allAttr.Where(x => x.Name.ToLower() == "id");
                    if (found.Any())
                        xml.Id = found.First().Value;

                    found = allAttr.Where(x => x.Name.ToLower() == "name");
                    if (found.Any())
                        xml.Name = found.First().Value;
                }
                return xml;
            }
            return null;
        }

        public static XMLJobEducationLevel FetchByColumn(string column, string value)
        {
            return new XMLJobEducationLevel().GetByColumn(column, value);
        }

        public static XMLJobEducationLevel FetchById(object id)
        {
            return new XMLJobEducationLevel().GetById(id);
        }

        public XMLJobEducationLevel GetByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
                return ConvertTo(node);
            return null;
        }

        public XMLJobEducationLevel GetById(object id)
        {
            return GetByColumn("Id", id.ToString());
        }

        public List<XMLJobEducationLevel> GetAll()
        {
            XmlNodeList allNode = FetchAll(FileName);
            if (allNode != null && allNode.Count > 0)
            {
                List<XMLJobEducationLevel> lst = new List<XMLJobEducationLevel>();
                foreach (XmlNode item in allNode)
                    lst.Add(ConvertTo(item));
                return lst;
            }
            return null;
        }

        public static List<XMLJobVacancy> FetchAll()
        {
            return new XMLJobVacancy().GetAll();
        }

        #endregion

        #region Dictionary

        public static Dictionary<string, string> FetchDictionaryById(string value)
        {
            return FetchDictionaryByColumn("Id", value);
        }

        public static Dictionary<string, string> FetchDictionaryByColumn(string column, string value)
        {
            return new XMLPosition().GetDictionaryByColumn(column, value);
        }

        public Dictionary<string, string> GetDictionaryById(string value)
        {
            return GetDictionaryByColumn("Id", value);
        }

        public Dictionary<string, string> GetDictionaryByColumn(string column, string value)
        {
            XmlNode node = FetchById(column, value);
            if (node != null)
            {
                Dictionary<string, string> dic = new Dictionary<string, string>();
                var allAttr = node.Attributes.Cast<XmlAttribute>();
                if (allAttr.Any())
                {
                    foreach (XmlAttribute item in allAttr)
                        dic.Add(item.Name, item.Value);
                }
                return dic;
            }
            return null;
        }

        #endregion
    }
}