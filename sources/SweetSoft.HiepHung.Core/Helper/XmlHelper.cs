﻿using System;
using System.Xml;
using System.Linq;
using System.Xml.XPath;
using System.Collections;
using System.Net;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Collections.Generic;
using System.Xml.Linq;
using System.Web;

namespace SweetSoft.HiepHung.Core.Helper
{

    ///<summary> 
    /// This class attempts to wrap up some common things 
    /// we need to do when dealing with Xml and C# classes: 
    /// Load, Save, Add/Remove Attributes/Elements, et al. 
    /// </summary> 
    public class XmlHelper
    {
        private XmlDocument m_xmlDocument;
        private XPathNavigator m_nav;
        private string m_sLastErrorMessage;

        public enum LoadType
        {
            FromString, FromLocalFile, FromURL
        }

        // Constructor 
        public XmlHelper()
        {
            m_sLastErrorMessage = "";
            m_xmlDocument = new XmlDocument();
        }

        public XmlHelper(string filePath, LoadType loadType)
        {
            m_sLastErrorMessage = "";
            m_xmlDocument = new XmlDocument();
            LoadXML(filePath, loadType);
        }

        public XmlHelper(string filePath)
        {
            m_sLastErrorMessage = "";
            m_xmlDocument = new XmlDocument();
            LoadXML(filePath, LoadType.FromLocalFile);
        }

        // Properties... 
        public string LastErrorMessage
        {
            get
            {
                return m_sLastErrorMessage;
            }
            set
            {
                m_sLastErrorMessage = value;
            }
        }

        public XmlNode RootNode
        {
            get
            {
                return m_xmlDocument.DocumentElement;
            }
        }

        public XmlDocument Document
        {
            get
            {
                return m_xmlDocument;
            }
        }

        public XPathNavigator Navigator
        {
            get
            {
                return m_nav;
            }
        }


        // delegates - more complex save operations can do it themselves... 
        public delegate bool Save(string sTargetXML);

        ///<summary> 
        /// Save the XML to a target file. 
        ///</summary> 
        public bool SaveToFile(string sTargetFileName)
        {
            bool bResult = false;

            try
            {
                m_xmlDocument.Save(sTargetFileName);
                bResult = true;
            }
            catch (XmlException e)
            {
                HandleException(e);
            }

            return bResult;
        }


        ///<summary> 
        /// Easy way to get the entire Xml string 
        ///</summary> 
        public override string ToString()
        {
            return m_xmlDocument.OuterXml;
        }

        private void DoPostLoadCreateInit()
        {
            m_nav = m_xmlDocument.CreateNavigator();
            MoveToRoot();
        }


        ///<summary> 
        /// Easy way to load XML from a file or URL 
        ///</summary> 
        public bool LoadXML(string sourceXMLOrFile, LoadType loadType)
        {
            bool bLoadResult = false;

            try
            {
                switch (loadType)
                {
                    case XmlHelper.LoadType.FromString:
                        m_xmlDocument.LoadXml(sourceXMLOrFile); // loading from source XML text 
                        break;

                    case XmlHelper.LoadType.FromLocalFile:
                        m_xmlDocument.Load(sourceXMLOrFile); // loading from a file 
                        break;

                    case XmlHelper.LoadType.FromURL:
                        {
                            string sURLContent = GetURLContent(sourceXMLOrFile);
                            m_xmlDocument.LoadXml(sURLContent);
                            break;
                        }

                    default:
                        string sErr = "Developer note:  No LoadType case supported for " + loadType.ToString();
                        throw (new Exception(sErr));
                }

                DoPostLoadCreateInit();

                bLoadResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bLoadResult;
        }

        ///<summary> 
        /// Helper method to get string content from a URL - not necessarily XML, but probably 
        ///</summary> 
        public string GetURLContent(string sURL)
        {
            string s = "";

            try
            {
                HttpWebRequest webreq = (HttpWebRequest)WebRequest.Create(sURL);
                HttpWebResponse webresp = (HttpWebResponse)webreq.GetResponse();

                StreamReader stream = new StreamReader(webresp.GetResponseStream(), Encoding.ASCII);
                s = stream.ReadToEnd();
                stream.Close();
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return s;
        }

        ///<summary> 
        /// Helper function if navigation is used to ensure we're at the root node. 
        ///</summary> 
        public bool MoveToRoot()
        {
            bool bResult = false;
            try
            {
                m_nav.MoveToRoot(); // go to root node! 
                bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        #region get attribute value

        ///<summary> 
        /// Gets an ArrayList of XmlNode children using an xPath expression 
        /// </summary> 
        public ArrayList GetChildNodesFromCriteria(string xPathExpression)
        {
            ArrayList al = new ArrayList();

            try
            {
                XmlNodeList nl = m_xmlDocument.SelectNodes(xPathExpression);

                if (nl != null)
                {
                    for (int i = 0; i < nl.Count; i++)
                        al.Add(nl.Item(i));
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return al;
        }

        ///<summary> 
        /// Get first child node given an XPath expression 
        /// </summary> 
        public XmlNode GetFirstChildNodeFromCriteria(string xPathExpression)
        {

            XmlNode node = null;

            try
            {
                node = m_xmlDocument.SelectSingleNode(xPathExpression);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return node;
        }

        ///<summary> 
        /// Get the Attribute value from a given XmlNode 
        /// </summary> 
        public string GetAttributeValue(XmlNode node, string sAttributeName)
        {
            string sVal = "";
            try
            {
                XmlAttributeCollection attribColl = node.Attributes;
                XmlAttribute attrib = attribColl[sAttributeName, ""];
                sVal = Decode(attrib.Value);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return sVal;
        }

        ///<summary> 
        /// Get the Attribute int32 (int) value from a given XmlNode 
        /// </summary> 
        public int GetAttributeInt32Value(XmlNode node, string sAttributeName)
        {
            string sVal = GetAttributeValue(node, sAttributeName);

            return sVal != "" ? Convert.ToInt32(sVal) : 0;
        }

        ///<summary> 
        /// Get the Attribute floating point/Single value from a given XmlNode 
        /// </summary> 
        public float GetAttributeFloatValue(XmlNode node, string sAttributeName)
        {
            string sVal = GetAttributeValue(node, sAttributeName);
            return sVal != "" ? Convert.ToSingle(sVal) : 0;
        }

        ///<summary> 
        /// Get the Attribute double value from a given XmlNode 
        /// </summary> 
        public double GetAttributeDoubleValue(XmlNode node, string sAttributeName)
        {
            string sVal = GetAttributeValue(node, sAttributeName);
            return sVal != "" ? Convert.ToDouble(sVal) : 0.00;
        }

        ///<summary> 
        /// Get the Attribute boolean value from a given XmlNode 
        /// </summary> 
        public bool GetAttributeBooleanValue(XmlNode node, string sAttributeName)
        {
            string sVal = GetAttributeValue(node, sAttributeName);
            return sVal != "" ? Convert.ToBoolean(sVal) : false;
        }

        #endregion

        #region get element value

        ///<summary> 
        /// Get the Element value from a given XmlNode 
        /// </summary> 
        public string GetElementValue(XmlNode xmlNode)
        {
            string sVal = "";

            try
            {
                sVal = Decode(xmlNode.InnerXml);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return sVal;
        }

        ///<summary> 
        /// Get the Element Int32 value from a given XmlNode 
        /// </summary> 
        public int GetElementInt32Value(XmlNode xmlNode)
        {
            string sVal = GetElementValue(xmlNode);
            return sVal != "" ? Convert.ToInt32(sVal) : 0;
        }

        ///<summary> 
        /// Get the Element float/single floating point value from a given XmlNode 
        /// </summary> 
        public float GetElementFloatValue(XmlNode xmlNode)
        {
            string sVal = GetElementValue(xmlNode);
            return sVal != "" ? Convert.ToSingle(sVal) : 0;
        }

        ///<summary> 
        /// Get the Element Double value from a given XmlNode 
        /// </summary> 
        public double GetElementDoubleValue(XmlNode xmlNode)
        {
            string sVal = GetElementValue(xmlNode);
            return sVal != "" ? Convert.ToDouble(sVal) : 0.00;
        }

        ///<summary> 
        /// Get the Element Boolean value from a given XmlNode 
        /// </summary> 
        public bool GetElementBooleanValue(XmlNode xmlNode)
        {
            string sVal = GetElementValue(xmlNode);
            return sVal != "" ? Convert.ToBoolean(sVal) : false;
        }

        #endregion

        #region get child element value

        ///<summary> 
        /// Get the first Child Element value from a given XmlNode 
        /// </summary> 
        public string GetChildElementValue(XmlNode parentNode, string sElementName)
        {
            string sVal = "";
            try
            {
                XmlNodeList childNodes = parentNode.ChildNodes;
                foreach (XmlNode childNode in childNodes)
                {
                    if (childNode.Name == sElementName)
                    {
                        sVal = GetElementValue(childNode);
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return sVal;
        }

        ///<summary> 
        /// Get the Child Element int32 value from a given XmlNode and ElementName 
        /// </summary> 
        public int GetChildElementInt32Value(XmlNode parentNode, string sElementName)
        {
            string sVal = GetChildElementValue(parentNode, sElementName);
            return sVal != "" ? Convert.ToInt32(sVal) : 0;
        }

        ///<summary> 
        /// Get the Child Element floating point/single value from a given XmlNode and ElementName 
        /// </summary> 
        public float GetChildElementFloatValue(XmlNode parentNode, string sElementName)
        {
            string sVal = GetChildElementValue(parentNode, sElementName);
            return sVal != "" ? Convert.ToSingle(sVal) : 0;
        }

        ///<summary> 
        /// Get the Child Element double value from a given XmlNode and ElementName 
        /// </summary> 
        public double GetChildElementDoubleValue(XmlNode parentNode, string sElementName)
        {
            string sVal = GetChildElementValue(parentNode, sElementName);
            return sVal != "" ? Convert.ToDouble(sVal) : 0.00;
        }

        ///<summary> 
        /// Get the Child Element boolean value from a given XmlNode and ElementName 
        /// </summary> 
        public bool GetChildElementBooleanValue(XmlNode parentNode, string sElementName)
        {
            string sVal = GetChildElementValue(parentNode, sElementName);
            return sVal != "" ? Convert.ToBoolean(sVal) : false;
        }

        #endregion

        ///<summary> 
        /// Returns the first XmlNode object matching this element name 
        ///<seealso cref='GetFirstChildXmlNode'/>  
        ///</summary> 
        public XmlNode GetFirstChildXmlNodeFromRoot(string sElementName)
        {
            // TODO:  isn't there a better/faster/more effiecient way to do this?  couldn't find it sifting through documentation! 
            XmlNodeList nodeList = GetChildNodesFromRoot(sElementName);
            if (nodeList.Count > 0) return nodeList[0];

            return null;
        }

        ///<summary> 
        /// Returns the first XmlNode object matching this element name  
        /// NOTE:  this doesn't seem to work if parent is Root!  Use GetFirstChildXmlNodeFromRoot 
        ///<seealso cref='GetFirstChildXmlNodeFromRoot'/> 
        ///</summary> 
        public XmlNode GetFirstChildXmlNode(XmlNode parentNode, string sElementName)
        {
            // NOTE:  this doesn't seem to work if parent is Root!  Use GetFirstChildXmlNodeFromRoot 
            XmlNode foundChildNode = null;
            try
            {
                XmlNodeList childNodes = parentNode.ChildNodes;
                foreach (XmlNode childNode in childNodes)
                {
                    if (childNode.Name == sElementName)
                    {
                        foundChildNode = childNode;
                        break;
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return foundChildNode;
        }

        ///<summary> 
        /// Returns an XmlNodeList of child nodes matching this element name 
        ///</summary> 
        public XmlNodeList GetChildNodesFromRoot(string sElementName)
        {
            return m_xmlDocument.GetElementsByTagName(sElementName);
        }

        ///<summary> 
        /// Returns an ArrayList (boxed XmlNode objects) of child nodes matching this element name 
        /// This function is recursive in that it will find ALL the children, even if their in  
        /// sub folders (sub child nodes) 
        ///</summary> 
        public ArrayList GetRecursiveChildNodesFromParent(XmlNode parentNode, string sElementName)
        {
            ArrayList elementList = new ArrayList();

            try
            {
                XmlNodeList children = parentNode.ChildNodes;
                foreach (XmlNode child in children)
                {
                    if (child.Name == sElementName) elementList.Add(child);

                    if (child.HasChildNodes == true)
                    {
                        ArrayList childrenList = GetRecursiveChildNodesFromParent(child, sElementName);
                        if (childrenList.Count > 0)
                        {
                            foreach (XmlNode subChild in childrenList)
                                elementList.Add(subChild);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return elementList;
        }

        ///<summary> 
        /// Create an Element under the given parent based on the name and value pair. 
        ///</summary> 
        public XmlElement CreateNodeElement(XmlNode parentNode, string sElementName, string sElementValue)
        {
            XmlElement newElem = null;

            try
            {
                newElem = m_xmlDocument.CreateElement(sElementName);
                if (sElementValue != null)
                    newElem.InnerXml = Encode(sElementValue);
                XmlDocument ownerDoc = parentNode.OwnerDocument;

                if (ownerDoc != null)
                {
                    parentNode.AppendChild(newElem);
                }
                else
                {
                    XmlElement root = m_xmlDocument.DocumentElement;
                    root.AppendChild(newElem);
                }

            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return newElem;
        }

        ///<summary> 
        /// Creates and adds a comment before the given node.  If root node, or null,  
        /// the comment node is Appended to the tree. 
        ///</summary> 
        public XmlNode CreateComment(XmlNode insertAfterThisNode, string sVal)
        {

            if (insertAfterThisNode == null) return null;

            XmlNode createdNode = null;
            try
            {
                XmlComment commentNode = m_xmlDocument.CreateComment(Encode(sVal));
                createdNode = insertAfterThisNode.AppendChild(commentNode);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return createdNode;
        }


        public XmlNode CreateXmlDeclaration(string version, string encoding, string standalone)
        {

            XmlNode createdNode = null;
            try
            {
                XmlDeclaration dec = m_xmlDocument.CreateXmlDeclaration(version, encoding, standalone);
                createdNode = m_xmlDocument.PrependChild(dec);
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return createdNode;
        }


        ///<summary> 
        /// Delete an XmlNode from the tree 
        ///</summary> 
        public bool DeleteNodeElement(XmlNode targetNode)
        {
            bool bResult = false;

            try
            {
                XmlNode xmlNode = RootNode.RemoveChild(targetNode);
                if (xmlNode != null) bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        ///<summary> 
        /// Modify an XmlNode elment with a new value. 
        ///</summary> 
        public bool ModifyNodeElementValue(XmlNode targetNode, string sNewElementValue)
        {
            bool bResult = false;

            try
            {
                targetNode.InnerXml = Encode(sNewElementValue);
                bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        ///<summary> 
        /// Create a new attribute given an XmlElement (XmlNode) target 
        /// </summary> 
        public XmlAttribute CreateNodeAttribute(XmlElement targetElement, string sAttributeName, string sAttributeValue)
        {
            XmlAttribute newAttr = null;

            try
            {
                newAttr = m_xmlDocument.CreateAttribute(sAttributeName);
                targetElement.SetAttributeNode(newAttr);
                targetElement.SetAttribute(sAttributeName, "", Encode(sAttributeValue));
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return newAttr;
        }


        ///<summary> 
        /// Delete an attribute from the given target node. 
        /// </summary> 
        public bool DeleteNodeAttribute(XmlNode targetNode, string sAttributeName)
        {
            bool bResult = false;

            try
            {
                XmlAttributeCollection attrColl = targetNode.Attributes;
                XmlAttribute xmlAttribute = attrColl.Remove((XmlAttribute)attrColl[sAttributeName, ""]);
                if (xmlAttribute != null) bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        ///<summary> 
        /// GenerateSchema a schema file from a given target file 
        /// </summary> 
        public bool GenerateSchema(string sTargetFile)
        {

            bool bResult = false;
            try
            {
                DataSet data = new System.Data.DataSet();
                data.ReadXml(new XmlNodeReader(RootNode), XmlReadMode.Auto);
                data.WriteXmlSchema(sTargetFile);
                bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        ///<summary> 
        /// GenerateSchemaAsString based on the currently loaded Xml 
        /// </summary> 
        public string GenerateSchemaAsString()
        {

            string sSchemaXmlString = "";
            try
            {
                DataSet data = new System.Data.DataSet();
                data.ReadXml(new XmlNodeReader(RootNode), XmlReadMode.Auto);

                string sTempFile = Path.GetTempFileName();

                data.WriteXmlSchema(sTempFile);

                // read the data into a string 
                StreamReader sr = new StreamReader(sTempFile);
                sSchemaXmlString = sr.ReadToEnd();
                sr.Close();

                if (File.Exists(sTempFile) == true) File.Delete(sTempFile);
            }
            catch (Exception e)
            {
                HandleException(e);
                sSchemaXmlString = "<root><error>" + LastErrorMessage + "</error></root>";
            }

            return sSchemaXmlString;
        }

        ///<summary> 
        /// Modify an attribute value to a new value 
        /// </summary> 
        public bool ModifyNodeAttributeValue(XmlNode targetNode, string sAttributeName, string sNewAttributeValue)
        {
            bool bResult = false;

            try
            {
                XmlAttributeCollection attrColl = targetNode.Attributes;
                XmlAttribute xmlAttribute = (XmlAttribute)attrColl[sAttributeName, ""];
                xmlAttribute.Value = Encode(sNewAttributeValue);
                bResult = true;
            }
            catch (Exception e)
            {
                HandleException(e);
            }

            return bResult;
        }

        ///<summary> 
        /// Internal method used to process errors and exception handling 
        /// </summary> 
        private void HandleException(Exception e)
        {
            m_sLastErrorMessage = e.Message;
            Console.WriteLine(m_sLastErrorMessage + " Stack Trace:  " + e.StackTrace + " Source: " + e.Source);
        }

        public List<Dictionary<string, string>> ToDictionary()
        {
            return ParseToDictionary(m_xmlDocument);
        }

        public DataSet ToDataSet()
        {
            try
            {
                return ParseToDataSet(ToString());
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        /*
        public DataTable ToDataTable()
        {
            return ParseToDataTable(ToString());
        }
        */
        public static void CreateEmptyXMLFile(string rootTagName, string filePath)
        {
            XmlTextWriter xmlWriter = new XmlTextWriter(filePath, System.Text.Encoding.UTF8);
            xmlWriter.WriteRaw("<" + rootTagName + " />");
            xmlWriter.Flush();
            xmlWriter.Close();
        }

        ///<summary> 
        /// Internal method used to ensure that HTML and XML tags are encoded within their values 
        ///</summary> 
        public static string Encode(string input)
        {
            string output = input;
            output = Regex.Replace(output, "&", "&");
            output = Regex.Replace(output, "<", "<");
            output = Regex.Replace(output, ">", ">");
            output = Regex.Replace(output, "\"", "'");

            return output;
        }

        ///<summary> 
        /// Internal method used to ensure that HTML and XML tags are decoded for display in other systems 
        ///</summary> 
        public static string Decode(string input)
        {
            string output = input;
            output = Regex.Replace(output, "&", "&");
            output = Regex.Replace(output, "<", "<");
            output = Regex.Replace(output, ">", ">");
            output = Regex.Replace(output, "'", "\"");
            return output;
        }

        ///<summary>
        /// Checks whether the input text is a valid xml string.
        ///</summary>
        /// <param name="text">The text to check.</param>
        ///<returns>Yes if the text is a valid xml string, false otherwise.</returns>
        public static bool IsXml(string text)
        {
            try
            {
                if (string.IsNullOrEmpty(text))
                {
                    return false;
                }
                using (var stringReader = new StringReader(text))
                {
                    using (var xmlReader = XmlReader.Create(stringReader))
                    {
                        while (xmlReader.Read())
                        {
                        }
                    }
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        ///<summary>
        /// Indents the xml contained in the XmlDocument object.
        ///</summary>
        /// <param name="document">The XmlDocument containing the Xml to indent.</param>
        /// <param name="encoding"></param>
        ///<returns>Xml indented</returns>
        public static string Indent(XmlDocument document, Encoding encoding)
        {
            if (document == null)
            {
                return null;
            }
            var settings = new XmlWriterSettings
            {
                Encoding = encoding ?? new UTF8Encoding(false),
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (var stream = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    document.Save(writer);
                    writer.Flush();
                }
                return settings.Encoding.GetString(stream.ToArray());
            }
        }

        ///<summary>
        /// Indents the xml contained in the string object.
        ///</summary>
        /// <param name="xml">The string containing the Xml to indent.</param>
        /// <param name="encoding"></param>
        ///<returns>Xml indented</returns>
        public static string Indent(string xml, Encoding encoding)
        {
            if (string.IsNullOrEmpty(xml))
            {
                return null;
            }
            if (!IsXml(xml))
            {
                return xml;
            }
            var document = new XmlDocument();
            document.LoadXml(xml);
            var settings = new XmlWriterSettings
            {
                Encoding = encoding ?? new UTF8Encoding(false),
                Indent = true,
                IndentChars = "  ",
                NewLineChars = "\r\n",
                NewLineHandling = NewLineHandling.Replace
            };
            using (var stream = new MemoryStream())
            {
                using (var writer = XmlWriter.Create(stream, settings))
                {
                    document.Save(writer);
                    writer.Flush();
                }
                return settings.Encoding.GetString(stream.ToArray());
            }
        }

        public static List<Dictionary<string, string>> ParseToDictionary(string xmlContents)
        {
            List<Dictionary<string, string>> dataDictionary = new List<Dictionary<string, string>>();
            XmlHelper xdoc = new XmlHelper();
            bool isLoad = xdoc.LoadXML(xmlContents, LoadType.FromString);
            if (isLoad)
            {
                if (xdoc.Document != null)
                    return ParseToDictionary(xdoc.Document);
            }
            return dataDictionary;
        }

        public static List<Dictionary<string, string>> ParseToDictionary(XmlDocument xdoc)
        {
            List<Dictionary<string, string>> dataDictionary = new List<Dictionary<string, string>>();

            if (xdoc.DocumentElement != null)
            {
                XmlNodeList lst = xdoc.DocumentElement.ChildNodes;
                if (lst != null && lst.Count > 0)
                {
                    foreach (XmlNode item in lst)
                    {
                        Dictionary<string, string> dic = new Dictionary<string, string>();
                        if (item.Attributes != null && item.Attributes.Count > 0)
                        {
                            foreach (XmlAttribute attribute in item.Attributes)
                                dic.Add(attribute.Name, attribute.Value);
                        }

                        if (item.ChildNodes != null && item.ChildNodes.Count > 0)
                            dic.Add("InnerXml", item.InnerXml);
                        else
                            dic.Add("InnerText", item.InnerText);

                        dataDictionary.Add(dic);
                    }
                }
            }
            return dataDictionary;
        }

        public static DataSet ParseToDataSet(string xmlContents)
        {
            DataSet dataSet = new DataSet();
            StringReader xmlSR = new StringReader(xmlContents);
            try
            {
                dataSet.ReadXml(xmlSR);
            }
            catch (Exception ex)
            {

            }
            return dataSet;
        }
        /*
        public static DataTable ParseToDataTable(string xmlContents)
        {
            DataTable dataTable = new DataTable();
            StringReader xmlSR = new StringReader(xmlContents);
            try
            {
                dataTable.ReadXml(xmlSR);
            }
            catch (Exception ex)
            {

            }
            return dataTable;
        }
        */


        public static bool CreateFileXML(string filePath, string rootName,
            string nodeName, string[] dataArray)
        {
            XmlHelper xdoc = new XmlHelper();
            if (File.Exists(filePath) == false)
                XmlHelper.CreateEmptyXMLFile(rootName, filePath);

            bool isLoaded = xdoc.LoadXML(filePath, XmlHelper.LoadType.FromLocalFile);
            if (isLoaded)
            {
                //xdoc.RootNode.RemoveAll();
                if (xdoc.Document.ChildNodes[0].ChildNodes.Count == 0)
                {
                    XmlAttribute attr = null;
                    XmlElement node = null;
                    int id = 1;
                    foreach (string item in dataArray)
                    {
                        node = xdoc.CreateNodeElement(xdoc.RootNode, nodeName, null);
                        if (node != null)
                        {
                            attr = xdoc.CreateNodeAttribute(node, "Name", item);
                            attr = xdoc.CreateNodeAttribute(node, "Id", id.ToString());
                            id += 1;
                        }
                    }
                    xdoc.SaveToFile(filePath);
                }
                return true;
            }

            return false;
        }

    } // end of XmlHelper class 


}