﻿using SweetSoft.HiepHung.Manager;

using SweetSoft.HiepHung.DataAccess;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.SessionState;

namespace SweetSoft.HiepHung.Core.Helper
{
    /// <summary>
    /// The ApplicationContext represents common properties and settings used through out of a Request. All data stored
    /// in the context will be cleared at the end of the request
    /// 
    /// This object should be safe to use outside of a web request, but querystring and other values should be prepopulated
    /// 
    /// Each CS thread must create an instance of the ApplicationContext using one of the Three Create overloads. In some cases, 
    /// the CreateEmptyContext method may be used, but it is NOT recommended.
    /// </summary>
    public sealed class ApplicationContext
    {
        #region Private Containers

        //Generally expect 10 or less items
        private HybridDictionary m_Items = new HybridDictionary();
        private NameValueCollection m_QueryString = null;
        private string m_SiteUrl = null;
        private Uri m_CurrentUri;
        private string m_RawUrl;
        HttpContext m_HttpContext = null;

        #endregion

        #region Core Properties
        /// <summary>
        /// Simulates Context.Items and provides a per request/instance storage bag
        /// </summary>
        public IDictionary Items
        {
            get { return m_Items; }
        }

        /// <summary>
        /// Provides direct access to the .Items property
        /// </summary>
        public object this[string key]
        {
            get
            {
                return this.Items[key];
            }
            set
            {
                this.Items[key] = value;
            }
        }

        /// <summary>
        /// Allows access to QueryString values
        /// </summary>
        public NameValueCollection QueryString
        {
            get { return m_QueryString; }
        }

        /// <summary>
        /// Quick check to see if we have a valid web reqeust. Returns false if HttpContext == null
        /// </summary>
        public bool IsWebRequest
        {
            get { return this.Context != null; }
        }

        public HttpContext Context
        {
            get
            {
                return m_HttpContext;
            }
        }

        public string SiteUrl
        {
            get { return m_SiteUrl; }
        }

        public Uri CurrentUri
        {
            get
            {
                if (m_CurrentUri == null)
                    m_CurrentUri = new Uri("http://localhost/");

                return m_CurrentUri;

            }
            set { m_CurrentUri = value; }
        }

        private string _hostPath = null;
        /// <summary>
        /// 
        /// </summary>
        public string HostPath
        {
            get
            {
                if (_hostPath == null)
                {
                    string portInfo = CurrentUri.Port == 80 ? string.Empty : ":" + CurrentUri.Port.ToString();
                    _hostPath = string.Format("{0}://{1}{2}", CurrentUri.Scheme, CurrentUri.Host, portInfo);
                }
                return _hostPath;
            }
        }

        #endregion

        #region Initialize  and contructors

        public void ClearSession(string sessionName)
        {
            Session[SessionPrefix + sessionName] = null;
        }
        public void SetSession(string sessionName, object value)
        {
            Session[SessionPrefix + sessionName] = value;
        }
        /// <summary>
        /// Create/Instatiate items that will vary based on where this object 
        /// is first created
        /// 
        /// We could wire up Path, encoding, etc as necessary
        /// </summary>
        private void Initialize(NameValueCollection qs, Uri uri, string rawUrl, string siteUrl)
        {
            m_QueryString = qs;
            m_SiteUrl = siteUrl;
            m_CurrentUri = uri;
            m_RawUrl = rawUrl;
        }

        /// <summary>
        /// cntr called when no HttpContext is available
        /// </summary>
        private ApplicationContext(Uri uri, string siteUrl)
        {
            Initialize(new NameValueCollection(), uri, uri.ToString(), siteUrl);
        }

        /// <summary>
        /// cnst called when HttpContext is avaiable
        /// </summary>
        /// <param name="context"></param>
        private ApplicationContext(HttpContext context, bool includeQueryString)
        {
            this.m_HttpContext = context;

            if (includeQueryString)
            {
                Initialize(new NameValueCollection(context.Request.QueryString), context.Request.Url, context.Request.RawUrl, GetSiteUrl());
            }
            else
            {
                Initialize(null, context.Request.Url, context.Request.RawUrl, GetSiteUrl());
            }
        }

        #endregion

        #region State

        private static readonly string dataKey = "ApplicationContextStore";

        [ThreadStatic]
        private static ApplicationContext currentContext = null;

        /// <summary>
        /// Returns the current instance of the CMSContext from the ThreadData Slot. If one is not found and a valid HttpContext can be found,
        /// it will be used. Otherwise, an exception will be thrown. 
        /// </summary>
        public static ApplicationContext Current
        {
            get
            {
                HttpContext httpContext = HttpContext.Current;

                if (httpContext != null)
                {
                    if (httpContext.Items.Contains(dataKey))
                        return httpContext.Items[dataKey] as ApplicationContext;
                    else
                    {
                        ApplicationContext context = new ApplicationContext(httpContext, true);
                        SaveContextToStore(context);
                        return context;
                    }
                }

                if (currentContext == null)
                    throw new Exception("No ApplicationContext exists in the Current Application. AutoCreate fails since HttpContext.Current is not accessible.");
                return currentContext;
            }
        }

      

        private static void SaveContextToStore(ApplicationContext context)
        {
            if (context.IsWebRequest)
            {
                context.Context.Items[dataKey] = context;
            }
            else
            {
                currentContext = context;
            }
        }

        /// <summary>
        /// Remove current context out of memmory
        /// </summary>
        public static void Unload()
        {
            currentContext = null;
        }
        #endregion

        #region Session context
        public const string SessionPrefix = "BCA";
        public HttpSessionState Session
        {
            get
            {
                return Context.Session;
            }
        }
        #endregion

        #region URL context

        public string MapPath(string path)
        {
            if (Context != null)
                return Context.Server.MapPath(path);
            else
                return PhysicalPath(path.Replace("/", Path.DirectorySeparatorChar.ToString()).Replace("~", ""));
        }

        public string PhysicalPath(string path)
        {
            return string.Concat(RootPath().TrimEnd(Path.DirectorySeparatorChar), Path.DirectorySeparatorChar.ToString(), path.TrimStart(Path.DirectorySeparatorChar));
        }

        private string m_RootPath;
        private string RootPath()
        {
            if (m_RootPath == null)
            {
                m_RootPath = AppDomain.CurrentDomain.BaseDirectory;
                string dirSep = Path.DirectorySeparatorChar.ToString();

                m_RootPath = m_RootPath.Replace("/", dirSep);
            }
            return m_RootPath;
        }

        private string GetSiteUrl()
        {
            string hostName = Context.Request.Url.Host.Replace("www.", string.Empty);
            string applicationPath = Context.Request.ApplicationPath;

            if (applicationPath.EndsWith("/"))
                applicationPath = applicationPath.Remove(applicationPath.Length - 1, 1);

            return hostName + applicationPath;

        }
        #endregion

        #region Language Context

        private const string CURRENT_LANGUAGE_CODE = "BCA_CURRENT_LANGUAGE_CODE";

        private string m_CurrentLanguageCode;
        /// <summary>
        /// Get current language code
        /// </summary>
        public string CurrentLanguageCode
        {
            get
            {
                if (string.IsNullOrEmpty(m_CurrentLanguageCode))
                {   //Try to get from cookie
                    HttpCookie cookie = HttpContext.Current.Request.Cookies[CURRENT_LANGUAGE_CODE];
                    if (cookie != null)
                        m_CurrentLanguageCode = cookie.Value;
                    else
                        m_CurrentLanguageCode = CultureHelper.DEFAULT_LANGUAGE_CODE;
                }

                return m_CurrentLanguageCode;
            }
            set
            {
                m_CurrentLanguageCode = value;
                WriteLanguageIdToCookie(m_CurrentLanguageCode);
            }
        }

        private void WriteLanguageIdToCookie(string languaugeCode)
        {
            if (HttpContext.Current.Request.Cookies[CURRENT_LANGUAGE_CODE] != null)
            {
                HttpContext.Current.Request.Cookies[CURRENT_LANGUAGE_CODE].Value = languaugeCode;
                HttpContext.Current.Response.Cookies.Set(HttpContext.Current.Request.Cookies[CURRENT_LANGUAGE_CODE]);
            }
            else
                HttpContext.Current.Response.Cookies.Set(new HttpCookie(CURRENT_LANGUAGE_CODE, languaugeCode));

            //Cookie will be expired in 7 days
            HttpContext.Current.Response.Cookies[CURRENT_LANGUAGE_CODE].Expires = DateTime.Now.AddDays(7);

        }

        public void AddCommonUser(TblCommonUser user)
        {
            Session[SessionName.User_CurrentUser] = user;
        }

      

        public void ClearUser()
        {
            Session[SessionName.User_CurrentUser] = null;
            Session.Remove(SessionName.User_CurrentUser);
        }

        public void ClearUserFlag()
        {
            Session[SessionName.User_Flag] = null;
            Session.Remove(SessionName.User_Flag);
        }

        #endregion

        #region APEVN Data

        public bool IsLogin
        {
            get
            {
                return this.CommonUser != null;
            }
        }

        /// <summary>
        /// Return the current logged in User. This user value to be anonymous if no user is logged in.
        /// This value can be set if necessary
        /// </summary>

        public TblCommonUser CommonUser
        {
            get
            {

                if (Session[SessionName.User_CurrentUser] != null)
                    return Session[SessionName.User_CurrentUser] as TblCommonUser;
                return null;
            }
        }

     

        

        public string CurrentUserIp
        {
            get
            {
                if (Session[SessionName.User_CurrentIP] != null)
                    return Session[SessionName.User_CurrentIP] as string;
                return string.Empty;
            }
            set
            {
                Session[SessionName.User_CurrentIP] = value;
            }
        }

        public bool IsAdministrator
        {
            get
            {
                return ApplicationContext.Current.CommonUser != null && ApplicationContext.Current.CommonUser.IsAdmin.HasValue && ApplicationContext.Current.CommonUser.IsAdmin.Value;
            }
        }

        //private Dictionary<string, FunctionPermission> m_UserFunctionPermission;
        //public Dictionary<string, FunctionPermission> UserFunctionPermission
        //{
        //    get
        //    {
        //        if (Session[SessionPrefix + "UserFunctionPermission"] != null)
        //            m_UserFunctionPermission = (Dictionary<string, FunctionPermission>)Session[SessionPrefix + "UserFunctionPermission"];

        //        if (m_UserFunctionPermission == null)
        //        {
        //            m_UserFunctionPermission = new Dictionary<string, FunctionPermission>();// RoleManager.GetFunctionPermissionByUserId(User.Id);
        //            Session[SessionPrefix + "UserFunctionPermission"] = m_UserFunctionPermission;
        //        }

        //        return m_UserFunctionPermission;
        //    }
        //}


        #endregion
    }
}
