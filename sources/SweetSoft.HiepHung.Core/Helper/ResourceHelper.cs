﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SweetSoft.HiepHung.Core
{
    public class ResourceHelper
    {
        public struct BE
        {
            #region Master Page
            public static string Master_BarTop_btnToggleTitle = "Master_BarTop_btnToggleTitle";
            public static string Master_BarTop_Profile = "Master_BarTop_Profile";
            public static string Master_BarTop_SignOut = "Master_BarTop_SignOut";
            #endregion

            #region Position Management
            public static string Position_Title = "Position_Title";
            public static string Position_AddNew = "Position_AddNew";
            public static string Position_Name = "Position_Name";
            public static string Position_Name_Required = "Position_Name_Required";
            #endregion

            #region Grid
            public static string Grid_NodataFound = "Grid_NodataFound";
            #endregion

            #region Default
            public static string Default_Add = "Default_Add";
            public static string Default_All = "Default_All";
            public static string Default_Edit = "Default_Edit";
            public static string Default_Save = "Default_Save";
            public static string Default_Update = "Default_Update";
            public static string Default_Delete = "Default_Delete";
            public static string Default_Filter = "Default_Filter";
            public static string Default_Search = "Default_Search";
            public static string Default_Search_Cancel = "Default_Search_Cancel";
            public static string Default_Search_Placeholder = "Default_Search_Placeholder";
            public static string Default_Description = "Default_Description";
            public static string Default_Content = "Default_Content";
            public static string Default_Status = "Default_Status";
            public static string Default_Status_Active = "Default_Status_Active";
            public static string Default_Status_InActive = "Default_Status_InActive";
            public static string Default_Close = "Default_Close";
            public static string Default_First_name = "Default_First_name";
            public static string Default_Last_name = "Default_Last_name";
            public static string Default_Name = "Default_Name";
            public static string Default_Phone = "Default_Phone";
            public static string Default_Email = "Default_Email";
            public static string Default_Address = "Default_Address";
            public static string Default_Image = "Default_Image";
            public static string Default_Gender = "Default_Gender";
            public static string Default_Birthday = "Default_Birthday";
            public static string Default_Dept = "Default_Dept";
            public static string Default_Position = "Default_Position";
            public static string Default_Title_System = "Default_Title_System";
            public static string Default_Save_Error = "Default_Save_Error";
            public static string Default_Role_Assign = "Default_Role_Assign";
            public static string Default_Role_Assign_Success_Title = "Default_Role_Assign_Success_Title";
            public static string Default_Role_Assign_Success = "Default_Role_Assign_Success";
            public static string Default_No = "Default_No";
            public static string Default_Multiple_Text_Format = "Default_Multiple_Text_Format";
            public static string Default_No_Selected = "Default_No_Selected";
            public static string Default_404_Title = "Default_404_Title";
            public static string Default_500_Title = "Default_500_Title";
            #endregion

            #region Login
            public static string Login_Submit = "Login_Submit";
            public static string Login_UserName = "Login_UserName";
            public static string Login_UserName_Required = "Login_UserName_Required";
            public static string Login_Password = "Login_Password";
            public static string Login_Password_Confirm = "Login_Password_Confirm";
            public static string Login_Password_Required = "Login_Password_Required";
            public static string Login_TitleSystem = "Login_TitleSystem";
            public static string Login_Remember = "Login_Remember";
            public static string Login_Forget = "Login_Forget";
            public static string Login_SelectLanguage = "Login_SelectLanguage";
            public static string Login_Register = "Login_Register";
            public static string Login_Register_Email = "Login_Register_Email";
            public static string Login_Register_Email_Title = "Login_Register_Email_Title";
            public static string Login_Register_Email_Required = "Login_Register_Email_Required";
            public static string Login_Register_Email_Valid = "Login_Register_Email_Valid";
            public static string Login_Register_Email_Format = "Login_Register_Email_Format";
            public static string Login_Register_FullName = "Login_Register_FullName";
            public static string Login_Register_ForeignName = "Login_Register_ForeignName";
            public static string Login_Register_UserName = "Login_Register_UserName";
            public static string Login_Register_UserName_Title = "Login_Register_UserName_Title";
            public static string Login_Register_UserName_Valid = "Login_Register_UserName_Valid";
            public static string Login_Register_UserName_Required = "Login_Register_UserName_Required";
            public static string Login_Register_Password_Title = "Login_Register_Password_Title";
            public static string Login_Register_Password_Valid = "Login_Register_Password_Valid";
            public static string Login_Register_Password_Required = "Login_Register_Password_Required";
            public static string Login_Register_Password_Retype_Difference = "Login_Register_Password_Retype_Difference";
            public static string Login_Register_Now = "Login_Register_Now";
            public static string Login_Register_Text = "Login_Register_Title";
            public static string Login_Register_Website_Prefix = "Login_Register_Website_Prefix";
            public static string Login_Register_Wensite_Valid_Name = "Login_Register_Wensite_Valid_Name";
            public static string Login_Register_Accept_Rule_Required = "Login_Register_Accept_Rule_Required";
            public static string Login_Register_Config_Account = "Login_Register_Config_Account";
            public static string Login_Register_Config_User = "Login_Register_Config_User";
            public static string Login_Register_Config_Success = "Login_Register_Config_Success";
            public static string Login_Register_Config_Error = "Login_Register_Config_Error";
            public static string Login_Email_Required = "Login_Email_Required";
            public static string Login_Error_Code_1 = "Login_Error_Code_1";
            public static string Login_Error_Code_2 = "Login_Error_Code_2";
            public static string Login_Error_Code_3 = "Login_Error_Code_3";
            public static string Login_Error_Code_4 = "Login_Error_Code_4";

            public static string Register_Check_Shortname_Business_Exist = "Register_Check_Shortname_Business_Exist";

            public static string Login_Forgot_Password = "Login_Forgot_Password";
            public static string Login_Change_Password = "Login_Change_Password";
            public static string Login_ChangePassword_Old = "Login_ChangePassword_Old";
            public static string Login_ChangePassword_New = "Login_ChangePassword_New";
            public static string Login_ChangePassword_Error_Code_1 = "Login_ChangePassword_Error_Code_1";
            public static string Login_ChangePassword_Title = "Login_ChangePassword_Title";

            #endregion

            #region SystemConfiguration

            public static string System_Conf_General = "System_Conf_General";
            public static string System_Conf_SMTP = "System_Conf_SMTP";

            //General 
            public static string System_Conf_SMTP_Page_Title = "System_Conf_SMTP_Page_Title";

            public static string System_Conf_SMTP_SiteTitle = "System_Conf_SMTP_SiteTitle";
            public static string System_Conf_SMTP_SiteTitle_Tooltip = "System_Conf_SMTP_SiteTitle_Tooltip";

            //SMTP
            public static string System_Conf_Error_Receiver_Email = "System_Conf_Error_Receiver_Email";
            public static string System_Conf_Error_Receiver_Email_Tooltip = "System_Conf_Error_Receiver_Email_Tooltip";

            public static string System_Conf_SMTP_Mail_Server_Address = "System_Conf_SMTP_Mail_Server_Address";
            public static string System_Conf_SMTP_Mail_Server_Address_Tooltip = "System_Conf_SMTP_Mail_Server_Address_Tooltip";

            public static string System_Conf_SMTP_Port = "System_Conf_SMTP_Port";
            public static string System_Conf_SMTP_Port_Tooltip = "System_Conf_SMTP_Port_Tooltip";

            public static string System_Conf_Smtp_Using_SSL = "System_Conf_Smtp_Using_SSL";
            public static string System_Conf_Smtp_Using_SSL_Tooltip = "System_Conf_Smtp_Using_SSL_Tooltip";

            public static string System_Conf_Smtp_Sender_Account = "System_Conf_Smtp_Sender_Account";
            public static string System_Conf_Smtp_Sender_Account_Tooltip = "System_Conf_Smtp_Sender_Account_Tooltip";

            public static string System_Conf_Smtp_Sender_Password = "System_Conf_Smtp_Sender_Password";
            public static string System_Conf_Smtp_Sender_Password_Tooltip = "System_Conf_Smtp_Sender_Password_Tooltip";

            public static string System_Conf_Send_Mail_Test = "System_Conf_Send_Mail_Test";
            public static string System_Conf_Send_Mail_Test_Tooltip = "System_Conf_Send_Mail_Test_Tooltip";

            #endregion

            #region Message

            public static string MSG_Successfull_Title = "MSG_Successfull_Title";
            public static string MSG_Error_Title = "MSG_Error_Title";
            public static string MSG_Save_Successfull = "MSG_Save_Successfull";
            public static string MSG_Delete_Successfull = "MSG_Delete_Successfull";
            public static string MSG_Save_Error = "MSG_Save_Error";
            public static string MSG_Delete_Error = "MSG_Delete_Error";
            public static string MSG_Delete_Confirm = "MSG_Delete_Confirm";
            public static string MSG_Delete_Confirm_Title = "MSG_Delete_Confirm_Title";
            public static string MSG_Notify_Title = "MSG_Notify_Title";

            public static string MSG_Confirm_Title = "MSG_Confirm_Title";

            public static string MSG_Error_Email_Title = "MSG_Error_Email_Title";
            public static string MSG_Error_Email_Format = "MSG_Error_Email_Format";
            public static string MSG_Error_Email_Exist = "MSG_Error_Email_Exist";

            #endregion

            #region Title Dialog

            public static string Title_Add_Relationship = "Title_Add_Relationship";
            public static string Title_Add_Customer = "Title_Add_Customer";
            public static string Title_Add_Source_Customer = "Title_Add_Source_Customer";
            public static string Title_Add_Staff = "Title_Add_Staff";

            #endregion

            #region Gender

            public static string Gender_Male = "Gender_Male";
            public static string Gender_Female = "Gender_Female";

            #endregion

            #region Business Title Page

            public static string Business_Manager_Staff = "Business_Manager_Staff";
            public static string Business_Manager_Customer = "Business_Manager_Customer";
            public static string Business_Contact_Customer = "Business_Contact_Customer";
            public static string Business_Anniversary_Customer = "Business_Anniversary_Customer";
            public static string Business_Source_Customer = "Business_Source_Customer";
            public static string Business_User_Information = "Business_User_Information";
            public static string Business_Dashboard = "Business_Dashboard";

            public static string Business_Information = "Business_Information";

            #endregion

            #region Admin Title Page

            public static string Admin_Business_Management = "Admin_Business_Management";

            #endregion

            #region Template
            public static string Business_Template_Title = "Business_Template_Title";
            public static string Business_Template_Add = "Business_Template_Add";
            public static string Business_Template_Detail = "Business_Template_Detail";
            public static string Business_Template_Name = "Business_Template_Name";
            public static string Business_Template_Name_Require = "Business_Template_Name_Require";
            public static string Business_Template_Type_Not_Found = "Business_Template_Type_Not_Found";
            public static string Business_Template_ID_Not_Found = "Business_Template_ID_Not_Found";
            public static string Business_Template_Save_Success = "Business_Template_Save_Success";
            public static string Business_Template_Search_Placeholder = "Business_Template_Search_Placeholder";
            public static string Business_Template_Delete_Confirm = "Business_Template_Delete_Confirm";
            public static string Business_Template_Delete_Success = "Business_Template_Delete_Success";
            public static string Business_Template_Delete_Error = "Business_Template_Delete_Error";
            #endregion

            #region Business Staff
            public static string Business_Staff_AddNew = "Business_Staff_AddNew";
            public static string Business_Staff_Name = "Business_Staff_Name";
            public static string Business_Staff_List = "Business_Staff_List";
            public static string Business_Staff_NotFound = "Business_Staff_NotFound";
            public static string Business_Staff_Role_Assign = "Business_Staff_Role_Assign";
            public static string Business_Staff_Code = "Business_Staff_Code";
            public static string Business_Staff_Assign_Success_Title = "Business_Staff_Assign_Success_Title";
            public static string Business_Staff_Assign_Success = "Business_Staff_Assign_Success";
            public static string Busines_Anniversary_NotFound = "Busines_Anniversary_NotFound";
            public static string Business_Anniversary_List = "Business_Anniversary_List";
            #endregion

            #region Business Role
            public static string Business_Role_AddNew = "Business_Role_AddNew";
            public static string Business_Role_Update = "Business_Role_Update";
            public static string Business_Role_Name = "Business_Role_Name";
            public static string Business_Role_List = "Business_Role_List";
            public static string Business_Role_NotFound = "Business_Role_NotFound";
            public static string Business_Role_Right_Assign = "Business_Role_Right_Assign";
            public static string Business_Role_Delete = "Business_Role_Delete";
            public static string Business_Role_Delete_Success = "Business_Role_Delete_Success";
            public static string Business_Role_Delete_Error = "Business_Role_Delete_Error";
            public static string Business_Role_Name_Require = "Business_Role_Name_Require";
            public static string Business_Role_Update_Success = "Business_Role_Update_Success";
            public static string Business_Role_Insert_Success = "Business_Role_Insert_Success";
            public static string Business_Role_Delete_Default_Error = "Business_Role_Delete_Default_Error";

            #endregion

            #region Customer Anniversary
            public static string Customer_Anniversary_Name = "Customer_Anniversary_Name";
            public static string Customer_Anniversary_Date = "Customer_Anniversary_Date";
            public static string Customer_Anniversary_Description = "Customer_Anniversary_Description";

            #endregion

            #region Business Customer
            public static string Business_Customer_Name = "Business_Customer_Name";
            public static string Default_Relationship = "Default_Relationship";

            #endregion
        }
    }

    public enum ResourceType
    {
        [Description("ResourceText")]
        Default,
        [Description("ResourceLog")]
        Log,
    }
}
