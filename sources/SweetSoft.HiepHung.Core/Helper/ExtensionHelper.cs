﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Globalization;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.ComponentModel;

namespace SweetSoft.HiepHung.Core
{
    public static class ExtensionHelper
    {
        #region DataTable, DataSet
        /// <summary>
        /// Định nghĩa thêm cho DataTable phương thức ToCollection
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="dt">DataTable chủ</param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataTable dt)
        {
            List<T> lst = new System.Collections.Generic.List<T>();
            Type tClass = typeof(T);
            PropertyInfo[] pClass = tClass.GetProperties();
            List<DataColumn> dc = dt.Columns.Cast<DataColumn>().ToList();
            T cn;
            foreach (DataRow item in dt.Rows)
            {
                cn = (T)Activator.CreateInstance(tClass);
                foreach (PropertyInfo pc in pClass)
                {
                    try
                    {

                        DataColumn d = dc.Find(c => (c.ColumnName.ToLower().Replace("_", string.Empty) == pc.Name.ToLower()));
                        if (d != null)
                        {
                            object value = item[d.ColumnName];
                            if (value == null) continue;
                            switch (d.DataType.FullName)
                            {
                                case "System.UInt16":
                                case "System.Int16":
                                    Int16 value16 = -1;
                                    if (Int16.TryParse(value.ToString(), out value16))
                                        pc.SetValue(cn, value16, null);
                                    break;
                                case "System.UInt32":
                                case "System.Int32":
                                    Int32 value32 = -1;
                                    if (Int32.TryParse(value.ToString(), out value32)) ;
                                    pc.SetValue(cn, value32, null);
                                    break;
                                case "System.UInt64":
                                case "System.Int64":
                                    Int64 value64 = -1;
                                    if (Int64.TryParse(value.ToString(), out value64))
                                        pc.SetValue(cn, value64, null);
                                    break;
                                case "System.Decimal":
                                case "System.Double":
                                case "System.Single":
                                    Decimal decima = -1;
                                    if (Decimal.TryParse(value.ToString(), out decima))
                                        pc.SetValue(cn, decima, null);
                                    break;
                                case "System.Boolean":
                                    bool b = false;
                                    Boolean.TryParse(value.ToString(), out b);
                                    pc.SetValue(cn, b, null);
                                    break;
                                case "System.DateTime":
                                    DateTime date = DateTime.MinValue;
                                    DateTime.TryParse(value.ToString(), out date);
                                    pc.SetValue(cn, date, null);
                                    break;
                                case "System.Byte[]":
                                    BinaryFormatter bf = new BinaryFormatter();
                                    byte[] arr = value as byte[];
                                    //using (MemoryStream ms = new MemoryStream())
                                    //{
                                    //    bf.Serialize(ms, value);
                                    //    arr = ms.ToArray();
                                    //}
                                    string vl = Encoding.UTF8.GetString(arr);
                                    pc.SetValue(cn, vl, null);
                                    break;
                                default:
                                    if (pc.PropertyType == typeof(Boolean))
                                        pc.SetValue(cn, bool.Parse(value.ToString()), null);
                                    else if (value.GetType() != typeof(DBNull))
                                        pc.SetValue(cn, value, null);
                                    break;
                            }
                        }
                    }
                    catch
                    {
                    }
                }
                lst.Add(cn);
            }
            return lst;
        }

        public static T ToEmement<T>(this DataTable dt)
        {
            List<T> lst = dt.ToList<T>();
            if (lst.Count > 0) return lst[0];
            return default(T);
        }
        /// <summary>
        /// Định nghĩa thêm cho DataSet phương thức ToCollection
        /// </summary>
        /// <typeparam name="T">Kiểu dữ liệu</typeparam>
        /// <param name="ds">DataSet chủ</param>
        /// <returns></returns>
        public static List<T> ToList<T>(this DataSet ds)
        {
            List<T> lst = new System.Collections.Generic.List<T>();
            if (ds != null && ds.Tables.Count > 0)
            {
                lst = ds.Tables[0].ToList<T>();
            }
            return lst;
        }

        public static DataTable ToDataTable<T>(this IEnumerable<T> collection)
        {
            DataTable dt = new DataTable();
            Type t = typeof(T);
            PropertyInfo[] pia = t.GetProperties();
            //Create the columns in the DataTable
            foreach (PropertyInfo pi in pia)
            {
                dt.Columns.Add(pi.Name, pi.PropertyType);
            }
            //Populate the table
            foreach (T item in collection)
            {
                DataRow dr = dt.NewRow();
                dr.BeginEdit();
                foreach (PropertyInfo pi in pia)
                {
                    dr[pi.Name] = pi.GetValue(item, null);
                }
                dr.EndEdit();
                dt.Rows.Add(dr);
            }
            return dt;
        }
        #endregion

        #region Decimal
        /// <summary>
        /// Lấy chuỗi xuất giá trị theo format
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ToStringWithLanguage(this decimal value)
        {
            CultureInfo cul = new CultureInfo("vi-VN");

            if (value % 1 == 0)
                return value.ToString("N0", cul);
            return value.ToString("N3", cul);
        }

        public static decimal ExecptionMod(this decimal value)
        {
            decimal d = value - (int)value;
            if (d > 0)
                return value;
            return (decimal)(double)value;
        }

        public static decimal? ExtendParse(this decimal value, object parse)
        {
            decimal? result = null;
            if (parse != null)
            {
                decimal d = -1;
                if (decimal.TryParse(parse.ToString(), out d)) result = d;
            }
            return result;
        }

        /// <summary>
        /// Làm tròn giá trị
        /// </summary>
        /// <param name="value"></param>
        /// <param name="modValue"></param>
        /// <param name="INC"></param>
        public static decimal RoundValue(this decimal value, int modValue, bool INC)
        {
            long phannguyen = (long)(value / 1000);
            int phandu = (int)(value % 1000);
            if (phandu >= modValue && INC)
                value = phannguyen + 1;
            else
                value = phannguyen;
            value *= 1000;
            return value;
        }
        #endregion

        #region Enum
        public static string GetDescription(this Enum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }

        public static string GetDefaultValue(this Enum val)
        {
            DefaultValueAttribute[] attributes = (DefaultValueAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DefaultValueAttribute), false);
            return attributes.Length > 0 && attributes[0].Value != null ? attributes[0].Value.ToString() : string.Empty;
        }

        #endregion

        #region List
        private static Random rng = new Random();

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }
        #endregion

        #region DateTime
        public static double GetDifferentTime(string yourTimeZoneId)
        {
            try
            {
                var yourTimeZone = TimeZoneInfo.FindSystemTimeZoneById(yourTimeZoneId);
                var serverTimeZone = TimeZoneInfo.Local;
                var now = DateTimeOffset.UtcNow;
                TimeSpan yourOffset = yourTimeZone.GetUtcOffset(now);
                TimeSpan serverOffset = serverTimeZone.GetUtcOffset(now);
                TimeSpan different = yourOffset - serverOffset;
                return different.TotalHours;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        public static DateTime GetCurrentDateTime(DateTime yourDateTime)
        {
            try
            {
                if (string.IsNullOrEmpty(yourDateTime.ToString()) || yourDateTime == DateTime.MinValue)
                    return yourDateTime.ToLocalTime();
                String timeZoneId = "SE Asia Standard Time";
                double differentTime = GetDifferentTime(timeZoneId);
                if (TimeZoneInfo.Local.Id != "SE Asia Standard Time")
                {
                    if (yourDateTime.Month <= 3)
                    {
                        if (yourDateTime.Month == 3)
                        {
                            return yourDateTime.AddDays(1).ToLocalTime();
                        }
                        else
                            return yourDateTime.AddDays(1).AddHours(differentTime).ToLocalTime();
                    }
                    else
                        return yourDateTime.AddHours(differentTime).ToLocalTime();
                }
                else
                    return yourDateTime.AddHours(differentTime).ToLocalTime();
            }
            catch
            {
                return yourDateTime.ToLocalTime();
            }
        }
        #endregion

    }
}
