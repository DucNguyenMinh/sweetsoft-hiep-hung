﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace SweetSoft.HiepHung.Core
{
    public class ServiceHelper
    {

    }

    #region Code Result Login
    public enum BussinessLoginState
    {
        [Description("Login_Code_Success")]/// Đăng nhập thành công
        Success,
        [Description("Login_Code_Bussiness_Not_Avaliable")]/// Doanh nghiệp không tồn tại
        Bussiness_Not_Avaliable,
        [Description("Login_Code_User_Not_Avaliable")]/// Tài khoản không tồn tại
        User_Not_Avaliable,
        [Description("Login_Code_Wrong_Password")]/// Sai mật khẩu tài khoản
        Wrong_Password,
        [Description("Login_Code_Lock")]/// Tài khoản bị khóa
        Lock,
        [Description("Login_Code_ExpriedDate")]/// Tài khoản quá hạn sử dụng
        ExpriedDate,
    }

    public static class MyEnumExtensions
    {
        public static string EnumToString(this Enum val)
        {
            DescriptionAttribute[] attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : string.Empty;
        }
    }
    #endregion

    public enum ServiceType { 
        [Description("email")]
        Email,
        [Description("sms")]
        SMS,
        [Description("phone")]
        Phone,
    }

    public enum ServiceCampainStatus
    {

        [Description("Waitting")]
        Waitting,
        [Description("Running")]
        Running,
        [Description("Pause")]
        Pause,
        [Description("Stop")]
        Stop,
        [Description("Destroy")]
        Destroy,
        [Description("Completed")]
        Completed
    }
}
